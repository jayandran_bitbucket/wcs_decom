package pageobjects.OF;


public class HomePage {

	public String ordersearch_link = "//a[@class='headingLink' and contains(text(),'Order')]";
	public String ordersearch_loc = "xpath";
	
	public String custsearch_link = "//a[@class='headingLink' and contains(text(),'Customer')]";
	public String custsearch_loc = "xpath";
	
	public String equipmentloanorder_link = "Equipment Loan Order";
	public String equipmentloanorder_loc = "linkText";
	
	public String waitroseentertaining_link = "//a[text()='WaitroseEntertaining']";
	public String waitroseentertaining_loc = "xpath";
	
	public String homepage_lnk = "//a[contains(text(),'Home')]";
	public String homepage_loc = "xpath";
	
	public String findmessages_lnk = "//a[contains(text(),'Find Messages')]";
	public String findmessages_loc = "xpath";
	
	public String shopinbrachwedeliver_lnk = "//a[text()='Shop in Branch we deliver']";
	public String shopinbrachwedeliver_loc = "xpath";
	
	public String dailyslots_lnk = "//a[contains(text(),'Daily Slots')]";
	public String dailyslots_loc = "xpath";
	
	public String signout_lnk = "//a[contains(text(),'Sign')]";
	public String signout_loc = "xpath";
	
}