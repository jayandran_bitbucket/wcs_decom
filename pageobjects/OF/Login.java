package pageobjects.OF;


public class Login {

	public String userid_txt = "userid";
	public String userid_loc = "name";
	
	public String passwd_txt = "password";
	public String passwd_loc = "name";
	
	public String signon_btn = "//*[@title='Click here to sign on']";
	public String signon_loc = "xpath";
	
	public String returnToAdminPhone_lnk = "//li[@class='phone']/a";
	public String returnToAdminPhone_loc = "xpath";
	
	public String returnToAdminPerson_lnk = "//li[@class='person']/a";
	public String returnToAdminPerson_loc = "xpath";
}