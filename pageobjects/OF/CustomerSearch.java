package pageobjects.OF;


public class CustomerSearch {

	public String emailId_txt = "logonId";
	public String emailId_loc = "name";
	
	public String searchCustomer_btn = "search";
	public String searchCustomer_loc = "name";
	
	public String placeorder_btn = "//input[@value='Place Order']";
	public String placeorder_loc = "xpath";
	
	public String wedeliver_btn = "//input[@value='WE Delivery']";
	public String wedeliver_loc = "xpath";
	
	public String wecollect_btn="//input[@value='WE Collection']";
	public String wecollect_loc = "xpath";
	
	public String SOAC_link = "//input[@value='sign on as customer']";
	public String SOAC_loc = "xpath";
	
	public String bookdelivery_lnk = "//input[@value='book delivery']";
	public String bookdelivery_loc = "xpath";

	public String latestOrder="(//tr/td[1]/a)[1]";
	public String latestOrder_loc="xpath";
	
	public String imgdelivery="(//tr/td[2]/img)[1]";
	public String imgdelivery_loc="xpath";
	
}