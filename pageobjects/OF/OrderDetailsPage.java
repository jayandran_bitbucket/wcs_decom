package pageobjects.OF;


public class OrderDetailsPage {

	public String orderAddress_txt = "(//tr//td[text()='deliver to']/../td[@class='text'])[1]";
	public String orderAddress_loc = "xpath";
	
	public String edit_btn = "//img[@title='Click here to edit this order.']";
	public String edit_loc = "xpath";
	
	
	public String giftvoucher_txt = "//a[contains(text(),'Gift Voucher')]";
	public String giftvoucher_loc = "xpath";
	
	public String giftcard_txt = "//a[contains(text(),'Gift Cards')]";
	public String giftcard_loc = "xpath";
	
	public String carriercharge_txt = "//td[contains(text(),'Carrier Bag')]";
	public String carriercharge_loc = "xpath";
	
	public String partnerdiscounttext = "//tbody/tr[14]/td[1]";
	public String partnerdiscounttext_loc = "xpath";
	
	public String partnerdiscount = "//tbody/tr[14]/td[2]";
    public String partnerdiscount_loc = "xpath";
    
	public String paymentcardtext = "//tbody/tr[2]/td[3]";
	public String paymentcardtext_loc =  "xpath";
	
	public String paymentcard = "//tbody/tr[2]/td[4]";
	public String paymentcard_loc =  "xpath";
	
	public String txtEstTotal="(//tr/td[@class='label' and contains(text(),'est. order total')]/../td)[3]";
	public String txtEstTotal_loc="xpath";
	
	public static String txtOFTotal="";
	
}