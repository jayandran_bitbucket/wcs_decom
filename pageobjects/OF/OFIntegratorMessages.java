package pageobjects.OF;

public class OFIntegratorMessages {


	public String date_txt = "//input[@value='ccyy-mm-dd']";
	public String date_loc = "xpath";

	public String ordersearch_txt = "//input[@name='messageContent']";
	public String ordersearch_loc = "xpath";


	public String selectSearchButton ="(//input[@value='search'])[2]";
	public  String selectSearchButton_Loc ="xpath";

	//verify Integrator message
	public  String verifyMessage ="//td[contains(text(),'No Error Found')]";
	public String verifyMessage_loc ="xpath";

	//click error message number link
	public  String linkErrorMessageNo ="//td[@class='text']//a";
	public  String linkErrorMessageNo_Loc ="xpath";

	//Select Message type
	public  String messageType="messageType";
	public String messageType_Loc="name";

	//verify order number in xml

	public  String textOrderreferancenumber = "//textarea[@name='messageContent']";
	public String textOrderreferancenumber_Loc ="xpath";
	
	// message number
	public String message = "//tbody/tr[2]/td[1]/a";
	public String message_loc ="xpath";
	
	// amended message number
	public String amended_msg = "//tbody/tr[3]/td[1]/a";
	public String amended_msg_loc = "xpath";
	
	
}
