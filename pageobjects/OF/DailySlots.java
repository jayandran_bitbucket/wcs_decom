package pageobjects.OF;

public class DailySlots {

	//Select Dropdown - Date Selection
	public final static String txtSlotDateDropdown="//select[@name='date_select']";
	public final static String txtSlotDateDropdown_Loc="xpath";


	//Select Dropdown Date Selection - 1st option
	public final static String txtSelectedSLot="(//select[@name='date_select']//option)[1]";
	public final static String txtSelectedSLot_Loc="xpath";

	//Select Dropdown - Branch Name 
	public final static String txtBranchNameDropdown="branchSelection";
	public final static String txtBranchNameDropdown_Loc="id";

	//Select Dropdown - Branch Name - Option values
	public final static String txtBranchNameOptionDropdown="//select[@id='branchSelection']//option";
	public final static String txtBranchNameOptionDropdown_Loc="xpath";

	//Select Dropdown - Service Type
	public final static String txtServiceDropdown="serviceSelection";
	public final static String txtServiceDropdown_Loc="id";

	//Select Dropdown - Service Type - Option values
	public final static String txtServiceSelectionOptionDropdown="//select[@id='serviceSelection']//option";

	//Select Dropdown - Location Selection
	public final static String txtLocationDropdown="locationSelection";
	public final static String txtLocationDropdown_Loc="id";

	//Select Dropdown - Lcoation Selection - Option values
	public final static String txtLocationSelectionOptionDropdown="//select[@id='locationSelection']//option";	

	//Select Dropdown - Location Selection
	public final static String linkGoButton="go";
	public final static String linkGoButton_Loc="linkText";

	//TOTAl Capacity
	public final static String txtTotalSlotCapacity="(//tr//td[text()='totals']/../td)[2]";
	public final static String txtTotalSlotCapacity_Loc="xpath";

	//TOTAl Capacity - Restricted Capacity
	public final static String txtRestrictedTotalSlotCapacity="(//tr//td[text()='totals']/../td)[5]";
	public final static String txtRestrictedTotalSlotCapacity_Loc="xpath";

	//Free Slot Capacity of First Slot
	public final static String txtSlotCapacityOfFirstSlot="((//tr[@class='normal'])[1]//td)[2]";
	public final static String txtSlotCapacityOfFirstSlot_Loc="xpath";				

	//Free Slot Capacity of First Slot
	public final static String txtSlotRestrictedCapacityOfFirstSlot="((//tr[@class='normal'])[1]//td)[5]";
	public final static String txtSlotRestrictedCapacityOfFirstSlot_Loc="xpath";		

	//Free Slot Capacity of First Slot
	public final static String txtFreeSlotCapacityOfFirstSlot="((//tr[@class='normal'])[1]//td)[4]";
	public final static String txtFreeSlotCapacityOfFirstSlot_Loc="xpath";

	//Restricted - Free Slot Capacity of First Slot
	public final static String txtRestrictedFreeSlotCapacityOfFirstSlot="((//tr[@class='normal'])[1]//td)[7]";
	public final static String txtRestrictedFreeSlotCapacityOfFirstSlot_Loc="xpath";

	//Restricted - Free Slot Capacity of First Slot
	public final static String txtRestrictedPlacedSlotCapacityOfFirstSlot="((//tr[@class='normal'])[1]//td)[6]";
	public final static String txtRestrictedPlacedSlotCapacityOfFirstSlot_Loc="xpath";

	//Placed Slot Capacity of First Slot
	public final static String txtPlacedSlotCapacityOfFirstSlot="((//tr[@class='normal'])[1]//td)[3]";
	public final static String txtPlacedSlotCapacityOfFirstSlot_Loc="xpath";

	public static int slotCapacity = -1;
	public static int freeSlotCapacity = -1;
	public static int placedSlotCapacity = -1;
	public static int restrictedfreeSlotCapacity = -1;
	public static int restrictedplacedSlotCapacity = -1;
	public static String slotDetails = null;
	public static String slotDate = null;
	public static int SlotCapacityOfFirstSlot = -1;


	//Daily Slot Page - Window Header
	public final static String txtWindowHeader="//th[text()='window']";
	public final static String txtWindowHeader_Loc="xpath";

	//Daily Slot Page - Capacity All Header
	public final static String txtCapacityHeader="(//th[contains(text(),'capacity')])[1]";
	public final static String txtCapacityHeader_Loc="xpath";

	//Daily Slot Page - Capacity All - Placed Order Header
	public final static String txtCapacityAllPlacedOrderHeader="(//th[text()='placed orders'])[1]";
	public final static String txtCapacityAllPlacedOrderHeader_Loc="xpath";

	//Daily Slot Page - Capacity All - Placed Order Header
	public final static String txtCapacityAllFreeOrderHeader="(//th[text()='free slots'])[1]";
	public final static String txtCapacityAllFreeOrderHeader_Loc="xpath";

	//Daily Slot Page - Capacity All - Placed Order Header
	public final static String txtSlotTiming="(//td[@nowrap and @class='centeredText'])";
	public final static String txtSlotTiming_Loc="xpath";

	public final static String txtEndSLot="(//select[@name='end_date_select']//option)[7]";
	public final static String txtEndSlot_Loc="xpath";
}
