package pageobjects.OF;


public class OrderSearch {

	public String searchorder_txt = "//tr//td[text()='order number']/..//input[@name='order_rn']";
	public String searchorder_loc = "xpath";
	
	public String orderEntryBox="order_rn";
	public String orderEntryBox_loc="name";
	
	public String orderSearchbtn="(//img[@alt='Search'])[2]";
	public String orderSearchbtn_loc="xpath";
}