package pageobjects.wcs;

public class FastMail{
	
	public String login_txt = "v-Login-title";
	public String login_loc = "className";
	
	public String emailid_box = "username";
	public String emailid_loc = "name";
	
	public String password_box = "password";
	public String password_loc = "name";
	
	public String logon_btn = "//button[text()='Log In']";
	public String logon_loc = "xpath";
	
	public String searchmail_box = "//input[@placeholder='Search mail']";
	public String searchmail_loc = "xpath";
	
	public String mailList_lnk = "(//a[@class='app-listItem-link'])";
	public String mailList_loc = "xpath";
	
	public String pyoconfirmation_lnk = "//span[contains(text(),'Confirmation of your PYO Offers')]";
	public String pyoconfirmation_loc = "xpath";
	
	public String myaccount_lnk = "//span[@class='v-MainNavToolbar-userName']";
	public String myaccount_loc = "xpath";
	
	public String logout_lnk = "//a[@class='s-nav-logout']";
	public String logout_loc = "xpath";
	
	public String apdtext = "//td[@class='defanged1-PartnerDiscountText']";
	public String apdtext_loc = "xpath";
	
	public String resetlogindeatials_lnk = "//span[contains(text(),'Reset your login details')]";
	public String resetlogindeatials_loc = "xpath";
	
	public String createnewpasswordlink_lnk = "(//a[@style='text-decoration:underline;color:rgb(92, 128, 24);'])[1]";
	public String createnewpasswordlink_loc = "xpath";
}