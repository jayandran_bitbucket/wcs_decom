package pageobjects.wcs;

public class MyAddress {

	public String addnewaddress_btn = "add-new-address";
	public String addnewaddress_loc = "className";
	
	public String postcode_txt = "//input[@class='postcode']";
	public String postcode_loc = "xpath";
	
	public String checkpostcode_btn = "//input[@value='Check postcode']";
	public String checkpostcode_loc = "xpath";
	
	public String savethisaddress_btn = "//*[@value='Save this address']";
	public String savethisaddress_loc = "xpath";
	
	public String defaultaddress_chkbx  = "//input[@name='setDefaultAddr']";
	public String defaultaddress_loc = "xpath";
	
}
