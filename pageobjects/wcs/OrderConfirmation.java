package pageobjects.wcs;


public class OrderConfirmation{
	
	public static String orderNumber = null;
	public static String orderDeliveryAddress = null;
	public static String orderCollectionAddress = null;
	public String ordervalue = null;
	
	public String orderReference_txt = "//tr/th[contains(text(),'Order reference number')]/../td";
	public String orderReference_loc = "xpath";
	
	public String orderDeliveryAddress_txt = "//tr//th[contains(text(),'Delivery Address:')]/../td";
	public String orderDeliveryAddress_loc = "xpath";
	
	public String orderCollectionAddress_txt = "//tr//th[contains(text(),'Collection Address:')]/../td";
	public String orderCollectionAddress_loc = "xpath";
	
	public String orderDSCollectionAddress_txt = "//tr//th[contains(text(),'We will deliver to')]/../td";
	public String orderDSCollectionAddress_loc = "xpath";

	public String orderTotal_txt = "(//tr//th[contains(text(),'Total')])[2]/../td";
	public String orderTotal_loc = "xpath";
	
	public String orderItemTotal_txt = "//tr//th[text()='Item Total:']/../td";
	public String orderItemTotal_loc = "xpath";
	
	public String partnershipdiscount_txt = "//span[contains(text(),'Partner discount will be applied')]";//"//a[text()='Sign up today' and contains(@href,'partnershipcard')]";
	public String partnershipdiscount_loc = "xpath";
	
	public String myWaitroseSavings_txt = "//tr/th[text()='myWaitrose Savings']/../td";
	public String myWaitroseSavings_loc = "xpath";
	
	public String deliveryNote_txt = "//tr//th[contains(text(),'Delivery Note')]/following-sibling::td";
	public String deliveryNote_loc = "xpath";
	
	public String collectionnote_txt = "//tr//th[contains(text(),'Collection Note')]/..//td";
	public String collectionnote_loc ="xpath";
	
	public String giftvoucher_txt = "//tr//th[contains(text(),'Payment by Vouchers')]/..//span";
	public String giftvoucher_loc = "xpath";
	
	public String totalvouchervalue_txt = "//tr//th[contains(text(),'Vouchers')]/..//td";
	public String totalvouchervalue_loc= "xpath";
	public static String  vouchervalue = "";
	
	public  String totalcardvalue_txt = "//tr//th[contains(text(),'Gift card')]/..//td";
	public  String totalcardvalue_loc = "xpath";
	public static String cardvalue = "";
	
	public String myoffer_txt = "//tr//th[contains(text(),'My Offers Savings')]/..//td";
	public String myoffer_loc = "xpath";
	
	public String fastorder_txt = "//p[contains(text(),'This is a Fast track order')]";
	public String fastorder_loc = "xpath";
	
	public String mywaitroseoffer_txt = "//tr//th[contains(text(),'myWaitrose Savings')]/..//td";
	public String mywaitroseoffer_loc = "xpath";
	
	public String carrierbagcharge_txt = "//tr//th[contains(text(),'Carrier bag charge:')]/..//td";
	public String carrierbagcharge_loc = "xpath";
	
	public String orderReference_res = "//p[@class='delivery-date']";
	public String orderReference_res_loc = "xpath";
	
	
	public String txtLoanItem = "//div[@id='order-details']/div[@class='details']/p";
	public String txtLoanItem_Loc = "xpath";
	
	public String txtloanItemName = "//div[@id='order-details']/div[@class='details']/table/tbody/tr/td";
	public String txtloanItemName_Loc = "xpath";
	
	public static String SelectedProductName="";
	
}