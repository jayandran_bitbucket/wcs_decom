package pageobjects.wcs;


public class MyOrders{
	
	public String amendorder_btn = "//a[contains(text(),'Amend order')]";
	public String amendorder_loc = "xpath";
	
	public String amendordercontinue_btn = "overlay-continue";
	public String amendordercontinue_loc = "id";
	
	public String addtopendingorder_lnk = "//a[contains(text(),'Add the current contents of your trolley')]";
	public String addtopendingorder_loc = "xpath";
	
	public String cancelconfirm_btn = "yes-button";
	public String cancelconfirm_loc  = "className";
	
	public String getorderstatus_txt = "//td[@class='order-status']";
	public String getorderstatus__loc = "xpath";
	
	public String shopfromthisorder_btn ="//a[contains(text(),'Shop from this order')]";
	public String shopfromthisorder_loc = "xpath";
	
	public String previousOrderHeader_txt = "//h1[contains(text(),'Shop from previous order')]";
	public String previousOrderHeader_loc = "xpath";
}