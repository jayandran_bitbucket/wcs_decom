package pageobjects.wcs;

public class BookSlot{

	public static String slotdatee = "";
	
	public static String slottime = "";
	
	public String Deliver_btn = "delivery";
	public String Deliver_loc = "id";
	
	public String Collection_btn = "chooseCollectionLocation";
	public String Collection_loc = "id";
	
	public String ShopInBranch_lnk = "delivery-service";
	public String ShopInBranch_loc = "id";
	
	public String Delivertonewaddress_btn = "deliver-to-new-address";
	public String Delivertonewaddress_loc = "id";
	
	public String enterPostcode_textbox = "edit_postcode_existing_address";
	public String enterPostcode_loc = "id";
	
	public String checkPostcode_btn = "(//input[@value='Check postcode'])[2]";
	public String checkPostcode_loc = "xpath";
	
	public String enterPostcode1_textbox = "postcode_lookup_input";
	public String enterPostcode1_loc = "id";
	
	public String PostcodesearchOk_btn = "//input[@value='Ok']";
	public String PostcodesearchOk_loc = "xpath";
	
	public String CollectionPostcodeSearch_txt = "townname";
	public String CollectionPostcodeSearch_loc = "id";
	
	public String CollectionPostcodeFind_btn = "update-location";
	public String CollectionPostcodeFind_loc = "id";
	
	public String CollectionBranch_box = "(//div[contains(@class,'branch result-branch result-branch')])";
	public String CollectionBranch_loc = "xpath";
	
	public String CollectionNormalBranch_logo = "//div[contains(@class,'branch-logo BRANCH')]";
	public String CollectionNormalBranch_loc = "xpath";
	
	public String CollectionBranchType_txt = "(//div[contains(@class,'branch-collection-type')]/p)";
	public String CollectionBranchType_loc = "xpath";
	
	public String collectfromhere_btn = "(//a[contains(text(),'Collect from here')])";
	public String collectfromhere_loc = "xpath";
	
	public String collectfrmhr_btn = "(//div[@class='button content-button no-image'])";
	
	public String next5_link = "//a[contains(text(),'View next 5')]"; //View next 5
	public String next5_loc = "xpath"; //linkText
	
	public String SlotTable_table = "//div[@class='slots']/ul";
	public String SlotTable_loc = "xpath";
	
	public String next7days_link = "next";//"Next 7 days";
	public String next7days_loc = "className";//"linkText";
	
	public String slotconfirmcontinueInDelivery_btn = "(//a[@id='continueButton1'])[4]";
	public String slotconfirmcontinueInDelivery_loc = "xpath";
	
	public String slotconfirmDeliveryServiceReserve_btn = "//div[@class='toolbox-container collapsible toolbox-container-large']//a[contains(text(),'Confirm reservation')]";
	public String slotconfirmDeliveryServiceReserve_loc = "xpath";
	
	//public String slotconfirmcontinueInCollection_btn = "(//a[@id='continueButton2'])[2]";
	//public String slotconfirmcontinueInCollection_loc = "xpath";
	
	public String slotconfirmcontinueInCollection_btn = "(//a[contains(text(),'Continue')])";
	public String slotconfirmcontinueInCollection_loc = "xpath";
	
	public String collectionslotconfirmation_header = "(//*[text()='Please confirm your collection slot'])[2]";
	
	
	public String bookslotheader_txt = "//*[text()='Book a Slot']";
	public String bookslotheader_loc = "xpath";
	
	public String slotconfirmationoverlay = "toolbox-container collapsible toolbox-delivery-confirmation";
	
	public String slottime_txt = "//dd[@class='slot-time']";
	public String slottime_loc = "xpath";
	
	public String Entertaining_tab = "weTab";
	public String Entertaining_loc = "id";
	
	public String grocery_tab = "genTab";
	public String grocery_loc = "id";
	
	public String EditSlotDisabled_btn = "//button[text()='Edit slot' and @disabled]";
	public String EditSlotDisabled_loc = "xpath";

	public String SlotDate_txt = "//dd[@class='slot-date']";
	public String SlotDate_loc = "xpath";
	
	public String christmasslot_btn = "//a[text()='Christmas Slots']";
	public String christmasslot_loc = "xpath";
	
    public String alcoholcontinue_btn = "//div[@class='alcohol-restriction-cta']//a[contains(text(),'Continue')]";
    public String alcoholcontinue_loc = "xpath";
    
    public String bookedslot_txt = "//span[@class='booked' and contains(text(),'BOOKED')]";
    public String bookedslot_loc= "xpath";
    
    public String weddingcake_lnk = "//a[contains(text(),'Wedding cake')]";
    public String weddingcake_loc = "xpath";
    
    public String calender_icon = "//a[@class='calendar-icon']";
    public String calender_loc = "xpath";
    
    public String activedate_lnk = "//div[@class='cell is-active']";
    public String activedate_loc = "xpath";
    
    public String reservethisdate_btn = "reserveDate";
    public String reservethisdate_loc = "id";
    
    public String check_btn = "//input[@class='button primary-cta postcode-check']";
    public String check_loc = "xpath";
    
    public String registerandbookslot = "book-collection-slot";
    public String registerandbookslot_loc = "id";
    
    public String johnlewisstore = "//div[@class='branch-logo EXTERNAL John Lewis Store']";
    public String johnlewisstore_loc = "xpath";
    
    public String doddlestore = "//div[@class='branch-logo EXTERNAL Doddle']";
    public String doddlestore_loc = "xpath";
    
    public String dcoverlaycontinue = "//a[@id='continue']";  //div[@class='button content-button right']
	public String dcoverlaycontinue_loc = "xpath";
   
	public String lightpostcode_txt = "logon-postcode";
    public String lightpostcode_loc = "id";
    
    public String postcodecheck_btn = "//input[@class='button primary-cta postcode-check']";
    public String postcodecheck_loc = "xpath";
    
    public String badNews_btn = "//a[contains(text(),'Book Collection Slot')]";
	public String badNews_loc = "xpath";
    
	public String bookslotforlight_btn = "book-slot";
    public String bookslotforlight_loc = "id";
    
    public String postenterbox_txt="//input[@type='text']";
	public String postenterbox_loc="xpath";

	public String enterPostcodeForBadnews_textbox="//input[@type='text']";
	public String enterPostcodeForBadnews_loc="xpath";

	public String checkPostcodeForBadnews_loc="xpath";
	public String checkPostcodeForBadnews_btn="//input[@type='submit']";

	public String enterPostcode2_loc="xpath";
	public String enterPostcode2_textbox="//input[@type='text']";

	public String postenternewbox_txt="//input[@type='text']";
	public String postenternewbox_loc="xpath";

	public String doPostCodeLookUpForGoodnews_loc="xpath";
	public String doPostCodeLookUpForGoodnews_textbox="//input[@type='text']";

	public String PostCodeLookUpForGoodnews_btn="//input[@type='submit']";
	public String PostCodeLookUpForGoodnews_loc="xpath";

	public String enterPostcode3_loc="xpath";
	public String enterPostcode3_textbox="//input[@type='text']";

}