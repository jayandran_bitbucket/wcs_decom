package pageobjects.wcs;


public class Interstitials{

public String interstitialheader_txt = "//div[@class='IntHeaderMsg']";
public String interstitialheader_loc = "xpath";

public String finishedhere_btn = "//button[contains(text(),'Finished here')]";
public String finishedhere_loc = "xpath";

public String continuesoftconflict_btn = "conflicts-overlay-continue";
public String continuesoftconflict_loc = "id";

public String removefromtrolley_lnk = "conflict-product-remove";//"conflict-product-remove-parent";
public String removefromtrolley_loc = "id";

public String continueShopping_btn = "//a[@id='threshold-overlay-continue']";
public String continueShopping_loc = "xpath";

public String productGrid_btn = "m-product-cell";
public String productGrid_loc = "className";

public String addmoremissout_txt = "//div[@class='offer-heading']//*[contains(text(),'more')]";
public String addmoremissout_loc = "xpath";

public String offercomplete_txt = "//span[contains(text(),'Offer complete')]";
public String offercomplete_loc = "xpath";

public String incproductquantity_btn = "//div[@class='button add-button ']";
public String incproductquantity_loc = "xpath";

public String seeallthisoffer_btn = "//a[contains(text(),'See all in this offer')]";
public String seeallthisoffer_loc = "xpath";

public String hardconflictviewtrolley_btn = "(//div[@class='conflict-order-content']//a[@id='conflicts-overlay-trolley-button'])[2]";
public String hardconflictviewtrolley_loc = "xpath";

public String hardconflictchangeslot_btn = "(//div[@class='conflict-order-content']//div[@id='conflict-overlay-change-slot'])[2]";
public String hardconflictchangeslot_loc = "xpath";

public String continuebtndisabled_btn= "//div[@class='button content-button is-disabled']";
public String continuebtndisabled_loc = "xpath";

}