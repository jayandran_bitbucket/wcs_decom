package pageobjects.wcs;import java.util.List;

import com.cognizant.framework.Status;

public class HomePage{

	public static List<String> favproductid;

	public String Btn_BookSlot = "//button[text()='Book a slot']";
	public String locator_bookslot = "xpath";

	public String GroceryHeader_link = "//div[@id='headertopnav']//a[text()='Groceries']";
	public String GroceryHeader_loc = "xpath";

	public String search_box = "search";
	public String searchbox_loc = "id";

	public String search_btn = "search";
	public String serachbtn_loc = "name";

	public String trolley_btn = "headerMiniTrolley";
	public String trolleybtn_loc = "id";

	public String checkout_btn = "//div[@id='headerMiniTrolley']//button[text()='Checkout']";//"//button[text()='Checkout' and not(contains(@class,'disabled'))]";
	public String checkout_loc = "xpath";

	public String startshopping_btn = "//a[text()='Start shopping']";
	public String startshopping_loc = "xpath";

	public String trolleyprice_txt = "trolley-total";
	public String trolleyprice_loc = "className";

	public String myaccount_lnk = "headerMyAccountMenu";
	public String myaccount_loc = "id";

	public String myorder_lnk ="My Orders";
	public String myorder_loc = "linkText";

	public String favourite_lnk = "js-navbar-favourites";
	public String favourite_loc = "className";

	public String editslot_btn = "//button[text()='Edit slot']";
	public String editslot_loc = "xpath";

	public String changeslotInEditSlotOverlay_btn = "//button[text()='Change slot']";
	public String changeslotInEditSlotOverlay_loc = "xpath";

	public String autosuggestcategory_lnk = "//a[@class='autosuggestion category']";
	public String autosuggestcategory_loc = "xpath";

	public String groceryheader_lnk = "(//a[text()='Groceries'])[1]";
	public String groceryheader_loc = "xpath";

	public String EntertainingHeader_link = "//div[@id='headertopnav']//a[text()='Entertaining']";
	public String EntertainingHeader_loc = "xpath";
//acpt--//button[text()='Browse Shop']
	public String browseshop_btn = "//button[text()='Browse Shop']";
	public String browseshop_loc = "xpath";
	
	public String favouritesProductID_lnk = "//div[@class='favourite-products']//*[@class='m-product-cell']";
	public String favouritesProductID_loc = "xpath";

	public String pyo_lnk = "pyoOffer";
	public String pyo_loc = "className";

	public String myaccountinlist_lnk = "js-view-my-account";
	public String myaccountinlist_loc = "className";

	public String changeserivice_btn = "//button[contains(text(),'Change service')]";
	public String changeserivice_loc = "xpath";

	public String offertab_lnk = "//a[@class='fly-out-opener' and contains(text(),'Offers')]";
	public String offerstab_loc = "xpath";

	public String viewalloffers_lnk = "//img[@title='view all grocery offers']";
	public String viewalloffers_loc = "xpath";

	public String totalsavings_txt = "//span[@class='trolley-savings']";
	public String totalsavings_loc = "xpath";


	public  String returntoadminphone_lnk="//a[contains(text(),'Phone')]";
	public String returntoadminperson_lnk="//a[contains(tesxt(),'Person')]";
	public  String returntoadmin_Loc="xpath";

	public String Warrent1_img = "warrant1";
	public String Warrent1_loc = "className";


	public String Warrent2_img = "warrant2";
	public String Warrent2_loc = "className";

	public String findbranch_lnk = "find-a-branch";
	public String findbranch_loc = "className";

	public String mywaitrose_lnk = "my-waitrose";
	public String mywaitrose_loc = "className";

	public String itemcount_txt = "//span[@class='items']";
	public String itemcount_loc = "xpath";

	public String checkoutthreshold_txt = "//div[@class='tooltip']//strong[contains(text(),'Why can')]";
	public String checkoutthreshold_loc = "xpath";

	public String thresholddel_txt = "//div[@class='tooltip']//li[contains(text(),'The minimum order value for delivery is')]";
	public String thresholddel_loc = "xpath";

	public String thresholdcollection_txt = "//div[@class='tooltip']//*[contains(text(),'The minimum order value for collection is')]";
	public String thresholdcollection_loc = "xpath";

	public String productnotintrolleyfav_prod = "//div[contains(@class,'m-product  single') and not(contains(@class,'is-inbasket'))]/parent::div";
	public String productnotintrolleyfav_loc = "xpath";
	
	public static final String txtSearch="search";
	public static final String txtSearch_loc="id";
	
	public static final String keywordSearch="//form[@id='header-search-form']//article[@class='autosuggest-keyword-list']";
	public static final String keywordSearch_loc="xpath";  
	
	
	public static final String categorySearch="//form[@id='header-search-form']//article[@class='autosuggest-category-list']";
	public static final String categorySearch_loc="xpath";  
	
	public static final String productSearch="//form[@id='header-search-form']//h3[@class='suggested-product-title']";
	public static final String productSearch_loc="xpath";
	
	public static final String lnkAutoSuggestKeywords = "//article[@class='autosuggest-keyword-list']//a[@class='autosuggestion keyword']";
	
	public static final String breadCrumb = "//span[contains(text(),'Search results:')]";
	public static final String breadCrumb_Loc = "xpath"; 

	
	public static final String recipesGroupHeaderComp = "//div[@class='filter-section']//span[contains(@class, 'arrow-down')]";
	public static final String recipesGroupHeaderComp_loc = "xpath";
	
	public static final String chkleftPanelFilterRecipes = "//div[@class='filter-section']//div[@id='filter-section-rolled' and @style='display: block;']//input[@type='checkbox']";
	public static final String chkleftPanelFilterRecipes_loc = "xpath";
	
	public static final String lblCurrentBreadCrumb = "current-breadcrumb";
	public static final String lblCurrentBreadCrumb_loc = "id";
	
	public static String recipes = "//a[contains(text(),'Recipes')]";
	public static String recipes_loc ="xpath";
	
	public static String viewallrecipes = "//img[@alt='waitrose recipes']";
	public static String viewallrecipes_loc = "xpath";
	
	public static String viewrecipe = "//a[contains(text(),'VIEW RECIPES')]";
	public static String viewrecipe_loc = "xpath";
	
	public static String addselectedtotrolley = "//div[@id='add_to_trolley']";//a[contains(text(),'Add selected to trolley')]
	public static String addselectedtotrolley_loc = "xpath";
	
	public String mylist_lnk = "list";
	public String mylist_loc = "className";
	
	public String openlist_lnk = "//td[@class='listName']//a[@class='view']";
	public String openlist_loc = "xpath";
	
	public String addallitemsfromist_btn = "//a[contains(text(),'Add all items')]";
	public String addallitemsfromist_loc = "xpath";
	
	public String productselectlist_chkbx = "//div[@class='products-grid products-list']//input[@type='checkbox']";
	public String productselectlist_loc = "xpath";
	
	public String productquantity_txt = "//div[@class='products-grid products-list']//input[@type='number']";
	public String productquantity_loc = "xpath";
	
	public String addproductlist_btn = "//div[@class='products-grid products-list']//a[@title='Add a single item to trolley']";
	public String addproductlist_loc = "xpath";
	
	public String addselecteditemtotrolley_btn = "//input[@value='Add selected to trolley']";
	public String addselecteditemtotrolley_loc = "xpath";
	
	public String productunavailable_btn = "(//div[@class='items-not-added-confirm']//a[contains(text(),'Ok')])[5]";
	public String productunavailable_loc = "xpath"; 
	
	public String editslotvalidation_btn = "//button[contains(text(),'Edit slot')]";
	public String editslotvalidation_loc ="xpath";
	
	public String addallitems = "//a[contains(text(),'Add all to my trolley')]";
	public String addallitems_loc = "xpath";
	
	public String postcode = "//input[@class='input-field postcode js-postcode-lookup-text']";
	public String postcode_loc = "xpath";
	
	public String checkavailability= "//input[@class='postcode-check primary-cta button js-postcode-lookup']";
	public String checkavailability_loc = "xpath";

	public String BookSlotEnt_Btn = "//a[@class='bookslot-button']";
	public String bookSlotEnt_loc = "xpath";
	
	
	public String bookslot_btn="//a[@title='Book slot']"; 
	public String bookslot_loc="xpath";

	public String minitrolley_img="//div[@class='trolley-item-qty']/p";
	public String minitrolley_loc="xpath";
	
	public String lnkSelectedSlot = "//*[contains(text(),'Edit slot')]";
    public String lnkSelectedSlot_Loc = "xpath";
    
    public String lnkInspiration="//a[contains(text(),'Inspiration')]";
	public String lnkInspiration_loc="xpath";
	
	public String txtInpirationHdng="//h1[contains(text(),'Inspiration')]";
	public String txtInpirationHdng_loc="xpath";
	
	public String txtRecipesHdng="//h1[@class='pageTitle' and contains(text(),'Recipes')]";
	public String txtRecipesHdng_loc="xpath";
	
	public String lnkRecipes="Recipes";
	public String lnkRecipes_loc="linkText";
	
	public String TVhome="//a[@class='home']";
	public String TVhome_loc="xpath";
	
	public String TVlnk="//a[contains(text(),'TV')]";
	public String TVlnk_loc="xpath";
	
	public String txtFlowers="//span[contains(text(),'Florist')]";
	public String txtFlowers_loc="xpath";
	
	public String txtGifts="//span[contains(text(),'Gifts')]";
	public String txtGifts_loc="xpath";
	
	public String txtGarden="//span[contains(text(),'Garden')]";
	public String txtGarden_loc="xpath";
	
	public String lnkfootercontacttxt_loc = "id";
	public String lnkfootercontacttxt = "footer_foot"; 
	
	public String footer_loc = "className";
	public String footer = "footerFloristWLogo";
	
	public String lnkfooter = "//div[@class='footer-column']";
	public String lnkfooter_loc = "xpath";

	public String lnkfootercontact = "//div[@class='footer-column last']";
	public String lnkfootercontact_loc = "xpath";
	
	public String linkCompanyInfo = "footer-tab";////a[contains(text(),'Company information')] footer-wrapper
	public String linkCompanyInfo_loc = "className";//xpath
	
	public String linkmyWaitroiseCard_loc = "xpath";
	public String linkmyWaitroiseCard ="//a[contains(text(),'myWaitrose')]";
	
}