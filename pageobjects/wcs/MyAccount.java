package pageobjects.wcs;
import java.util.List;

import com.cognizant.framework.Status;

public class MyAccount{
	
	public String myaddresses = "//a[contains(text(),'My Addresses')]";
	public String myaddresses_loc = "xpath";
	//Screen Name updated
			public static final String txtScreenName="//input[@id='screen-name-1']";//input[@id='screen-name']";
			public static final String txtScreenName_loc="xpath";
			
			public static final String btnSave="//input[@value='Save']";
			public static final String btnSave_Loc="xpath";
	
	public String signout_btn = "//a[contains(text(),'Sign out')]";
	public String signout_loc = "xpath";
	
	public String joinmywaitrose_lnk = "//a[contains(text(),'Join myWaitrose')]";
	public String joinmywaitrose_loc = "xpath";
	
	public String registercardonline_lnk = "//label[contains(text(),'Register my card online')]";
	public String registercardonline_loc = "xpath";
	
	public String cardno_txt = "myWaitroseCard";
	public String cardno_loc = "id";
	
	public String continue_btn = "Continue";
	public String continue_loc = "linkText";
	
	public String mywaitrosecardnumber_lnk = "myWaitrose card number";
	public String mywaitrosecardnumber_loc = "linkText";
	
	public String optout_lnk = "Opt out of myWaitrose";
	public String optout_loc = "linkText";
	
	public String optoutmywaitrose_btn = "//input[@value='Opt out of myWaitrose']";
	public String optoutmywaitrose_loc = "xpath";
	
	public String optoutoverlayclose_btn = "//a[@class='close']";
	public String optoutoverlayclose_loc = "xpath";
	
	public String mwnickname_txt = "//p[@class='nickname' and contains(text(),'myWaitrose address')]";
	public String mwnickname_loc = "xpath";
	
	public String paymentdetails_lnk = "//a[contains(text(),'Payment Details')]";
	public String paymentdetails_loc = "xpath";
	
	public String save_btn = "//input[@value='Save']";
	public String save_loc = "xpath";
	
	public String cardNumber_txt = "card_number";
	public String cardNumber_loc = "id";
	
	public String endMonth_dropdown = "endMonth";
	public String endMonth_loc = "id";
	
	public String endYear_dropdown = "//select[@id='endYear']";
	public String endYear_loc = "xpath";
	
	public String enterName_txt = "card_name";
	public String enterName_loc = "id";
	
	public String MobileNumber = "//input[@id='phone2']";
	public String MobileNumber_loc = "xpath";
	
	public String PersonalDetails = "//a[contains(text(),'Personal details')]";
	public String PersonalDetails_loc = "xpath";
	
	public String orderpref_yes = "txtMsgs";
	public String orderpref_yes_loc = "id";
	
	public String orderpref_no = "noTxtMsgs";
	public String orderpref_no_loc = "id";
	
	public static String preference;
	
}