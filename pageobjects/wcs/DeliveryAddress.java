package pageobjects.wcs;

public class DeliveryAddress{
	
	public String DeliveryAddress_pageheader = "";
	public String DeliveryAddress_loc = "";
	
	public String nickName_txt = "nickName";
	public String nickName_loc = "id";
	
	public String title_txt = "title";
	public String title_loc = "id";
	
	public String firstName_txt = "firstName";
	public String firstName_loc = "id";
	
	public String lastName_txt = "lastName";
	public String lastName_loc = "id";
	
	public String findMyAddress_btn = "//a[contains(text(),'Find my address')]";
	public String findMyAddress_loc = "xpath";
	
	public String postcodeaddress_link = "address";
	public String postcodeaddress_loc = "className";
	
	public String phonenumber_txt = "phone1";
	public String phonenumber_loc = "id";
	
	public String saveandcontinue_btn = "//*[@value='Save and continue']";
	public String saveandcontinue_loc = "xpath";
	
	public String postcode_txt = "zipCode";
	public String postcode_loc = "id";
	
	public String savebillingaddress = "//input[@id='save_new_billing_address']";
	public String savebillingaddress_loc = "xpath";
	
	
	
}