package pageobjects.wcs;

import java.util.List;

public class PYO {
	
	public static List<String> PYOProductID;
	public String PYOProducts_btn = "//div[@class='pyoOffers-grid']//div[@class='m-product-cell']";
	public String PYOProducts_loc = "xpath";
	
	public String uomPYO_txt = "//div[@class='l-content pyo-all-offers']//span[contains(text(),'per')]";
	public String uomPYO_loc = "xpath";
	
	public String reviewsPYO_txt = "//div[@class='l-content pyo-all-offers']//span[contains(text(),'reviews')]";
	public String reviewsPYO_loc = "xpath";
	
	public String waspricePYO_txt = "//span[@class='was-price was']";
	public String waspricePYO_loc = "xpath";
	
	public String nowpricePYO_txt = "//div[@class='l-content pyo-all-offers']//span[@class='price trolley-price']";
	public String nowpricePYO_loc = "xpath";
	
	public String perofferred_lnk = "//a[contains(text(),'20% OFF')]";
	public String perofferred_loc = "xpath";
	
	public String perofferlogo_img = "//span[contains(text(),'20% OFF')]";
	public String perofferlogo_loc = "xpath";
	
	
	public String prodalt_img = "//div[@class='l-content pyo-all-offers']//img[@alt]";
	public String prodalt_loc = "xpath";
	
	public String pickbutton_btn = "//div[@class='products-grid']//div[@class='button content-button select-offer']//a[@class='add']";
	public String pickbutton_loc = "xpath";
	
	public String confirmmypicksdisable_btn = "disableSave";
	public String confirmmypicksdisable_loc = "className";
	
	public String confirmmypicks_btn = "save-offers";
	public String confirmmypicks_loc = "id";
	
	public String removemypicks_btn = "//div[@class='pyoOfferSelected']//div[@class='m-product-cell']//a[@class='remove']";
	public String removemypicks_loc = "xpath";
	
	public String pickdisabled_btn = "//div[@class='button content-button select-offer is-disabled']";
	public String pickdisabled_loc = "xpath";
	
	public String selectedPYOprod_grid = "//div[@class='pyoOfferSelected']//div[@class='m-product-cell']//img";
	public String selectedPYOprod_loc = "xpath";
	
	public String confirmpicksoverlay_header = "//div[@class='lightbox-container content-wrapper']//h2[contains(text(),'Confirm Your Picks')]";
	public String confirmpicksoverlay_loc = "xpath";
	
	public String confirmoverlayconfirm_btn = "//a[@id='continue']";
	public String confirmoverlayconfirm_loc = "xpath";
	
	public String confirmoverlaycancel_btn = "cancel";
	public String confirmoverlaycancel_loc = "id";
	
	public String linktoPDP = "//div[@class='l-content pyo-all-offers']//a[@class='m-product-open-details']";
	public String linktoPDP_loc = "xpath";
	
	public String confirmedproduct_tag = "//span[@class='label confirmed']";
	public String confirmedproduct_loc = "xpath";
	
	public String registercard_btn = "register-card";
	public String registercard_loc = "id";
	
	public String joinmywaitrose_btn = "join-waitrose";
	public String joinmywaitrose_loc  = "id";
	
	
	public String cancel_btn = "cancel";
	public String cancel_loc = "id";
	
public String youalmost_txt = "//p[contains(text(),'You have almost reached the maximum number of Unpicks allowed for this period. You can only make another 2 Unpick')]";
public String youalmost_loc = "xpath";

public String youreached_txt = "//p[contains(text(),'You have reached the maximum number of Unpicks')]";
public String youreached_loc = "xpath";



public String warningcount_txt = "//p[@class='limit-warning-message']";
public String warningcount_loc = "xpath";
	
}