package pageobjects.wcs;


public class Trolley{
	
	public static double TrolleypriceBfr = 0.0;
	public static int TrolleyquantityBfr = 0;
	
	public static double TrolleypriceAfr = 0.0;
	public static int TrolleyquantityAfr = 0;
	
	public static double WCSTrolleyprice = 0.0;
	public static int WCSTrolleyquantity = 0;
	
	public String Trolleytotal_txt = "trolley-total";
	public String Trolleytotal_loc = "className";
	
	public String Trolleyquantity_txt ="items";
	public String Trolleyquantity_loc = "className";
	
	public String emptyTrolley_lnk = "//a[text()='Empty trolley']";
	public String emptyTrolley_loc = "xpath";
	
	public String confirmEmptyTrolley_btn = "//a[text()='Confirm']";
	public String confirmEmptyTrolley_loc = "xpath";
	
	public String noofitems_txt = "total-items";
	public String noofitems_loc = "id";
	
	public String allowsubstitution_chkbx = "trolley-substitution-link";
	public String allowsubstitution_loc = "className";
	
	public String plusbutton_btn = "//a[@class='plus']";
	public String plusbutton_loc = "xpath";
	
	public String minusbutton_btn = "//a[@class='minus']";
	public String minusbutton_loc = "xpath";
	
	public String removeproduct_lnk = "(//span[contains(text(),'Remove from trolley')])[2]"; //initial one://span[contains(text(),'Remove from trolley')]
	public String removeproduct_loc = "xpath";
	
	public String addalltopendingorder_lnk = "//a[contains(text(),'Add all to pending order')]";
	public String addalltopendingorder_loc = "xpath";
	
	public String amendorder_txt = "//p[contains(text(),'You are amending order')]";
	public String amendorder_loc = "xpath";
	
	public String cancelchanges_lnk = "//a[contains(text(),'Cancel changes')]";
	public String cancelchanges_loc = "xpath";
	//li[contains(text(),' My Offers Savings ')]//span
	public String myoffersaving_txt = "//span[@class='trolley-savings']";
	public String myoffersaving_loc = "xpath";
	
	public static String myoffersav = "";
	
	public  String changeslot_btn = "(//div[@class='section section-conflicts-overlay-entertaining-gen-collection']//a[@id='change-entertaining-gen-collection'])[2]";
	public  String changeslot_loc = "xpath";
	
	public  String changetoent_txt = "(//p[contains(text(),'Your Groceries collection slot has been changed to Entertaining collection slot. You can now proceed to Checkout.')])[2]";
	public  String changetoent_loc = "xpath";
	
	public String closechangeslot_btn = "(//div[@class='button content-button no-image']//a[@id='change-entertaining-gen-collection'])[2]";
	public String closechangeslot_loc = "xpath";
	//(//div[@class='button content-button no-image']//a[@id='change-entertaining-gen-collection'])[2]
//(//div[@class='toolbox-close']//a)[2]	
}