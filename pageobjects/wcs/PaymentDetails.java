package pageobjects.wcs;


public class PaymentDetails{
	
	public String paymentpage_header="payment-card";
	public String paymentpage_loc = "class";
	
	public String cardNumber_txt = "cardNumber";
	public String cardNumber_loc = "id";
	
	public String endMonth_dropdown = "endMonth";
	public String endMonth_loc = "id";
	
	public String endYear_dropdown = "endYear";
	public String endYear_loc = "id";
	
	public String enterName_txt = "nameOnCard";
	public String enterName_loc = "id";
	
	public String save_chckbox = "saveCard";
	public String save_loc = "id";
	
	public String placeorderupper_btn = "place-order-upper";
	public String placeorderupper_loc = "id";

	public String GiftcardNum_box="giftCardNumber";
	public String GiftcardNum_loc="id";
	
	public String GiftPin_box="giftCardPIN";
	public String GiftPin_loc="id";
	
	public String ApplyGift_btn="add-gift-card";
	public String ApplyGift_loc="id";
	
	public String viewGiftCard_lnk="view-gift-card";
	public String viewGiftCard_loc="id";
	
	public String acceptAPDDiscount_btn = "acceptCard";
	public String acceptAPDDiscount_loc = "id";
	
	public String rejectAPDDiscount_btn = "declineCard";
	public String rejectAPDDiscount_loc = "id";
	
	public String payoncollection_btn = "payOnCollection";
	public String payoncollection_loc = "id";
	
	public String removegiftcard_lnk = "removeGiftCard";
	public String removegiftcard_loc = "id";
	
	public String removegiftvoucher_lnk = "remove_voucher";
	public String removegiftvoucher_loc = "className";
	
	public String viewgiftvoucher_lnk = "view-gift-voucher";
	public String viewgiftvoucher_loc = "id";
	
	public String voucherserialno_txt = "serialNumberId";
	public String voucherserialno_loc = "id";
	
	public String voucherserialcode_txt = "securityCodeId";
	public String voucherserialcode_loc = "id";
	
	public String addgiftvoucher_btn = "add-gift-voucher";
	public String addgiftvoucher_loc = "id";
	
	public String vouchercannotberemoved_txt = "//div[@class='payment_voucher_added used']//p[@class='remove_voucher' and contains(text(),'Cannot be removed')]";
	public String vouchercannotberemoved_loc = "xpath";
	
	public String giftcardcannotberemoved_txt = "//div[@class='payment_gift_added used']//p[@class='remove_voucher' and contains(text(),'Cannot be removed')]";
	public String giftcardcannotberemoved_loc = "xpath";
	
	public String paywithdiffcard_lnk = "Pay with a different card";
	public String paywithdiffcard_loc = "linkText";
	
	public String newbilladd_radio = "newBillingAddressRadio";
	public String newbilladd_loc = "id";
	
	public String placeorder_res = "processnext";
	public String placeorder_res_loc = "id";

	public String cardNumber = "card_number";//cardNumber
	public String cardNumbertxt_loc = "id";
	
	public String Nameoncard_txt ="card_name";
	public String Nameoncard_loc ="id";
	
	public String savebtn = "//input[@value='Save']";
	public String savebtn_loc = "xpath";
	
}