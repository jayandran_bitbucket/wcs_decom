package pageobjects.wcs;

import java.util.List;

public class BrowseShop{
	
	public static List<String> filtertags;
	
	public String browseshop_btn = "//button[text()='Browse Shop']";
	public String browseshop_loc = "xpath";
	
	public String browsecategoryactive_lnk = "//div[@class='dropdown-menu megaMenu']//a[@class='btn btn-default js-categories active']";
	//public String browsecategoryactive_lnk = "btn btn-default js-categories active";
	public String browsecategoryactive_loc = "xpath";
	//
	public String browsecategory_lnk = "//a[@class='btn btn-default js-categories active']";
	public String browsecategory_loc = "xpath";
	//btn btn-default js-categories
	public String firstcategory_lnk = "//a[@data-level='1']";
	public String firstcategory_loc = "xpath";
	
	public String secondcategory_lnk = "//a[@data-level='2']";
	public String secondcategory_loc = "xpath";
	
	public String thirdcategory_lnk = "//a[@data-level='3']";
	public String thirdcategory_loc = "xpath";
	
	public String filtersec_lnk = "//div[@id='filter-section-rolled']//a";
	public String filtersec_loc = "xpath";
	
	////a[@class='offer' and contains(text(),'Add')]
	public String offerred_txt = "//a[@class='offer' and contains(text(),'Add')]"; // this one is for buy 2 save 1 pound ; //a[@class='offer' and contains(text(),'Add')]-- this is for the offer'ADD 2 get offer like that'
	public String offerred_loc = "xpath";
	
	public String productthatare_filter = "//span[contains(text(),'Products that are')]";
	public String productthatare_loc = "xpath";
	
	public String onoffer_chkbx = "//input[@name='On Offer']";
	public String onoffer_loc = "xpath";
	
	
	
}