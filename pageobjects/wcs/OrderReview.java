package pageobjects.wcs;


public class OrderReview{
	
	public static String[] productslinenumber = null;
	
	public String continuetopayment_btn = "processnext";
	public String continuetopayment_loc = "id";
	
	public String productlinenumber_img = "//div[@class='preview-item']//div[@class='image']/img[not(contains(@title,'Shelf'))]";//"//div[@class='preview-item']//div[@class='image']/img";
	public String productlinenumber_loc = "xpath";
	
	public String placeorderloan_btn = "loanprocessnext";
	public String placeorderloan_loc = "className";
	
	public String loanlinenumber_img = "//div[@class='image']/img";
	public String loanlinenumber_loc = "xpath";
	
	public String incentivecode_box = "addIncentiveCode";
	public String incentivecode_loc = "id";
	
	public String ordertotal_txt = "orderTotal";
	public String ordertotal_loc = "id";
	
	public String addIncentivecode_btn = "addIncentiveCodeButton";
	public String addIncentivecode_loc = "id";
	
	public String incentivesavingvalue_txt = "existingIncentiveSaving";
	public String incentivesavingvalue_loc = "id";
	
	public String myWaitroseSavings_txt = "//tr/th[text()='myWaitrose Savings']/../td";
	public String myWaitroseSavings_loc = "xpath";
	
	public String myOfferSavings_txt= "//tr//th[text()='My Offers Savings']/../td";
	public String myOfferSavings_loc = "xpath";
	
	public String deliverynote_txt = "deliveryNote";
	public String deliverynote_loc = "id";
	
	public String collectionnote_txt = "//textarea[@name='fulfilmentnotes']";
	public String collectionnote_loc = "xpath";
	
	public String fasttrack_opt = "fasttrack";
	public String fasttrack_loc = "id";
	
	public String carriercharge_txt = "//tr[@id='carrierBagCharge']//th/following-sibling::td//span";
	public String carriercharge_loc = "xpath";
	
	public static String carrierbagchargevalue = "";
	
	public String nobag_opt = "nobag";
	public String nobag_loc = "id";
	
	public String yesbag_opt = "buybag";
	public String yesbag_loc = "id";
	
	public String whatsthis_lnk = "//a[contains(text(),'What is this?')]";
	public String whatsthis_loc = "xpath";
	
	public String carriercontent_txt = "//p[contains(text(),'Carrier')]";
	public String carriercontent_loc = "xpath";
	
	public String carrierclose_btn = "//div[@class='contentview lightbox-container content-wrapper undefined']//a[@class='close']";
	public String carrierclose_loc = "xpath";
	
	public String tooltip_btn = "//a[@class='help-content']";
	public String tooltip_loc = "xpath";
	
	public String tooltopcontent1_txt = "//p[contains(text(),'You can choose to opt for a bagless delivery or collection at no charge.')]";
	public String tooltopcontent2_txt = "//p[contains(text(),'If you opt for a delivery with bags you will be charged a flat rate of 40p or 30p for collection orders.')]";
	public String tooltopcontent3_txt = "//p[contains(text(),'All proceeds from carrier bag sales in England will go into a Community and Environmental Fund.')]";
	public String tooltopcontent_loc = "xpath";
	
	public String mobilenumber = "//input[@id='phone2']";
	public String mobilenumber_loc = "xpath";
	
	public String errormsg = "//div[@class='error-msg']";
	public String errormsg_loc = "xpath";
	
	public String obj = "//input[@id='addIncentiveCode']";
	public String obj_loc = "xpath";
	
	public String claim = "acceptCard";
	public String claim_loc = "id";
	
	public String dontclaim = "declineCard";
	public String dontclaim_loc = "id";
	
	public static double promodiscount;
	
	public String removepromo_loc = "xpath";
	public String removepromo = "//a[@class='remove_incentive_icon']";
	
	public String btnPlaceYourOrderInLoanItem = "(//a[text()='Place your order'])";
	public String btnPlaceYourOrderInLoanItem_Loc = "xpath";
	
	public String varOrderNumber="//tr//th[contains(text(),'reference number')]/../td";
	public String varOrderNumber_Loc="xpath";
}