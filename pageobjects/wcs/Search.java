package pageobjects.wcs;

public class Search {
	
	public String PLPproduct_table = "m-product-details-container";
	public String PLPproducttable_loc = "className";
	
	public String PLPproductNames_txt = "m-product-open-details";
	public String PLPproductNames_loc = "className";
	
	public String PLPproductQty_field = "quantity-input";
	public String PLPproductQty_loc = "className";
	
	public String PLPproductAdd_btn = "//a[text()='Add']";
	public String PLPproductAdd_loc = "xpath";
	
	public String PDPquantity_box = "//input[@class='quantity-input']";
	public String PDPquantity_loc = "xpath";
	
	public String PDPAddtotrolley_btn = "//a[text()='Add to trolley']";
	public String PDPAddtotrolley_loc = "xpath";
	
	public String personalizedmsg_box = "//textarea[contains(@id,'txtGreeting')]";
	public String personalizedmsg_loc = "xpath";
	
	public String personalizedmsgsave_btn = "//a[text()='Save and continue']";
	public String personalizedmsgsave_loc = "xpath";	
	
	public String PYOqty_txt = "//div[@class='products-grid pyo-selected-offers']//input";
	public String PYOqty_loc = "xpath";
	
	public String PYOadd_btn = "//div[@class='products-grid pyo-selected-offers']//a[text()='Add']";
	public String PYOadd_loc = "xpath";
	
	 public static final String txtAllProdPrice="//div[@class='m-product-cell']//span[@class='price trolley-price']";
	 public static final String txtAllProdPrice_loc="xpath";
	    
	 public static final String lnkSort="sort";//div[@class='tab groceries']/div/div[2]/form/fieldset/select[@id='sort']";
	    public static final String lnkSort_loc="id";
	    
	    public static final String linkOurBrandView="(//div[@class='filter-view-control'])[1]/a[2]";
		public static final String linkOurBrandView_loc="xpath";
		//offertesxt
		public static final String txtOfffil="current-breadcrumb";
		public static final String txtOfffil_loc="id";
		
		public static final String chkOffer="((//div[@id='filter-section-rolled'])[1]/ul/li/input)[1]";
		public static final String chkOffer_loc="xpath";
		
		 public static final String txtnoproducts="//div[@class='products-grid']/div[@class='products-row']/div";
		    public static final String txtnoproducts_loc="xpath";
		    
		    public static final String leftPanelSearchTopHeader ="//div[@class='leftNavCategory  js-collapsible']/ul/li/a";   // Old Obj Identifier - "//nav[@class='refinement']/ul/li/a";
			public static final String leftPanelSearchTopHeader_Loc = "xpath";
			
			public static final String leftPanelSearchFirstLevelParentProduct = "//*[@id='content']/div/div[2]/nav/nav/ul/li/a";
			public static final String leftPanelSearchFirstLevelParentProduct_Loc = "xpath";
				
			//Left Search left Panel Search Next Level Product 
			public static final String leftPanelSearchNextLevelProduct = "//*[@id='content']/div/div[2]/nav/nav/ul/li/ul[1]/li/a";
			public static final String leftPanelSearchNextLevelProduct_Loc = "xpath";
			
			public static final String productContainerRow = "//div[@class='products-row']";//div[@class='filter-section']/li/div/ul/div[@id='filter-section-rolled']/li/input
			public static final String productContainerRow_Loc = "xpath";
			
			public static final String leftPanelFilterLinks = "//div[@role='tabpanel' and @style='display: block;']//div[@id='filter-section-rolled']/li/a";//div[@class='filter-section']/li/div/ul/div[@id='filter-section-rolled']/li/input
			public static final String leftPanelFilterLinks_loc = "xpath";
			
			public static final String chkleftPanelUnFilterRecipes = "//div[@class='filter-section']//div[@id='filter-section-rolled' and contains(@class, 'block')]//input[@type='checkbox']";
			public static final String chkleftPanelUnFilterRecipes_loc = "xpath";
			
			public static final String chkleftPanelFilterInspiration = "//nav[@class='refinement']//input[@type='checkbox']";
			public static final String chkleftPanelFilterInspiration_loc = "xpath";
			
			public static final String leftpanel ="//a[starts-with(@class,'menutitle')]";
			public static final String leftpanel_loc = "xpath";
			
			public String multisearch_btn = "//a[contains(text(),'Multi-search')]";
			public String multisearch_loc  = "xpath";
			
			public String jottersearch_txt = "//textarea[@name='jotter-search']";
			public String jottersearch_loc = "xpath";
			
			public String searchtheseitems_btn = "//input[@value='Search these items']";
			public String searchtheseitems_loc= "xpath";
			
			public String jottersearchresults_txt = "//div[@class='jotter-result products products-grid']//h2";
			public String jottersearchresults_loc = "xpath";
			
			public String noticeperiod_txt = "//span[@class='notice-time-display']";
			public String noticeperiod_loc = "xpath";
			
			public String bookslotfromoverlay_btn = "//a[@class='bookslot-button']";
			public String bookslotfromoverlay_loc = "xpath";
			
			public String seasonalcontinuebookslot_btn = "//a[contains(text(),'Continue Shopping')]";
			public String seasonalcontinuebookslot_loc = "xpath";
			
			public String addtolist_lnk = "//a[contains(text(),'Add to a list')]";
			public String addtolist_loc = "xpath";
			
			public String createnewlist_txt = "create_new_list";
			public String createnewlist_loc = "id";
			
			public String addtolistbtn_btn = "//a[@class='add-button']";
			public String addtolistbtn_loc = "xpath";
			
			public String oklist_btn= "//a[@class='cancel-list-button']";
			public String oklist_loc = "xpath";
			
			
			
	
}