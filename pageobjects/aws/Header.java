package pageobjects.aws;

public class Header {

	public static double AWSTrolleypriceBfr = 0.0;
	public static int AWSTrolleyquantityBfr = 0;
	
	public static double AWSTrolleypriceAfr = 0.0;
	public static int AWSTrolleyquantityAfr = 0;
	
	public static double AwsTrolleyprice = 0.0;
	public static int AwsTrolleyquantity = 0;
	
	public String AWSTrolleytotal_txt = "//span[@id='tTp']/strong";
	public String AWSTrolleytotal_loc = "xpath";
	
	public String AWSTrolleyquantity_txt ="tTq";
	public String AWSTrolleyquantity_loc = "id";
	
	public static String AWSSlotDate = "";
	public static String AWSSlotMonth = "";
	
	public String slotdate_txt = "tSd";
	public String slotdate_loc = "id";
	
	public String checkout_btn	= "//button[text()='Checkout']";//"//div[@id='tCc']/a";
	public String checkout_loc = "xpath";
	
	public String slot_link = "//*[contains(@class,'slotDetails')]";//"//div[contains(@class,'slotDetails')]";
	public String slot_loc = "xpath";
	
	public String AWSTrolleySavings_txt = "tTs";//*[@id='tTs']//text()[2]";//"tTs";
	public String AWSTrolleySavings_loc = "id";
	
	public String AWSTrolleyHeader_lnk = "tTh";
	public String AWSTrolleyHeader_loc = "id";
	
}