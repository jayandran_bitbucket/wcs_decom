package pageobjects.aws;

public class Search {
	
	public String searchTerm_box = "searchTerm";
	public String searchTerm_loc = "id";
	
	public String searchbtn = "searchSubmit";
	public String searchbtn_loc = "id";
	
	public String PLPproduct_table = "tPp";
	public String PLPproducttable_loc = "id";
	
	public String PLPproductNames_txt = "//div[@data-test='product-pod-name']/div";
	public String PLPproductNames_loc = "xpath";
	
	public String PLPproductQty_field = "tQty";
	public String PLPproductQty_loc = "id";
	
	public String PLPproductAdd_btn = "//button[contains(@id,'tAbtn')]";
	public String PLPproductAdd_loc = "xpath";
	
	public String PLPproductRemove_btn = "//button[contains(@id,'tRbtn')]";
	public String PLPproductRemove_loc = "xpath";
	
	public String suggestedtag_slide = "//section[contains(@class,'suggestedTags')]//ul";//"//section[@class='suggestedTags___3VKts']//ul";
	public String suggestedtag_loc = "xpath";
	
	public String entertaining_lnk = "//a[text()='entertaining']";
	public String entertaining_loc = "xpath";
	
	public String clearall_btn = "searchClear";
	public String clearall_loc = "id";
	
	public String Pyo_logo = "//span[contains(@class,'icon-pyoTag')]";
	public String Pyo_loc = "xpath";
	
	public String fav_logo = "//span[contains(@class,'icon-heart')]";
	public String fav_loc = "xpath";
	
}