package testscripts.wcssmoke;

import java.util.concurrent.TimeUnit;

import org.testng.annotations.Test;

import pageobjects.wcs.Trolley;

import com.cognizant.framework.Status;

import functionallibraries.OF.Func_OFCustomerSearch;
import functionallibraries.OF.Func_OFHomePage;
import functionallibraries.OF.Func_OFIntegratorMessages;
import functionallibraries.OF.Func_OFLogin;
import functionallibraries.OF.Func_OFOrderDetailsPage;
import functionallibraries.OF.Func_OFOrderSearch;
import functionallibraries.OF.Func_OFDailySlots;
import functionallibraries.aws.Func_AWSHeader;
import functionallibraries.aws.Func_AWSSearch;
import functionallibraries.wcs.Func_BookSlot;
import functionallibraries.wcs.Func_BrowseShop;
import functionallibraries.wcs.Func_DeliveryAddressDetails;
import functionallibraries.wcs.Func_FastMail;
import functionallibraries.wcs.Func_HomePage;
import functionallibraries.wcs.Func_Interstitials;
import functionallibraries.wcs.Func_Login;
import functionallibraries.wcs.Func_MyAccount;
import functionallibraries.wcs.Func_MyAddressPage;
import functionallibraries.wcs.Func_MyOrdersPage;
import functionallibraries.wcs.Func_OrderConfirmation;
import functionallibraries.wcs.Func_OrderReview;
import functionallibraries.wcs.Func_PaymentDetails;
import functionallibraries.wcs.Func_Search;
import functionallibraries.wcs.Func_Trolley;
import supportlibraries.DriverScript;
import supportlibraries.TestCase;

public class SS_12 extends TestCase
{
	private Func_Login LoginPage;
	private Func_HomePage HomePage;
	private Func_AWSHeader AWSHeader;
	private Func_BookSlot BookSlot;
	private Func_Search Search;
	private Func_Interstitials interstitials;
	private Func_DeliveryAddressDetails deliveryaddress;
	private Func_OrderReview review;
	private Func_PaymentDetails payment;
	private Func_OrderConfirmation confirmation;
	private Func_OFLogin oflogin;
	private Func_OFHomePage ofhome;
	private Func_OFCustomerSearch ofcustomersearch;
	private Func_OFOrderSearch ofordersearch;
	private Func_OFOrderDetailsPage oforderdetails;
	private Func_FastMail fastmail;
	private Func_Trolley WcsTrolley;
	private Func_MyOrdersPage myorders;
	private Func_MyAccount myaccount;
	private Func_MyAddressPage myaddress;
	private Func_BrowseShop Browseshop;
	private Func_OFIntegratorMessages OFmsg;
	private Func_OFDailySlots dailyslots;
	public double TrolleyPrice;
	public double TrolleyPriceBefOrder,TrolleyPriceAfterOrder;
	public String savings;

	@Test()
	public void runTC1()
	{

		//	testParameters.setCurrentTestDescription("Beta Customer - Grocery Delivery Amend Order Flow");
		//	testParameters.setIterationMode(IterationOptions.RunOneIterationOnly);
		//testParameters.setBrowser(Browser.HtmlUnit);

		driverScript = new DriverScript(testParameters);
		driverScript.driveTestExecution();
	}

	@Override
	public void setUp()
	{

		LoginPage = new Func_Login(scriptHelper);
		HomePage = new Func_HomePage(scriptHelper);
		AWSHeader = new Func_AWSHeader(scriptHelper);
		BookSlot = new Func_BookSlot(scriptHelper);
		Browseshop = new Func_BrowseShop(scriptHelper);
		Search = new Func_Search(scriptHelper);
		interstitials = new Func_Interstitials(scriptHelper);
		review = new Func_OrderReview(scriptHelper);
		oflogin = new Func_OFLogin(scriptHelper);
		ofhome = new Func_OFHomePage(scriptHelper);
		payment = new Func_PaymentDetails(scriptHelper);
		confirmation = new Func_OrderConfirmation(scriptHelper);
		deliveryaddress = new Func_DeliveryAddressDetails(scriptHelper);
		ofcustomersearch = new Func_OFCustomerSearch(scriptHelper);
		ofordersearch = new Func_OFOrderSearch(scriptHelper);
		oforderdetails = new Func_OFOrderDetailsPage(scriptHelper);
		fastmail = new Func_FastMail(scriptHelper);
		WcsTrolley = new Func_Trolley(scriptHelper);
		myorders = new Func_MyOrdersPage(scriptHelper);
		myaccount = new Func_MyAccount(scriptHelper);
		myaddress = new Func_MyAddressPage(scriptHelper);
		OFmsg = new Func_OFIntegratorMessages(scriptHelper);
		dailyslots = new Func_OFDailySlots(scriptHelper);
		report.addTestLogSection("Setup");
		LoginPage.setChromeExtension();
		driver.get(properties.getProperty("ApplicationUrl"));
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		report.updateTestLog("Invoke Application", "Invoke the application under test @ " +
				properties.getProperty("ApplicationUrl"), Status.DONE);
	}
	@Override
	public void executeTest() 
	{
		LoginPage.register();
		HomePage.clickMyAccountLink();
		HomePage.clickMyAccountInMyAccountList();
		myaccount.clickMyAddressesInMyAccount();
		myaddress.clickAddNewAddress();
		myaddress.enterNewAddress();
		HomePage.clickMyAccountLink();
		myaccount.clickSignOut();
		oflogin.launchOFApp();
		oflogin.loginOF();
		ofhome.clickEquipmentLoanOrderLink();
		ofcustomersearch.performCustomerSearch();
		ofcustomersearch.clickCustomerEmailLink();
		ofcustomersearch.clickPlaceOrderButtonInLoan();
		BookSlot.selectCollectionPoint();
		BookSlot.selectSlot();
		BookSlot.clickAlcoholContinue();
		BookSlot.clickContinueBtnInCollectionSlotOverlay();
		HomePage.clickStartShoppingButton();
		Search.enterQtyandAddProductInPLP();
		HomePage.clickCheckOutBtn();
		review.clickbtnPlaceYourOrderinLoanItem();
		confirmation.chkLoanItemDetails();
		confirmation.getOrderReferenceNumber();
		report.addTestLogSection("Setup");
		LoginPage.setChromeExtension();
		driver.get(properties.getProperty("ApplicationUrl"));
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		report.updateTestLog("Invoke Application", "Invoke the application under test @ " +
				properties.getProperty("ApplicationUrl"), Status.DONE);
		LoginPage.login();
		//HomePage.clickStartShoppingButton();
		HomePage.navigateToMyOrdersPage();
		myorders.verifyLastedOrderIsPresent();
		}


	@Override
	public void tearDown() {
		// TODO Auto-generated method stub

	}


}

