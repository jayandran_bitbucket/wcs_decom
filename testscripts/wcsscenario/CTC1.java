package testscripts.wcsscenario;

import java.util.concurrent.TimeUnit;

import org.testng.annotations.Test;

import com.cognizant.framework.Status;

import functionallibraries.OF.Func_OFCustomerSearch;
import functionallibraries.OF.Func_OFHomePage;
import functionallibraries.OF.Func_OFLogin;
import functionallibraries.OF.Func_OFOrderDetailsPage;
import functionallibraries.OF.Func_OFOrderSearch;
import functionallibraries.aws.Func_AWSHeader;
import functionallibraries.aws.Func_AWSSearch;
import functionallibraries.wcs.Func_BookSlot;
import functionallibraries.wcs.Func_BrowseShop;
import functionallibraries.wcs.Func_DeliveryAddressDetails;
import functionallibraries.wcs.Func_FastMail;
import functionallibraries.wcs.Func_HomePage;
import functionallibraries.wcs.Func_Interstitials;
import functionallibraries.wcs.Func_Login;
import functionallibraries.wcs.Func_MyAccount;
import functionallibraries.wcs.Func_MyAddressPage;
import functionallibraries.wcs.Func_MyOrdersPage;
import functionallibraries.wcs.Func_OrderConfirmation;
import functionallibraries.wcs.Func_OrderReview;
import functionallibraries.wcs.Func_PaymentDetails;
import functionallibraries.wcs.Func_Search;
import functionallibraries.wcs.Func_Trolley;
import supportlibraries.DriverScript;
import supportlibraries.TestCase;

public class CTC1 extends TestCase
{
	private Func_Login LoginPage;
	private Func_HomePage HomePage;
	private Func_AWSHeader AWSHeader;
	private Func_BookSlot BookSlot;
	private Func_Search Search;
//	private Func_AWSSearch Search;
	private Func_Interstitials interstitials;
	private Func_DeliveryAddressDetails deliveryaddress;
	private Func_OrderReview review;
	private Func_PaymentDetails payment;
	private Func_OrderConfirmation confirmation;
	private Func_OFLogin oflogin;
	private Func_OFHomePage ofhome;
	private Func_OFCustomerSearch ofcustomersearch;
	private Func_OFOrderSearch ofordersearch;
	private Func_OFOrderDetailsPage oforderdetails;
	private Func_FastMail fastmail;
	private Func_MyAccount myaccount;
	private Func_MyAddressPage myaddress;
	private Func_Trolley WcsTrolley;
	private Func_BrowseShop Browseshop;
	private Func_MyOrdersPage myorders;
	
	
	
	@Test()
	public void runTC1()
	{
		
	//	testParameters.setCurrentTestDescription("Beta Customer - Grocery Delivery Amend Order Flow");
	//	testParameters.setIterationMode(IterationOptions.RunOneIterationOnly);
		//testParameters.setBrowser(Browser.HtmlUnit);
		
		driverScript = new DriverScript(testParameters);
		driverScript.driveTestExecution();
	}
	
	@Override
	public void setUp()
	{
		LoginPage = new Func_Login(scriptHelper);
		HomePage = new Func_HomePage(scriptHelper);
		AWSHeader = new Func_AWSHeader(scriptHelper);
		BookSlot = new Func_BookSlot(scriptHelper);
//		Search = new Func_AWSSearch(scriptHelper);
		Search = new Func_Search(scriptHelper);
		interstitials = new Func_Interstitials(scriptHelper);
		review = new Func_OrderReview(scriptHelper);
		oflogin = new Func_OFLogin(scriptHelper);
		ofhome = new Func_OFHomePage(scriptHelper);
		payment = new Func_PaymentDetails(scriptHelper);
		confirmation = new Func_OrderConfirmation(scriptHelper);
		deliveryaddress = new Func_DeliveryAddressDetails(scriptHelper);
		ofcustomersearch = new Func_OFCustomerSearch(scriptHelper);
		ofordersearch = new Func_OFOrderSearch(scriptHelper);
		oforderdetails = new Func_OFOrderDetailsPage(scriptHelper);
		fastmail = new Func_FastMail(scriptHelper);
		myaccount = new Func_MyAccount(scriptHelper);
		WcsTrolley = new Func_Trolley(scriptHelper);
		Browseshop = new Func_BrowseShop(scriptHelper);
		myorders = new Func_MyOrdersPage(scriptHelper);
		myaddress = new Func_MyAddressPage(scriptHelper);
		report.addTestLogSection("Setup");
		LoginPage.setChromeExtension();
		driver.get(properties.getProperty("ApplicationUrl"));
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		report.updateTestLog("Invoke Application", "Invoke the application under test @ " +
								properties.getProperty("ApplicationUrl"), Status.DONE);
	}
	@Override
	public void executeTest() 
	{
		LoginPage.login();
		LoginPage.register();
		HomePage.clickGroceryHeaderLink();
		HomePage.clickMyAccountLink();
		HomePage.clickMyAccountInMyAccountList();
		myaccount.clickMyAddressesInMyAccount();
		myaddress.clickAddNewAddress();
		myaddress.enterNewAddress();
		HomePage.clickGroceryHeaderLink();
		HomePage.lazyLoad();
		HomePage.clickBookSlotBtn();
		BookSlot.chooseDeliveryType_D();
		//BookSlot.doPostCodeLookUpForDelivery();
		BookSlot.selectSlot();
		BookSlot.clickAlcoholContinue();
		BookSlot.clickContinueBtnInDeliverySlotOverlay();
		HomePage.enterKeywordAndSearch();
		Search.enterQtyandAddProductInPLP();
//		HomePage.clickGroceryHeaderLink();
		Search.performJotterSearch();
		HomePage.enterKeywordAndSearch();
		Search.chooseLowToHighOption();
		Search.valideLowtoHighPrice();
		Search.enterQtyandAddProductInPLP();
		HomePage.clickHeaderTrolleyLink();
		WcsTrolley.incProductQuantity();
		WcsTrolley.decProductQuantity();
		HomePage.clickCheckOutBtn();
		interstitials.clickFinishedHere();
		review.enterPromoCodeAndValidateTotal();
		review.clickContinuetopayment();
		//payment.clickIWantClaimButton();
		payment.enterPaymentDetailsAndSave();
		payment.applyGiftCard();
		payment.addGiftVoucher();
		payment.clickPlaceOrderUpper();
		confirmation.getOrderReferenceNumber();
		confirmation.getOrderAddressDetailsForDelivery();
		confirmation.verifyPartnershipDiscountTextIsDisplayed();
		oflogin.launchOFApp();
		oflogin.loginOF();
		ofhome.clickOrderSearchLink();
		ofordersearch.enterOrderNumberAndSearch();
		oforderdetails.verifyOrderDetails();
		fastmail.LaunchFastMailApplication();
		fastmail.performLogin();
		fastmail.checkOrderConfirmationMail();

		
	}
	@Override
	public void tearDown() {
		// TODO Auto-generated method stub
		
	}
	

}
