package testscripts.wcsscenario;

import java.util.concurrent.TimeUnit;

import org.testng.annotations.Test;

import pageobjects.wcs.Trolley;

import com.cognizant.framework.Status;

import functionallibraries.OF.Func_OFCustomerSearch;
import functionallibraries.OF.Func_OFHomePage;
import functionallibraries.OF.Func_OFLogin;
import functionallibraries.OF.Func_OFOrderDetailsPage;
import functionallibraries.OF.Func_OFOrderSearch;
import functionallibraries.aws.Func_AWSHeader;
import functionallibraries.aws.Func_AWSSearch;
import functionallibraries.wcs.Func_BookSlot;
import functionallibraries.wcs.Func_BrowseShop;
import functionallibraries.wcs.Func_DeliveryAddressDetails;
import functionallibraries.wcs.Func_FastMail;
import functionallibraries.wcs.Func_HomePage;
import functionallibraries.wcs.Func_Interstitials;
import functionallibraries.wcs.Func_Login;
import functionallibraries.wcs.Func_MyAccount;
import functionallibraries.wcs.Func_MyAddressPage;
import functionallibraries.wcs.Func_MyOrdersPage;
import functionallibraries.wcs.Func_OrderConfirmation;
import functionallibraries.wcs.Func_OrderReview;
import functionallibraries.wcs.Func_PYO;
import functionallibraries.wcs.Func_PaymentDetails;
import functionallibraries.wcs.Func_Search;
import functionallibraries.wcs.Func_Trolley;
import supportlibraries.DriverScript;
import supportlibraries.TestCase;

public class HTC22 extends TestCase
{
	private Func_Login LoginPage;
	private Func_HomePage HomePage;
	private Func_AWSHeader AWSHeader;
	private Func_BookSlot BookSlot;
	private Func_Search Search;
	private Func_Interstitials interstitials;
	private Func_DeliveryAddressDetails deliveryaddress;
	private Func_OrderReview review;
	private Func_PaymentDetails payment;
	private Func_OrderConfirmation confirmation;
	private Func_OFLogin oflogin;
	private Func_OFHomePage ofhome;
	private Func_OFCustomerSearch ofcustomersearch;
	private Func_OFOrderSearch ofordersearch;
	private Func_OFOrderDetailsPage oforderdetails;
	private Func_FastMail fastmail;
	private Func_Trolley WcsTrolley;
	private Func_MyOrdersPage myorders;
	private Func_BrowseShop Browseshop;
	private Func_MyAccount myaccount;
	private Func_PYO pyopage;
	private Func_MyAddressPage myaddress;
	public double TrolleyPriceBefOrder,TrolleyPriceAfterOrder;
	public String TrolleySavingBefOrder,TrolleySavingAfterOrder;
	public double TrolleyPrice;

	@Test()
	public void runTC1()
	{

		//	testParameters.setCurrentTestDescription("Beta Customer - Grocery Delivery Amend Order Flow");
		//	testParameters.setIterationMode(IterationOptions.RunOneIterationOnly);
		//testParameters.setBrowser(Browser.HtmlUnit);

		driverScript = new DriverScript(testParameters);
		driverScript.driveTestExecution();
	}

	@Override
	public void setUp()
	{

		LoginPage = new Func_Login(scriptHelper);
		HomePage = new Func_HomePage(scriptHelper);
		AWSHeader = new Func_AWSHeader(scriptHelper);
		BookSlot = new Func_BookSlot(scriptHelper);
		Search = new Func_Search(scriptHelper);
		interstitials = new Func_Interstitials(scriptHelper);
		review = new Func_OrderReview(scriptHelper);
		oflogin = new Func_OFLogin(scriptHelper);
		ofhome = new Func_OFHomePage(scriptHelper);
		payment = new Func_PaymentDetails(scriptHelper);
		confirmation = new Func_OrderConfirmation(scriptHelper);
		deliveryaddress = new Func_DeliveryAddressDetails(scriptHelper);
		ofcustomersearch = new Func_OFCustomerSearch(scriptHelper);
		ofordersearch = new Func_OFOrderSearch(scriptHelper);
		oforderdetails = new Func_OFOrderDetailsPage(scriptHelper);
		fastmail = new Func_FastMail(scriptHelper);
		WcsTrolley = new Func_Trolley(scriptHelper);
		myorders = new Func_MyOrdersPage(scriptHelper);
		myaccount = new Func_MyAccount(scriptHelper);
		myaddress = new Func_MyAddressPage(scriptHelper);
		Browseshop = new Func_BrowseShop(scriptHelper);
		pyopage = new Func_PYO(scriptHelper);
		report.addTestLogSection("Setup");
		LoginPage.setChromeExtension();
		driver.get(properties.getProperty("ApplicationUrl"));
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		report.updateTestLog("Invoke Application", "Invoke the application under test @ " +
				properties.getProperty("ApplicationUrl"), Status.DONE);
	}
	@Override
	public void executeTest() 
	{
		HomePage.homePageValidation();
		LoginPage.register();
		HomePage.clickGroceryHeaderLink();
		HomePage.clickMyAccountLink();
		HomePage.clickMyAccountInMyAccountList();
		myaccount.clickJoinMyWaitrose();
		myaccount.enterDeliveryAddressDetails();
		HomePage.clickGroceryHeaderLink();
		HomePage.clickFavouriteLink();
		HomePage.clickPYOLink();
		pyopage.PYOValidations();
		pyopage.addFullSetToSetBuilder();
		pyopage.confirmMyPicks();
		HomePage.clickFavouriteLink();
		HomePage.clickPYOLink();
		pyopage.removeSingleProductAndValidate();
		HomePage.clickFavouriteLink();
		HomePage.clickPYOLink();
		pyopage.addToSetBuilder();
		pyopage.validatePickButtonIsDisabled();
		HomePage.clickFavouriteLink();
		HomePage.clickPYOLink();
		pyopage.removeSingleProductAndValidate();
		pyopage.confirmMyPicks();
		HomePage.clickBrowseShopButton();
		Browseshop.performBrowseShopNavigation();
		Search.validatePYOOfferNotDisplayed();
		HomePage.moveToOfferPage();
		Browseshop.chooseOfferFilter();
		Search.validatePYOOfferNotDisplayed();
		HomePage.enterKeywordAndSearch();
		Search.navigateToPDP();
		Search.validatePYOOfferNotDisplayed();
		HomePage.clickFavouriteLink();
		HomePage.clickPYOLink();
		Search.enterQtyandAddProductInPYO();
		HomePage.clickHeaderTrolleyLink();
		WcsTrolley.validatePYOSavingIsApplied();
		WcsTrolley.getTrolleyValuesAfter();
		TrolleySavingBefOrder = HomePage.getTotalSavings();
		WcsTrolley.incProductQuantity();
		TrolleySavingAfterOrder = HomePage.getTotalSavingsAfter();
		HomePage.validatePYOSavingIsApplied(TrolleySavingBefOrder, TrolleySavingAfterOrder);
		TrolleySavingBefOrder = HomePage.getTotalSavings();
		WcsTrolley.decProductQuantity();
		TrolleySavingAfterOrder = HomePage.getTotalSavingsAfter();
		HomePage.validatePYOSavingIsApplied(TrolleySavingBefOrder, TrolleySavingAfterOrder);
		HomePage.clickHomeImg();
		HomePage.clickHeaderTrolleyLink();
		WcsTrolley.performEmptyTrolley();
		HomePage.clickFavouriteLink();
		HomePage.clickPYOLink();
		Search.enterQtyandAddProductInPYO();
		HomePage.clickHeaderTrolleyLink();
		WcsTrolley.validatePYOSavingIsApplied();
		HomePage.clickCheckOutBtn();
		BookSlot.chooseDeliveryType_D();
		//BookSlot.doPostCodeLookUpForDelivery();
		BookSlot.selectSlot();
		BookSlot.clickAlcoholContinue();
		BookSlot.clickContinueBtnInDeliverySlotOverlay();
		HomePage.clickCheckOutBtn();
		interstitials.clickFinishedHere();
		//deliveryaddress.enterDeliveryAddress();
		review.getProductLineNumbers();
		review.enterPromoCodeAndValidateTotal();
		review.verifyAndGetPYOSavings();
		review.clickContinuetopayment();
		payment.enterPaymentDetailsAndSave();
		payment.clickPlaceOrderUpper();
		confirmation.getOrderReferenceNumber();
		confirmation.getOrderAddressDetailsForDelivery();
		confirmation.validatePYOSavings();
		fastmail.LaunchFastMailApplication();
		fastmail.performLogin();
		fastmail.checkOrderConfirmationMail();
		fastmail.validatePYOSavings();
		
		
		
		


	}


	@Override
	public void tearDown() {
		// TODO Auto-generated method stub

	}


}
