package testscripts.wcsscenario;

import java.util.concurrent.TimeUnit;

import org.testng.annotations.Test;

import com.cognizant.framework.Status;

import supportlibraries.DriverScript;
import functionallibraries.wcs.Func_HomePage;
import functionallibraries.wcs.Func_Login;
import supportlibraries.TestCase;

public class TestTry extends TestCase {
		
	private Func_Login LoginPage;
	private Func_HomePage HomePage;
	
	
	@Test()
	public void runTC1()
	{
		
		driverScript = new DriverScript(testParameters);
		driverScript.driveTestExecution();
		
	}
			
	@Override
	public void executeTest() {
		// TODO Auto-generated method stub
		
		HomePage.homePageValidation();
		LoginPage.register();
		LoginPage.clickStartShoppingBtn();
		
	}
	@Override
	public void tearDown() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void setUp() {
		// TODO Auto-generated method stub
		
		LoginPage = new Func_Login(scriptHelper);
		HomePage = new Func_HomePage(scriptHelper);
		
		LoginPage.setChromeExtension();
		driver.get(properties.getProperty("ApplicationUrl"));
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		report.updateTestLog("Invoke Application", "Invoke the application under test @ " +
				properties.getProperty("ApplicationUrl"), Status.DONE);
		
		
	}
	
	
	
	
	

}
