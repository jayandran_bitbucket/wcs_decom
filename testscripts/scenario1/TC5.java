package testscripts.scenario1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.cognizant.framework.IterationOptions;
import com.cognizant.framework.Status;

import functionallibraries.OF.Func_OFCustomerSearch;
import functionallibraries.OF.Func_OFHomePage;
import functionallibraries.OF.Func_OFLogin;
import functionallibraries.OF.Func_OFOrderDetailsPage;
import functionallibraries.OF.Func_OFOrderSearch;
import functionallibraries.wcs.Func_BookSlot;
import functionallibraries.wcs.Func_DeliveryAddressDetails;
import functionallibraries.wcs.Func_FastMail;
import functionallibraries.wcs.Func_HomePage;
import functionallibraries.wcs.Func_Interstitials;
import functionallibraries.wcs.Func_Login;
import functionallibraries.wcs.Func_OrderConfirmation;
import functionallibraries.wcs.Func_OrderReview;
import functionallibraries.wcs.Func_PaymentDetails;
import functionallibraries.wcs.Func_Search;
import functionallibraries.wcs.Func_Trolley;
import supportlibraries.DriverScript;
import supportlibraries.TestCase;


/**
 * Test for login with valid user credentials
 * @author Cognizant
 */
public class TC5 extends TestCase
{
	private Func_Login LoginPage;
	private Func_HomePage HomePage;
	private Func_BookSlot BookSlot;
	private Func_Search Search;
	private Func_Interstitials interstitials;
	private Func_DeliveryAddressDetails deliveryaddress;
	private Func_OrderReview review;
	private Func_PaymentDetails payment;
	private Func_OrderConfirmation confirmation;
	private Func_OFLogin oflogin;
	private Func_OFHomePage ofhome;
	private Func_OFCustomerSearch ofcustomersearch;
	private Func_OFOrderSearch ofordersearch;
	private Func_OFOrderDetailsPage oforderdetails;
	private Func_Trolley Trolley;
	private Func_FastMail fastmail;
	@Test()
	public void runTC1()
	{
	//	testParameters.setCurrentTestDescription("Test for login with valid user credentials");
	//	testParameters.setIterationMode(IterationOptions.RunOneIterationOnly);
		//testParameters.setBrowser(Browser.HtmlUnit);
		
		driverScript = new DriverScript(testParameters);
		driverScript.driveTestExecution();
	}
	
	@Override
	public void setUp()
	{
		LoginPage = new Func_Login(scriptHelper);
		HomePage = new Func_HomePage(scriptHelper);
		BookSlot = new Func_BookSlot(scriptHelper);
		Search = new Func_Search(scriptHelper);
		interstitials = new Func_Interstitials(scriptHelper);
		review = new Func_OrderReview(scriptHelper);
		oflogin = new Func_OFLogin(scriptHelper);
		ofhome = new Func_OFHomePage(scriptHelper);
		payment = new Func_PaymentDetails(scriptHelper);
		confirmation = new Func_OrderConfirmation(scriptHelper);
		deliveryaddress = new Func_DeliveryAddressDetails(scriptHelper);
		ofcustomersearch = new Func_OFCustomerSearch(scriptHelper);
		ofordersearch = new Func_OFOrderSearch(scriptHelper);
		oforderdetails = new Func_OFOrderDetailsPage(scriptHelper);
		Trolley = new Func_Trolley(scriptHelper);
		fastmail = new Func_FastMail(scriptHelper);
		report.addTestLogSection("Setup");
		LoginPage.setChromeExtension();
		driver.get(properties.getProperty("OFUrl"));
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		report.updateTestLog("Invoke Application", "Invoke the application under test @ " +
								properties.getProperty("OFUrl"), Status.DONE);
	}
	
	
	
	@Override
	public void executeTest()
	{
		oflogin.loginOF();
		ofhome.clickCustomerSearchLink();
		ofcustomersearch.performCustomerSearch();
		ofcustomersearch.clickCustomerEmailLink();
		ofcustomersearch.clickSignOnAsCustomerLink();
		HomePage.clickStartShoppingButton();
		HomePage.clickBookSlotBtn();
		BookSlot.chooseDeliveryType();
		BookSlot.selectCollectionPoint();
		BookSlot.selectSlot();
		BookSlot.clickContinueBtnInCollectionSlotOverlay();
		HomePage.clickHeaderTrolleyLink();
		Trolley.getTrolleyValuesBefore();
		HomePage.enterKeywordAndSearch();
		Search.validateSearchResultsInPLP();
		Search.enterQtyandAddProductInPLP();
		Trolley.getTrolleyValuesAfter();
		Trolley.validateIncrementTrolleyValue();
		HomePage.clickCheckOutBtn();
		interstitials.clickFinishedHere();
		review.getProductLineNumbers();
		review.clickContinuetopayment();
		payment.enterPaymentDetailsAndSave();
		payment.clickPlaceOrderUpper();
		confirmation.getOrderReferenceNumber();
		confirmation.verifyOrderDetailsInWCSDB();
		if(properties.getProperty("OFExecution").equalsIgnoreCase("True")){
		oflogin.returnToAdmin();
		oflogin.launchOFApp();
		oflogin.loginOF();
		ofhome.clickOrderSearchLink();
		ofordersearch.enterOrderNumberAndSearch();
		oforderdetails.verifyOrderDetails();}
		//fastmail.verifyOrderConfirmationMail();
		
	}
 
	
	@Override
	public void tearDown()
	{
		// Nothing to do
	}
}