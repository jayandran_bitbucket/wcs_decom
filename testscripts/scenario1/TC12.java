package testscripts.scenario1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.cognizant.framework.IterationOptions;
import com.cognizant.framework.Status;

import functionallibraries.OF.Func_OFCustomerSearch;
import functionallibraries.OF.Func_OFHomePage;
import functionallibraries.OF.Func_OFLogin;
import functionallibraries.OF.Func_OFOrderDetailsPage;
import functionallibraries.OF.Func_OFOrderSearch;
import functionallibraries.wcs.Func_BookSlot;
import functionallibraries.wcs.Func_DeliveryAddressDetails;
import functionallibraries.wcs.Func_FastMail;
import functionallibraries.wcs.Func_HomePage;
import functionallibraries.wcs.Func_Interstitials;
import functionallibraries.wcs.Func_Login;
import functionallibraries.wcs.Func_MyOrdersPage;
import functionallibraries.wcs.Func_OrderConfirmation;
import functionallibraries.wcs.Func_OrderReview;
import functionallibraries.wcs.Func_PYO;
import functionallibraries.wcs.Func_PaymentDetails;
import functionallibraries.wcs.Func_Trolley;
import functionallibraries.aws.Func_AWSHeader;
import functionallibraries.aws.Func_AWSSearch;
import supportlibraries.DriverScript;
import supportlibraries.TestCase;


/**
 * Test for login with valid user credentials
 * @author Cognizant
 */
public class TC12 extends TestCase{
	private Func_Login LoginPage;
	private Func_HomePage HomePage;
	private Func_AWSHeader AWSHeader;
	private Func_BookSlot BookSlot;
	private Func_AWSSearch Search;
	private Func_Interstitials interstitials;
	private Func_DeliveryAddressDetails deliveryaddress;
	private Func_OrderReview review;
	private Func_PaymentDetails payment;
	private Func_OrderConfirmation confirmation;
	private Func_OFLogin oflogin;
	private Func_OFHomePage ofhome;
	private Func_OFCustomerSearch ofcustomersearch;
	private Func_OFOrderSearch ofordersearch;
	private Func_OFOrderDetailsPage oforderdetails;
	private Func_FastMail fastmail;
	private Func_Trolley WcsTrolley;
	private Func_MyOrdersPage myorders;
	private Func_PYO pyopage;
	
	@Test()
	public void runTC1(){
		
		testParameters.setCurrentTestDescription("Beta Customer - Grocery Delivery Flow");
	//	testParameters.setIterationMode(IterationOptions.RunOneIterationOnly);
		//testParameters.setBrowser(Browser.HtmlUnit);
		
		driverScript = new DriverScript(testParameters);
		driverScript.driveTestExecution();
	}
	
	@Override
	public void setUp(){
		LoginPage = new Func_Login(scriptHelper);
		HomePage = new Func_HomePage(scriptHelper);
		AWSHeader = new Func_AWSHeader(scriptHelper);
		BookSlot = new Func_BookSlot(scriptHelper);
		Search = new Func_AWSSearch(scriptHelper);
		interstitials = new Func_Interstitials(scriptHelper);
		review = new Func_OrderReview(scriptHelper);
		oflogin = new Func_OFLogin(scriptHelper);
		ofhome = new Func_OFHomePage(scriptHelper);
		payment = new Func_PaymentDetails(scriptHelper);
		confirmation = new Func_OrderConfirmation(scriptHelper);
		deliveryaddress = new Func_DeliveryAddressDetails(scriptHelper);
		ofcustomersearch = new Func_OFCustomerSearch(scriptHelper);
		ofordersearch = new Func_OFOrderSearch(scriptHelper);
		oforderdetails = new Func_OFOrderDetailsPage(scriptHelper);
		fastmail = new Func_FastMail(scriptHelper);
		WcsTrolley = new Func_Trolley(scriptHelper);
		myorders = new Func_MyOrdersPage(scriptHelper);
		pyopage = new Func_PYO(scriptHelper);
		report.addTestLogSection("Setup");
		LoginPage.setChromeExtension();
		driver.get(properties.getProperty("ApplicationUrl"));
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		report.updateTestLog("Invoke Application", "Invoke the application under test @ " +
								properties.getProperty("ApplicationUrl"), Status.DONE);
	}
	
	
	
	@Override
	public void executeTest(){
		LoginPage.login();
		HomePage.clickGroceryHeaderLink();
		HomePage.clickBookSlotBtn();
		BookSlot.chooseDeliveryType();
		//BookSlot.doPostCodeLookUpForDelivery();
		BookSlot.selectSlot();
		BookSlot.clickContinueBtnInDeliverySlotOverlay();
		HomePage.clickFavouriteLink();
		HomePage.clickPYOLink();
		pyopage.getPYOProductID();
		HomePage.enterKeywordAndSearch();
		Search.performPYOProductsSearchAndAddToTrolley();
		AWSHeader.getPYOSavings();
		AWSHeader.verifySlotDateInAWS();
		Search.performAWSSearch();
		//Search.validateAWSSearchResultsInPLP();
		Search.enterQtyandAddProductInPLP();
		AWSHeader.getAWSTrolleyValue();
		//AWSHeader.clickCheckOutButton();
		AWSHeader.clickTrolleyLink();
		WcsTrolley.getWCSTrolleyvalue();
		WcsTrolley.verifyTrolleySync();
		HomePage.clickCheckOutBtn();
		interstitials.clickFinishedHere();
		//deliveryaddress.enterDeliveryAddress();
		review.getProductLineNumbers();
		review.verifyAndGetPYOSavings();
		review.verifyWCSPYOSavingsWithAWSPYOSavings();
		//review.enterPromoCodeAndValidateTotal();
		review.clickContinuetopayment();
		payment.enterPaymentDetailsAndSave();
		payment.clickPlaceOrderUpper();
		confirmation.getOrderReferenceNumber();
		confirmation.verifyOrderDetailsInWCSDB();
		//oforderdetails.writeOrderDetails();
		oflogin.launchOFApp();
		oflogin.loginOF();
		ofhome.clickOrderSearchLink();
		ofordersearch.enterOrderNumberAndSearch();
		oforderdetails.verifyOrderDetails();
		//fastmail.verifyOrderConfirmationMail();
	}
	
	
	
	@Override
	public void tearDown(){
		// Nothing to do
	}
	

}