package functionallibraries.wcs;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.cognizant.framework.Status;
import com.thoughtworks.selenium.webdriven.commands.IsChecked;

import functionallibraries.aws.ReusableComponents;
import pageobjects.wcs.*;
import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;

public class Func_MyAccount extends ReusableLibrary{
	
	public Func_MyAccount(ScriptHelper scriptHelper){
		super(scriptHelper);
	}
	WebDriverWait Wait = new WebDriverWait(driver,20);
	ReusableComponents helper = new ReusableComponents(scriptHelper);
	MyAccount myaccount_ui = new MyAccount();
	PaymentDetails payment = new PaymentDetails();
	DeliveryAddress delivery = new DeliveryAddress();
	
	
	
	public void clickMyAddressesInMyAccount(){
		Wait.until(ExpectedConditions.elementToBeClickable(By.xpath(myaccount_ui.myaddresses)));
		helper.clickLink(driver, myaccount_ui.myaddresses_loc,myaccount_ui.myaddresses);
		report.updateTestLog("Home Page", " My addresses Link is selected  ", Status.PASS);
	}
	
	public void clickSignOut(){
		Wait.until(ExpectedConditions.elementToBeClickable(By.xpath(myaccount_ui.signout_btn)));
		helper.clickObject(driver, myaccount_ui.signout_loc, myaccount_ui.signout_btn);
		if(helper.isElementPresent(driver, "xpath", "//button[text()='Sign in/Register']"))
			report.updateTestLog("Home Page", " Customer is signed out successfully", Status.PASS);
		else report.updateTestLog("Home Page", " Customer is not signed out successfully", Status.FAIL);
			
			
		
	}
	
	public void enterScreenName()//using in smoke suite
	{
		try
		{
			if(helper.isElementPresent(driver, myaccount_ui.txtScreenName_loc, myaccount_ui.txtScreenName))
			{
				SimpleDateFormat sdfScrn = new SimpleDateFormat("ddMMyyyyHHmmss");
				String scrnName = "A" + sdfScrn.format(new Date()); 
				System.out.println(scrnName);
				helper.typeinTextbox(driver, myaccount_ui.txtScreenName_loc, myaccount_ui.txtScreenName, scrnName);
				//												ReusableComponents.typeinEditbox(driver, MyAccountPage.txtScreenName_loc, MyAccountPage.txtScreenName,dataTable.getData("General_Data" , "ScreenName"));
				helper.clickLink(driver, myaccount_ui.btnSave_Loc,myaccount_ui.btnSave);
				report.updateTestLog("Screen Name", "Screen Name entered", Status.DONE);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			report.updateTestLog("Screen Name", "Screen Name is not Entered"+e.toString(), Status.FAIL);
		}
	}
	
	public void clickJoinMyWaitrose(){
		Wait.until(ExpectedConditions.elementToBeClickable(By.xpath(myaccount_ui.joinmywaitrose_lnk)));
		helper.clickLink(driver, myaccount_ui.joinmywaitrose_loc,myaccount_ui.joinmywaitrose_lnk);
		report.updateTestLog("Home Page", " Join my waitrose Link is selected  ", Status.PASS);
	}
	
	public void cardRegisterOnline(){
		String card =  dataTable.getData( "General_Data","Waitrose Card No");
		Wait.until(ExpectedConditions.elementToBeClickable(By.xpath(myaccount_ui.registercardonline_lnk)));
		helper.clickLink(driver, myaccount_ui.registercardonline_loc,myaccount_ui.registercardonline_lnk);
		helper.typeinTextbox(driver, myaccount_ui.cardno_loc, myaccount_ui.cardno_txt,card);
		enterDeliveryAddressDetails();
	}
	
	public void cardRegisterOnline_PYO(){
		String card =  dataTable.getData( "General_Data","Waitrose Card No");
		Wait.until(ExpectedConditions.elementToBeClickable(By.xpath(myaccount_ui.registercardonline_lnk)));
		helper.clickLink(driver, myaccount_ui.registercardonline_loc,myaccount_ui.registercardonline_lnk);
		helper.typeinTextbox(driver, myaccount_ui.cardno_loc, myaccount_ui.cardno_txt,card);
		enterDeliveryAddressDetails_PYO();
	}
	
	public void enterDeliveryAddressDetails(){
		String title = dataTable.getData("DeliveryDetails_Data", "Title");
		String firstname = dataTable.getData("DeliveryDetails_Data", "FirstName");
		String lastname = dataTable.getData("DeliveryDetails_Data", "LastName");
		String phonenumber = dataTable.getData("DeliveryDetails_Data", "PhoneNumber");
		String postcode = dataTable.getData("DeliveryDetails_Data", "Postcode");
		Wait.until(ExpectedConditions.elementToBeClickable(By.xpath(delivery.findMyAddress_btn)));
		//Wait.until(ExpectedConditions.presenceOfElementLocated(By.id(delivery.nickName_loc)));
		
		helper.typeinTextbox(driver, delivery.title_loc, delivery.title_txt, title);
		helper.typeinTextbox(driver, delivery.firstName_loc, delivery.firstName_txt, firstname);
		helper.typeinTextbox(driver, delivery.lastName_loc, delivery.lastName_txt, lastname);
		if(driver.findElement(By.id("zipCode")).isEnabled()){
			helper.typeinTextbox(driver, delivery.postcode_loc, delivery.postcode_txt, postcode);
		}
		helper.clickLink(driver, delivery.findMyAddress_loc, delivery.findMyAddress_btn);
		List<WebElement> addresslinks = helper.findWebElements(driver, delivery.postcodeaddress_loc, delivery.postcodeaddress_link);
		for(WebElement w:addresslinks){
			w.click();
			break;
		}
		helper.typeinTextbox(driver, delivery.phonenumber_loc, delivery.phonenumber_txt, phonenumber);
		helper.clickLink(driver, delivery.saveandcontinue_loc, delivery.saveandcontinue_btn);
		helper.clickLink(driver, myaccount_ui.continue_loc, myaccount_ui.continue_btn);
		/*if(helper.isElementPresent(driver, myaccount_ui.mywaitrosecardnumber_loc, myaccount_ui.mywaitrosecardnumber_lnk))
		report.updateTestLog ("Join my waitrose", "Mywaitrose card is linked to the customer", Status.PASS);
		else report.updateTestLog ("Join my waitrose", "Mywaitrose card is not linked to the customer", Status.FAIL);*/
	}
	
	public void enterDeliveryAddressDetails_PYO(){
		String title = dataTable.getData("DeliveryDetails_Data", "Title");
		String firstname = dataTable.getData("DeliveryDetails_Data", "FirstName");
		String lastname = dataTable.getData("DeliveryDetails_Data", "LastName");
		String phonenumber = dataTable.getData("DeliveryDetails_Data", "PhoneNumber");
		String postcode = dataTable.getData("DeliveryDetails_Data", "Postcode");
		Wait.until(ExpectedConditions.elementToBeClickable(By.xpath(delivery.findMyAddress_btn)));
		//Wait.until(ExpectedConditions.presenceOfElementLocated(By.id(delivery.nickName_loc)));
		
		helper.typeinTextbox(driver, delivery.title_loc, delivery.title_txt, title);
		helper.typeinTextbox(driver, delivery.firstName_loc, delivery.firstName_txt, firstname);
		helper.typeinTextbox(driver, delivery.lastName_loc, delivery.lastName_txt, lastname);
		if(driver.findElement(By.id("zipCode")).isEnabled()){
			helper.typeinTextbox(driver, delivery.postcode_loc, delivery.postcode_txt, postcode);
		}
		helper.clickLink(driver, delivery.findMyAddress_loc, delivery.findMyAddress_btn);
		List<WebElement> addresslinks = helper.findWebElements(driver, delivery.postcodeaddress_loc, delivery.postcodeaddress_link);
		for(WebElement w:addresslinks){
			w.click();
			break;
		}
		helper.typeinTextbox(driver, delivery.phonenumber_loc, delivery.phonenumber_txt, phonenumber);
		helper.clickLink(driver, delivery.saveandcontinue_loc, delivery.saveandcontinue_btn);
		helper.clickLink(driver, myaccount_ui.continue_loc, myaccount_ui.continue_btn);
		report.updateTestLog ("Join my waitrose", "Mywaitrose card is linked to the customer", Status.PASS);
	}
	
	public void optOutOfWaitroseCard(){
		helper.clickLink(driver, myaccount_ui.optout_loc, myaccount_ui.optout_lnk);
		helper.clickLink(driver, myaccount_ui.optoutmywaitrose_loc, myaccount_ui.optoutmywaitrose_btn);
		if(helper.isElementPresent(driver, myaccount_ui.optoutoverlayclose_loc, myaccount_ui.optoutoverlayclose_btn))
			helper.clickLink(driver, myaccount_ui.optoutoverlayclose_loc, myaccount_ui.optoutoverlayclose_btn);
		if(helper.isElementPresent(driver, myaccount_ui.joinmywaitrose_loc, myaccount_ui.joinmywaitrose_lnk))
			report.updateTestLog ("Opt out of my waitrose", "Card is removed from the user successfully", Status.PASS);
		else report.updateTestLog ("Join my waitrose", "Card is not removed from the user successfully", Status.FAIL);
		
	}
	
	public void joinMyWaitroseForExistingUser(){
		helper.clickLink(driver, delivery.saveandcontinue_loc, delivery.saveandcontinue_btn);
		helper.clickLink(driver, myaccount_ui.continue_loc, myaccount_ui.continue_btn);
		if(helper.isElementPresent(driver, myaccount_ui.mywaitrosecardnumber_loc, myaccount_ui.mywaitrosecardnumber_lnk))
		report.updateTestLog ("Join my waitrose", "Mywaitrose card is linked to the customer", Status.PASS);
	}
	public void registerCardForExistingUser(){
		String card =  dataTable.getData( "General_Data","Waitrose Card No");
		helper.typeinTextbox(driver, myaccount_ui.cardno_loc, myaccount_ui.cardno_txt,card);
		helper.clickLink(driver, delivery.saveandcontinue_loc, delivery.saveandcontinue_btn);
		helper.clickLink(driver, myaccount_ui.continue_loc, myaccount_ui.continue_btn);
		report.updateTestLog ("Join my waitrose", "Mywaitrose card is linked to the customer", Status.PASS);
	}
	
	public void validateMywaitroseAddressNickName(){
		if(helper.isElementPresent(driver, myaccount_ui.mwnickname_loc, myaccount_ui.mwnickname_txt))
			report.updateTestLog ("Join my waitrose", "Mywaitrose address is saved in myaddresses with mywaitrose address nickname", Status.PASS);
		else report.updateTestLog ("Join my waitrose", "Mywaitrose address is not saved in myaddresses with mywaitrose address nickname", Status.FAIL);
		
	}
	
	public void clickMyPaymentDetailsLink(){
		Wait.until(ExpectedConditions.elementToBeClickable(By.xpath(myaccount_ui.paymentdetails_lnk)));
		helper.clickLink(driver, myaccount_ui.paymentdetails_loc,myaccount_ui.paymentdetails_lnk);
		report.updateTestLog("Home Page", " My Payment Link is selected  ", Status.PASS);
	}
	
	public void enterPaymentDetailsAndSaveInMyAccount(){
		String cardNumber = dataTable.getData("PaymentDetails_Data", "CardNumber");
		String endMonth = dataTable.getData("PaymentDetails_Data", "EndMonth");
		String endYear = dataTable.getData("PaymentDetails_Data", "EndYear").substring(2, 4);
		String cardName = dataTable.getData("PaymentDetails_Data", "CardName");
		Wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(myaccount_ui.cardNumber_txt)));
		helper.typeinTextbox(driver, myaccount_ui.cardNumber_loc, myaccount_ui.cardNumber_txt, cardNumber);
		helper.SelectValuefromDropDown1(driver, myaccount_ui.endMonth_loc, myaccount_ui.endMonth_dropdown, endMonth);
		helper.SelectValuefromDropDown1(driver, myaccount_ui.endYear_loc, myaccount_ui.endYear_dropdown, endYear);
		helper.typeinTextbox(driver, myaccount_ui.enterName_loc, myaccount_ui.enterName_txt, cardName);
		helper.clickLink(driver, myaccount_ui.save_loc,myaccount_ui.save_btn);
		report.updateTestLog("Payment Details Page", " Entered Payment Card Details and saved in my account ", Status.DONE);
	}
	
	public void verifyorderpreferencethroughtext(){
		helper.clickLink(driver, myaccount_ui.PersonalDetails_loc, myaccount_ui.PersonalDetails);
		String mobilenum = dataTable.getData("General_Data", "Mobile_Number");
		String mob = helper.getAttribute(driver, myaccount_ui.MobileNumber_loc, myaccount_ui.MobileNumber, "value");
		if((mob.equals(mobilenum)))
			report.updateTestLog("mobile number", "Mobile number is same as entered in order review page", Status.PASS);
		else
			report.updateTestLog("mobile number", "Mobile number is not entered", Status.PASS);
		if(driver.findElement(By.id(myaccount_ui.orderpref_yes)).isSelected()){
			 myaccount_ui.preference = "yes";
			report.updateTestLog("verifying order preference through text", "order preference through text is set as Yes", Status.PASS);}
		if(driver.findElement(By.id(myaccount_ui.orderpref_no)).isSelected()){
			myaccount_ui.preference = "no";
			report.updateTestLog("verifying order preference through text", "order preference through text is set as No", Status.PASS);}
	}
	public void orderpreferencethroughtext(){
		helper.clickLink(driver, myaccount_ui.PersonalDetails_loc, myaccount_ui.PersonalDetails);
		String option = dataTable.getData("General_Data", "order_preference");
		String title = dataTable.getData("DeliveryDetails_Data", "Title");
		String firstname = dataTable.getData("DeliveryDetails_Data", "FirstName");
		String lastname = dataTable.getData("DeliveryDetails_Data", "LastName");
		String phonenumber = dataTable.getData("DeliveryDetails_Data", "PhoneNumber");
		if((helper.getText(driver, delivery.title_loc, delivery.title_txt)).isEmpty()){
		helper.typeinTextbox(driver, delivery.title_loc, delivery.title_txt, title);
		helper.typeinTextbox(driver, delivery.firstName_loc, delivery.firstName_txt, firstname);
		helper.typeinTextbox(driver, delivery.lastName_loc, delivery.lastName_txt, lastname);
		helper.typeinTextbox(driver, delivery.phonenumber_loc, delivery.phonenumber_txt, phonenumber);}
		if(option.equalsIgnoreCase("yes")){
			helper.selectRadiobutton(driver, myaccount_ui.orderpref_yes_loc, myaccount_ui.orderpref_yes);
			helper.clickLink(driver, myaccount_ui.save_loc, myaccount_ui.save_btn);
			report.updateTestLog("order preference", "order preference is selected as" +option, Status.PASS);}
		if(option.equalsIgnoreCase("no")){
			helper.selectRadiobutton(driver, myaccount_ui.orderpref_no_loc, myaccount_ui.orderpref_no);
			helper.clickLink(driver, myaccount_ui.save_loc, myaccount_ui.save_btn);
			report.updateTestLog("order preference", "order preference is selected as" +option, Status.PASS);}
	}

}
	
