package functionallibraries.wcs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Status;

import functionallibraries.aws.ReusableComponents;
import pageobjects.wcs.*;
import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;




/**
 * Functional Library class
 * @author Cognizant
 */
public class Func_Search extends ReusableLibrary
{
	/**
	 * Constructor to initialize the functional library
	 * @param scriptHelper The {@link ScriptHelper} object passed from the {@link DriverScript}
	 */
	public Func_Search(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	JavascriptExecutor jse = (JavascriptExecutor)driver;
	WebDriverWait Wait = new WebDriverWait(driver,20);
	ReusableComponents helper = new ReusableComponents(scriptHelper);
	HomePage home_ui = new HomePage();
	Search search = new Search();
	

	public void validateSearchResultsInPLP(){
		String keyword = dataTable.getData("General_Data", "Search").toLowerCase();
		String ProductName;
		WebDriverWait Wait = new WebDriverWait(driver,30);
		Wait.until(ExpectedConditions.presenceOfElementLocated(By.className(search.PLPproduct_table)));
		List<WebElement> productName = driver.findElements(By.className(search.PLPproduct_table));
		int size = productName.size();
		int i =1;
		for(WebElement w:productName){
			ProductName = w.findElement(By.className(search.PLPproductNames_txt)).getText().toLowerCase();
			if(!ProductName.contains(keyword)){
				report.updateTestLog("Home Page",  i+" th Product Name is not having "+keyword+" keyword ", Status.FAIL);
				report.updateTestLog("Home Page",  " Resulted product name :  "+ProductName, Status.DONE);
			}else if(i==size){
				report.updateTestLog("Home Page", keyword+" : Keyword Result is displayed for all listed products ", Status.PASS);
			}			i++;
		}
		report.updateTestLog("Home Page", " Search Result Validated for : "+i+" Products", Status.DONE);
	}

	public void enterQtyandAddProductInPLP(){
		try{
		String qtyvalue = dataTable.getData("General_Data", "PLP_Qty");
		int qty = Integer.parseInt(dataTable.getData("General_Data", "PLP_Qty"));
		int NumberOfProducts = Integer.parseInt(dataTable.getData("General_Data", "NumberOfProducts"));
		List<WebElement> qtyfields = helper.findWebElements(driver, search.PLPproductQty_loc, search.PLPproductQty_field);
		List<WebElement> addBtns = helper.findWebElements(driver, search.PLPproductAdd_loc, search.PLPproductAdd_btn);
		int size = qtyfields.size();
		if(NumberOfProducts<=size){
			report.updateTestLog("Search Page ", NumberOfProducts+" Products are displayed in PLP to add", Status.DONE);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			timedelay();
			for(int i=1;i<=NumberOfProducts;i++){
				if(qtyfields.get(i-1).isDisplayed()){
				qtyfields.get(i-1).clear();
				qtyfields.get(i-1).sendKeys(qtyvalue);
				Wait.until(ExpectedConditions.elementToBeClickable(addBtns.get(i-1)));
				//addBtns.get(i-1).sendKeys(Keys.DOWN);
				//addBtns.get(i-1).click();
				jse.executeScript("arguments[0].click();", addBtns.get(i-1));
				}
			}	
			report.updateTestLog("Search Page ", NumberOfProducts+" Products are added to trolley with "+qtyvalue+" each", Status.PASS);
		}else {
			for(int i=1;i<=size;i++){
				report.updateTestLog("Search Page ", size+" Products are displayed in PLP to add", Status.DONE);
				qtyfields.get(i-1).sendKeys(qtyvalue);
				//Wait.until(ExpectedConditions.elementToBeClickable(addBtns.get(i-1)));
				//addBtns.get(i-1).sendKeys(Keys.DOWN);
				//addBtns.get(i-1).click();
				//jse.executeScript("arguments[0].click();", addBtns.get(i-1));
				driver.findElement(By.xpath("//div[@class='button content-button add-button no-image']")).click();
			}
			report.updateTestLog("Search Page ", size+" Products are added to trolley with "+qtyvalue+" each", Status.PASS);
		}
	
	}
	catch(Exception e){
		e.printStackTrace();
		
	}
	}
	
	
	public void validateSearchResultsForAWStoWCS(){
		String keyword = dataTable.getData("General_Data", "AWStoWCS").toLowerCase();
		String ProductName;
		WebDriverWait Wait = new WebDriverWait(driver,30);
		Wait.until(ExpectedConditions.presenceOfElementLocated(By.className(search.PLPproduct_table)));
		List<WebElement> productName = driver.findElements(By.className(search.PLPproduct_table));
		int size = productName.size();
		int i =1;
		for(WebElement w:productName){
			ProductName = w.findElement(By.className(search.PLPproductNames_txt)).getText().toLowerCase();
			if(!ProductName.contains(keyword)){
				report.updateTestLog("Home Page",  i+" th Product Name is not having "+keyword+" keyword ", Status.FAIL);
				report.updateTestLog("Home Page",  " Resulted product name :  "+ProductName, Status.DONE);
			}else if(i==size){
				report.updateTestLog("Home Page", keyword+" : Keyword Result is displayed for all listed products ", Status.PASS);
			}
			i++;
		}

		report.updateTestLog("Home Page", " Search Result Validated for : "+i+" Products", Status.DONE);
	}
	
	public void addSingleProductInPLP(){
		
		int NumberOfProducts = Integer.parseInt(dataTable.getData("General_Data", "NumberOfProducts"));
		List<WebElement> qtyfields = helper.findWebElements(driver, search.PLPproductQty_loc, search.PLPproductQty_field);
		List<WebElement> addBtns = helper.findWebElements(driver, search.PLPproductAdd_loc, search.PLPproductAdd_btn);
		int size = qtyfields.size();
		report.updateTestLog("Search Page ", NumberOfProducts+" Products are displayed in PLP to add", Status.DONE);
		for(int i=1;i<=NumberOfProducts;i++){
			if(qtyfields.get(i-1).isDisplayed()){
			qtyfields.get(i-1).clear();
			qtyfields.get(i-1).sendKeys("1");
			Wait.until(ExpectedConditions.elementToBeClickable(addBtns.get(i-1)));
			//addBtns.get(i-1).sendKeys(Keys.DOWN);
			//addBtns.get(i-1).click();
			jse.executeScript("arguments[0].click();", addBtns.get(i-1));
			report.updateTestLog("Search Page ", " Single Product is added", Status.DONE);
			break;
			}
		}	
		
		
	}

public void markProductFavInPLP(){
String noprod = dataTable.getData("General_Data", "NoOfFavProduct");
int n= Integer.parseInt(noprod);
for(int i=1;i<=n;i++){
driver.findElement(By.xpath("(//a[@title='Add to Favourites'])['"+i+"']")).click();
}
report.updateTestLog("Search Page ", " Product is added to favourite", Status.DONE);
} 

public void markProductFavInPLP1(){
String noprod = dataTable.getData("General_Data", "NoOfFavProduct1");
int n= Integer.parseInt(noprod);
for(int i=1;i<=n;i++){
driver.findElement(By.xpath("(//a[@title='Add to Favourites'])['"+i+"']")).click();
}
report.updateTestLog("Search Page ", " Product is added to favourite", Status.DONE);
} 
	
	
public void checkserachresultInAllTabs()
{
String keyword = dataTable.getData("General_Data","Search2");
String word = helper.getText(driver, "xpath", "//p[@class='search-info']/strong[contains(text(),'"+keyword+"')]");
driver.findElement(By.id("groceries")).click();
Wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("(//div[@class='m-product-details-container']/a)[1]")));
if(keyword.contains(word))
report.updateTestLog("Groceries tab ", "Searched Product is displayed", Status.PASS);
else
report.updateTestLog("Groceries tab ", "Searched Product is  displayed", Status.FAIL);	
driver.findElement(By.id("entertaining")).click();
Wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//ul[@id='breadcrumb']/li[2]/a[contains(text(),'Enter')]")));
helper.checkPageReadyState(2000);
if(keyword.contains(word))
report.updateTestLog("Entertaining tab ", "Searched Product is displayed", Status.PASS);
else
report.updateTestLog("Entertaining tab ", "Searched Product is displayed", Status.FAIL);
driver.findElement(By.id("recipes")).click();
Wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//ul[@id='breadcrumb']/li[2]/a[contains(text(),'Recipes')]")));
helper.checkPageReadyState(2000);
if(keyword.contains(word))
report.updateTestLog("Recipes tab ", "Searched Product is displayed", Status.PASS);
else
report.updateTestLog("Recipes tab ", "Searched Product is displayed", Status.FAIL);
driver.findElement(By.id("inspiration")).click();
Wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//ul[@id='breadcrumb']/li[2]/a[contains(text(),'Ins')]")));
helper.checkPageReadyState(2000);
if(keyword.contains(word))
report.updateTestLog("Insipration tab ", "Searched Product is displayed", Status.PASS);
else
report.updateTestLog("Insipration tab ", "Searched Product is displayed", Status.FAIL);
}	

public void clickEntertainingtab(){
	driver.findElement(By.id("entertaining")).click();
	Wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//ul[@id='breadcrumb']/li[2]/a[contains(text(),'Enter')]")));
	helper.checkPageReadyState(2000);
	JavascriptExecutor jse = (JavascriptExecutor)driver;
	jse.executeScript("window.scrollBy(0,300)");

	report.updateTestLog("Entertaining tab ", "Searched Product is displayed", Status.PASS);
}

public void validatePYOOfferNotDisplayed(){
	WebDriverWait Wait = new WebDriverWait(driver,1);
	try{
	if(!helper.isElementPresent(driver, "xpath", "//span[contains(text(),'20% OFF')]"))	
		report.updateTestLog("PLP/PDP", "PYO offer is not displayed", Status.PASS);
	else report.updateTestLog("PLP/PDP", "PYO offer is  displayed", Status.FAIL);
	}catch(Exception e){
		report.updateTestLog("PLP/PDP", "PYO offer is not displayed", Status.PASS);	
		}
	
}

public void navigateToPDP(){
	helper.clickLink(driver, "xpath", "//a[@class='m-product-open-details']");
	if(helper.isElementPresent(driver, "xpath", "//div[contains(@class,'product-detail')]"))
		report.updateTestLog("Search", "PDP page is not displayed", Status.DONE);	
	else report.updateTestLog("Search", "PDP page is not displayed", Status.FAIL);
}

public void enterQtyandAddProductInPYO(){
	WebElement qtyy = driver.findElement(By.xpath("//div[@class='products-grid pyo-selected-offers']//div"));
	JavascriptExecutor jse = (JavascriptExecutor)driver;
	jse.executeScript("window.scrollBy(0,200)",qtyy);
	String qtyvalue = dataTable.getData("General_Data", "PYO_Qty");
	int qty = Integer.parseInt(dataTable.getData("General_Data", "PYO_Qty"));
	int NumberOfProducts = Integer.parseInt(dataTable.getData("General_Data", "NumberOfProductsPYO"));
	List<WebElement> qtyfields = helper.findWebElements(driver, search.PYOqty_loc, search.PYOqty_txt);
	List<WebElement> addBtns = helper.findWebElements(driver, search.PYOadd_loc, search.PYOadd_btn);
	int size = qtyfields.size();
	if(NumberOfProducts<=size){
		report.updateTestLog("Search Page ", NumberOfProducts+" Products are displayed in PLP to add", Status.DONE);
		for(int i=1;i<=NumberOfProducts;i++){
			if(qtyfields.get(i-1).isDisplayed()){
			qtyfields.get(i-1).clear();
			qtyfields.get(i-1).sendKeys(qtyvalue);
			Wait.until(ExpectedConditions.elementToBeClickable(addBtns.get(i-1)));
			//addBtns.get(i-1).sendKeys(Keys.DOWN);
			//addBtns.get(i-1).click();
			jse.executeScript("arguments[0].click();", addBtns.get(i-1));
			}
		}	
		report.updateTestLog("Search Page ", NumberOfProducts+" Products are added to trolley with "+qtyvalue+" each", Status.PASS);
	}else {
		for(int i=1;i<=size;i++){
			report.updateTestLog("Search Page ", size+" Products are displayed in PLP to add", Status.DONE);
			qtyfields.get(i-1).sendKeys(qtyvalue);
			Wait.until(ExpectedConditions.elementToBeClickable(addBtns.get(i-1)));
			//addBtns.get(i-1).sendKeys(Keys.DOWN);
			//addBtns.get(i-1).click();
			jse.executeScript("arguments[0].click();", addBtns.get(i-1));
		}
		report.updateTestLog("Search Page ", size+" Products are added to trolley with "+qtyvalue+" each", Status.PASS);
	}
}

public void valideHightoLowPrice()
{

		Wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(search.txtAllProdPrice)));
		helper.checkPageReadyState(2000);
		if(driver.findElements(By.xpath(search.txtAllProdPrice)).size()>0)
		{
			List<WebElement> priceHightoLow=driver.findElements(By.xpath(search.txtAllProdPrice));
			List<String> priceListHightoLow= new ArrayList<String>();
			int abc=0;
			String strPrice,str;
			for(WebElement w:priceHightoLow){
				if(w.isDisplayed()){
					strPrice=w.getText();
					str=strPrice.replaceAll("\\D+", "");
					abc=Integer.parseInt(str);
					priceListHightoLow.add(String.valueOf(abc));
				}
			}


			System.out.println(priceListHightoLow);
			String[] priceArrayLow=priceListHightoLow.toArray(new String[priceListHightoLow.size()]);
			boolean flag=true;
			for(int i=1;i<priceArrayLow.length;i++)
			{
				int prevValue=Integer.parseInt(priceArrayLow[i-1]);
				int curValue=Integer.parseInt(priceArrayLow[i]);
				//System.out.println(prevValue+" ,"+curValue);
				if(prevValue>=curValue)
				{
					System.out.println("Done"); 
				}
				else
				{
					flag=false;
				}
			}
			if(flag==true)
			{
				report.updateTestLog("Sort Price High to Low","Sorting Price from High to Low has done", Status.PASS);                                                      

			}
			else
			{
				report.updateTestLog("Sort Price High to Low","Sorting Price from High to Low is not completed", Status.FAIL);                                                      

			}
		}else{
			report.updateTestLog("Sort Price High to Low ","Products not displayed in product grid", Status.FAIL);
		}
}
		
		public void chooseLowToHighOption()
		{
			
			helper.checkPageReadyState(3000);
				helper.clickLink(driver, search.lnkSort_loc, search.lnkSort);
				new Select(driver.findElement(By.id(search.lnkSort))).selectByVisibleText("Price: Low - High");
				report.updateTestLog("Sort option", "Sort Option for Low TO high is selected", Status.DONE);	
				
			}
		
		public void valideLowtoHighPrice()
		{
			helper.checkPageReadyState(2000);					
			Wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(search.txtAllProdPrice)));
				if(driver.findElements(By.xpath(search.txtAllProdPrice)).size()>0)
				{
					List<WebElement> priceLowtoHigh=driver.findElements(By.xpath(search.txtAllProdPrice));			
					List<String> priceListLowtoHigh= new ArrayList<String>();
					int abc=0;
					String strPrice,str;
					for(WebElement w:priceLowtoHigh){
						if(w.isDisplayed()){
							strPrice=w.getText();
							str=strPrice.replaceAll("\\D+", "");
							abc=Integer.parseInt(str);
							priceListLowtoHigh.add(String.valueOf(abc));
						}
					}		
					System.out.println(priceListLowtoHigh);
					String[] priceArrayLow=priceListLowtoHigh.toArray(new String[priceListLowtoHigh.size()]);
					boolean flag=true;
					for(int i=1;i<priceArrayLow.length;i++)
					{
						int prevValue=Integer.parseInt(priceArrayLow[i-1]);
						int curValue=Integer.parseInt(priceArrayLow[i]);
						//System.out.println(prevValue+" ,"+curValue);
						if(prevValue<=curValue)
						{
							System.out.println("Done"); 
						}
						else
						{
							flag=false;
						}
					}
					if(flag==true)
					{
						report.updateTestLog("Sort Price Low to High ","Sorting Price from Low to high has done ", Status.PASS);                                                      

					}
					else
					{
						report.updateTestLog("Sort Price Low to High ","Sorting Price from low to high has done", Status.FAIL);                                                      

					}
				}else{
					report.updateTestLog("Sort Price Low to High ","Products not displayed in product grid", Status.FAIL);
				}

			}
		
		public void clickLeftHandFilterAndValidate()
		{
			
				String G1="";
				List<WebElement> lstG1Filter = driver.findElements(By.xpath(search.leftPanelFilterLinks));
				for (WebElement webElement : lstG1Filter) {
					G1=webElement.getText().trim();
					if(G1 != "" && !G1.isEmpty())
					{
						webElement.click();
					}else{
						continue;
					}

					WebDriverWait Wait = new WebDriverWait(driver,20);

					List<WebElement> lstBreadcrumb = driver.findElements(By.xpath("//a[starts-with(@id,'breadcrumb')]"));
					for (WebElement webElement2 : lstBreadcrumb) {
						if((webElement2.getText().trim()).equals(G1))
						{
							report.updateTestLog("Left Hand filter: G1", "G1 left hand filter is applied successfully", Status.PASS);
							return;
						}
					}
				}
				report.updateTestLog("Left Hand filter: G1", "G1 left hand filter is not applied", Status.FAIL);

}

		public void clickFilterNverifyNoPrds()
		{
			helper.checkPageReadyState(5000);
			Wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(search.txtOfffil)));
			String NumprdOffFilterbef=driver.findElement(By.id(search.txtOfffil)).getText().split("\\(")[1].split("\\)")[0].toString();
				
			JavascriptExecutor jse=(JavascriptExecutor)driver;

			jse.executeScript("scroll(0,4500)"); //otherwise use 500. it differs for the types of filter we are going to add

			String LHNdrop = dataTable.getData("General_Data", "LHNSelect");

			String chbxLHN = dataTable.getData("General_Data", "CheckbxLHN");

			System.out.println("(//div[@class='leftNavFilter js-collapsible']//span[contains(text(),'"+LHNdrop+"')])[2]");

			WebElement LHN = driver.findElement(By.xpath("(//div[@class='leftNavFilter js-collapsible']//span[contains(text(),'"+LHNdrop+"')])[2]"));
			
			

			Actions build = new Actions(driver);

			build.moveToElement(LHN).perform();

			LHN.click();

			helper.checkPageReadyState(500);
			jse.executeScript("scroll(0,1000)");

			driver.findElement(By.xpath("(//input[@name='"+chbxLHN+"'])[2]")).click(); // comment this and uncomment the next line if you use waitrose own label filter. (Critical)
			//driver.findElement(By.xpath("(//input[@name='Waitrose Own Label'])[1]")).click();

			helper.checkPageReadyState(3000);

			System.out.println(driver.findElement(By.id(search.txtOfffil)).getText());

			String NumprdOffFilterafter=driver.findElement(By.id(search.txtOfffil)).getText().split("\\(")[1].split("\\)")[0].toString();
			jse.executeScript("scroll(0,-2500)");
			if(!NumprdOffFilterbef.equals(NumprdOffFilterafter))

			report.updateTestLog("Our brand Filter", LHNdrop+">"+chbxLHN+"Filter is applied successfully", Status.PASS);

			else report.updateTestLog("Our brand Filter", LHNdrop+">"+chbxLHN+"Filter is not applied successfully", Status.FAIL);
			
}
		
		public void validateSearchResultsInRecipesTab(){
			helper.clickLink(driver, "xpath", "//button[@class='button secondary-cta left-nav-btn-back js-left-nav-btn-back js-search-back']");
			helper.clickLink(driver, "xpath", "//a[@id='recipes']");
			String TabName = driver.findElement(By.xpath("//li[@class='is-active']//a")).getText();
			System.out.println("Active TabName : "+TabName);
			try{
				if(TabName.equalsIgnoreCase("Recipes")){
					report.updateTestLog("Tab Details ", " Recipes Tab is displayed ", Status.SCREENSHOT);
				}else{
					System.out.println("Recipes Tab is not displayed , So now clicking Recipes Tab");
					helper.clickLink(driver, "xpath", "//a[@id='recipes']");
					report.updateTestLog("Tab Details ", " Recipes Tab is displayed ", Status.SCREENSHOT);
				}
			}catch(Exception e){
				e.printStackTrace();
				report.updateTestLog("Tab Details ", " Exception occured while validating Recipes Tab is "+e.toString(), Status.SCREENSHOT);
			}
			String keyword = dataTable.getData("General_Data" , "Search").toLowerCase();
			String ProductName;
			System.out.println("User Given Keyword : "+keyword);
			int i = 0;
			try{		
				for(i =1;i<5;i++){

					if(helper.isElementPresent(driver, "xpath","(//div[@class='recipe-search-results']//h3)["+i+"]")){
						ProductName = driver.findElement(By.xpath("(//div[@class='recipe-search-results']//h3)["+i+"]")).getText();
						System.out.println("Product name of "+i+" item in Search Results : "+ProductName);
						if(ProductName.toLowerCase().contains(keyword)){
							report.updateTestLog("Recipes Tab - Search Results ", i+" : Product in SearchResult having Keyword of "+keyword, Status.PASS);
						}else{
							report.updateTestLog("Recipes Tab - Search Results ", i+" : Product in SearchResult not having Keyword of "+keyword, Status.FAIL);
						}
					}else{
						report.updateTestLog("Recipes Tab - Search Results ", "  There is no products in search results for validating "+i+" iteration ", Status.SCREENSHOT);
						break;
					}
				}		
			}catch(Exception e){
				e.printStackTrace();
				report.updateTestLog("Recipes Tab - Search Results ", "Exception Occured while validating "+i+"  product in search Results "+e.toString(), Status.SCREENSHOT);
			}
		}

		
		public void validateSearchResultInEntertainingTab()
		{
			String text = dataTable.getData("General_Data" , "Search");
			String breadCrumb = "//span[@id='current-breadcrumb' and contains(text(), '"+text.toLowerCase()+"')]";
					//"//span[contains(text(),'Search results:' "+text+")]";
			helper.clickLink(driver, "xpath", "//a[@id='entertaining']");
			String plpText = "//div[@class='tab entertaining' and @style='display: block;']//p[contains(text(),'You searched for')]/strong[text()='"+text.toLowerCase()+"']";
					//"//*[@id='content']/div[2]/div/div[1]/div/div[3]/";
			
			if(helper.isElementPresent(driver, HomePage.breadCrumb_Loc, breadCrumb))	
			{
				if(helper.getText(driver, HomePage.breadCrumb_Loc, breadCrumb).contains("Search results: "))
				{
					report.updateTestLog("Search Result in Breadcrumb","Search Result: "+text.toLowerCase()+" is displayed in Breadcrumb", Status.PASS);
				}else report.updateTestLog("Search Result in Breadcrumb","Search Result is not displayed properly in Breadcrumb", Status.FAIL);
				
			}
			else
			{
				report.updateTestLog("Search Result in Breadcrumb","Search Result: "+text.toLowerCase()+" is not displayed in Breadcrumb", Status.FAIL);
			}
			
			if(helper.isElementPresent(driver, "xpath", plpText))	
			{
				report.updateTestLog("Search Result in PLP","Search Result: "+text.toLowerCase()+" is displayed in PLP", Status.PASS);
			}
			else
			{
				report.updateTestLog("Search Result","Search Result: "+text.toLowerCase()+" is not displayed in PLP", Status.FAIL);
			}
		}

		public void clickLeftHandFilterAndValidate_Recipes()
		{
			try{
				
				if(helper.isElementPresent(driver, HomePage.recipesGroupHeaderComp_loc, HomePage.recipesGroupHeaderComp))
				{
					helper.clickObject(driver, HomePage.recipesGroupHeaderComp_loc, HomePage.recipesGroupHeaderComp);
				}
				
				
				String beforeFilterCnt = ((driver.findElement(By.id(HomePage.lblCurrentBreadCrumb)).getText()).split("[(]")[1]).split("[)]")[0];		
				int intbeforeFilterCnt = Integer.parseInt(beforeFilterCnt);
				if(helper.isElementPresent(driver, HomePage.chkleftPanelFilterRecipes_loc, HomePage.chkleftPanelFilterRecipes))
				{
					List<WebElement> lstRecipesFilter = driver.findElements(By.xpath(HomePage.chkleftPanelFilterRecipes));
					lstRecipesFilter.get(0).click();
					Thread.sleep(5000);
					String afterFilterCnt = ((driver.findElement(By.id(HomePage.lblCurrentBreadCrumb)).getText()).split("[(]")[1]).split("[)]")[0];
					int intafterFilterCnt = Integer.parseInt(afterFilterCnt);
					
					if(intafterFilterCnt < intbeforeFilterCnt)
					{
						report.updateTestLog("Recipes Left hand filter validation", "Recipes Left hand filter is applied successfully", Status.PASS);
					}else
					{
						report.updateTestLog("Recipes Left hand filter validation", "Recipes Left hand filter is not applied item before count: "+intbeforeFilterCnt+" and after count: "+intafterFilterCnt, Status.FAIL);
					}
				}else
				{
					report.updateTestLog("Recipes Left hand filter validation", "Recipes Left hand filter is not displayed", Status.FAIL);
				}
					

			}catch(Exception e)
			{
				e.printStackTrace();
				report.updateTestLog("Recipes Left Hand filter: G1", "Recipes G1 left hand filter is not displayed", Status.FAIL);
			}
		}
		
		public void removeFilterAndValidate_Recipes()
		{
			try{
				String beforeFilterCnt = ((driver.findElement(By.id(HomePage.lblCurrentBreadCrumb)).getText()).split("[(]")[1]).split("[)]")[0];
				int intbeforeFilterCnt = Integer.parseInt(beforeFilterCnt);
				if(helper.isElementPresent(driver, Search.chkleftPanelUnFilterRecipes_loc, Search.chkleftPanelUnFilterRecipes))
				{
					List<WebElement> lstRecipesFilter = driver.findElements(By.xpath(Search.chkleftPanelUnFilterRecipes));
					lstRecipesFilter.get(0).click();
					Thread.sleep(5000);
					String afterFilterCnt = ((driver.findElement(By.id(HomePage.lblCurrentBreadCrumb)).getText()).split("[(]")[1]).split("[)]")[0];
					int intafterFilterCnt = Integer.parseInt(afterFilterCnt);
					
					if(intafterFilterCnt > intbeforeFilterCnt)
					{
						report.updateTestLog("Recipes Left hand remove filter validation", "Recipes Left hand remove filter is applied successfully", Status.PASS);
					}else
					{
						report.updateTestLog("Recipes Left hand remove filter validation", "Recipes Left hand remove filter is not applied item before count: "+intbeforeFilterCnt+" and after count: "+intafterFilterCnt, Status.FAIL);
					}
				}else
				{
					report.updateTestLog("Recipes Left hand remove filter validation", "Recipes Left hand remove filter is not displayed", Status.FAIL);
				}
					

			}catch(Exception e)
			{
				e.printStackTrace();
				report.updateTestLog("Recipes Remove Left Hand filter", "Recipes Remove left hand filter link is not displayed", Status.FAIL);
			}
		}
		
		public void validateSearchResultsInInspirationTab(){
			helper.clickLink(driver, "xpath", "//a[@id='inspiration']");
			String TabName = driver.findElement(By.xpath("//li[@class='is-active']//a")).getText();
			System.out.println("Active TabName : "+TabName);
			try{
				if(TabName.contains("Inspiration")){
					report.updateTestLog("Tab Details ", " Inspiration & TV Tab is displayed ", Status.SCREENSHOT);
				}else{
					System.out.println("Inspiration & TV Tab is not displayed , So now clicking Inspiration & TV Tab");
					helper.clickLink(driver, "xpath", "//a[@id='inspiration']");
					report.updateTestLog("Tab Details ", " Inspiration & TV Tab is displayed ", Status.SCREENSHOT);
				}
			}catch(Exception e){
				e.printStackTrace();
				report.updateTestLog("Tab Details ", " Exception occured while validating Inspiration & TV Tab is "+e.toString(), Status.SCREENSHOT);
			}
			String keyword = dataTable.getData("General_Data" , "Search").toLowerCase();
			String ProductName;
			System.out.println("User Given Keyword : "+keyword);
			int i = 0;
			try{		
				for(i =1;i<5;i++){

					if(helper.isElementPresent(driver, "xpath","(//ol[@class='inspiration']//a[@class='contentUserFont boldText'])["+i+"]")){
						ProductName = driver.findElement(By.xpath("(//ol[@class='inspiration']//a[@class='contentUserFont boldText'])["+i+"]")).getText();
						System.out.println("Product name of "+i+" item in Search Results : "+ProductName);
						if(ProductName.toLowerCase().contains(keyword)){
							report.updateTestLog("Inspiration & TV Tab - Search Results ", i+" : Product in SearchResult having Keyword of "+keyword, Status.PASS);
						}else{
							report.updateTestLog("Inspiration & TV Tab - Search Results ", i+" : Product in SearchResult not having Keyword of "+keyword, Status.FAIL);
						}
					}else{
						report.updateTestLog("Inspiration & TV Tab - Search Results ", "  There is no search results for validating "+i+" iteration ", Status.SCREENSHOT);
						break;
					}
				}		
			}catch(Exception e){
				e.printStackTrace();
				report.updateTestLog("Inspiration & TV Tab - Search Results ", "Exception Occured while validating "+i+"  product in search Results "+e.toString(), Status.SCREENSHOT);
			}
		}
		
		public void clickLeftHandFilterAndValidate_Inspiration()
		{
			try{
				
				String beforeFilterCnt = ((driver.findElement(By.id(HomePage.lblCurrentBreadCrumb)).getText()).split("[(]")[1]).split("[)]")[0];
				int intbeforeFilterCnt = Integer.parseInt(beforeFilterCnt);
				if(helper.isElementPresent(driver, Search.chkleftPanelFilterInspiration_loc, Search.chkleftPanelFilterInspiration))
				{
					List<WebElement> lstRecipesFilter = driver.findElements(By.xpath(Search.chkleftPanelFilterInspiration));
					lstRecipesFilter.get(0).click();
					Thread.sleep(5000);
					String afterFilterCnt = ((driver.findElement(By.id(HomePage.lblCurrentBreadCrumb)).getText()).split("[(]")[1]).split("[)]")[0];
					int intafterFilterCnt = Integer.parseInt(afterFilterCnt);
					
					if(intafterFilterCnt < intbeforeFilterCnt)
					{
						report.updateTestLog("Inspiration Left hand filter validation", "Inspiration Left hand filter is applied successfully", Status.PASS);
					}else
					{
						report.updateTestLog("Inspiration Left hand filter validation", "Inspiration Left hand filter is not applied item before count: "+intbeforeFilterCnt+" and after count: "+intafterFilterCnt, Status.FAIL);
					}
				}else
				{
					report.updateTestLog("Inspiration Left hand filter validation", "Inspiration Left hand filter is not displayed", Status.FAIL);
				}
					

			}catch(Exception e)
			{
				e.printStackTrace();
				report.updateTestLog("Inspiration Left Hand filter: G1", "Inspiration G1 left hand filter is not displayed", Status.FAIL);
			}
		}
		public void removeFilterAndValidate_Inspiration()
		{
			try{
				String beforeFilterCnt = ((driver.findElement(By.id(HomePage.lblCurrentBreadCrumb)).getText()).split("[(]")[1]).split("[)]")[0];
				int intbeforeFilterCnt = Integer.parseInt(beforeFilterCnt);
				if(helper.isElementPresent(driver, Search.chkleftPanelFilterInspiration_loc, Search.chkleftPanelFilterInspiration))
				{
					List<WebElement> lstRecipesFilter = driver.findElements(By.xpath(Search.chkleftPanelFilterInspiration));
					lstRecipesFilter.get(0).click();
					Thread.sleep(5000);
					String afterFilterCnt = ((driver.findElement(By.id(HomePage.lblCurrentBreadCrumb)).getText()).split("[(]")[1]).split("[)]")[0];
					int intafterFilterCnt = Integer.parseInt(afterFilterCnt);
					
					if(intafterFilterCnt > intbeforeFilterCnt)
					{
						report.updateTestLog("Inspiration Left hand remove filter validation", "Inspiration Left hand remove filter is applied successfully", Status.PASS);
					}else
					{
						report.updateTestLog("Inspiration Left hand remove filter validation", "Inspiration Left hand remove filter is not applied item before count: "+intbeforeFilterCnt+" and after count: "+intafterFilterCnt, Status.FAIL);
					}
				}else
				{
					report.updateTestLog("Inspiration Left hand remove filter validation", "Inspiration Left hand remove filter is not displayed", Status.FAIL);
				}
					

			}catch(Exception e)
			{
				e.printStackTrace();
				report.updateTestLog("Inspiration Remove Left Hand filter", "Inspiration Remove left hand filter link is not displayed", Status.FAIL);
			}
		}
		
		public void performJotterSearch(){
			helper.clickLink(driver, search.multisearch_loc, search.multisearch_btn);
			List<String> jsearch = Arrays.asList(dataTable.getData("General_Data", "JotterSearch").split(","));
			System.out.println(jsearch.size());
			for(int i=0; i<jsearch.size(); i++){
				driver.findElement(By.xpath(search.jottersearch_txt)).sendKeys(jsearch.get(i));
				driver.findElement(By.xpath(search.jottersearch_txt)).sendKeys(Keys.ENTER);
			}
			helper.clickLink(driver, search.searchtheseitems_loc, search.searchtheseitems_btn);
			WebDriverWait Wait = new WebDriverWait(driver,30);
			helper.checkPageReadyState(2000);
			try{
				Thread.sleep(3000);
			}
				catch (Exception e) {
					// TODO: handle exception
				}
			Wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(search.jottersearchresults_txt)));
			List<WebElement> jotter = driver.findElements(By.xpath(search.jottersearchresults_txt));
			
			List<String> searchresults = null;
			int i = 0;
			for(WebElement js : jotter){
				if(jsearch.contains(js.getText().trim().toLowerCase())){
					i++;
				}
			}
			
			if(jsearch.size()==i)
				report.updateTestLog("Jotter Search", "Jotter search is successfull for the products: "+searchresults, Status.PASS);
			else report.updateTestLog("Jotter Search", "Jotter search is not successfull for the products: "+searchresults, Status.FAIL);
			
			
		}
		
		public void clickBookSlotFromNoticePeriodOverlay(){
			if(helper.isElementPresent(driver, search.noticeperiod_loc, search.noticeperiod_txt)){
				helper.clickLink(driver, search.bookslotfromoverlay_loc, search.bookslotfromoverlay_btn);
				report.updateTestLog("Light Registered user", "Notice period overlay is displayed for entertaining product", Status.PASS);
			}
		}
		
		public void clickContinueFromNoticePeriodOverlay(){
			if(helper.isElementPresent(driver, search.seasonalcontinuebookslot_loc, search.seasonalcontinuebookslot_btn)){
				helper.clickLink(driver,  search.seasonalcontinuebookslot_loc, search.seasonalcontinuebookslot_btn);
				report.updateTestLog("Light Registered user", "Continue button from overlay is displayed for entertaining product", Status.PASS);
			}
			}
		
		public void createNewList(){
			Wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(search.addtolist_lnk)));
			helper.clickLink(driver, search.addtolist_loc, search.addtolist_lnk);
			Wait.until(ExpectedConditions.presenceOfElementLocated(By.id(search.createnewlist_txt)));
			helper.typeinTextbox(driver,  search.createnewlist_loc, search.createnewlist_txt, "mylist");
			helper.clickLink(driver, search.addtolistbtn_loc, search.addtolistbtn_btn);
			helper.clickLink(driver, search.oklist_loc, search.oklist_btn);
		}
		public void addToExistingList(){
			Wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(search.addtolist_lnk)));
			helper.clickLink(driver, search.addtolist_loc, search.addtolist_lnk);
//			Wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(search.createnewlist_txt)));
//			helper.typeinTextbox(driver,  search.createnewlist_loc, search.createnewlist_txt, "mylist");
			helper.clickLink(driver, search.addtolistbtn_loc, search.addtolistbtn_btn );
			helper.clickLink(driver, search.oklist_loc, search.oklist_btn);
		}
		public void timedelay() throws InterruptedException{
			WebDriverWait wait = new WebDriverWait(driver,30);
		    boolean pageLoaded =false;
				JavascriptExecutor executor = (JavascriptExecutor)driver;
				do{
					if((executor.executeScript("return document.readyState").equals("complete")))
					{
						System.out.println("Page completely loaded successfully");
						pageLoaded=true;

					} else{
						Thread.sleep(1000);
					}
				}
				while(pageLoaded==false);
		}
		
		 public void clickFilterNverifyNoPrds2()
			{
				try{
					
				driver.navigate().refresh();
				helper.checkPageReadyState(5000);
//				jse.executeScript("scroll(0, -2500);");
				Wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(search.txtOfffil)));
				String NumprdOffFilterbef=driver.findElement(By.id(search.txtOfffil)).getText().split("\\(")[1].split("\\)")[0].toString();
					
				JavascriptExecutor jse=(JavascriptExecutor)driver;

				jse.executeScript("scroll(0,800)");

				String LHNdrop = dataTable.getData("General_Data", "LHNSelect1");

				String chbxLHN = dataTable.getData("General_Data", "CheckbxLHN1");

				System.out.println("(//div[@class='leftNavFilter js-collapsible']//span[contains(text(),'"+LHNdrop+"')])[2]");
				//jse.executeScript("scroll(0,2500)");
				WebElement LHN = driver.findElement(By.xpath("(//div[@class='leftNavFilter js-collapsible']//span[contains(text(),'"+LHNdrop+"')])[1]"));

				

				Actions build = new Actions(driver);

				build.moveToElement(LHN).perform();

				LHN.click();

				helper.checkPageReadyState(500);
				//jse.executeScript("scroll(0,1000)");
				Thread.sleep(4000);
				driver.findElement(By.xpath("(//input[@name='"+chbxLHN+"'])[1]")).click();

				helper.checkPageReadyState(3000);

				System.out.println(driver.findElement(By.id(search.txtOfffil)).getText());

				String NumprdOffFilterafter=driver.findElement(By.id(search.txtOfffil)).getText().split("\\(")[1].split("\\)")[0].toString();
				jse.executeScript("scroll(0,-2500)");
				if(!NumprdOffFilterbef.equals(NumprdOffFilterafter))

				report.updateTestLog("Our brand Filter", LHNdrop+">"+chbxLHN+"Filter is applied successfully", Status.PASS);

				else report.updateTestLog("Our brand Filter", LHNdrop+">"+chbxLHN+"Filter is notapplied successfully", Status.FAIL);
				}
				catch (Exception e){
					
				}
						
				
			}
		
		 public void clickfirstproductmodallinkandadd()
		    {
		   	 try
		   	 {
		   		 if (helper.isElementPresent(driver, "xpath", "(//div[@class='m-product-details-container'])[1]/a[@class='m-product-open-details']"))
		   		 { 
		   			helper.clickLink(driver, "xpath", "(//div[@class='m-product-details-container'])[1]/a[@class='m-product-open-details']");
		   			report.updateTestLog("verify Product selection", "Product is Selected", Status.DONE);
		   			String productqty = dataTable.getData("General_Data", "PDP_Qty");
		   			helper.typeinTextbox(driver, search.PDPquantity_loc, search.PDPquantity_box, productqty);
		   			helper.clickLink(driver, search.PDPAddtotrolley_loc, search.PDPAddtotrolley_btn);
		   			report.updateTestLog(" PDP "," Searched and added product to enable minimum checkout ", Status.PASS);
		   		 }
		   		 else
		   		 	{
		   			 report.updateTestLog("verify Product selection", "Product is not Selected", Status.FAIL);
		   		 	}
		   	 }
		   	 catch (Exception e) 
		   	 {
		   		 e.printStackTrace();
		   		 report.updateTestLog("verify Product selection", "Product is not Selected"+e.toString(), Status.FAIL);
		   	 }
		    }
		
}