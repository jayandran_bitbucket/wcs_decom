package functionallibraries.wcs;



import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.FrameworkException;
import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import functionallibraries.aws.ReusableComponents;
import pageobjects.wcs.OrderConfirmation;
import pageobjects.wcs.OrderReview;
import pageobjects.wcs.PaymentDetails;
import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;



public class Func_OrderConfirmation extends ReusableLibrary
{
	/**
	 * Constructor to initialize the functional library
	 * @param scriptHelper The {@link ScriptHelper} object passed from the {@link DriverScript}
	 */
	public Func_OrderConfirmation(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	WebDriverWait Wait = new WebDriverWait(driver,20);
	OrderReview orderreview = new OrderReview();
	OrderConfirmation confirmation = new OrderConfirmation();
	ReusableComponents helper = new ReusableComponents(scriptHelper);

	public void getOrderReferenceNumber(){
		Wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(confirmation.orderReference_txt)));
		String OrderNumber = helper.getText(driver, confirmation.orderReference_loc, confirmation.orderReference_txt);
		String OrderItemTotal = helper.getText(driver, confirmation.orderItemTotal_loc, confirmation.orderItemTotal_txt).trim();
		OrderConfirmation.orderNumber = OrderNumber;
		dataTable.putData("OrderDetails", "OrderNumber", OrderNumber);
		System.out.println("Order ID : "+OrderNumber);
		report.updateTestLog(" Order Confirmation Page", " Order have been placed and order reference number :  "+OrderNumber, Status.PASS);
	}
	
	public void getOrderReferenceNumber_Res(){
		
		Wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(confirmation.orderReference_res)));
		String OrderNumber = helper.getText(driver, confirmation.orderReference_res_loc, confirmation.orderReference_res);
		//String OrderItemTotal = helper.getText(driver, confirmation.orderItemTotal_loc, confirmation.orderItemTotal_txt).trim();
		OrderConfirmation.orderNumber = OrderNumber;
		dataTable.putData("OrderDetails", "OrderNumber", OrderNumber);
		System.out.println("Order ID : "+OrderNumber);
		report.updateTestLog(" Order Confirmation Page", " Order have been placed and order reference number :  "+OrderNumber, Status.PASS);
	}

	public void getDSOrderReferenceNumber(){
		Wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(confirmation.orderReference_txt)));
		String OrderNumber = helper.getText(driver, confirmation.orderReference_loc, confirmation.orderReference_txt);
		OrderConfirmation.orderNumber = OrderNumber;
		dataTable.putData("OrderDetails", "OrderNumber", OrderNumber);
		System.out.println("Order ID : "+OrderNumber);
		report.updateTestLog(" Order Confirmation Page", " Order have been placed and order reference number :  "+OrderNumber, Status.PASS);
	}

	public void getOrderAddressDetailsForDelivery(){
		String orderAdr;
		String orderAd = helper.getText(driver, confirmation.orderDeliveryAddress_loc, confirmation.orderDeliveryAddress_txt);
		int len = orderAd.length();
		//OrderConfirmation.orderDeliveryAddress = orderAd.substring(len-8, len);
		orderAdr = orderAd.substring(len-8, len);
		dataTable.putData("OrderDetails", "OrderPostCode", orderAdr);
		System.out.println("Order Address : "+orderAdr);
		report.updateTestLog(" Order Confirmation Page", " Order Address :  "+orderAdr, Status.PASS);
	}

	public void getOrderAddressDetailsForCollection(){
		String orderAd = helper.getText(driver, confirmation.orderCollectionAddress_loc, confirmation.orderCollectionAddress_txt);
		int len = orderAd.length();
		OrderConfirmation.orderCollectionAddress = orderAd.substring(len-26, len-18);
		System.out.println("Order Address : "+ OrderConfirmation.orderCollectionAddress);
		report.updateTestLog(" Order Confirmation Page", " Order Address :  "+OrderConfirmation.orderCollectionAddress, Status.PASS);
	}

	public void getDSOrderAddressDetailsForCollection(){
		String orderAd = helper.getText(driver, confirmation.orderDSCollectionAddress_loc, confirmation.orderDSCollectionAddress_txt);
		int len = orderAd.length();
		OrderConfirmation.orderCollectionAddress = orderAd.substring(len-26, len-18);
		System.out.println("Order Address : "+ OrderConfirmation.orderCollectionAddress);
		report.updateTestLog(" Order Confirmation Page", " Order Address :  "+OrderConfirmation.orderCollectionAddress, Status.PASS);
	}


	public void verifyPartnershipDiscountTextIsDisplayed(){
		if(helper.isElementPresent(driver, confirmation.partnershipdiscount_loc, confirmation.partnershipdiscount_txt)){
			report.updateTestLog(" Order Confirmation Page", " Partnership Discount Text is Displayed  ", Status.PASS);
		}else{
			report.updateTestLog(" Order Confirmation Page", " Partnership Discount Text is Not Displayed  ", Status.FAIL);
		}
	}

	public void verifyPartnershipDiscountTextIsNotDisplayed(){
		try{
			if(helper.isElementPresent(driver, confirmation.partnershipdiscount_loc, confirmation.partnershipdiscount_txt)){
				report.updateTestLog(" Order Confirmation Page", " Partnership Discount Text is Displayed  ", Status.FAIL);
			}else{
				report.updateTestLog(" Order Confirmation Page", " Partnership Discount Text is Not Displayed  ", Status.PASS);
			}
		}catch(Exception e){
			report.updateTestLog(" Order Confirmation Page", " Partnership Discount Text is not Displayed  ", Status.PASS);
		}
	}

	public void verifyMyWaitroseSavings(){

		if(helper.findWebElements(driver,confirmation.myWaitroseSavings_loc, confirmation.myWaitroseSavings_txt).size() > 0){
			String val = helper.getText(driver, confirmation.myWaitroseSavings_loc, confirmation.myWaitroseSavings_txt);	
			String reviewval = dataTable.getData("OrderDetails", "MyWaitroseSavings");
			report.updateTestLog(" Order Confirmation Page", " My Waitrose Savings Text is Displayed  , Value : "+val, Status.PASS);
			if(reviewval.trim().equalsIgnoreCase(val.trim())){
				report.updateTestLog(" Order Confirmation Page", "My Waitrose Savings value matches betweem Order Review & Confirmation page ", Status.PASS);
			}else{
				report.updateTestLog(" Order Confirmation Page", "My Waitrose Savings value is not matches betweem Order Review & Confirmation page ", Status.FAIL);
				report.updateTestLog(" Order Confirmation Page", "Order Review value : "+reviewval, Status.DONE);
				report.updateTestLog(" Order Confirmation Page", "Order Confirmation value : "+val, Status.DONE);
			}
		}else{
			report.updateTestLog(" Order Confirmation Page", " My Waitrose Savings Text is not Displayed  ", Status.FAIL);
		}
	}

	public synchronized void  verifyOrderDetailsInWCSDB(){
		String jdbcClassName="com.ibm.db2.jcc.DB2Driver";
		String url="jdbc:db2://uaawecd1:50060/DAWEC00";
		String user="infos00r";
		String password="gl4c13r";
		Statement stmt;
		ResultSet rs;
		Connection connection = null;
		String orderNumber = dataTable.getData("OrderDetails", "OrderNumber").trim();
		String orderSlotDate = dataTable.getData("OrderDetails", "SlotDate_DB").trim();
		try {
			//Load class into memory
			Class.forName(jdbcClassName);
			//Establish connection
			connection = DriverManager.getConnection(url, user, password);
			stmt = connection.createStatement();
			report.updateTestLog(" Database Validation ", " Order Number :  "+orderNumber, Status.DONE);
			report.updateTestLog(" Database Validation ", " ************************************", Status.DONE);
			String query1 = "select STATUS from LWECAPP.ORDERS where ORDERS_ID = "+orderNumber;
			rs = stmt.executeQuery(query1);
			if(rs.next()){

				System.out.println(rs.getString(1));
				if(rs.getString(1).trim().equalsIgnoreCase("M")){
					report.updateTestLog(" Database Validation ", " Order Status is updated as M ", Status.PASS);
				}else{
					report.updateTestLog(" Database Validation ", " Order Status is not updated as M ", Status.FAIL);
				}

			}else{
				report.updateTestLog(" Database Validation ", " Order Status is not updated for the order ", Status.FAIL);
			}
			report.updateTestLog(" Database Validation ", " ************************************", Status.DONE);
			//String query2 = "SELECT SLOTDATE FROM LWECAPP.XSLOTRSVTN where ORDERS_ID = "+orderNumber;
			String query2 = "select Slotdate from lwecapp.XSLOTRSVTN where ORDERS_ID = "+orderNumber+" and Reservationstatus = 'R'";
			rs = stmt.executeQuery(query2);
			if(!rs.next()){
				report.updateTestLog(" Database Validation ", " Order Slot date is not updated in Database  "+orderSlotDate, Status.FAIL);
			}else{
				System.out.println(rs.getString(1));
				if(rs.getString(1).trim().equalsIgnoreCase(orderSlotDate.trim())){
					report.updateTestLog(" Database Validation ", " Application Slot date  "+orderSlotDate, Status.DONE);
					report.updateTestLog(" Database Validation ", " DB Slot date  "+rs.getString(1), Status.DONE);
					report.updateTestLog(" Database Validation ", " Slot date is updated properly as "+orderSlotDate, Status.PASS);
				}else{
					report.updateTestLog(" Database Validation ", " Application Slot date  "+orderSlotDate, Status.DONE);
					report.updateTestLog(" Database Validation ", " DB Slot date  "+rs.getString(1), Status.DONE);	
					report.updateTestLog(" Database Validation ", " Slot date is not updated properly "+orderSlotDate, Status.FAIL);
				}

			}
			report.updateTestLog(" Database Validation ", " ************************************", Status.DONE);

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			if(connection!=null){
				System.out.println("Connected successfully.");
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}





	public synchronized void  verifyEditOrderDetailsInWCSDB(){
		String jdbcClassName="com.ibm.db2.jcc.DB2Driver";
		String url="jdbc:db2://uaawecd1:50060/DAWEC00";
		String user="infos00r";
		String password="gl4c13r";
		Statement stmt;
		ResultSet rs;
		Connection connection = null;
		String orderNumber = dataTable.getData("OrderDetails", "OrderNumber").trim();
		String orderSlotDate = dataTable.getData("OrderDetails", "SlotDate_DB").trim();
		try {
			//Load class into memory
			Class.forName(jdbcClassName);
			//Establish connection
			connection = DriverManager.getConnection(url, user, password);
			stmt = connection.createStatement();
			report.updateTestLog(" Database Validation ", " Parent Order Number :  "+orderNumber, Status.DONE);
			report.updateTestLog(" Database Validation ", " ************************************", Status.DONE);
			String query1 = "select STATUS from LWECAPP.ORDERS where ORDERS_ID = "+orderNumber;
			rs = stmt.executeQuery(query1);
			if(rs.next()){
				System.out.println(rs.getString(1));
				if(rs.getString(1).trim().equalsIgnoreCase("J")){
					report.updateTestLog(" Database Validation ", " Order Status is updated as J ", Status.PASS);
				}else{
					report.updateTestLog(" Database Validation ", " Order Status is not updated as J ", Status.FAIL);
				}

			}else{
				report.updateTestLog(" Database Validation ", " Order Status is not updated for the order ", Status.FAIL);
			}
			report.updateTestLog(" Database Validation ", " ************************************", Status.DONE);
			String childOrderNumberQuery = "SELECT CHILD_ORDER_ID FROM XORDERREL where PARENT_ORDER_ID = "+orderNumber;
			String childorderNumber;
			rs = stmt.executeQuery(childOrderNumberQuery);
			if(rs.next()){
				childorderNumber = rs.getString(1);
				report.updateTestLog(" Database Validation ", " Child Order Number :  "+childorderNumber, Status.PASS);
				String query2 = "select STATUS from LWECAPP.ORDERS where ORDERS_ID = "+childorderNumber;
				rs = stmt.executeQuery(query2);
				if(rs.next()){

					System.out.println(rs.getString(1));
					if(rs.getString(1).trim().equalsIgnoreCase("M")){
						report.updateTestLog(" Database Validation ", " Order Status is updated as M ", Status.PASS);
					}else{
						report.updateTestLog(" Database Validation ", " Order Status is not updated as M ", Status.FAIL);
					}

				}else{
					report.updateTestLog(" Database Validation ", " Order Status is not updated for the order ", Status.FAIL);
				}
				report.updateTestLog(" Database Validation ", " ************************************", Status.DONE);

				query2 = "SELECT SLOTDATE FROM LWECAPP.XSLOTRSVTN where ORDERS_ID = "+orderNumber;
				rs = stmt.executeQuery(query2);
				if(!rs.next()){
					report.updateTestLog(" Database Validation ", " Order Slot date is not updated in Database  "+orderSlotDate, Status.FAIL);
				}else{
					System.out.println(rs.getString(1));
					if(rs.getString(1).trim().equalsIgnoreCase(orderSlotDate.trim())){
						report.updateTestLog(" Database Validation ", " Application Slot date  "+orderSlotDate, Status.DONE);
						report.updateTestLog(" Database Validation ", " DB Slot date  "+rs.getString(1), Status.DONE);
						report.updateTestLog(" Database Validation ", " Slot date is updated properly as "+orderSlotDate, Status.PASS);
					}else{
						report.updateTestLog(" Database Validation ", " Application Slot date  "+orderSlotDate, Status.DONE);
						report.updateTestLog(" Database Validation ", " DB Slot date  "+rs.getString(1), Status.DONE);	
						report.updateTestLog(" Database Validation ", " Slot date is not updated properly "+orderSlotDate, Status.FAIL);
					}

				}
				report.updateTestLog(" Database Validation ", " ************************************", Status.DONE);


			}else{
				report.updateTestLog(" Database Validation ", " Child Order Number is not updated for the parent order : "+orderNumber, Status.FAIL);
			}


		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			if(connection!=null){
				System.out.println("Connected successfully.");
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void verifyDeliveryNoteIsDisplayed(){
		String Delnote;
		Delnote = helper.getText(driver, confirmation.deliveryNote_loc, confirmation.deliveryNote_txt).trim();
		if(Delnote.equalsIgnoreCase(dataTable.getData("General_Data", "DeliveryNote")))
			report.updateTestLog(" Order Confirmation ", "Delivery note is displayed " +Delnote, Status.PASS);
		else report.updateTestLog(" Order Confirmation ", "Delivery note is not displayed " +Delnote, Status.FAIL);

	}


	public double getTotalAmount(){
		String OrderItemTotal = helper.getText(driver, confirmation.orderItemTotal_loc, "(//th[contains(text(),'Estimated total*:')])[1]/../td").trim().split("�")[1];
		double price = Double.parseDouble(OrderItemTotal);
		return price;
	}

	public void validateNewBillingAddress(){
		String addres = helper.getText(driver, "xpath", "//tr//th[contains(text(),'Billing Address')]/..//td");
		System.out.println(addres);
		String title = dataTable.getData("PaymentDetails_Data", "Title");
		String firstname = dataTable.getData("PaymentDetails_Data", "FirstName");
		String lastname = dataTable.getData("PaymentDetails_Data", "LastName");
		String phonenumber = dataTable.getData("PaymentDetails_Data", "PhoneNumber");
		String postcode = dataTable.getData("PaymentDetails_Data", "Postcode").substring(0, 2);

		if(addres.contains(title) &&  addres.contains(firstname) && addres.contains(lastname) && addres.contains(phonenumber) && addres.contains(postcode))
			report.updateTestLog(" Order Confirmation ", " Billing Address is displayed correctly", Status.PASS);
		else 	report.updateTestLog(" Order Confirmation ", " Billing Address is not displayed correctly", Status.FAIL);



	}

	public void validateCollectionNote(){
		String colnote;
		colnote = helper.getText(driver, confirmation.collectionnote_loc, confirmation.collectionnote_txt).trim();
		if(colnote.equalsIgnoreCase(dataTable.getData("General_Data", "CollectionNote")))
			report.updateTestLog(" Order Confirmation ", "Collection note is displayed " +colnote, Status.PASS);
		else report.updateTestLog(" Order Confirmation ", "Collection note is not displayed " +colnote, Status.FAIL);
	}

	public void validateGiftCardApplied(){
		String cardno = dataTable.getData("PaymentDetails_Data", "GiftCard").trim().substring(dataTable.getData("PaymentDetails_Data", "GiftCard").length()-4, dataTable.getData("PaymentDetails_Data", "GiftCard").length());
		System.out.println(cardno);
		if(helper.isElementPresent(driver, "xpath", "//tr//th/..//span[contains(text(),'"+cardno+"')]"))
			report.updateTestLog(" Order Confirmation ", "Gift card1: " +dataTable.getData("PaymentDetails_Data", "GiftCard").trim()+" is  applied successfully", Status.PASS);
		else report.updateTestLog(" Order Confirmation ", "Gift card1: " +dataTable.getData("PaymentDetails_Data", "GiftCard").trim()+" is not applied successfully", Status.FAIL);
		confirmation.cardvalue = helper.getText(driver, confirmation.totalcardvalue_loc, confirmation.totalcardvalue_txt).trim().split("-")[1];


	}
	public void validateGiftVoucherApplied(){
		String voucherserialno = dataTable.getData("PaymentDetails_Data", "GVSerialNumber").trim();
		if(helper.isElementPresent(driver, "xpath", "//tr//th/..//span[contains(text(),'"+voucherserialno+"')]"))
			report.updateTestLog(" Order Confirmation ", "Gift voucher1: " +voucherserialno+" is  applied successfully", Status.PASS);
		else report.updateTestLog(" Order Confirmation ", "Gift voucher1: " +voucherserialno+" is not applied successfully", Status.FAIL);
		confirmation.vouchervalue = helper.getText(driver, confirmation.totalvouchervalue_loc, confirmation.totalvouchervalue_txt).trim().split("-")[1];

	}
	public void validateGiftCardApplied2(){
		String cardno = dataTable.getData("PaymentDetails_Data", "GiftCard1").trim().substring(dataTable.getData("PaymentDetails_Data", "GiftCard1").length()-4, dataTable.getData("PaymentDetails_Data", "GiftCard1").length());
		if(helper.isElementPresent(driver, "xpath", "//tr//th/..//span[contains(text(),'"+cardno+"')]"))
			report.updateTestLog(" Order Confirmation ", "Gift card2: " +dataTable.getData("PaymentDetails_Data", "GiftCard1").trim()+" is  applied successfully", Status.PASS);
		else report.updateTestLog(" Order Confirmation ", "Gift card2: " +dataTable.getData("PaymentDetails_Data", "GiftCard1").trim()+" is not applied successfully", Status.FAIL);
	}

	public void validateGiftVoucherApplied2(){
		String voucherserialno = dataTable.getData("PaymentDetails_Data", "GVSerialNumber1").trim();
		if(helper.isElementPresent(driver, "xpath", "//tr//th/..//span[contains(text(),'"+voucherserialno+"')]"))
			report.updateTestLog(" Order Confirmation ", "Gift voucher2: " +voucherserialno+" is  applied successfully", Status.PASS);
		else report.updateTestLog(" Order Confirmation ", "Gift voucher2: " +voucherserialno+" is not applied successfully", Status.FAIL);
	}
	
	public void validatePYOSavings(){
		String saving = dataTable.getData("OrderDetails", "WCS_PYOSavings");
		if(helper.getText(driver, confirmation.myoffer_loc, confirmation.myoffer_txt).contains(saving))
			report.updateTestLog(" Order Confirmation ", "PYO offer is displayed correctly "+saving, Status.PASS);
		else report.updateTestLog(" Order Confirmation ", "PYO offer is not displayed correctly "+saving, Status.FAIL);
		
	}
	
	public void validateMyWaitroseSavings(){
		String saving = dataTable.getData("OrderDetails", "MyWaitroseSavings");
		if(helper.getText(driver, confirmation.myoffer_loc, confirmation.myoffer_txt).contains(saving))
			report.updateTestLog(" Order Confirmation ", "PYO offer is displayed correctly "+saving, Status.PASS);
		else report.updateTestLog(" Order Confirmation ", "PYO offer is not displayed correctly "+saving, Status.FAIL);
		
	}
	
	public void validateFastOrder(){
		if(helper.isElementPresent(driver, confirmation.fastorder_loc, confirmation.fastorder_txt))
			report.updateTestLog(" Order Confirmation ", "Fast Track order is placed ", Status.PASS);
		else report.updateTestLog(" Order Confirmation ", "Fast Track Order is not placed", Status.FAIL);
	}
	public void validateCarrierBagCharge(){
		String carriercharge = orderreview.carrierbagchargevalue ;
		if(helper.getText(driver, confirmation.carrierbagcharge_loc, confirmation.carrierbagcharge_txt).contains(carriercharge))
			report.updateTestLog(" Order Confirmation ", "Carrier charge is displayed correctly "+carriercharge, Status.PASS);
		else report.updateTestLog(" Order Confirmation ", "Carrier charge  is displayed correctly "+carriercharge, Status.PASS);
		
	}
	
	public void verifyPYOSavingsNotDisplayed(){
		List<WebElement> savingss = driver.findElements(By.xpath(confirmation.myoffer_txt));
		if(!(savingss.size()>0))
			report.updateTestLog(" Order Confirmation ", "PYO offer is not displayed in order confirmation page", Status.PASS);
		else report.updateTestLog(" Order Confirmation ", "PYO offer is displayed in order confirmation page", Status.FAIL);
	}
	
	public void chkLoanItemDetails()
	{
	try{
	
	
		String ss = "";
		String sLoanItemName = "";
		if(helper.isElementPresent(driver, confirmation.txtLoanItem_Loc, confirmation.txtLoanItem)){
			ss = helper.getText(driver, confirmation.txtLoanItem_Loc, confirmation.txtLoanItem);
			sLoanItemName = helper.getText(driver, confirmation.txtloanItemName_Loc, confirmation.txtloanItemName);
		    if(sLoanItemName != null){
		    	confirmation.SelectedProductName = sLoanItemName;
		    System.out.println(confirmation.SelectedProductName);
		    report.updateTestLog("Loan Item Name ","Updated Successfully", Status.PASS);
		    }else{
		    	System.out.println(sLoanItemName);
		    	report.updateTestLog("Loan Item Name ","Failed to Update", Status.FAIL);
		    }
			report.updateTestLog("Details of Loan Item", ss, Status.PASS);
			
		  }
		else{
			report.updateTestLog("Details of Loan Item", "Loan Item details are missing", Status.FAIL);
	    }	
		
		
	}catch(Exception e)
		{
		e.printStackTrace();
		report.updateTestLog("Details of Loan Item", "Loan Item details are missing"+e.toString(), Status.FAIL);
		}	
	}

}