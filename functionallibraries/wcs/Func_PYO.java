package functionallibraries.wcs;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Status;

import functionallibraries.aws.ReusableComponents;
import pageobjects.wcs.*;
import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;


/**
 * Functional Library class
 * @author Cognizant
 */
public class Func_PYO extends ReusableLibrary
{
	/**
	 * Constructor to initialize the functional library
	 * @param scriptHelper The {@link ScriptHelper} object passed from the {@link DriverScript}
	 */
	public Func_PYO(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	JavascriptExecutor jse = (JavascriptExecutor)driver;
	WebDriverWait Wait = new WebDriverWait(driver,20);
	ReusableComponents helper = new ReusableComponents(scriptHelper);
	PYO pyo = new PYO();
	HomePage home_ui = new HomePage();

	public void getPYOProductID(){
		List<WebElement> pyoz = helper.findWebElements(driver, pyo.PYOProducts_loc, pyo.PYOProducts_btn);
		if(pyoz.size() > 0 ){
			pyo.PYOProductID = new ArrayList<String>();
			int i = 0;
			for(WebElement w: pyoz){
				String productid = w.getAttribute("data-linenumber");
				System.out.println(productid);
				pyo.PYOProductID.add(productid);
			}
			System.out.println(pyo.PYOProductID);

		}else{
			report.updateTestLog("Pick Your Own Offer ", " No PYO products are picked in PYO Grid ", Status.FAIL);
		}
	}

	public void PYOValidations(){
		// UOM validation
		List<WebElement> uomPYO = driver.findElements(By.xpath(pyo.uomPYO_txt));
		for(WebElement uom : uomPYO){
			if(uom.isDisplayed() && uomPYO.size()>15){
				report.updateTestLog("Pick Your Own Offer ", "UOM Text is displayed for more than 15 products in the list", Status.PASS);
				break;
			}else report.updateTestLog("Pick Your Own Offer ", "UOM Text is not displayed for more than 15 products in the list", Status.FAIL);

		}

		// WAS validation

		List<WebElement> wasPYO = driver.findElements(By.xpath(pyo.waspricePYO_txt));
		for(WebElement was : wasPYO){
			if(was.isDisplayed() && wasPYO.size()>15){
				report.updateTestLog("Pick Your Own Offer ", "WAS Text is displayed for more than 15 products in the list", Status.PASS);
				break;
			}else report.updateTestLog("Pick Your Own Offer ", "WAS Text is not displayed for more than 15 products in the list", Status.FAIL);

		}

		// NOW validation

		List<WebElement> nowPYO = driver.findElements(By.xpath(pyo.nowpricePYO_txt));
		for(WebElement now : nowPYO){
			if(now.isDisplayed() && nowPYO.size()>15){
				report.updateTestLog("Pick Your Own Offer ", "NOW Text is displayed for more than 15 products in the list", Status.PASS);
				break;
			}else report.updateTestLog("Pick Your Own Offer ", "NOW Text is not displayed for more than 15 products in the list", Status.FAIL);

		}

		// PYO product image alt tag validation

		List<WebElement> altPYO = driver.findElements(By.xpath(pyo.prodalt_img));
		for(WebElement alt : altPYO){
			if(alt.isDisplayed() && altPYO.size()>15){
				report.updateTestLog("Pick Your Own Offer ", "PYO product image alt tag  is displayed for more than 15 products in the list", Status.PASS);
				break;
			}else report.updateTestLog("Pick Your Own Offer ", "PYO product image alt tag is not displayed for more than 15 products in the list", Status.FAIL);

		}

		// PYO offer logo validation

		List<WebElement> logoPYO = driver.findElements(By.xpath(pyo.perofferlogo_img));
		for(WebElement logo : logoPYO){
			if(logo.isDisplayed() && logoPYO.size()>20){
				report.updateTestLog("Pick Your Own Offer ", "PYO offer logo  is displayed for more than 20 products in the list", Status.PASS);
				break;
			}else report.updateTestLog("Pick Your Own Offer ", "PYO offer logo is not displayed for more than 20 products in the list", Status.FAIL);

		}

		// PYO offer red text validation

		List<WebElement> offerlinkPYO = driver.findElements(By.xpath(pyo.perofferred_lnk));
		for(WebElement offer : offerlinkPYO){
			if(offer.isDisplayed() && offerlinkPYO.size()>20){
				report.updateTestLog("Pick Your Own Offer ", "offer red text  is displayed for more than 20 products in the list", Status.PASS);
				break;
			}else report.updateTestLog("Pick Your Own Offer ", "offer red text is not displayed for more than 20 products in the list", Status.FAIL);

		}
		// Pick button validation

		List<WebElement> pickbtn = driver.findElements(By.xpath(pyo.pickbutton_btn));
		for(WebElement pick : pickbtn){
			if(pick.isDisplayed() && pickbtn.size()>20){
				report.updateTestLog("Pick Your Own Offer ", "Add to basket button is not displayed, pick button  is displayed for more than 20 products in the list", Status.PASS);
				break;
			}else report.updateTestLog("Pick Your Own Offer ", "Add to basket button is not displayed, pick button is not displayed", Status.FAIL);

		}
		// Link to PDP validation

		List<WebElement> lnkPDP = driver.findElements(By.xpath(pyo.linktoPDP));
		for(WebElement pdp : lnkPDP){
			if(pdp.isDisplayed() && lnkPDP.size()>20){
				report.updateTestLog("Pick Your Own Offer ", "Link to PDP page  is displayed for more than 20 products in the list", Status.PASS);
				break;
			}else report.updateTestLog("Pick Your Own Offer ", "Link to PDP page is not displayed", Status.FAIL);

		}

		// bread crumbs 
		if(helper.isElementPresent(driver, "xpath", "//ul[@class='breadcrumb']"))

			report.updateTestLog("Pick Your Own Offer ", "Bread crumb is displayed correctly as Home>groceries>Pick your own offers", Status.PASS);
		else report.updateTestLog("Pick Your Own Offer ", "Bread crumb is not displayed correctly as Home>groceries>Pick your own offers", Status.FAIL);

		// PYO review  validation

		List<WebElement> reviewPYO = driver.findElements(By.xpath(pyo.reviewsPYO_txt));
		for(WebElement review : reviewPYO){
			if(review.isDisplayed() && reviewPYO.size()>10){
				report.updateTestLog("Pick Your Own Offer ", " PYO review  is displayed for more than 20 products in the list", Status.PASS);
				break;
			}else report.updateTestLog("Pick Your Own Offer ", "PYO review  is not displayed for more than 20 products in the list", Status.FAIL);

		}
	}

	public void addToSetBuilder(){
		String countt = dataTable.getData("General_Data", "AddProductPYO");
		int count = Integer.parseInt(countt);
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		WebElement btn = driver.findElement(By.xpath(pyo.pickbutton_btn));
		Actions actions = new Actions(driver);
		actions.moveToElement(btn);
		
		actions.perform();
		int i = 0;

		List<WebElement> pickbtn = driver.findElements(By.xpath(pyo.pickbutton_btn));
		for(WebElement pick : pickbtn){

			pick.click();
			i++;
			if(count==i)
				break;
		}
		report.updateTestLog("Pick Your Own Offer ", "Products are added to set builder", Status.DONE);


	}
	public void validatePickButtonIsDisabled(){
		List<WebElement> prodcount = driver.findElements(By.xpath(pyo.selectedPYOprod_grid));
		if( prodcount.size()==10 && helper.isElementPresent(driver, pyo.pickdisabled_loc, pyo.pickdisabled_btn))
			report.updateTestLog("Pick Your Own Offer ", " Unable to pick more than 10 products", Status.PASS);
		else report.updateTestLog("Pick Your Own Offer ", " pick button is not disabled", Status.FAIL);
	}

	
	public void addFullSetToSetBuilder(){
		
		int count = 10;
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,500)",pyo.pickbutton_btn);
		int i = 0;

		List<WebElement> pickbtn = driver.findElements(By.xpath(pyo.pickbutton_btn));
		for(WebElement pick : pickbtn){

			pick.click();
			i++;
			if(count==i)
				break;
		}
		report.updateTestLog("Pick Your Own Offer ", "10 Products are added to set builder", Status.DONE);

	}

	public void confirmMyPicks(){
		helper.clickLink(driver, pyo.confirmmypicks_loc, pyo.confirmmypicks_btn);
		helper.clickLink(driver, pyo.confirmoverlayconfirm_loc, pyo.confirmoverlayconfirm_btn);
		helper.checkPageReadyState(2000);
		if(helper.isElementPresent(driver, pyo.confirmedproduct_loc, pyo.confirmedproduct_tag))
			report.updateTestLog("Pick Your Own Offer ", " Product is picked and confirmed", Status.PASS);
		else report.updateTestLog("Pick Your Own Offer ", " Product is picked and confirmed", Status.FAIL);
	}
	
	public void checkConfirmMyPicksIsDisabled(){
		if(helper.isElementPresent(driver, pyo.confirmmypicksdisable_loc, pyo.confirmmypicksdisable_btn))
			report.updateTestLog("Pick Your Own Offer ", " Confirm my picks is disabled", Status.PASS);
		else report.updateTestLog("Pick Your Own Offer ", " Confirm my picks is not disabled", Status.FAIL);
	}

	public void removeSingleProductAndValidate(){

		WebElement PYOselectedprod = driver.findElement(By.xpath(pyo.selectedPYOprod_grid));
		WebElement PYORemove = driver.findElement(By.xpath(pyo.removemypicks_btn));
		WebElement prodid =driver.findElement(By.xpath("//div[@class='pyoOfferSelected']//div[@class='m-product-cell']"));
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,100)",PYOselectedprod);
		helper.checkPageReadyState(500);
		Actions builder = new Actions(driver);
		builder.moveToElement(PYOselectedprod).perform();
		String id = prodid.getAttribute("data-linenumber");
		System.out.println(id);
		helper.checkPageReadyState(500);
		PYORemove.click();
		System.out.println("//div[@class='m-product-cell']//a[@data-linenumber='"+id+"' and @class='add']");
		WebElement prod = driver.findElement(By.xpath("//div[@class='m-product-cell']//a[@data-linenumber='"+id+"' and @class='add']"));
		Actions actions = new Actions(driver);
		actions.moveToElement(prod);
		
		actions.perform();
		if(prod.isDisplayed())
			report.updateTestLog("Pick Your Own Offer ", "Product removed "+id+"is displayed with pick button", Status.PASS);
		else 	report.updateTestLog("Pick Your Own Offer ", "Product removed "+id+"is not  displayed with pick button", Status.FAIL);



	}
	public void confirmMyPicksForRemoval(){
		helper.clickLink(driver, pyo.confirmmypicks_loc, pyo.confirmmypicks_btn);
		helper.clickLink(driver, pyo.confirmoverlayconfirm_loc, pyo.confirmoverlayconfirm_btn);
		helper.checkPageReadyState(2000);
		if(helper.isElementPresent(driver, pyo.confirmedproduct_loc, pyo.confirmedproduct_tag))
		report.updateTestLog("Pick Your Own Offer ", " Products are removed and confirmed", Status.PASS);
		else report.updateTestLog("Pick Your Own Offer ", " Products are removed and confirmed", Status.FAIL);
	}
	
	public void removeFromSetBuilder(){
		List<WebElement> PYOselectedprod = driver.findElements(By.xpath(pyo.selectedPYOprod_grid));
		List<WebElement> PYORemove = driver.findElements(By.xpath(pyo.removemypicks_btn));
		String countt = dataTable.getData("General_Data", "RemoveProductPYO");
		int count = Integer.parseInt(countt);
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,200)",PYOselectedprod);
		int j = 0;
		WebElement prod,remove;
		for(int i=1; i<=PYOselectedprod.size();i++) {
			driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
			Actions builder = new Actions(driver);
			prod = PYOselectedprod.get(i);
			remove = PYORemove.get(i);
			builder.moveToElement(prod).perform();
			helper.checkPageReadyState(500);
			remove.click();
			helper.checkPageReadyState(1000);
			j++;
				if(count==i){
					report.updateTestLog("Pick Your Own Offer "," Products "+count+" have been revomed", Status.DONE);
					break;
				
				}
		}
	}
	
	public void clickRegisterYourCard(){
		helper.clickLink(driver, pyo.registercard_loc, pyo.registercard_btn);
		report.updateTestLog("Pick Your Own Offer ", " Register your card is clicked", Status.DONE);
		
	}
	
	public void clickJoinMyWaitrose(){
		helper.clickLink(driver, pyo.joinmywaitrose_loc, pyo.joinmywaitrose_btn);
		report.updateTestLog("Pick Your Own Offer ", " join my waitrose is clicked", Status.DONE);
		
	}
	
	
	
	public int getWarningCount(){
		String count = helper.getText(driver, pyo.warningcount_loc,pyo.warningcount_txt).split("Once confirmed, you can make ")[1].substring(0, 2).trim();

		System.out.println(count);
		int countt = Integer.parseInt(count);
		return countt;

	}
	
	public int validateCountIsDec(int count){
		String countwarn = helper.getText(driver, pyo.warningcount_loc,pyo.warningcount_txt).split("Once confirmed, you can make ")[1].substring(0, 2).trim();
		System.out.println(count);
		int countt = Integer.parseInt(countwarn);
		String ve = dataTable.getData("General_Data", "RemoveProductPYO");
		int removec = Integer.parseInt(ve);
		count = count-removec;
		if(count==countt)
			report.updateTestLog("Pick Your Own Offer ", " Warning count is modified from count:"+count+" to  count: "+countt, Status.PASS);
		else 			report.updateTestLog("Pick Your Own Offer ", " Warning count is not modified from count:"+count+" to  count: "+countt, Status.FAIL);
		return countt;

	}
	
	public int validateCountIsDec1(int count){
		String countwarn = helper.getText(driver, pyo.warningcount_loc,pyo.warningcount_txt).split("Once confirmed, you can make ")[1].substring(0, 2).trim();
		System.out.println(count);
		int countt = Integer.parseInt(countwarn);
		String ve = dataTable.getData("General_Data", "RemoveProductPYO1");
		int removec = Integer.parseInt(ve);
		count = count-removec;
		if(count==countt)
			report.updateTestLog("Pick Your Own Offer ", " Warning count is modified from count:"+count+" to  count: "+countt, Status.PASS);
		else 			report.updateTestLog("Pick Your Own Offer ", " Warning count is not modified from count:"+count+" to  count: "+countt, Status.FAIL);
		return countt;

	}


	public int validateCountIsNotsDec(int count){
		String countwarn = helper.getText(driver, pyo.warningcount_loc,pyo.warningcount_txt).split("Once confirmed, you can make ")[1].substring(0, 2).trim();
		System.out.println(count);
		int countt = Integer.parseInt(countwarn);
		if(count==countt)
			report.updateTestLog("Pick Your Own Offer ", " Warning count is not modified from count:"+countt, Status.PASS);
		else 		report.updateTestLog("Pick Your Own Offer ", " Warning count is  modified from count:"+countt, Status.FAIL);
		return countt;

	}

	public void clickConfirmMyPicks(){
		helper.clickLink(driver, pyo.confirmmypicks_loc, pyo.confirmmypicks_btn);
		report.updateTestLog("Pick Your Own Offer ", "confrim my picks button is clicked is clicked", Status.DONE);		
	}

	public void confirmMypicksOverlay(){

		if(helper.isElementPresent(driver, "xpath", "(//p[contains(text(),'Please confirm your new Picks. These changes will be applied right away, both in store and online.')])[3]"))
			report.updateTestLog("Pick Your Own Offer ", "PLease confirm your picks message is displayed", Status.PASS);
		else report.updateTestLog("Pick Your Own Offer ", "PLease confirm your picks message is not displayed", Status.FAIL);
		if(helper.isElementPresent(driver, "xpath", "(//p[contains(text(),'Your Picks will be emailed to you so you can keep them handy for the next time you shop.')])[2]"))
			report.updateTestLog("Pick Your Own Offer ", "Your Email will be triggered message is displayed", Status.PASS);
		else report.updateTestLog("Pick Your Own Offer ", "Your Email will be triggered message is not displayed", Status.FAIL);
		if(helper.isElementPresent(driver, pyo.cancel_loc, pyo.cancel_btn))
			report.updateTestLog("Pick Your Own Offer ", "Cancel button is present", Status.PASS);
		else report.updateTestLog("Pick Your Own Offer ", "Cancel button is not present", Status.FAIL);
		helper.clickLink(driver, pyo.confirmoverlayconfirm_loc, pyo.confirmoverlayconfirm_btn);
		helper.checkPageReadyState(2000);
		Wait.until(ExpectedConditions.elementToBeClickable(By.className(home_ui.favourite_lnk)));
		report.updateTestLog("Pick Your Own Offer ", "Confirm button is clicked", Status.PASS);
	}	
	
	public void confirmMypicksOverlayRemoval(){

		if(helper.isElementPresent(driver, "xpath", "(//p[contains(text(),'Please confirm your new Picks. These changes will be applied right away, both in store and online.')])[3]"))
			report.updateTestLog("Pick Your Own Offer ", "PLease confirm your picks message is displayed", Status.PASS);
		else report.updateTestLog("Pick Your Own Offer ", "PLease confirm your picks message is not displayed", Status.FAIL);
		if(helper.isElementPresent(driver, pyo.cancel_loc, pyo.cancel_btn))
			report.updateTestLog("Pick Your Own Offer ", "Cancel button is present", Status.PASS);
		else report.updateTestLog("Pick Your Own Offer ", "Cancel button is not present", Status.FAIL);
		helper.clickLink(driver, pyo.confirmoverlayconfirm_loc, pyo.confirmoverlayconfirm_btn);
		helper.checkPageReadyState(2000);
		Wait.until(ExpectedConditions.elementToBeClickable(By.className(home_ui.favourite_lnk)));
		report.updateTestLog("Pick Your Own Offer ", "Confirm button is clicked", Status.PASS);
	}	
	
	public void clickCancelButton(){
		helper.clickLink(driver, pyo.cancel_loc, pyo.cancel_btn);
		report.updateTestLog("Pick Your Own Offer ", "Cancel button is clicked", Status.DONE);


	}
	
	public void verifyYouAlmostReachedMsgIsDisplayed(){
		if(helper.isElementPresent(driver, pyo.youalmost_loc, pyo.youalmost_txt))
			report.updateTestLog("Pick Your Own Offer ", "You have almost reached you limits message is displayed", Status.PASS);
		else report.updateTestLog("Pick Your Own Offer ", "You have almost reached you limits message is not displayed", Status.FAIL);
	}
	

	public void verifyYouReachedMsgIsDisplayed(){
		if(helper.isElementPresent(driver, pyo.youreached_loc, pyo.youreached_txt))
			report.updateTestLog("Pick Your Own Offer ", "You have  reached you limits message is displayed", Status.PASS);
		else report.updateTestLog("Pick Your Own Offer ", "You have  reached you limits message is not displayed", Status.FAIL);
	}
	
	public void verifyUnpickIsDsiabled(){
		List<WebElement> PYOselectedprod = driver.findElements(By.xpath(pyo.selectedPYOprod_grid));
		List<WebElement> PYORemove = driver.findElements(By.xpath("//div[@class='pyoOfferSelected']//div[@class='m-product-cell']//div[contains(@class,'disabled')]"));
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,200)",PYOselectedprod);
		
		WebElement prod,remove;
		System.out.println(PYORemove.size());
		for(int i=0; i<PYOselectedprod.size();i++) {
			Actions builder = new Actions(driver);
			prod = PYOselectedprod.get(i);
			
			builder.moveToElement(prod).build().perform();
			helper.checkPageReadyState(200);
			remove = PYORemove.get(i);
			if(remove.isDisplayed())
				report.updateTestLog("Pick Your Own Offer ", "UnPick button is disabled", Status.PASS);
			else report.updateTestLog("Pick Your Own Offer ", "UnPick button is not disabled", Status.FAIL);
			

			
		}
	}
	public void removeFromSetBuilder1(){
		List<WebElement> PYOselectedprod = driver.findElements(By.xpath(pyo.selectedPYOprod_grid));
		List<WebElement> PYORemove = driver.findElements(By.xpath(pyo.removemypicks_btn));
		String countt = dataTable.getData("General_Data", "RemoveProductPYO1");
		int count = Integer.parseInt(countt);
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,200)",PYOselectedprod);
		
		WebElement prod,remove;
		System.out.println(PYORemove.size());
		for(int i=0; i<PYOselectedprod.size();i++) {
			if(count==i){
				report.updateTestLog("Pick Your Own Offer "," Products "+count+" have been revomed", Status.DONE);
				break;
			}
			Actions builder = new Actions(driver);
			prod = PYOselectedprod.get(i);
			
			builder.moveToElement(prod).build().perform();
			helper.checkPageReadyState(200);
			remove = PYORemove.get(i);
			remove.click();
			//helper.checkPageReadyState(1000);
			
			
			

			
		}
	}


}