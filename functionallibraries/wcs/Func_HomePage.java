package functionallibraries.wcs;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.rules.Timeout;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.cognizant.framework.Status;
import com.thoughtworks.selenium.SeleniumException;

import functionallibraries.aws.ReusableComponents;
import pageobjects.wcs.*;
import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;






public class Func_HomePage extends ReusableLibrary
{
	/**
	 * Constructor to initialize the functional library
	 * @param scriptHelper The {@link ScriptHelper} object passed from the {@link DriverScript}
	 */
	public Func_HomePage(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	WebDriverWait Wait = new WebDriverWait(driver,20);
	ReusableComponents helper = new ReusableComponents(scriptHelper);
	HomePage home_ui = new HomePage();
	Func_BookSlot bkslot = new Func_BookSlot(scriptHelper);
	Search search = new Search();

	// to select Book a slot in home page
	public void clickBookSlotBtn(){
	WebElement bookslotbtn = (new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(home_ui.Btn_BookSlot)));
	bookslotbtn.click();	
	report.updateTestLog("Home Page", " Book Slot Button is selected ", Status.DONE);
	}
	
	public void clickGroceryHeaderLink(){
		//driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		helper.clickLink(driver, home_ui.GroceryHeader_loc,home_ui.GroceryHeader_link);
		report.updateTestLog("Home Page", " Grocery Header Link is selected ", Status.DONE);
		Assert.assertTrue(true, "Selected Grocery Header Link");
	}
	
public void enterKeywordAndSearch(){
	helper.checkPageReadyState(500);
	driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
	String keyword = dataTable.getData("General_Data","Search");
	helper.typeinTextbox(driver, home_ui.searchbox_loc, home_ui.search_box, keyword);
	helper.clickLink(driver, home_ui.serachbtn_loc, home_ui.search_btn);
	report.updateTestLog("Home Page", "Performed keyword search for :  "+keyword, Status.PASS);
}	

public void clickHeaderTrolleyLink()
{
	helper.checkPageReadyState(500);
	helper.clickLink(driver, home_ui.trolleybtn_loc, home_ui.trolley_btn);
	report.updateTestLog("Home Page", " Selected trolley icon in header  ", Status.DONE);
}

public void clickCheckOutBtn(){
	try{
	bkslot.returnPageSource(10000);
	timedelay();
	helper.clickObject(driver, home_ui.checkout_loc, home_ui.checkout_btn);
	report.updateTestLog("Home Page", " Checkout Button is selected  ", Status.DONE);
	}
	catch(Exception e){
		e.printStackTrace();
	}
}

public void clickStartShoppingButton(){
	helper.clickLink(driver, home_ui.startshopping_loc, home_ui.startshopping_btn);
	report.updateTestLog("Home Page", " Start Shopping Button is selected  ", Status.DONE);
}

public void getTrolleyPrice(){
	String trolleyprice = helper.getText(driver, home_ui.trolleyprice_loc, home_ui.trolleyprice_txt);
	System.out.println("Trolley Price : "+trolleyprice);
}

public void navigateToMyOrdersPage(){
	clickMyAccountLink();
	clickMyOrdersLink();
}

public void clickMyAccountLink(){
	//driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
	Wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(home_ui.myaccount_lnk)));
	helper.clickLink(driver, home_ui.myaccount_loc, home_ui.myaccount_lnk);
	try{
		Thread.sleep(3000);
	}
		catch (Exception e){
		}
	report.updateTestLog("Home Page", " My Account link is selected  ", Status.PASS);
}

public void clickMyOrdersLink(){
	Wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText(home_ui.myorder_lnk)));
	helper.clickLink(driver, home_ui.myorder_loc, home_ui.myorder_lnk);
	report.updateTestLog("Home Page", " My Order link is selected  ", Status.PASS);
}

public void clickFavouriteLink(){
	helper.checkPageReadyState(3000);
	Wait.until(ExpectedConditions.elementToBeClickable(By.className(home_ui.favourite_lnk)));
	helper.clickLink(driver, home_ui.favourite_loc, home_ui.favourite_lnk);
	report.updateTestLog("Home Page", " Favourite Link is selected  ", Status.PASS);

}

public void clickEditSlot(){
	Wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(home_ui.editslot_btn)));
	helper.clickLink(driver, home_ui.editslot_loc, home_ui.editslot_btn);
	report.updateTestLog("Home Page", " Edit Slot button is selected  ", Status.PASS);
}

public void clickChangeSlotInEditSlotOverlay(){
	Wait.until(ExpectedConditions.visibilityOf(helper.findWebElement(driver, home_ui.changeslotInEditSlotOverlay_loc, home_ui.changeslotInEditSlotOverlay_btn)));
	helper.clickLink(driver, home_ui.changeslotInEditSlotOverlay_loc, home_ui.changeslotInEditSlotOverlay_btn);
	report.updateTestLog("Home Page", " Change Slot button is selected from edit slot overlay ", Status.PASS);
}

public void enterKeywordAndSelectAutoSuggestCategory(){
	helper.checkPageReadyState(500);
	String keyword = dataTable.getData("General_Data","SearchCategory");
	helper.typeinTextbox(driver, home_ui.searchbox_loc, home_ui.search_box, keyword);
	helper.checkPageReadyState(1000);
	helper.clickLink(driver, home_ui.autosuggestcategory_loc, home_ui.autosuggestcategory_lnk);
	report.updateTestLog("Home Page", "Performed Category search for :  "+keyword, Status.PASS);
}	

public void clickGroceryLinkInHeader(){
	helper.clickLink(driver, home_ui.groceryheader_loc, home_ui.groceryheader_lnk);
	helper.checkPageReadyState(1000);
	report.updateTestLog("Home Page", " Grocery Link in header is selected  ", Status.PASS);
}


public void performProductSearchAndAddProduct(){
	String productid = dataTable.getData("General_Data", "Search_ProductID");
	String productqty = dataTable.getData("General_Data", "PDP_Qty");
	//Wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("home_ui.search_box")));
	driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	helper.typeinTextbox(driver, home_ui.searchbox_loc, home_ui.search_box, productid);
	helper.findWebElement(driver, home_ui.searchbox_loc, home_ui.search_box).sendKeys(Keys.ENTER);
	helper.typeinTextbox(driver, search.PDPquantity_loc, search.PDPquantity_box, productqty);
	helper.clickLink(driver, search.PDPAddtotrolley_loc, search.PDPAddtotrolley_btn);
	report.updateTestLog(" PDP "," Searched and added product to enable minimum checkout ", Status.PASS);
}

public void performProductSearchAndAddProduct1(){
	String productid = dataTable.getData("General_Data", "Search_ProductID1");
	String productqty = dataTable.getData("General_Data", "PDP_Qty");
	helper.typeinTextbox(driver, home_ui.searchbox_loc, home_ui.search_box, productid);
	helper.findWebElement(driver, home_ui.searchbox_loc, home_ui.search_box).sendKeys(Keys.ENTER);
	helper.typeinTextbox(driver, search.PDPquantity_loc, search.PDPquantity_box, productqty);
	helper.clickLink(driver, search.PDPAddtotrolley_loc, search.PDPAddtotrolley_btn);
	report.updateTestLog(" PDP "," Searched and added product to enable minimum checkout ", Status.PASS);
}

public void enterPersonalizedMsg(){
	String msg = dataTable.getData("General_Data", "PersonalizedMsg");
	Wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(search.personalizedmsg_box)));
	helper.typeinTextbox(driver, search.personalizedmsg_loc, search.personalizedmsg_box, msg);
	helper.clickLink(driver, search.personalizedmsgsave_loc, search.personalizedmsgsave_btn);
	report.updateTestLog(" PDP "," Entered Personalized Msg & Continued ", Status.PASS);
}

public void clickEntertainingLinkInHeader(){
	//clickHomeImg();
	helper.clickLink(driver, home_ui.EntertainingHeader_loc, home_ui.EntertainingHeader_link);
	helper.checkPageReadyState(1000);
	report.updateTestLog("Home Page", " Entertaining Link in header is selected  ", Status.PASS);
}

public void enterKeywordAndSearch2(){
	helper.checkPageReadyState(500);
	String keyword = dataTable.getData("General_Data","Search2");
	helper.typeinTextbox(driver, home_ui.searchbox_loc, home_ui.search_box, keyword);
	helper.clickLink(driver, home_ui.serachbtn_loc, home_ui.search_btn);
	report.updateTestLog("Home Page", "Performed keyword search for :  "+keyword, Status.PASS);
}

public void clickBrowseShopButton(){
	Wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(home_ui.browseshop_btn)));
	if(helper.isElementPresent(driver, home_ui.browseshop_loc, home_ui.browseshop_btn)){
		helper.clickLink(driver, home_ui.browseshop_loc, home_ui.browseshop_btn);
		report.updateTestLog("Home Page", "Browse Shop button is selected :  ", Status.DONE);
	}else{
		report.updateTestLog("Home Page", "Browse Shop button is not displayed :  ", Status.FAIL);
	}
}

public void getFavouritesProductID(){
	Wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(home_ui.favouritesProductID_lnk)));
	List<WebElement> favprdid = helper.findWebElements(driver, home_ui.favouritesProductID_loc, home_ui.favouritesProductID_lnk);
	HomePage.favproductid = new ArrayList<String>();
	for(WebElement f:favprdid){
		String id = f.getAttribute("data-linenumber");
		System.out.println(id);
		HomePage.favproductid.add(id);
	}
	System.out.println(HomePage.favproductid);
}

public void getFavouritesProductIDNotInTrolley(){
	Wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(home_ui.productnotintrolleyfav_prod)));
	List<WebElement> favprdid = helper.findWebElements(driver, home_ui.productnotintrolleyfav_loc, home_ui.productnotintrolleyfav_prod);
	HomePage.favproductid = new ArrayList<String>();
	for(WebElement f:favprdid){
		String id = f.getAttribute("data-linenumber");
		System.out.println(id);
		HomePage.favproductid.add(id);
	}
	System.out.println(HomePage.favproductid);
}

public void clickPYOLink(){;
	driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	Wait.until(ExpectedConditions.elementToBeClickable(By.className(home_ui.pyo_lnk)));
	helper.clickLink(driver, home_ui.pyo_loc,home_ui.pyo_lnk);
	report.updateTestLog("Home Page", " PYO Link is selected  ", Status.PASS);
}

public void clickMyAccountInMyAccountList(){
	Wait.until(ExpectedConditions.elementToBeClickable(By.className(home_ui.myaccountinlist_lnk)));
	helper.clickLink(driver, home_ui.myaccountinlist_loc,home_ui.myaccountinlist_lnk);
	report.updateTestLog("Home Page", " My account Link is selected  ", Status.PASS);
}

public void clickChangeService(){
	Wait.until(ExpectedConditions.elementToBeClickable(By.xpath(home_ui.changeserivice_btn)));
	helper.clickObject(driver, home_ui.changeserivice_loc, home_ui.changeserivice_btn);
	
	report.updateTestLog("Home Page", " Change service is displayed ", Status.DONE);
	
}

public void moveToOfferPage(){
	Wait.until(ExpectedConditions.elementToBeClickable(By.xpath(home_ui.offertab_lnk)));
	helper.clickLink(driver, home_ui.offerstab_loc, home_ui.offertab_lnk);
	Wait.until(ExpectedConditions.elementToBeClickable(By.xpath(home_ui.viewalloffers_lnk)));
	if(helper.isElementPresent(driver, home_ui.viewalloffers_loc, home_ui.viewalloffers_lnk))
		helper.clickLink(driver, home_ui.viewalloffers_loc, home_ui.viewalloffers_lnk);
	report.updateTestLog("Home Page", " Offer link is clicked", Status.DONE);
}

public String getTotalSavings(){
	driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
	Wait.until(ExpectedConditions.elementToBeClickable(By.xpath(home_ui.totalsavings_txt)));
	String savings = helper.getText(driver, home_ui.totalsavings_loc, home_ui.totalsavings_txt).split("�")[1];
	if(!savings.equals("0.00"))
		report.updateTestLog("Home Page", "Trolley saving is displayed", Status.PASS);
	else report.updateTestLog("Home Page", "Trolley saving is not displayed", Status.FAIL);
	return savings;
}

public void verifyMywaitroseSavingNotApplied(){
	String savings = helper.getText(driver, home_ui.totalsavings_loc, home_ui.totalsavings_txt).split("�")[1];
	if(savings.equals("0.00"))
		report.updateTestLog("Home Page", "Trolley saving is 0.00,  offer is not applied", Status.PASS);
	else report.updateTestLog("Home Page", "Trolley saving is 0.00,  offer is not applied", Status.FAIL);

}

public String getTotalSavingsAfter(){
	String savings = helper.getText(driver, home_ui.totalsavings_loc, home_ui.totalsavings_txt).split("�")[1];
	return savings;
}

public void validateMyWaitroseSavingIsApplied(String bef,String after){
	if(!bef.equals(after))
		report.updateTestLog("Home Page", "Trolley mywaitrose saving is updated", Status.PASS);
	else report.updateTestLog("Home Page", "Trolley mywaitrose saving is not updated", Status.FAIL);
}

public void validatePYOSavingIsApplied(String bef,String after){
	if(!bef.equals(after))
		report.updateTestLog("Home Page", "Trolley PYO saving is updated", Status.PASS);
	else report.updateTestLog("Home Page", "Trolley PYO saving is not updated", Status.FAIL);
}
public void homePageValidation(){
	driver.navigate().refresh();
	helper.clickLink(driver, "xpath", "//a[text()='Waitrose']");
	if(helper.isElementPresent(driver, home_ui.Warrent1_loc, home_ui.Warrent1_img))
		report.updateTestLog("Home Page", "Warrent 1 is displayed", Status.PASS);
		else report.updateTestLog("Home Page", "Warrent 1 is not displayed", Status.FAIL);
	
	if(helper.isElementPresent(driver, home_ui.Warrent2_loc, home_ui.Warrent2_img))
		report.updateTestLog("Home Page", "Warrent 2 is displayed", Status.PASS);
		else report.updateTestLog("Home Page", "Warrent 2 is not displayed", Status.FAIL);
	
	if(helper.isElementPresent(driver, home_ui.findbranch_loc, home_ui.findbranch_lnk))
		report.updateTestLog("Home Page", "Find Branch is displayed", Status.PASS);
		else report.updateTestLog("Home Page", "Find Branch is not displayed", Status.FAIL);
	

	if(helper.isElementPresent(driver, home_ui.mywaitrose_loc, home_ui.mywaitrose_lnk))
		report.updateTestLog("Home Page", "My waitrose is displayed", Status.PASS);
		else report.updateTestLog("Home Page", "My waitrose is not displayed", Status.FAIL);
	

	if(helper.isElementPresent(driver, "xpath", "//button[text()='Sign in/Register']"))
		report.updateTestLog("Home Page", "Sign/Register button is displayed", Status.PASS);
		else report.updateTestLog("Home Page", "Sign/Register button is not displayed", Status.FAIL);
}

public void verifyTrolleyHeaderIsEmpty(){
	if(helper.getText(driver, home_ui.trolleyprice_loc, home_ui.trolleyprice_txt).trim().contains("0.00"))
		report.updateTestLog("Home Page", "Trolley Total is 0.00", Status.PASS);
	else report.updateTestLog("Home Page", "Trolley total is not 0.00", Status.FAIL);
	
	if(helper.getText(driver, home_ui.itemcount_loc, home_ui.itemcount_txt).contains("0 items"))
		report.updateTestLog("Home Page", "Trolley item count is 0", Status.PASS);
	else report.updateTestLog("Home Page", "Trolley item count is not 0", Status.FAIL);
	
	if(helper.getText(driver, home_ui.totalsavings_loc, home_ui.totalsavings_txt).trim().contains("Est. savings �0.00"))
		report.updateTestLog("Home Page", "Trolley saving is 0.00", Status.PASS);
	else report.updateTestLog("Home Page", "Trolley saving is not 0.00", Status.FAIL);
	
}

public void clickHomeImg(){
	try{
		Thread.sleep(3000);
	}
		catch (Exception e) {
			
		}
	Wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[text()='Waitrose']")));
	helper.clickLink(driver, "xpath", "//a[text()='Waitrose']");
}

public void validateThresholdForDelAndCol(){
	WebElement checkout = driver.findElement(By.xpath(home_ui.checkout_btn));
	Actions mouse = new Actions(driver);
	mouse.moveToElement(checkout).perform();
	if(helper.isElementPresent(driver, home_ui.checkoutthreshold_loc, home_ui.checkoutthreshold_txt)){
		if(dataTable.getData("General_Data", "DeliveryType").equalsIgnoreCase("delivery")){
		report.updateTestLog("Checkout Threshold", "Checkout threshol;d message is displayed", Status.PASS);
	if(helper.getText(driver, home_ui.thresholddel_loc, home_ui.thresholddel_txt).contains("60"))
		report.updateTestLog("Checkout Threshold", "Delivery threshold 60 pounds is displayed", Status.PASS);
	else 		report.updateTestLog("Checkout Threshold", "Delivery threshold 60 pounds is not displayed", Status.FAIL);
		}
		else if(helper.getText(driver, home_ui.thresholdcollection_loc, home_ui.thresholdcollection_txt).contains("40"))
		report.updateTestLog("Checkout Threshold", "Collection threshold 40 pounds is displayed", Status.PASS);
	else 		report.updateTestLog("Checkout Threshold", "Collectiom threshold 40 pounds is not displayed", Status.FAIL);
	}
	else report.updateTestLog("Checkout Threshold", "Checkout threshol;d message is not displayed", Status.FAIL);
}


public void validateThresholdForDelOrCol(){
	WebElement checkout = driver.findElement(By.xpath(home_ui.checkout_btn));
	Actions mouse = new Actions(driver);
	mouse.moveToElement(checkout).perform();
	if(helper.isElementPresent(driver, home_ui.checkoutthreshold_loc, home_ui.checkoutthreshold_txt)){
		if(dataTable.getData("General_Data", "DeliveryType").equalsIgnoreCase("delivery")){
		report.updateTestLog("Checkout Threshold", "Checkout threshol;d message is displayed", Status.PASS);
	if(helper.getText(driver, home_ui.thresholddel_loc, home_ui.thresholddel_txt).contains("60"))
		report.updateTestLog("Checkout Threshold", "Delivery threshold 60 pounds is displayed", Status.PASS);
	else 		report.updateTestLog("Checkout Threshold", "Delivery threshold 60 pounds is not displayed", Status.FAIL);
		}
		else if(helper.getText(driver, home_ui.thresholdcollection_loc, home_ui.thresholdcollection_txt).contains("40"))
		report.updateTestLog("Checkout Threshold", "Collection threshold 40 pounds is displayed", Status.PASS);
	else 		report.updateTestLog("Checkout Threshold", "Collectiom threshold 40 pounds is not displayed", Status.FAIL);
	}
	else report.updateTestLog("Checkout Threshold", "Checkout threshol;d message is not displayed", Status.FAIL);
}


public void searchproductWithAutoSuggestion()
{
	try {		
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		if(helper.isElementPresent(driver, home_ui.txtSearch_loc, home_ui.txtSearch))	
		{
			driver.findElement(By.id(home_ui.txtSearch)).clear();
			
			driver.navigate().refresh();
			helper.typeinTextbox(driver, home_ui.txtSearch_loc, home_ui.txtSearch, dataTable.getData("General_Data" , "Search"));
			try{
				Thread.sleep(30000);
			}
				catch (Exception e) {
					// TODO: handle exception
				}
			

		}
	}
	catch(SeleniumException e)
	{
		e.printStackTrace();
		report.updateTestLog("Product Search", "Product Search field is NOT available"+e.toString(), Status.FAIL);
	}
}


public static int keywordCount;
public void verifyAutoSuggestKeywords()
{
	String searchString = dataTable.getData("General_Data" , "Search");
	boolean keywordSearch = searchKeyword(home_ui.keywordSearch_loc, home_ui.keywordSearch,searchString,"Keywords");
	if(keywordSearch)
	{
		report.updateTestLog("Keyword displayed", "Keywords displayed first", Status.PASS);
	}
	else
	{
		report.updateTestLog("Keyword displayed", "Keywords are not displayed first", Status.FAIL);
	}
}
private boolean searchKeyword(String searchLoc,String searchXpath, String searchString, String searchResult) {
	
	String keyWordResult;
	String keyWordArray[];

	if(helper.isElementPresent(driver, searchLoc, searchXpath))	
	{
		keyWordResult=driver.findElement(By.xpath(searchXpath)).getText();
		keyWordArray = keyWordResult.split("\n");
		
		if(searchResult.equalsIgnoreCase("keywords"))
		{
			//variable used in function validateDecreaseInKeywordSuggestion()
			keywordCount = keyWordArray.length;
		}
		
		String header = "Categories";
		String boldText = "Products";
		
		if(keyWordArray[0].equalsIgnoreCase(header) && (keyWordArray[0].contains(searchResult) || keyWordArray[0].contains(searchResult.toLowerCase())))
		{
			report.updateTestLog("Categories Header Displayed:", keyWordArray[0]+"Categories are displayed with Header", Status.PASS);
		}
		
		for(int i=1;i<keyWordArray.length;i++)
		{
			if(keyWordArray[i].contains(searchString) || keyWordArray[i].contains(searchString.toLowerCase()))
			{
				report.updateTestLog("Search Text"+searchString+":"+searchResult+"returned:", keyWordArray[i]+searchResult+" returned from FH", Status.PASS);
				
				if( helper.isElementPresent(driver, searchLoc, searchXpath+"/ul/li["+i+"]/a/b[contains(text(),"+searchString.toUpperCase()+")]") || helper.isElementPresent(driver, searchLoc, searchXpath+"/ul/li["+i+"]/a/b[contains(text(),"+searchString.toLowerCase()+")]"))	
				{
					report.updateTestLog("Bolded Text:" , keyWordArray[i]+searchResult+"Bolded text is displayed", Status.PASS);
				}
				else if(boldText!=searchResult)
				{
					report.updateTestLog("Bolded Text:" , keyWordArray[i]+searchResult+"Bolded text is not displayed", Status.FAIL);
				}
			}
			
		}
		
		return true;
	}
	else
	{
		report.updateTestLog(searchResult+"returned", searchResult+" are not returned from FH", Status.FAIL);
		return false;
	}
}

public void verifyAutoSuggestCategories()
{
	String searchString = dataTable.getData("General_Data" , "Search");
	boolean keywordSearch = searchKeyword(home_ui.categorySearch_loc, home_ui.categorySearch,searchString,"Categories");
	if(keywordSearch)
	{
		report.updateTestLog("Categories displayed", "Categories is displayed below to keywords", Status.PASS);
	}
	else
	{
		report.updateTestLog("Categories displayed", "Categories is not displayed below to keywords", Status.FAIL);
	}
}

public void verifyAutoSuggestProducts()
{
	String searchString = dataTable.getData("General_Data" , "Search");
	searchKeywordProducts(home_ui.productSearch_loc, home_ui.productSearch,searchString,"Products");
}

private boolean searchKeywordProducts(String searchLoc,String searchXpath, String searchString, String searchResult) {
	
	try{
		if(helper.isElementPresent(driver, searchLoc, searchXpath))	
		{
			List<WebElement> element=driver.findElements(By.xpath(searchXpath));
			int prodCnt = 0;
			for (WebElement webElement : element) {
				prodCnt = prodCnt + 1;
				String strEleTxt = webElement.getText();
				if((strEleTxt.toLowerCase()).contains(searchString.toLowerCase()))
				{
					report.updateTestLog("Search text displayed in products", "Search text: '" +searchString+ "' is displayed in product: "+strEleTxt, Status.PASS);
				}else report.updateTestLog("Search text displayed in products", "Search text: '" +searchString+ "' is not displayed in product: "+strEleTxt, Status.FAIL);
				
				if(prodCnt == 3)
				{
					break;
				}
			}
			
			return true;
		}
		else
		{
			report.updateTestLog(searchResult+"returned", searchResult+" are not returned from FH", Status.FAIL);
			return false;
		}
	}catch(Exception e)
	{
		e.printStackTrace();
		report.updateTestLog("Home Page", "Products not displayed in auto suggest: "+e.toString(), Status.FAIL);
	}
	return false;
	
}

public void clickProductKeyword()
{
	if(helper.isElementPresent(driver, home_ui.keywordSearch_loc, HomePage.keywordSearch))	
	{
		String text = dataTable.getData("General_Data" , "Search");
		
		List<WebElement> element=driver.findElements(By.xpath(home_ui.lnkAutoSuggestKeywords));
		for (WebElement webElement : element) {
			String strEleTxt = webElement.getText().trim();
			if(strEleTxt.equalsIgnoreCase(text))
			{
				webElement.click();
				report.updateTestLog("Search text found", "Search keyword: '" +text+ "' is found in keywords", Status.PASS);
				return;
			}
					
		}
		
		report.updateTestLog("Search text not found", "Search text: '" +text+ "' is not found in keywords", Status.FAIL);
	}
	
}

public void addAllProductsFromList(){
Wait.until(ExpectedConditions.elementToBeClickable(By.xpath(home_ui.addallitemsfromist_btn)));
helper.clickLink(driver, home_ui.addallitemsfromist_loc,home_ui.addallitemsfromist_btn);
// List<WebElement> unavailable = driver.findElements(By.xpath(home_ui.productunavailable_btn));
// if(unavailable.size()>0){
// for(WebElement un : unavailable){
// un.click();
// 
// }
// report.updateTestLog("Home Page", "Product unavailable alert is handled", Status.DONE);
// }
report.updateTestLog("Home Page", "Add all items to trolley button is clicked", Status.PASS);
}

public void addSingleProduct(){
Wait.until(ExpectedConditions.elementToBeClickable(By.xpath(home_ui.addallitemsfromist_btn)));
helper.typeinTextbox(driver, home_ui.productquantity_loc, home_ui.productquantity_txt, "2");
helper.clickLink(driver, home_ui.addproductlist_loc, home_ui.addproductlist_btn);
report.updateTestLog("Home Page", "Single product is added from list", Status.PASS);
}

public void addSelectedProductsToTrolley(){
Wait.until(ExpectedConditions.elementToBeClickable(By.xpath(home_ui.addallitemsfromist_btn)));
List<WebElement> checkbox = driver.findElements(By.xpath(home_ui.productselectlist_chkbx));
for(WebElement chk: checkbox){
chk.click();
if(driver.findElement(By.xpath(home_ui.addselecteditemtotrolley_btn)).isEnabled()){
driver.findElement(By.xpath(home_ui.addselecteditemtotrolley_btn)).click();
break;
}
}
// List<WebElement> unavailable = driver.findElements(By.xpath(home_ui.productunavailable_btn));
// if(unavailable.size()>0){
// for(WebElement un : unavailable){
// un.click();
// }
// report.updateTestLog("Home Page", "Product unavailable alert is handled", Status.DONE);
// }
report.updateTestLog("Home Page", "Add Selected product is clicked successfully", Status.PASS);
}

public void entertainingReservationoverlay()
{
	Wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("	//a[@class='continueRes']")));
	helper.clickLink(driver, "xpath", "//input[@id='checkres']");
	helper.clickLink(driver, "xpath", "//a[@class='continueRes']");
	report.updateTestLog("Reservation", "Reservation overlay is handled", Status.PASS);
}

public void addProducts_Recipe()
{
	clickRecipesTab();
	Wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(home_ui.viewallrecipes)));
	helper.clickLink(driver, home_ui.viewallrecipes_loc, home_ui.viewallrecipes);
	helper.clickLink(driver, "xpath", "//li/a[contains(text(),'Midweek recipes')]");
	helper.clickLink(driver, "xpath", "//div[@class='headerandimageonetwothreecol inner componentBottomMargin']/h2/a");// clicks the recipe name
	report.updateTestLog("add products from recipe","Recipe is clicked",Status.PASS);
	helper.clickLink(driver, "xpath", "//img[@alt='btn_shop-for-ingredients']");
}


public void clickRecipesTab(){
	
	helper.clickLink(driver, home_ui.recipes_loc,home_ui.recipes);
	report.updateTestLog("Recipes Tab", "Recipes tab is clicked", Status.PASS);
}

public void addAllProductsToTrolley(){
	
Wait.until(ExpectedConditions.elementToBeClickable(By.xpath(home_ui.addallitems)));
helper.clickLink(driver, home_ui.addallitems_loc,home_ui.addallitems);
report.updateTestLog("Home Page", "Add all items to trolley button is clicked", Status.PASS);

}

public void postcodeAvailabilityCheck()
{
	if(helper.isElementPresent(driver, home_ui.postcode_loc, home_ui.postcode)){
		report.updateTestLog("Postcode box in Homepage", "Post code box is present in Home page", Status.PASS);
		helper.typeinTextbox(driver, home_ui.postcode_loc, home_ui.postcode, dataTable.getData("General_Data", "CollectionPostcode"));
		helper.clickLink(driver, home_ui.checkavailability_loc, home_ui.checkavailability);
		report.updateTestLog("", "", Status.PASS);
	}
	else
		report.updateTestLog("Postcode box in Homepage", "Post code box is not present in Home page", Status.FAIL);
}

public void checkProductUnavailable()
{
	if(helper.isElementPresent(driver, "xpath", "//p[@class='product-unavailable']")){
		report.updateTestLog("Product unavailable", "The item is unavailable", Status.PASS);
		clickHomeImg();
	}
	else
		clickHomeImg();
}

public void bookSlotEntertaining(){
	Wait.until(ExpectedConditions.elementToBeClickable(By.xpath(home_ui.BookSlotEnt_Btn)));
	helper.clickLink(driver, home_ui.bookSlotEnt_loc, home_ui.BookSlotEnt_Btn);
	report.updateTestLog("Home Page", "BookSlot button is clicked", Status.PASS);
	} 

public void editSlotButtonValidation(){
	Wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(home_ui.editslotvalidation_btn)));
	//helper.clickLink(driver, home_ui.editslotvalidation_loc, home_ui.editslotvalidation_btn);
	report.updateTestLog("Home Page", "Single product is added from list", Status.PASS);
}

public void moveToMyList(){
	Wait.until(ExpectedConditions.elementToBeClickable(By.className(home_ui.mylist_lnk)));
	helper.clickLink(driver, home_ui.mylist_loc,home_ui.mylist_lnk);
	report.updateTestLog("Home Page", " My List Link is selected  ", Status.PASS);
}

public void addToTrolleyUnderList(){
	try{
		driver.findElement(By.xpath("//a[@class='add']")).click();
		report.updateTestLog("My List", "Under My List added products has been clicked Add To Trolley Button", Status.PASS);
	}
	catch(Exception e){
		e.printStackTrace();
	}
}

public void openList(){
	JavascriptExecutor jse = (JavascriptExecutor)driver;
	jse.executeScript("window.scrollBy(0,400)",home_ui.openlist_lnk);
	Wait.until(ExpectedConditions.elementToBeClickable(By.xpath(home_ui.openlist_lnk)));
	helper.clickLink(driver, home_ui.openlist_loc,home_ui.openlist_lnk);
	report.updateTestLog("Home Page", "List is opened ", Status.PASS);
}

//to select Book Slot button IN Good News Page
	/*
	 * 
	 */
	public void clickbookSLotInGoodnewsPage(){
		Wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(home_ui.bookslot_btn)));
		helper.clickLink(driver, home_ui.bookslot_loc,home_ui.bookslot_btn);
	}
	
	
	//To get back page list for added product should displayed in Mini trolley
		/*
		 * 
		 */
		

			public void miniTrolleyToCheckAddedProduct(){
				//Wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(home_ui.minitrolley_img)));
				String qty_1 = dataTable.getData("General_Data", "PLP_Qty");
				String qty= helper.getText(driver, home_ui.minitrolley_loc, home_ui.minitrolley_img);
				//helper.clickLink(driver, home_ui.minitrolley_img, home_ui.minitrolley_img);
				if(qty.equals(qty_1))
					report.updateTestLog("Added product in trolley", "Added products is present in the trolley", Status.PASS);
				else
					report.updateTestLog("Added product in trolley", "Added products is not present in the trolley", Status.FAIL);
				
			}
		

public void lazyLoad(){
		
			try{
				JavascriptExecutor jse=(JavascriptExecutor)driver;
				jse.executeScript("scroll(0,2500)");
				helper.checkPageReadyState(5000);
				
				if((driver.findElements(By.xpath("(//div[@class='products-grid'])[2]"))).size()>0){
					report.updateTestLog("LazyLoad", "After Scroll Down Lazy Load products have present", Status.PASS);
					report.updateTestLog("LazyLoad", "After Scroll Down Lazy Load products have present", Status.SCREENSHOT);
						}
				else
	 				report.updateTestLog("LazyLoad", "After Scroll Down Lazy Load products have present", Status.FAIL);
						}
						catch(Exception e){
							e.printStackTrace();
							
						}
			

}
public void timedelay() throws InterruptedException{
	WebDriverWait wait = new WebDriverWait(driver,30);
    boolean pageLoaded =false;
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		do{
			if((executor.executeScript("return document.readyState").equals("complete")))
			{
				System.out.println("Page completely loaded successfully");
				pageLoaded=true;

			} else{
				Thread.sleep(1000);
			}
		}
		while(pageLoaded==false);
}





public void checkoutButtonValidationOFF()
{
	try {
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if(driver.findElements(By.xpath("//div[contains(@class,'mini-trolley-checkout')]//button")).size() >0){
			String va = driver.findElement(By.xpath("//div[contains(@class,'mini-trolley-checkout')]//button")).getAttribute("class");
			System.out.println(va);
			if(va.contains("disabled")){
				report.updateTestLog("Chekout Button", "Checkout Button is disabled", Status.PASS);
				report.updateTestLog("Chekout Button", "Screenshot Chectout Button", Status.SCREENSHOT);
				
			}else{
				report.updateTestLog("Chekout Button", "Checkout Button is enabled", Status.FAIL);	
			}
		}
		
}
	catch(SeleniumException e)
	{e.printStackTrace();
	report.updateTestLog("Chekout Button", "Checkout Button is enabled", Status.FAIL);	}
}

public void checkoutButtonValidationON()
{
	try {
		helper.checkPageReadyState(3000);
		if(driver.findElements(By.xpath("//div[contains(@class,'mini-trolley-checkout')]//button")).size() >0){
			String va = driver.findElement(By.xpath("//div[contains(@class,'mini-trolley-checkout')]//button")).getAttribute("class");
			System.out.println(va);
			if(va.contains("disabled")){
				report.updateTestLog("Chekout Button", "Checkout Button is disabled", Status.FAIL);					
				
			}else{
				report.updateTestLog("Chekout Button", "Checkout Button is enabled", Status.PASS);
				report.updateTestLog("Chekout Button", "Screenshot Chectout Button", Status.SCREENSHOT);
			}
		}
		
	}
	catch(SeleniumException e)
	{e.printStackTrace();
	report.updateTestLog("Chekout Button", "Checkout Button is disabled", Status.FAIL);	} 
}

public void getSelectedSlot()
{
	try 
	{
		if(helper.isElementPresent(driver,home_ui.lnkSelectedSlot_Loc, home_ui.lnkSelectedSlot))
		{
			String ss = helper.getText(driver,home_ui.lnkSelectedSlot_Loc, home_ui.lnkSelectedSlot);
			//SelectSlotPage.varSlotDetails = ss;
			report.updateTestLog("Selected Slot", "The Slot Selected :"+ss, Status.DONE);
		}
		else
			report.updateTestLog("Selected Slot", "The slot link is missing", Status.WARNING);
	}
	catch(SeleniumException e)
	{e.printStackTrace();
	report.updateTestLog("Selected Slot", "The slot link is missing"+e.toString(), Status.FAIL);
	}
}



public void selectInspiration()
{
	try
	{
		if(helper.isElementPresent(driver, home_ui.lnkInspiration_loc,home_ui.lnkInspiration))
		{
			helper.clickLink(driver, home_ui.lnkInspiration_loc,home_ui.lnkInspiration);
			helper.clickLink(driver, "xpath", "(//div[@class='waitroseimage ']//a//img[@class='cq-dd-image'])[1]");
			helper.isElementPresent(driver, home_ui.txtInpirationHdng_loc,home_ui.txtInpirationHdng);
			report.updateTestLog("Click on Inspiration tab", "Inspiration page is displayed", Status.PASS);
		}
		else
		{
			report.updateTestLog("Click on Inspiration tab", "Inspiration page is not displayed", Status.FAIL);
		}
	}
	catch(Exception e)
	{
		e.printStackTrace();
		report.updateTestLog("Click on Inspiration tab", "Inspiration  page is not displayed"+e.toString(), Status.FAIL);
	}
}

public void selectRecipes()
{
	try
	{
		if(helper.isElementPresent(driver, home_ui.lnkRecipes_loc, home_ui.lnkRecipes))////a[contains(text(),'Recipes')]
		{
			//selecting Recipes
			helper.clickLink(driver, home_ui.lnkRecipes_loc, home_ui.lnkRecipes);
			helper.clickLink(driver, "xpath", "(//div[@class='waitroseimage ']//a//img[@class='cq-dd-image'])[1]");
			helper.isElementPresent(driver, home_ui.txtRecipesHdng_loc,home_ui.txtRecipesHdng);
			report.updateTestLog("Click on Recipes tab","Recipes page is displayed", Status.PASS);

		}
		else
		{
			report.updateTestLog("Click on Recipes tab", "Recipes page is not displayed", Status.FAIL);
		}

	}
	catch(Exception e)
	{
		e.printStackTrace();
		report.updateTestLog("Click on Recipes tab", "Recipes page is not displayed"+e.toString(), Status.FAIL);
	}
}

public void selectTV()
{
	try
	{
		if(helper.isElementPresent(driver, home_ui.TVlnk_loc,home_ui.TVlnk))
		{
			helper.clickLink(driver, home_ui.TVlnk_loc,home_ui.TVlnk);
			helper.clickLink(driver, "xpath", "(//div[@class='waitroseimage ']//a//img[@class='cq-dd-image'])[1]");
			helper.isElementPresent(driver, home_ui.TVhome_loc,home_ui.TVhome);
			report.updateTestLog("Click on TV tab", "TV is displayed", Status.PASS);

		}
		else
		{
			report.updateTestLog("Click on TV tab", "TV is  not displayed", Status.FAIL);
		}
	}
	catch(Exception e)
	{
		e.printStackTrace();
		report.updateTestLog("Click on TV tab", "TV is  not displayed"+e.toString(), Status.FAIL);
	}
}

public void selectFloristLink()
{
	try
	{
		if(helper.isElementPresent(driver, "xpath","(//a[contains(text(),'Florist')])[1]"))
		{
			helper.clickLink(driver, "xpath","(//a[contains(text(),'Florist')])[1]");
			 ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
			    driver.switchTo().window(tabs2.get(1));
			    helper.isElementPresent(driver, home_ui.txtFlowers_loc,"//div[@class='wordMark']");
			    //validateFooterHome_sites();
			    driver.close();
			    driver.switchTo().window(tabs2.get(0));
			
			
			report.updateTestLog("Click on Flowers tab", "Flowers page is displayed", Status.PASS);
		}
		else
		{
			report.updateTestLog("Click on Flowers tab", "Flowers page is not displayed", Status.FAIL);
		}
		
	}
	catch(Exception e)
	{
		e.printStackTrace();
		report.updateTestLog("Click on Flowers tab", "Flowers page is not displayed"+e.toString(), Status.FAIL);
}
}

public void selectGardenLink()
{
	try
	{
		if(helper.isElementPresent(driver, "xpath","(//a[contains(text(),'Garden')])[1]"))
		{
			helper.clickLink(driver, "xpath","(//a[contains(text(),'Garden')])[1]");
			 ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
			    driver.switchTo().window(tabs2.get(1));
			    //helper.checkPageReadyState(1500);
			    //helper.clickLink(driver, "xpath", "//div[@id='monetate_lightbox_content']");
			    helper.isElementPresent(driver, "id","site-logo");
			    //validateFooterHome_sites();
			    driver.close();
			    driver.switchTo().window(tabs2.get(0));
			
			
			report.updateTestLog("Click on Garden tab", "Garden page is displayed", Status.PASS);
		}
		else
		{
			report.updateTestLog("Click on Garden tab", "Garden page is not displayed", Status.FAIL);
		}
		
	}
	catch(Exception e)
	{
		e.printStackTrace();
		report.updateTestLog("Click on Garden tab", "Garden page is not displayed"+e.toString(), Status.FAIL);
}
}

public void selectCellarLink()
{
	try
	{
		if(helper.isElementPresent(driver, "xpath","(//a[contains(text(),'Cellar')])[1]"))
		{
			helper.clickLink(driver, "xpath","(//a[contains(text(),'Cellar')])[1]");
			 ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
			    driver.switchTo().window(tabs2.get(1));
			    helper.isElementPresent(driver, "xpath","//div[@class='wordMark']");
			    //validateFooterHome_sites();
			    driver.close();
			    driver.switchTo().window(tabs2.get(0));
			
			
			report.updateTestLog("Click on Cellar tab", "Cellar page is displayed", Status.PASS);
		}
		else
		{
			report.updateTestLog("Click on Cellar tab", "Cellar page is not displayed", Status.FAIL);
		}
		
	}
	catch(Exception e)
	{
		e.printStackTrace();
		report.updateTestLog("Click on Garden tab", "Cellar page is not displayed"+e.toString(), Status.FAIL);
}
}

public void selectGiftsLink()
{
	try
	{
		if(helper.isElementPresent(driver, "xpath","(//a[contains(text(),'Gifts')])[1]"))
		{
			helper.clickLink(driver, "xpath","(//a[contains(text(),'Gifts')])[1]");
			 ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
			    driver.switchTo().window(tabs2.get(1));
			    helper.isElementPresent(driver, home_ui.txtGifts_loc,"//div[@class='wordMark']");
			    //validateFooterHome_sites();
			    driver.close();
			    driver.switchTo().window(tabs2.get(0));
			
			
			report.updateTestLog("Click on Gifts tab", "Gifts page is displayed", Status.PASS);
		}
		else
		{
			report.updateTestLog("Click on Gifts tab", "Gifts page is not displayed", Status.FAIL);
		}
		
	}
	catch(Exception e)
	{
		e.printStackTrace();
		report.updateTestLog("Click on Gifts tab", "Gifts page is not displayed"+e.toString(), Status.FAIL);
}
}


public void validateFooterHome_sites(){
	//JavascriptExecutor jse = (JavascriptExecutor)driver;
	//jse.executeScript("window.scrollBy(0,2450)");
	try{
		
		String linkfoot;
		if(helper.isElementPresent(driver, home_ui.footer_loc, home_ui.footer))
		{
			
			report.updateTestLog("Footer in page", "Footer is present", Status.PASS);
		}
		else
			report.updateTestLog("Footer in page", "Footer is NOT present", Status.SCREENSHOT);

		if(helper.isElementPresent(driver, home_ui.lnkfootercontacttxt_loc, home_ui.lnkfootercontacttxt)){
			report.updateTestLog("Contact Links in footer", " footer contact details is displayed", Status.PASS);
			report.updateTestLog("Contact Links in footer", "footer contact details is not displayed", Status.SCREENSHOT);
		}
		else report.updateTestLog("Contact Links in footer", "Links in footer is displayed", Status.FAIL);


	}catch(Exception e){
		e.printStackTrace();
		report.updateTestLog("Links in footer", "Links in footer is not displayed"+e.toString(), Status.FAIL);
	}
	
}

public void clickMylistUnderMyaccount(){
	Wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//a[contains(text(),'My Lists')])[1]")));
	helper.clickLink(driver, "xpath","(//a[contains(text(),'My Lists')])[1]");
	report.updateTestLog("Home Page", " List Link is selected  ", Status.PASS);
}

//To check this function user should be registered and address should be saved.
public void checkBookSlotButtonAvailable(){// To check this function user should be registered and address should be saved.
	
	if(helper.isElementPresent(driver, "xpath", "//button[contains(text(),'Book a slot')]"))
		report.updateTestLog("Book slot button", "Book a slot button is present in the Home page", Status.PASS);
	else
		report.updateTestLog("Book slot button", "Book a slot button is not present in the Home page", Status.FAIL);

}

public void validateFooterLinks(){
	
	if(helper.isElementPresent(driver, "xpath", "//a[contains(text(),'Waitrose Wines, Flowers & Hampers')]")){
		helper.clickLink(driver, "xpath", "//a[contains(text(),'Waitrose Wines, Flowers & Hampers')]");
		ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
	    driver.switchTo().window(tabs2.get(1));
	    helper.isElementPresent(driver, home_ui.txtFlowers_loc,"//div[@class='wordMark']");
	    driver.close();
	    driver.switchTo().window(tabs2.get(0));
	    report.updateTestLog("Footer links", "Footer link for Florist is working",Status.PASS);
	}
	else
		report.updateTestLog("Footer links", "Footer link for Florist is not working",Status.FAIL);
	if(helper.isElementPresent(driver, "xpath", "//a[contains(text(),'Waitrose Garden')]")){
		helper.clickLink(driver, "xpath", "//a[contains(text(),'Waitrose Garden')]");
		ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
	    driver.switchTo().window(tabs2.get(1));
	    try{
	    	helper.checkPageReadyState(1500);
		    helper.clickLink(driver, "xpath", "//div[@id='monetate_lightbox_content']");
	    }
	    catch(Exception e)
	    {
	    	System.out.println("overlay is not present");
	    }
	    helper.isElementPresent(driver, "id","site-logo");
	    driver.close();
	    driver.switchTo().window(tabs2.get(0));
	    report.updateTestLog("Footer links", "Footer link for Garden is working",Status.PASS);
	}
	else
		report.updateTestLog("Footer links", "Footer link for Garden is not working",Status.FAIL);
}

public void validateFooterHome(){
	//JavascriptExecutor jse = (JavascriptExecutor)driver;
	//jse.executeScript("window.scrollBy(0,2450)");
	try{
		
		String linkfoot;
		if(helper.isElementPresent(driver, home_ui.linkCompanyInfo_loc, home_ui.linkCompanyInfo))
		{
			helper.clickLink(driver, home_ui.linkCompanyInfo_loc, home_ui.linkCompanyInfo);
			report.updateTestLog("CompanyInformation", "Company information link clicked", Status.PASS);
		}
		else
			report.updateTestLog("CompanyInformation", "Company information link NOT clicked", Status.SCREENSHOT);


		for(int i = 1; i<=4; i++){
			linkfoot = "(" +home_ui.lnkfooter +")" +"[" +i +"]";
			if(helper.isElementPresent(driver, home_ui.lnkfooter_loc, home_ui.lnkfooter))
			{
				report.updateTestLog("Links in footer", "Links in footer is displayed", Status.PASS);
				report.updateTestLog("Links in footer", "Links in footer is displayed", Status.SCREENSHOT);
			}
			else report.updateTestLog("Links in footer", "Links in footer is displayed", Status.FAIL);
		}
		if(helper.isElementPresent(driver, home_ui.lnkfootercontact_loc, home_ui.lnkfootercontact)){
			report.updateTestLog("Contact Links in footer", " footer contract details is displayed", Status.PASS);
			report.updateTestLog("Contact Links in footer", "footer contract details is  displayed", Status.SCREENSHOT);
		}
		else report.updateTestLog("Contact Links in footer", "Links in footer is displayed", Status.FAIL);


	}catch(Exception e){
		e.printStackTrace();
		report.updateTestLog("Links in footer", "Links in footer is not displayed"+e.toString(), Status.FAIL);
	}
	
}

public void validateHeaderAfterLogin(){
	try{

		if(helper.isElementPresent(driver,"xpath", "//a[text()='Waitrose']"))
			report.updateTestLog("Home page Waitrose image", "Home page Waitrose image is displayed", Status.PASS);
		else report.updateTestLog("Home page Waitrose image", "Home page Waitrose image is displayed", Status.FAIL);

		if(helper.isElementPresent(driver, "xpath", "(//a[contains(text(),'Florist')])[1]"))
			report.updateTestLog("Flowers", "Flowers is displayed", Status.PASS);
		else report.updateTestLog("Flowers", "Flowers is not displayed", Status.FAIL);
		if(helper.isElementPresent(driver, "xpath", "(//a[contains(text(),'Garden')])[1]"))
			report.updateTestLog("Garden", "Garden link is displayed", Status.PASS);
		else report.updateTestLog("Garden", "Garden link is displayed", Status.FAIL);
		if(helper.isElementPresent(driver, "xpath", "(//a[contains(text(),'Cellar')])[1]"))
			report.updateTestLog("Cellar", "Cellar is displayed", Status.PASS);
		else report.updateTestLog("Cellar", "Cellar is displayed", Status.FAIL);
		
		if(helper.isElementPresent(driver, home_ui.linkmyWaitroiseCard_loc, home_ui.linkmyWaitroiseCard))
			report.updateTestLog("My waitrose card", "My waitrose card is displayed", Status.PASS);
		else report.updateTestLog("My waitrose card", "My waitrose card is displayed", Status.FAIL);;
		if(helper.isElementPresent(driver, home_ui.myaccount_loc, home_ui.myaccount_lnk))
			report.updateTestLog("My account", "My account is displayed", Status.PASS);
		else report.updateTestLog("My account", "My account is displayed", Status.FAIL);
		if(helper.isElementPresent(driver, home_ui.findbranch_loc, home_ui.findbranch_lnk))
			report.updateTestLog("Find branch", "Find a branch is displayed", Status.PASS);
		else report.updateTestLog("Find branch", "Find a branch is displayed", Status.FAIL);

	}catch(Exception e){
		e.printStackTrace();
		report.updateTestLog("Header", "Header is not displayed"+e.toString(), Status.FAIL);
	}
}

}