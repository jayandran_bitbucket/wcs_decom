package functionallibraries.wcs;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.cognizant.framework.Status;

import functionallibraries.aws.ReusableComponents;
import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;

public class Func_Login extends ReusableLibrary{
	
	public Func_Login(ScriptHelper scriptHelper){
		super(scriptHelper);
	}
	ReusableComponents helper = new ReusableComponents(scriptHelper);
	private String Pwd;
	
	public void setChromeExtension(){
		Set<String> winSet = driver.getWindowHandles();
    	List<String> winList = new ArrayList<String>(winSet);
    	String newTab = winList.get(winList.size() - 1);
    	try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	driver.switchTo().window(newTab);
    	if(driver.getCurrentUrl().contains("chrome-extension")){
    		System.out.println("                      Chrome Extension is used! ");
			System.out.println(driver.getCurrentUrl());
			driver.findElement(By.id("login")).sendKeys("devdelen");
	    	driver.findElement(By.id("password")).sendKeys("monday876");
	    	driver.findElement(By.id("retry")).clear();
	    	driver.findElement(By.id("retry")).sendKeys("100000000");
	    	driver.findElement(By.id("save")).click();
	    	try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    	//System.out.println("                      Closing the tab");
	    	driver.close();
	    	winSet = driver.getWindowHandles();
	    	winList = new ArrayList<String>(winSet);
	    	newTab = winList.get(winList.size() - 1);
	    	driver.switchTo().window(newTab);

    	}else{
    		System.out.println("No Extension loaded");
    	}
    	
					
	}

	public void login(){
		clickStartShoppingBtn();
		clickSignInBtn();
		enterLoginDetails();
	}
	public void enterLoginDetails(){
		String userName = dataTable.getData("General_Data","Username");
		String password = dataTable.getData("General_Data","Password");
		driver.findElement(By.id("logon-email")).sendKeys(userName);
		System.out.println("Entered Login ID : "+userName);
		driver.findElement(By.xpath("//input[@value='Continue']")).click();
		driver.findElement(By.id("logon-password")).sendKeys(password);
		System.out.println("Entered Password");
		driver.findElement(By.xpath("//input[@value='Sign in']")).click();
		System.out.println("Login Successfull");
		report.updateTestLog("Login", "Login Successfull " +
				"Username = " + userName, Status.PASS);
		/*String userName = dataTable.getData("General_Data","Username");
		String password = dataTable.getData("General_Data","Password");
		String Appurl	= properties.getProperty("ApplicationUrl");
		String url = Appurl+"/webapp/wcs/stores/servlet/CustomerLogon?logonId="+userName+"&logonPassword="+password+"&_method=put";
		driver.get(url);
		driver.get(Appurl);
		driver.navigate().refresh();
		report.updateTestLog("Login", "Login Successfull " +
				"Username = " + userName, Status.PASS);
		Assert.assertTrue(true, "Login Successfull");*/
	}
	
	public String getWCSCookies(){
		//helper.checkPageReadyState(2000);
		String rawcookie = driver.manage().getCookieNamed("CookieWCS").toString();
		int point = rawcookie.indexOf("=");
		String rawcookie1 = rawcookie.substring(point+1);
		point = rawcookie1.indexOf(";");
		String cookie = rawcookie1.substring(0, point);
		System.out.println(cookie);
		/*Cookie wcscookie = driver.manage().getCookieNamed("CookieWCS");
		driver.get("https://qa.ecom.waitrose.com/pimkj07l4f945vjqk");
		driver.manage().addCookie(wcscookie);*/
		return cookie;
	}
	
	public void getAWSUrl(){
		String customizedurl = driver.getCurrentUrl();
		//customizedurl.replaceAll("https://qa.wtr-ecom.johnlewis.co.uk", "https://qa.ecom.waitrose.com");
		String url = customizedurl.replace("https://qa.wtr-ecom.johnlewis.co.uk", "https://qa.ecom.waitrose.com");
		System.out.println(url);
		driver.get(url);
		
	}
	

	public void clickSignInBtn(){
		System.out.println("--------- Function Started ----------");
		System.out.println();
		driver.findElement(By.xpath("//button[text()='Sign in/Register']")).click();
		/*List<WebElement> btns = driver.findElements(By.xpath("//button[text()='Sign in/Register']"));
		int i = 1;
		for(WebElement w:btns){
			if(w.isDisplayed()){
				w.click();
				System.out.println("Sign In Button is selected");
				break;		
			}

			if(i==btns.size()){
				System.out.println("No Sign Button is displayed");
			}
		}*/
		System.out.println();
		System.out.println("--------- Function Ended ----------");
	}
	

	public void clickStartShoppingBtn(){
		System.out.println("--------- Function Started ----------");
		System.out.println();
		//	driver.findElement(By.xpath("//a[text()='Start shopping']")).click();
		List<WebElement> btns = driver.findElements(By.xpath("//a[text()='Start shopping']"));
		int i = 1;
		for(WebElement w:btns){
			if(w.isDisplayed()){
				w.click();
				System.out.println("Start Shopping Button is selected");
				break;		
			}
			if(i==btns.size()){
				System.out.println("No Start Shopping Overlay");
			}
		}
		System.out.println();
		System.out.println("--------- Function Ended ----------");

	}
	
	public void loadWCSCookie(String cookie){
		helper.typeinTextbox(driver, "id", "cookieVal", cookie);
		helper.clickLink(driver, "xpath", "//input[@value='Create Cookie']");
		helper.checkPageReadyState(2000);
	}
	
	
	public void register(){
		//clickStartShoppingBtn();
		clickSignInBtn();
		enterRegistrationDetails();
	}
	
	public void enterRegistrationDetails(){
		String userName = dataTable.getData("General_Data","Username");
		String password = dataTable.getData("General_Data","Password");
		String EmailID;
		
		// Auto generate mail 
		Date TimeStamp = new Date( );
		SimpleDateFormat FormatObj = new SimpleDateFormat ("yyMMddhhmmss");
		String time = FormatObj.format(TimeStamp);
		System.out.println(time);
		
		EmailID = time + userName;
		
		System.out.println(EmailID);
			
		driver.findElement(By.id("logon-email")).sendKeys(EmailID);
		System.out.println("Entered Login ID : "+userName);
		driver.findElement(By.xpath("//input[@value='Continue']")).click();
		driver.findElement(By.id("logon-password-choose")).sendKeys(password);
		System.out.println("Entered Password");
		chooseRegisterOptions();
		driver.findElement(By.xpath("//input[@value='Create my account']")).click();
		System.out.println("Registeration Successfull");
		report.updateTestLog("Register", "Registeration Successfull " +	"Username = " + EmailID, Status.PASS);
	
		// Login Id is added to the datatable
		dataTable.putData("General_Data","Username", EmailID);
		
	}
	
	public void forgotPasswordErrormsg(){
		if(driver.findElement(By.xpath(("(//h3[@class='error-message'])[3]"))).isDisplayed()){
			report.updateTestLog("error ","Error msg is displayed", Status.PASS);
		}else
			report.updateTestLog("error ","Error msg is not displayed", Status.FAIL);
		
	}
	
	public void clickContinueincreateNewPasswordPage(){
		
		if(driver.findElement(By.xpath("//input[@class='button primary-cta button-continue js-reset-password-request']")).isDisplayed()){
			driver.findElement(By.xpath("//input[@class='button primary-cta button-continue js-reset-password-request']")).click();
		report.updateTestLog("Continue ","Continue button is clicked", Status.PASS);
		}else 
		report.updateTestLog("Continue ","Continue button is not clicked", Status.FAIL);
	}
	
	
	public void clickForgotpasswordlink(){
		if(driver.findElement(By.xpath("//a[contains(text(),'Forgotten your password?')]")).isDisplayed()){
			driver.findElement(By.xpath("//a[contains(text(),'Forgotten your password?')]")).click();
		report.updateTestLog("Forgotpassword ","Forgot password is clicked", Status.PASS);
		}else 
		report.updateTestLog("Forgotpassword ","Forgot password is not clicked", Status.FAIL);
	}
	
	public void logInforgotPassword(){
		String userName = dataTable.getData("General_Data","Username");
		String password = dataTable.getData("General_Data","Password");
		String Pwd;
		
		// Auto generate mail 
		Date TimeStamp = new Date( );
		SimpleDateFormat FormatObj = new SimpleDateFormat ("yyMMddhhmmss");
		String time = FormatObj.format(TimeStamp);
		System.out.println(time);
		
		Pwd = time + password;
		System.out.println(Pwd);
			
		driver.findElement(By.id("logon-email")).sendKeys(userName);
		System.out.println("Entered Login ID ");
		driver.findElement(By.xpath("//input[@class='button primary-cta button-continue']")).click();
		driver.findElement(By.id("logon-password")).sendKeys(Pwd);
		System.out.println("Entered Password : "+Pwd);
		//chooseRegisterOptions();
		driver.findElement(By.xpath("//input[@value='Sign in']")).click();
		System.out.println("Registeration Successfull");
		report.updateTestLog("Register", "Registeration Successfull " +
				"Password = " + Pwd, Status.PASS);
	
		// Login Id is added to the datatable
		dataTable.putData("General_Data","Password", Pwd);
	}
	
	public  void createNewPassword(){
		String password = dataTable.getData("General_Data", "Password");
		String pd;
		
		Date month = new Date();
		//DateFormat object = new DateFormat ("yyMMdd");
		SimpleDateFormat FormatObj = new SimpleDateFormat ("yy");
		String pass = FormatObj.format(month);
		System.out.println(pass);
		pd = pass + password;
		driver.findElement(By.xpath("//input[@id='password']")).sendKeys(pd);
		driver.findElement(By.xpath("//input[@id='password-confirm']")).sendKeys(pd);
//		helper.typeinTextbox(driver, "xpath", "//input[@id='password']", "pd");
//		helper.typeinTextbox(driver, "xpath", "//input[@id='password-confirm']", "pd");
		helper.clickLink(driver, "xpath", "//input[@value='Confirm new password']");
		helper.isElementPresent(driver, "xpath", "//h1[contains(text(),'Password successfully updated')]");		
		
	}
	
	public void clickContinuelink(){
		if(helper.clickLink(driver, "xpath", "//button[contains(text(),'Continue shopping')]")){
			helper.isElementPresent(driver, "xpath", "(//div/a[contains(text(),'Waitrose')])[1]");
		report.updateTestLog("clickContinuelink", "clickContinuelink is clicked", Status.PASS);
	}else
		report.updateTestLog("clickContinuelink", "clickContinuelink is clicked", Status.FAIL);
		
	}

public void chooseRegisterOptions(){
	String rwaitrose = dataTable.getData("General_Data", "Reg Waitrose Opt");
	String rjohnlewis = dataTable.getData("General_Data", "Reg Johnlewis Opt");
	String rfinance = dataTable.getData("General_Data", "Reg Finance Opt");
	
	if(rwaitrose.equalsIgnoreCase("Yes")){
		helper.clickObject(driver, "xpath", "//label[@for='logon-marketingPrefWaitrose']");
		report.updateTestLog("Register", "Waitrose preference is choosen", Status.PASS);
	}
	if(rjohnlewis.equalsIgnoreCase("Yes")){
		helper.clickLink(driver, "xpath", "//label[@for='logon-marketingPrefJohnLewis']");
		report.updateTestLog("Register", "John lewis preference is choosen", Status.PASS);
	}
	if(rfinance.equalsIgnoreCase("Yes")){
		helper.clickLink(driver, "xpath", "//label[@for='logon-marketingPrefFinance']");
		report.updateTestLog("Register", "Finance preference is choosen", Status.PASS);
	}
}

}