package functionallibraries.wcs;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.FrameworkException;
import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import functionallibraries.aws.ReusableComponents;
import pageobjects.wcs.DeliveryAddress;
import pageobjects.wcs.OrderReview;
import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;


/**
 * Functional Library class
 * @author Cognizant
 */
public class Func_DeliveryAddressDetails extends ReusableLibrary
{
	/**
	 * Constructor to initialize the functional library
	 * @param scriptHelper The {@link ScriptHelper} object passed from the {@link DriverScript}
	 */
	public Func_DeliveryAddressDetails(ScriptHelper scriptHelper){
		super(scriptHelper);
	}
	
	WebDriverWait Wait = new WebDriverWait(driver,20);
	ReusableComponents helper = new ReusableComponents(scriptHelper);
	DeliveryAddress delivery = new DeliveryAddress();
	
	public void enterDeliveryAddress(){
		
		try{
		Date myDate = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd:HH-mm-ss");
		String myDateString = sdf.format(myDate);
		String nickname = dataTable.getData("DeliveryDetails_Data", "NickName");
		nickname = nickname+myDateString;
		System.out.println(nickname);
		String title = dataTable.getData("DeliveryDetails_Data", "Title");
		String firstname = dataTable.getData("DeliveryDetails_Data", "FirstName");
		String lastname = dataTable.getData("DeliveryDetails_Data", "LastName");
		String phonenumber = dataTable.getData("DeliveryDetails_Data", "PhoneNumber");
		String postcode = dataTable.getData("DeliveryDetails_Data", "Postcode");
		Wait.until(ExpectedConditions.elementToBeClickable(By.xpath(delivery.findMyAddress_btn)));
		//Wait.until(ExpectedConditions.presenceOfElementLocated(By.id(delivery.nickName_loc)));
		helper.typeinTextbox(driver, delivery.nickName_loc, delivery.nickName_txt, nickname);
		helper.typeinTextbox(driver, delivery.title_loc, delivery.title_txt, title);
		helper.typeinTextbox(driver, delivery.firstName_loc, delivery.firstName_txt, firstname);
		helper.typeinTextbox(driver, delivery.lastName_loc, delivery.lastName_txt, lastname);
		if(driver.findElement(By.id("zipCode")).isEnabled()){
			helper.typeinTextbox(driver, delivery.postcode_loc, delivery.postcode_txt, postcode);
		}
		helper.clickLink(driver, delivery.findMyAddress_loc, delivery.findMyAddress_btn);
		List<WebElement> addresslinks = helper.findWebElements(driver, delivery.postcodeaddress_loc, delivery.postcodeaddress_link);
		for(WebElement w:addresslinks){
			w.click();
			break;
		}
		helper.typeinTextbox(driver, delivery.phonenumber_loc, delivery.phonenumber_txt, phonenumber);
		helper.clickObject(driver, delivery.saveandcontinue_loc, delivery.saveandcontinue_btn);
		report.updateTestLog ("Order Review Page", " Continue to payment button is clicked ", Status.DONE);
		}catch(Exception e){
			e.printStackTrace();
			System.out.println(e.toString());
			report.updateTestLog ("Order Review Page", " Not able to add Delivery Address "+e.toString(), Status.WARNING);
		}
	}
	
}