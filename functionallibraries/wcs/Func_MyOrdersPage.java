package functionallibraries.wcs;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.FrameworkException;
import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import functionallibraries.aws.ReusableComponents;
import pageobjects.wcs.MyOrders;
import pageobjects.wcs.OrderReview;
import pageobjects.wcs.Trolley;
import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;


/**
 * Functional Library class
 * @author Cognizant
 */
public class Func_MyOrdersPage extends ReusableLibrary
{
	/**
	 * Constructor to initialize the functional library
	 * @param scriptHelper The {@link ScriptHelper} object passed from the {@link DriverScript}
	 */
	public Func_MyOrdersPage(ScriptHelper scriptHelper){
		super(scriptHelper);
	}
	
	WebDriverWait Wait = new WebDriverWait(driver,20);
	MyOrders myorder = new MyOrders();
	Trolley trolley = new Trolley();
	ReusableComponents helper = new ReusableComponents(scriptHelper);
	
	public void clickAmendOrderButtonForLatestOrder(){
		helper.checkPageReadyState(2000);
		List<WebElement> amendorder = helper.findWebElements(driver, myorder.amendorder_loc, myorder.amendorder_btn);
		int i =1;
		for(WebElement w:amendorder){
			if(w.isDisplayed()){
				w.click();
				Wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(myorder.amendordercontinue_btn)));
				helper.clickLink(driver, myorder.amendordercontinue_loc, myorder.amendordercontinue_btn);
				report.updateTestLog("Home Page", "Amend Order Button is selected for latest order ", Status.PASS);
				Wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[text()='My Trolley']")));
				break;
			}else if(i == amendorder.size()){
				report.updateTestLog("Home Page", "Amend Order Button is not displayed ", Status.FAIL);
				break;
			}
		}
	}
	
	public void clickAmendOrderButtonForLatestOrder_DS(){
		helper.checkPageReadyState(2000);
		List<WebElement> amendorder = helper.findWebElements(driver, myorder.amendorder_loc, myorder.amendorder_btn);
		int i =1;
		for(WebElement w:amendorder){
			if(w.isDisplayed()){
				w.click();
				//Wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(myorder.amendordercontinue_btn)));
				//helper.clickLink(driver, myorder.amendordercontinue_loc, myorder.amendordercontinue_btn);
				report.updateTestLog("Home Page", "Amend Order Button is selected for latest order ", Status.PASS);
				
				break;
			}else if(i == amendorder.size()){
				report.updateTestLog("Home Page", "Amend Order Button is not displayed ", Status.FAIL);
				break;
			}
		}
	}
	
	public void verifyTrolleyPrice(double price){
		if(helper.isElementPresent(driver,"xpath", "//td[contains(text(),'"+price+"')]"))
			report.updateTestLog("My Orders Page", "Total amount is displayed correctly", Status.PASS);
		else report.updateTestLog("My Orders Page", "Total amount is not displayed correctly", Status.FAIL);
	}
	
	public void verifyTrolleySavings(String price){
		String orderid = dataTable.getData("OrderDetails", "OrderNumber");
		if(helper.isElementPresent(driver,"xpath", "//a[contains(text(),'"+orderid+"')]"))
			helper.clickLink(driver,"xpath", "//a[contains(text(),'"+orderid+"')]");
		report.updateTestLog("My Orders Page", "Order link is clicked", Status.DONE);
		String pr;
		if(helper.isElementPresent(driver,"xpath", "(//p[@class='savings'])[2]")){
			
			pr = helper.getText(driver,"xpath", "(//p[@class='savings'])[2]").substring(2, 7);
			if(pr.contains(price))
				report.updateTestLog("My Orders Page", "Total savings is displayed correctly", Status.PASS);
			else report.updateTestLog("My Orders Page", "Total amount is not displayed correctly", Status.FAIL); 
		}
	
	}
	
	public void addAllToPendingOrderInOrderPage(){
		helper.clickLink(driver, myorder.addtopendingorder_loc, myorder.addtopendingorder_lnk);
		Wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//a[contains(text(),'Continue')])[2]")));
		helper.clickLink(driver, "xpath","(//a[contains(text(),'Continue')])[2]");
		if(helper.isElementPresent(driver, trolley.amendorder_loc, trolley.amendorder_txt))
			report.updateTestLog("Trolley", "Products added to the pending order", Status.PASS);
		else report.updateTestLog("Trolley", "Products are not added to the pending order", Status.FAIL);
		
	}
	
	public void cancelLastestOrder(){
		String orderid = dataTable.getData("OrderDetails", "OrderNumber").trim().substring(0,7);
		System.out.println("//td[contains(@class,'"+orderid+"')]//span[contains(text(),'Cancel this order')]");
		if(helper.isElementPresent(driver, "xpath", "//td[contains(@class,'"+orderid+"')]//span[contains(text(),'Cancel this order')]"))
			helper.clickLink(driver, "xpath", "//td[contains(@class,'"+orderid+"')]//span[contains(text(),'Cancel this order')]");
		helper.clickLink(driver, myorder.cancelconfirm_loc, myorder.cancelconfirm_btn);
		if(helper.getText(driver, myorder.getorderstatus__loc, myorder.getorderstatus_txt).equalsIgnoreCase("Cancelled"))
			report.updateTestLog("Order Cancellation", "Order: "+orderid+ " is cancelled successfully", Status.PASS);
	}
	public void verifyLastedOrderIsPresent(){
		String orderid = dataTable.getData("OrderDetails", "OrderNumber").trim().substring(0,7);
		if(helper.isElementPresent(driver, "xpath", "//td[contains(@class,'"+orderid+"')]"))
			report.updateTestLog("Order Cancellation", "Order: "+orderid+ " is present", Status.PASS);
		else report.updateTestLog("Order Cancellation", "Order: "+orderid+ " is not present", Status.FAIL);
	}
	
	public void shopFromThisOrder(){
		helper.clickLink(driver, myorder.shopfromthisorder_loc, myorder.shopfromthisorder_btn);
		if(helper.isElementPresent(driver, myorder.previousOrderHeader_loc, myorder.previousOrderHeader_txt))
		report.updateTestLog("Order Cancellation", "Shop from this order is clicked", Status.PASS);
		else report.updateTestLog("Order Cancellation", "Shop from this order is not clicked", Status.FAIL);
		
	}
	
	public void getOrderStatus(){
		
		String order = dataTable.getData("OrderDetails", "OrderNumber");
		if(helper.isElementPresent(driver, "xpath", "//tr[@class='open-order orderid-"+order+" active-order']//td[4]")){
			String orderstatus = helper.getText(driver, "xpath", "//tr[@class='open-order orderid-"+order+" active-order']//td[4]");
			dataTable.putData("OrderDetails","OrderStatus",orderstatus);
			report.updateTestLog("Order Status", "Order status for "+order+"is "+orderstatus, Status.PASS);}
		else
			report.updateTestLog("Order Status", "Order status for"+order+"is not dispalyed", Status.FAIL);
	}
	public void getCancelledOrderStatus()
	{
		String cancel_order = helper.getText(driver, "xpath", "//table[@class='table table-bordered table-striped table-hovered table-orders previous-orders']/tbody/tr[1]/td[1]/a");
		String cancel = cancel_order.trim().substring(16,23);
		dataTable.putData("OrderDetails", "Cancelled_Order",cancel);
		String status = helper.getText(driver, "xpath", "//table[@class='table table-bordered table-striped table-hovered table-orders previous-orders']//tbody//tr[1]/td[4]");
		dataTable.putData("OrderDetails", "Cancelled_status",status);
		report.updateTestLog("order status", "Order status of "+cancel+"is: "+status, Status.PASS);
	}
	
}