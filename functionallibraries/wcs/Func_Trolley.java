package functionallibraries.wcs;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.FrameworkException;
import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import functionallibraries.aws.ReusableComponents;
import pageobjects.aws.Header;
import pageobjects.wcs.HomePage;
import pageobjects.wcs.Trolley;
import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;


/**
 * Functional Library class
 * @author Cognizant
 */
public class Func_Trolley extends ReusableLibrary
{
	/**
	 * Constructor to initialize the functional library
	 * @param scriptHelper The {@link ScriptHelper} object passed from the {@link DriverScript}
	 */

	//	public Func_Trolley WcsTrolley =new Func_Trolley(scriptHelper) ;
	public Func_Trolley(ScriptHelper scriptHelper){
		super(scriptHelper);
	}
	Trolley trolley = new Trolley();
	ReusableComponents helper = new ReusableComponents(scriptHelper);
	WebDriverWait Wait = new WebDriverWait(driver,30);
	HomePage homepage = new HomePage();
	



	public void getTrolleyValuesBefore(){

		trolley.TrolleypriceBfr = getTrolleyPrice();
		trolley.TrolleyquantityBfr = getTrolleyQuantity();
	}

	public void getTrolleyValuesAfter(){
		trolley.TrolleypriceAfr = getTrolleyPrice();
		trolley.TrolleyquantityAfr = getTrolleyQuantity();
	}

	public void getWCSTrolleyvalue(){
		helper.checkPageReadyState(3000);
		trolley.WCSTrolleyprice = getTrolleyPrice();
		trolley.WCSTrolleyquantity = getTrolleyQuantity();
	}

	public void validateIncrementTrolleyValue(){
		if(trolley.TrolleypriceAfr > trolley.TrolleypriceBfr){
			report.updateTestLog(" Trolley Value ", " Before : "+trolley.TrolleypriceBfr, Status.DONE);
			report.updateTestLog(" Trolley Value ", " After : "+trolley.TrolleypriceAfr, Status.DONE);
			report.updateTestLog(" Trolley Validation - Increment ", " Trolley value is increased ", Status.PASS);
		}else{
			report.updateTestLog(" Trolley Value ", " Before : "+trolley.TrolleypriceBfr, Status.DONE);
			report.updateTestLog(" Trolley Value ", " After : "+trolley.TrolleypriceAfr, Status.DONE);
			report.updateTestLog(" Trolley Validation - Increment ", "Trolley value is not increased ", Status.FAIL);
		}
	}

	public void validateDecrementTrolleyValue(){
		if(trolley.TrolleypriceAfr < trolley.TrolleypriceBfr){
			report.updateTestLog(" Trolley Value ", " Before : "+trolley.TrolleypriceBfr, Status.DONE);
			report.updateTestLog(" Trolley Value ", " After : "+trolley.TrolleypriceAfr, Status.DONE);
			report.updateTestLog(" Trolley Validation - Decrement ", " Trolley value is decreased ", Status.PASS);
		}else{
			report.updateTestLog(" Trolley Value ", " Before : "+trolley.TrolleypriceBfr, Status.DONE);
			report.updateTestLog(" Trolley Value ", " After : "+trolley.TrolleypriceAfr, Status.DONE);
			report.updateTestLog(" Trolley Validation - Decrement ", " Trolley value is not decreased ", Status.FAIL);
		}
	}


	public double getTrolleyPrice(){
		helper.checkPageReadyState(2000);
		if(helper.findWebElements(driver, trolley.Trolleytotal_loc, trolley.Trolleytotal_txt).size() > 0)
			Wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(trolley.Trolleytotal_txt)));
		String trolleyprice = helper.getText(driver, trolley.Trolleytotal_loc, trolley.Trolleytotal_txt);
		int loc = trolleyprice.indexOf('�');
		double price = Double.parseDouble(trolleyprice.substring(loc+1).trim());
		System.out.println("Trolley Price : "+price);
		return price;
	}

	public int getTrolleyQuantity(){
		//helper.checkPageReadyState(2000);
		Wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(homepage.trolley_btn)));
		String trolleyquantity = helper.getText(driver, trolley.Trolleyquantity_loc, trolley.Trolleyquantity_txt);
		int loc = trolleyquantity.indexOf("i");
		int quantity = Integer.parseInt(trolleyquantity.substring(0,loc).trim());
		System.out.println("Trolley Quantity : "+quantity);
		return quantity;
	}

	public void validateSavingDisplayed(){
  String saving = helper.getText(driver, "xpath", "//li[contains(text(),'savings')]//span");
  if(!saving.contains("0.00"))
	  report.updateTestLog(" Trolley ", "Saving is displayed", Status.PASS);
  else  report.updateTestLog(" Trolley ", "Saving is not displayed", Status.FAIL);
	}

	public void performEmptyTrolley(){
		report.updateTestLog(" Trolley ", " Empty Trolley ", Status.DONE);
		report.updateTestLog(" Trolley Value ", " Before : "+getTrolleyPrice(), Status.DONE);
		helper.clickLink(driver, trolley.emptyTrolley_loc, trolley.emptyTrolley_lnk);
		List<WebElement> confirm = helper.findWebElements(driver,  trolley.confirmEmptyTrolley_loc, trolley.confirmEmptyTrolley_btn);
		for(WebElement c:confirm){
			if(c.isDisplayed()){
				c.click();
				report.updateTestLog(" Trolley ", " Confirmed trolley empty ", Status.DONE);
				break;
			}
		}
		report.updateTestLog(" Trolley Value ", " After : "+getTrolleyPrice(), Status.DONE);
	}

	public void verifyTrolleySync(){
		/*if(trolley.WCSTrolleyprice == 0.0 || trolley.WCSTrolleyquantity == 0){
			report.updateTestLog(" Trolley Sync Validaiton ", " WCS Trolley Price & Quantity is not udpated ", Status.FAIL);
			report.updateTestLog(" Trolley ", " WCS Trolley Price :  "+trolley.WCSTrolleyprice, Status.DONE);
			report.updateTestLog(" Trolley ", " WCS Trolley Quantity :  "+trolley.WCSTrolleyquantity, Status.DONE);
		}else if(Header.AwsTrolleyprice == 0.0 || Header.AwsTrolleyquantity == 0) {
			report.updateTestLog(" Trolley Sync Validaiton ", " AWS Trolley Price & Quantity is not udpated ", Status.FAIL);
			report.updateTestLog(" Trolley ", " AWS Trolley Price :  "+Header.AwsTrolleyprice, Status.DONE);
			report.updateTestLog(" Trolley ", " AWS Trolley Quantity :  "+Header.AwsTrolleyquantity, Status.DONE);
		}else{*/
		if(trolley.WCSTrolleyprice == Header.AwsTrolleyprice){
			report.updateTestLog(" Trolley Sync Validaiton ", " WCS Trolley Price & AWS Trolley Price are properly synced ", Status.PASS);
			report.updateTestLog(" Trolley ", " WCS Trolley Price :  "+trolley.WCSTrolleyprice, Status.DONE);
			report.updateTestLog(" Trolley ", " AWS Trolley Price :  "+Header.AwsTrolleyprice, Status.DONE);
		}else{
			report.updateTestLog(" Trolley Sync Validaiton ", " WCS Trolley Price & AWS Trolley Price are not synced ", Status.FAIL);
			report.updateTestLog(" Trolley ", " WCS Trolley Price :  "+trolley.WCSTrolleyprice, Status.DONE);
			report.updateTestLog(" Trolley ", " AWS Trolley Price :  "+Header.AwsTrolleyprice, Status.DONE);
		}

		if(trolley.WCSTrolleyquantity == Header.AwsTrolleyquantity){
			report.updateTestLog(" Trolley Sync Validaiton ", " WCS Trolley Quantity & AWS Trolley Quantity are properly synced ", Status.PASS);
			report.updateTestLog(" Trolley ", " WCS Trolley Quantity :  "+trolley.WCSTrolleyquantity, Status.DONE);
			report.updateTestLog(" Trolley ", " AWS Trolley Quantity :  "+Header.AwsTrolleyquantity, Status.DONE);
		}else{
			report.updateTestLog(" Trolley Sync Validaiton ", " WCS Trolley Quantity & AWS Trolley Quantity are not synced ", Status.FAIL);
			report.updateTestLog(" Trolley ", " WCS Trolley Quantity :  "+trolley.WCSTrolleyquantity, Status.DONE);
			report.updateTestLog(" Trolley ", " AWS Trolley Quantity :  "+Header.AwsTrolleyquantity, Status.DONE);
		}
		//}
	}

	public void checkTrolleyPriceRetainedAfterOrder(double Pricebef, double PriceAfter){

		String itemcount = helper.getText(driver,trolley.noofitems_loc , trolley.noofitems_txt).trim();
		if(!itemcount.equalsIgnoreCase("0 items") && Pricebef == PriceAfter)
			report.updateTestLog(" Trolley ", "Products are retained after placing shop in brach, we deliver order", Status.PASS);
		else report.updateTestLog(" Trolley ", "Products are retained after placing shop in brach, we deliver order", Status.FAIL);
	}
	public void checkTrolleyAmtIncremented(double Pricebef, double PriceAfter){

	
		if(Pricebef < PriceAfter)
			report.updateTestLog(" Trolley ", "Trolley amt is incremented", Status.PASS);
		else report.updateTestLog(" Trolley ", "Trolley amt is not incremented", Status.FAIL);
	}
	public void verifyAllowSubsIsNotDisplayed(){
		try{
			if(!helper.isElementPresent(driver, trolley.allowsubstitution_loc, trolley.allowsubstitution_chkbx))
				report.updateTestLog(" Trolley ", "Allow substitution is not displayed for entertaining product", Status.PASS);
			else report.updateTestLog(" Trolley ", "Allow substitution is  displayed for entertaining product", Status.FAIL);
		}catch(Exception e){
			report.updateTestLog(" Trolley ", "Allow substitution is not displayed for entertaining product", Status.PASS);
		}
	}

	public void incProductQuantity(){
		double TrolleyPriceBefOrder,TrolleyPriceAfterOrder;
		List<WebElement> plusbtn = driver.findElements(By.xpath(trolley.plusbutton_btn));
		TrolleyPriceBefOrder = getTrolleyPrice();
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,500)",trolley.plusbutton_btn);

		int noofprod = Integer.parseInt(dataTable.getData("General_Data", "NoOfProdToInc"));
		if(noofprod<=plusbtn.size()){
			int i=1;
				for(WebElement plus : plusbtn){
					if((i<=noofprod)){
					
						plus.click();	
						helper.checkPageReadyState(2000);
					i++;
					}
					else break;
				}


			
		}
	//	driver.navigate().refresh();
		TrolleyPriceAfterOrder = getTrolleyPrice();
		if(TrolleyPriceAfterOrder > TrolleyPriceBefOrder)
			report.updateTestLog(" Trolley ", "Product quantity is incremented", Status.PASS);
		else  report.updateTestLog(" Trolley ", "Product quantity is not incremented", Status.FAIL);


	}

	public void decProductQuantity(){
		double TrolleyPriceBefOrder,TrolleyPriceAfterOrder;
		List<WebElement> minusbtn = driver.findElements(By.xpath(trolley.minusbutton_btn));
		TrolleyPriceBefOrder = getTrolleyPrice();
		int noofprod = Integer.parseInt(dataTable.getData("General_Data", "NoOfProdToDec"));
		if(noofprod<=minusbtn.size()){
			int i=1;
				for(WebElement minus : minusbtn){
					if((i<=noofprod)){
					minus.click();
					helper.checkPageReadyState(2000);
					i++;
					}
					else break;
				}


			
		}
		//driver.navigate().refresh();
		TrolleyPriceAfterOrder = getTrolleyPrice();
		if(TrolleyPriceAfterOrder < TrolleyPriceBefOrder)
			report.updateTestLog(" Trolley ", "Product quantity is decremented", Status.PASS);
		else  report.updateTestLog(" Trolley ", "Product quantity is not decremented", Status.FAIL);
	}
	
	public void removeProductFromTrolley(){
		driver.navigate().refresh();
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,500)",trolley.plusbutton_btn);
		double TrolleyPriceBefOrder,TrolleyPriceAfterOrder;
		TrolleyPriceBefOrder = getTrolleyPrice();
		helper.clickLink(driver, trolley.removeproduct_loc, trolley.removeproduct_lnk);
		driver.navigate().refresh();
		TrolleyPriceAfterOrder = getTrolleyPrice();
		if(TrolleyPriceAfterOrder < TrolleyPriceBefOrder)
			report.updateTestLog(" Trolley ", "Product is removed", Status.PASS);
		else  report.updateTestLog(" Trolley ", "Product quantity is not removed", Status.FAIL);
	}
	
	public void removeProductMiniTrolley(){
		WebElement closeButton = driver.findElement(By.xpath("//a[@class='erase']"));
		WebElement prodimge = driver.findElement(By.xpath("//div[@class='trolley-item-image']"));
		helper.checkPageReadyState(2000);
		Actions builder = new Actions(driver);
		builder.moveToElement(prodimge).perform();
		//div[@class='trolley-item-image']
		//wait.until(ExpectedConditions.visibilityOf(closeButton));
	
		if(helper.isElementPresent(driver, "xpath", "//a[@class='erase']")){
			JavascriptExecutor myExecutor = ((JavascriptExecutor) driver);
			myExecutor.executeScript("arguments[0].click();", closeButton);
			report.updateTestLog("Mini Trolley Flyout", "Close Button is selected", Status.PASS);

		}else {
			report.updateTestLog("Mini Trolley Flyout ", "Close Button is not selected", Status.FAIL);						
		}

	}
	
	public void addAllToPendingOrder(){
		double trolleybef = getTrolleyPrice();
		helper.clickLink(driver, trolley.addalltopendingorder_loc, trolley.addalltopendingorder_lnk);
		Wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='order-due-confirm']//a[contains(text(),'Confirm')])[2]")));
		helper.clickLink(driver, "xpath","(//div[@class='order-due-confirm']//a[contains(text(),'Confirm')])[2]");
		double trolleyAfter = getTrolleyPrice();
		if(helper.isElementPresent(driver, trolley.amendorder_loc, trolley.amendorder_txt) && trolleybef<trolleyAfter)
			report.updateTestLog("Trolley - Add all to pending order", "Products added to the pending order", Status.PASS);
		else report.updateTestLog("Trolley- Add all to pending order", "Products are not added to the pending order", Status.FAIL);
		
	}
	
	public void chancelChanges(){
		helper.clickLink(driver, trolley.cancelchanges_loc, trolley.cancelchanges_lnk);
		helper.clickLink(driver, "xpath", "(//div[@class='abandon-changes-details']//a[@id='abandon-edit-order'])[2]");
		if(helper.getText(driver, homepage.trolleyprice_loc, homepage.trolleyprice_txt).trim().contains("0.00"))
			report.updateTestLog("Home Page", "Trolley Total is 0.00- amend order is cancelled", Status.PASS);
		else report.updateTestLog("Home Page", "Trolley total is not 0.00 -  amend order is not cancelled", Status.FAIL);
		
		
	}
	
	public void changeToEntertainingCollection(){
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		Wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(trolley.changeslot_btn)));
		helper.clickLink(driver, trolley.changeslot_loc, trolley.changeslot_btn);
		if(helper.isElementPresent(driver, trolley.changetoent_loc, trolley.changetoent_txt)){
			helper.clickLink(driver, trolley.closechangeslot_loc, trolley.closechangeslot_btn);
			report.updateTestLog("Home Page", "Grocery collection order is changed to entertaining collection", Status.PASS);
		}
		else report.updateTestLog("Home Page", "Grocery collection order is not changed to entertaining collection", Status.FAIL);
	}
	
	public void validatePYOSavingIsApplied(){
		if(helper.isElementPresent(driver, trolley.myoffersaving_loc, trolley.myoffersaving_txt)){
			trolley.myoffersav = helper.getText(driver, trolley.myoffersaving_loc, trolley.myoffersaving_txt).split("�")[1];
			report.updateTestLog("Trolley page", "My offer saving is displayed", Status.PASS);
			
		}else report.updateTestLog("Home Page", "My offer saving is not displayed", Status.FAIL);
	}
	
	public void decProductMiniTrolley(){
		double TrolleyPriceBefOrder,TrolleyPriceAfterOrder;
		WebElement closeButton = driver.findElement(By.xpath("//a[@class='decrease']"));
		WebElement prodimge = driver.findElement(By.xpath("//div[@class='trolley-item-image']"));
		helper.checkPageReadyState(2000);
		Actions builder = new Actions(driver);
		builder.moveToElement(prodimge).perform();
		//div[@class='trolley-item-image']
		//wait.until(ExpectedConditions.visibilityOf(closeButton));
		TrolleyPriceBefOrder = getTrolleyPrice();
		if(helper.isElementPresent(driver, "xpath", "//a[@class='decrease']")){
			JavascriptExecutor myExecutor = ((JavascriptExecutor) driver);
			myExecutor.executeScript("arguments[0].click();", closeButton);
			TrolleyPriceAfterOrder = getTrolleyPrice();
			if(TrolleyPriceAfterOrder < TrolleyPriceBefOrder)
			report.updateTestLog("Mini Trolley Flyout", "decrease Button is decremented", Status.PASS);
			else  report.updateTestLog(" Mini Trolley ", "Product quantity is not decremented", Status.FAIL);
		}else {
			report.updateTestLog("Mini Trolley Flyout ", "decrease Button is not selected", Status.FAIL);						
		}

	}
	

	public void incProductMiniTrolley(){
		double TrolleyPriceBefOrder,TrolleyPriceAfterOrder;
		WebElement closeButton = driver.findElement(By.xpath("//a[@class='increase']"));
		WebElement prodimge = driver.findElement(By.xpath("//div[@class='trolley-item-image']"));
		helper.checkPageReadyState(2000);
		Actions builder = new Actions(driver);
		builder.moveToElement(prodimge).perform();
		//div[@class='trolley-item-image']
		//wait.until(ExpectedConditions.visibilityOf(closeButton));
		TrolleyPriceBefOrder = getTrolleyPrice();
		if(helper.isElementPresent(driver, "xpath", "//a[@class='increase']")){
			JavascriptExecutor myExecutor = ((JavascriptExecutor) driver);
			myExecutor.executeScript("arguments[0].click();", closeButton);
			TrolleyPriceAfterOrder = getTrolleyPrice();
			if(TrolleyPriceAfterOrder > TrolleyPriceBefOrder)
			report.updateTestLog("Mini Trolley Flyout", "increase Button is incremented", Status.PASS);
			else  report.updateTestLog(" Mini Trolley ", "increase quantity is not incremented", Status.FAIL);
		}else {
			report.updateTestLog("Mini Trolley Flyout ", "increase Button is not selected", Status.FAIL);						
		}

	}
	
	public void validatePYOSavingIsNotApplied(){
		List<WebElement> savings = driver.findElements(By.xpath(trolley.myoffersaving_txt));
		if(!(savings.size()>0))
			report.updateTestLog("Trolley page", "My offer saving is not displayed in trolley page", Status.PASS);
			
		else report.updateTestLog("Home Page", "My offer aving is displayed in trolley page", Status.FAIL);
	}

public void addAllToaList(){
		
		if(helper.clickLink(driver, "xpath", "//a[contains(text(),'Add all to a List')]"))
		report.updateTestLog("Trolley - Add all to a list", "Products added to the list", Status.PASS);
		else report.updateTestLog("Trolley- Add all to a list", "Products are not added to the list", Status.FAIL);
		
	}

public void CreateNewList(){
//	SimpleDateFormat sdfScrn = new SimpleDateFormat("ddMMyyyyHHmmss");
//	String listName = "A" + sdfScrn.format(new Date()); 
//	System.out.println(listName);
	//if(helper.isElementPresent(driver, "xpath", "//h2[contains(text(),'Which list would you like to add these products to?')]")){
	if(helper.typeinTextbox(driver, "xpath", "//input[@id='create_new_list']", "listA")){
	helper.clickLink(driver, "xpath", "//a[contains(text(),'Add to list')]");
	helper.clickLink(driver, "xpath", "//a[contains(text(),'Ok')]");
		report.updateTestLog(" listName", "List name is entered", Status.PASS);
	}else report.updateTestLog("listName ", "List name is not entered", Status.FAIL);
		
	
}
	
}
