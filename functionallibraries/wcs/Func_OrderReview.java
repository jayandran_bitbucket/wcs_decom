package functionallibraries.wcs;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.FrameworkException;
import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import functionallibraries.aws.ReusableComponents;
import pageobjects.wcs.OrderReview;
import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;



/**
 * Functional Library class
 * @author Cognizant
 */
public class Func_OrderReview extends ReusableLibrary
{
	/**
	 * Constructor to initialize the functional library
	 * @param scriptHelper The {@link ScriptHelper} object passed from the {@link DriverScript}
	 */
	public Func_OrderReview(ScriptHelper scriptHelper){
		super(scriptHelper);
	}
	
	WebDriverWait Wait = new WebDriverWait(driver,20);
	OrderReview orderreview = new OrderReview();
	ReusableComponents helper = new ReusableComponents(scriptHelper);
	
	public void clickContinuetopayment(){
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		Wait.until(ExpectedConditions.elementToBeClickable(By.id(orderreview.continuetopayment_btn)));
		helper.clickLink(driver, orderreview.continuetopayment_loc, orderreview.continuetopayment_btn);
		report.updateTestLog("Order Review Page", " Continue to payment button is clicked ", Status.DONE);
	}
	
	public void clickDSOrderConfirmation(){
		WebElement btn = driver.findElement(By.id(orderreview.continuetopayment_btn));
		Actions builder = new Actions(driver);
		builder.moveToElement(btn).perform();
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,-1000)",btn);
		Wait.until(ExpectedConditions.elementToBeClickable(By.id(orderreview.continuetopayment_btn)));
		helper.clickLink(driver, orderreview.continuetopayment_loc, orderreview.continuetopayment_btn);
		report.updateTestLog("Order Review Page", "Delivery Service Order Confirmation clicked ", Status.DONE);
	}
	
	public void getProductLineNumbers(){
		List<WebElement> products = helper.findWebElements(driver, orderreview.productlinenumber_loc, orderreview.productlinenumber_img);
		String productimg;
		int i =1;String numb;
		//OrderReview.productslinenumber = new String[products.size()];
		String product = "Product";
		for(WebElement w:products){
			productimg = w.getAttribute("src");
			System.out.println(productimg);
			numb = manipulateLineNumber(productimg);
			System.out.println(numb);
			dataTable.putData("OrderDetails", product+i, numb);
			System.out.println("Added Products "+dataTable.getData("OrderDetails", product+i));
			//OrderReview.productslinenumber[i] = numb ;
			System.out.println("Line Number : "+numb);
			i++;
		}
	}
	
	public String manipulateLineNumber(String imgsrc){
		int v = imgsrc.indexOf("LN");
		String ln = imgsrc.substring(v+3,v+9);
		return ln;
	}
	
	public void clickLoanPlaceOrder(){
		helper.clickLink(driver, orderreview.placeorderloan_loc, orderreview.placeorderloan_btn);
		report.updateTestLog("Order Review Page For Loan Order", " Place Order Button is selected ", Status.PASS);
	}
	
	public void getLoanLineNumbers(){
		List<WebElement> products = helper.findWebElements(driver, orderreview.loanlinenumber_loc, orderreview.loanlinenumber_img);
		String productimg;
		int i =1;String numb;
		//OrderReview.productslinenumber = new String[products.size()];
		String product = "Loan";
		for(WebElement w:products){
			productimg = w.getAttribute("src");
			System.out.println(productimg);
			numb = manipulateLineNumber(productimg);
			System.out.println(numb);
			dataTable.putData("OrderDetails", product+i, numb);
			System.out.println("Added Products "+dataTable.getData("OrderDetails", product+i));
			//OrderReview.productslinenumber[i] = numb ;
			System.out.println("Line Number : "+numb);
			i++;
		}
	}
	
	
	
	public void enterPromoCodeAndValidateTotal(){
		Double ordertotBfr,ordertotAfr,incentiveval;
		String code = dataTable.getData("PaymentDetails_Data", "IncentiveCode");
		String ordertotalBfr = helper.getText(driver, orderreview.ordertotal_loc, orderreview.ordertotal_txt).trim();
		System.out.println("Order Total Before : "+ordertotalBfr);
		helper.typeinTextbox(driver, orderreview.incentivecode_loc, orderreview.incentivecode_box, code);
		helper.clickLink(driver, orderreview.addIncentivecode_loc, orderreview.addIncentivecode_btn);
		String incentivesavings = helper.getText(driver, orderreview.incentivesavingvalue_loc, orderreview.incentivesavingvalue_txt).split("�")[1];
		System.out.println("Incentive Savings : "+incentivesavings);
		orderreview.promodiscount = Double.parseDouble(incentivesavings);
		System.out.println(orderreview.promodiscount);
		int point = incentivesavings.indexOf('-');
		String incentivesav = incentivesavings.substring(point+1);
		String ordertotalAfr = helper.getText(driver, orderreview.ordertotal_loc, orderreview.ordertotal_txt).trim();
		System.out.println("Order Total Before : "+ordertotalAfr);
		ordertotBfr = Double.parseDouble(ordertotalBfr.trim());
		ordertotAfr = Double.parseDouble(ordertotalAfr.trim());
		incentiveval = Double.parseDouble(incentivesav.trim());
		double ordertot  =  ordertotBfr-incentiveval;
		double rounded = (double) Math.round(ordertot * 100.0) / 100.0;
		if(ordertotAfr ==rounded){
			report.updateTestLog("Order Review Page ", " Incentive Savings is applied properly ", Status.PASS);
			report.updateTestLog("Order Review Page ", " Order Total Before "+ordertotBfr, Status.DONE);
			report.updateTestLog("Order Review Page ", " Order Total After "+ordertotAfr, Status.DONE);
			report.updateTestLog("Order Review Page ", " Promotion Incentive Savings "+incentiveval, Status.DONE);
		}else{
			report.updateTestLog("Order Review Page ", " Incentive Savings is not applied properly ", Status.FAIL);
			report.updateTestLog("Order Review Page ", " Order Total Before "+ordertotBfr, Status.DONE);
			report.updateTestLog("Order Review Page ", " Order Total After "+ordertotAfr, Status.DONE);
			report.updateTestLog("Order Review Page ", " Promotion Incentive Savings "+incentiveval, Status.DONE);
		}
		
	}
	
	public void changePromoCode(){
		Double ordertotBfr,ordertotAfr,incentiveval;
		String code = dataTable.getData("PaymentDetails_Data", "APDCode");
		String ordertotalBfr = helper.getText(driver, orderreview.ordertotal_loc, orderreview.ordertotal_txt).trim();
		System.out.println("Order Total Before : "+ordertotalBfr);
		helper.typeinTextbox(driver, orderreview.incentivecode_loc, orderreview.incentivecode_box, code);
		helper.clickLink(driver, orderreview.addIncentivecode_loc, orderreview.addIncentivecode_btn);
		report.updateTestLog("Order Review Page ", " Incentive Savings is applied , Code : "+code, Status.PASS);
	}
	
	
	public void enterPromoCodeAlone(){
		Double ordertotBfr,ordertotAfr,incentiveval;
		String code = dataTable.getData("PaymentDetails_Data", "IncentiveCode");
		String ordertotalBfr = helper.getText(driver, orderreview.ordertotal_loc, orderreview.ordertotal_txt).trim();
		System.out.println("Order Total Before : "+ordertotalBfr);
		helper.typeinTextbox(driver, orderreview.incentivecode_loc, orderreview.incentivecode_box, code);
		helper.clickLink(driver, orderreview.addIncentivecode_loc, orderreview.addIncentivecode_btn);
		report.updateTestLog("Order Review Page ", " Incentive Savings is applied , Code : "+code, Status.PASS);
		
	}
	
	public void verifyAndGetMyWaitroseSavings(){
		if(helper.findWebElements(driver, orderreview.myWaitroseSavings_loc, orderreview.myWaitroseSavings_txt).size() > 0){
			if(helper.isElementPresent(driver, orderreview.myWaitroseSavings_loc, orderreview.myWaitroseSavings_txt)){
				String sav = helper.getText(driver, orderreview.myWaitroseSavings_loc, orderreview.myWaitroseSavings_txt);
				dataTable.putData("OrderDetails", "MyWaitroseSavings", sav.trim());
				System.out.println("My Waitrose Savings : "+sav);
				report.updateTestLog("Order Review Page ", " My Waitrose Savings is displayed , value : "+sav, Status.PASS);
			}else{
				report.updateTestLog("Order Review Page ", " My Waitrose Savings is not displayed  ", Status.FAIL);
			}
		}else{
			report.updateTestLog("Order Review Page ", " My Waitrose Savings is not displayed  ", Status.FAIL);
		}
	}
	
	public void verifyAndGetPYOSavings(){
		if(helper.findWebElements(driver, orderreview.myOfferSavings_loc, orderreview.myOfferSavings_txt).size() > 0){
			if(helper.isElementPresent(driver, orderreview.myOfferSavings_loc, orderreview.myOfferSavings_txt)){
				String sav = helper.getText(driver, orderreview.myOfferSavings_loc, orderreview.myOfferSavings_txt);
				dataTable.putData("OrderDetails", "WCS_PYOSavings", sav.trim());
				System.out.println("PYO Savings : "+sav);
				report.updateTestLog("Order Review Page ", " PYO Savings is displayed , value : "+sav, Status.PASS);
			}else{
				report.updateTestLog("Order Review Page ", " PYO Savings is not displayed  ", Status.FAIL);
			}
		}else{
			report.updateTestLog("Order Review Page ", " PYO Savings is not displayed  ", Status.FAIL);
		}
	}
	
	public void verifyWCSPYOSavingsWithAWSPYOSavings(){
		String wcspyo = dataTable.getData("OrderDetails", "WCS_PYOSavings");
		String awspyo = dataTable.getData("OrderDetails", "AWS_PYOSavings");
		if(wcspyo.equalsIgnoreCase("") || awspyo.equalsIgnoreCase("")){
			report.updateTestLog("Order Review Page ", " PYO Savings is not captured ", Status.FAIL);
			report.updateTestLog("Order Review Page ", " AWS PYO Savings : "+awspyo, Status.DONE);
			report.updateTestLog("Order Review Page ", " WCS PYO Savings : "+wcspyo, Status.DONE);
		}else if(wcspyo.contains(awspyo)){
			report.updateTestLog("Order Review Page ", " PYO Savings are same ", Status.PASS);
			report.updateTestLog("Order Review Page ", " AWS PYO Savings : "+awspyo, Status.DONE);
			report.updateTestLog("Order Review Page ", " WCS PYO Savings : "+wcspyo, Status.DONE);
		}else{
			report.updateTestLog("Order Review Page ", " PYO Savings are not same ", Status.FAIL);
			report.updateTestLog("Order Review Page ", " AWS PYO Savings : "+awspyo, Status.DONE);
			report.updateTestLog("Order Review Page ", " WCS PYO Savings : "+wcspyo, Status.DONE);
		}
	}
	
	
	public void enterDeliveryNoteForDSOrder(){
		String note = dataTable.getData("General_Data", "CollectionNote");
		helper.typeinTextbox(driver, orderreview.deliverynote_loc, orderreview.deliverynote_txt,note );
		report.updateTestLog("Order Review Page ", "Delivery Note is entered: "+dataTable.getData("General_Data", "DeliveryNote"), Status.DONE);
	}

public void enterCollectionNote(){
	String note = dataTable.getData("General_Data", "CollectionNote");
	helper.typeinTextbox(driver, orderreview.collectionnote_loc, orderreview.collectionnote_txt,note );
	report.updateTestLog("Order Review Page ", "Collection Note is entered: "+dataTable.getData("General_Data", "CollectionNote"), Status.DONE);
}

public void selectFastTrackOption(){
	helper.clickLink(driver, orderreview.fasttrack_loc, orderreview.fasttrack_opt);
	report.updateTestLog("Order Review Page ", " Fasttrack option is selected ", Status.DONE);
}

public void checkCarrierBagCharge(){

	if(helper.isElementPresent(driver, orderreview.carriercharge_loc, orderreview.carriercharge_txt)){
		orderreview.carrierbagchargevalue = helper.getText(driver, orderreview.carriercharge_loc, orderreview.carriercharge_txt);
		if(!orderreview.carrierbagchargevalue.equals("0.00"))
		report.updateTestLog("Order Review Page ", " carrier bag charge is applied "+orderreview.carrierbagchargevalue, Status.PASS);
		else report.updateTestLog("Order Review Page ", " carrier bag charge is not applied "+orderreview.carrierbagchargevalue, Status.FAIL);
	}
}

public void validateWhatsThisCarrierBagContent(){
	helper.clickLink(driver, orderreview.whatsthis_loc, orderreview.whatsthis_lnk);
	if(helper.isElementPresent(driver, orderreview.carrierclose_loc, orderreview.carrierclose_btn)){
		report.updateTestLog("Order Review Page ", " Carrier bag content overlay is displayed", Status.PASS);
		helper.clickLink(driver, orderreview.carrierclose_loc, orderreview.carrierclose_btn);
	}else report.updateTestLog("Order Review Page ", " Carrier bag content overlay is not displayed", Status.FAIL);
	Actions build = new Actions(driver);
	WebElement whatsthis = driver.findElement(By.id(orderreview.continuetopayment_btn));
	build.moveToElement(whatsthis).perform();
	JavascriptExecutor jse = (JavascriptExecutor)driver;
	jse.executeScript("window.scrollBy(0,-300)",whatsthis);
}

public void validateCarrierBagToolTip(){
	helper.clickLink(driver, orderreview.tooltip_loc, orderreview.tooltip_btn);
	if(helper.isElementPresent(driver, orderreview.tooltopcontent_loc, orderreview.tooltopcontent1_txt)  && helper.isElementPresent(driver, orderreview.tooltopcontent_loc, orderreview.tooltopcontent2_txt)  && helper.isElementPresent(driver, orderreview.tooltopcontent_loc, orderreview.tooltopcontent3_txt)){
		report.updateTestLog("Order Review Page ", " Tool tip content is displayed", Status.PASS);
	}else report.updateTestLog("Order Review Page ", " Tool tip content is not displayed", Status.FAIL);
}
public void chooseCarrierBagOption(){
	Actions build = new Actions(driver);
	WebElement whatsthis = driver.findElement(By.id(orderreview.yesbag_opt));
	build.moveToElement(whatsthis).perform();
	JavascriptExecutor jse = (JavascriptExecutor)driver;
	jse.executeScript("window.scrollBy(0,300)",whatsthis);
	helper.clickLink(driver, orderreview.yesbag_loc, orderreview.yesbag_opt);
	report.updateTestLog("Order Review Page ", " Carrier bag option is selected", Status.DONE);
}

public void chooseNoBagOption(){
	helper.clickLink(driver, orderreview.nobag_loc, orderreview.nobag_opt);
	report.updateTestLog("Order Review Page ", " No Carrier bag option is selected", Status.DONE);
}

public void checkNoCarrierBag(){
	List<WebElement> carrierbag = driver.findElements(By.xpath( orderreview.carriercharge_txt));
	if(carrierbag.size()==0)
		report.updateTestLog("Order Review Page ", " No Carrier bag charge is not applied for the order", Status.PASS);
	else report.updateTestLog("Order Review Page ", " No Carrier bag charge is applied for the order", Status.FAIL);
}
public void addMobileNumber(){
	String mobile = dataTable.getData("General_Data", "Mobile_Number");
	helper.typeinTextbox(driver,orderreview.mobilenumber_loc,orderreview.mobilenumber,mobile);
	if(helper.isElementPresent(driver, "orderreview.errormsg_loc","orderreview.errormsg"))
		report.updateTestLog("Mobile Number", "Mobile number is not added", Status.FAIL);
	else
		report.updateTestLog("Mobile Number", "Mobile number is added", Status.PASS);
	}

public void mobileNumberValidation(){
	helper.typeinTextbox(driver,orderreview.mobilenumber_loc,orderreview.mobilenumber,"071235467891234567");
	driver.findElement(By.xpath(orderreview.obj)).sendKeys(Keys.ENTER);
	if(helper.isElementPresent(driver, orderreview.errormsg_loc,orderreview.errormsg))
		report.updateTestLog("Mobile Number", "Error message is shown when number is greater than 11 digits", Status.PASS);
	else
		report.updateTestLog("Mobile Number", "Error message is not shown when number is greater than 11 digits", Status.FAIL);
	driver.findElement(By.xpath(orderreview.mobilenumber)).clear();
	
	
	helper.typeinTextbox(driver,orderreview.mobilenumber_loc,orderreview.mobilenumber,"0712367");
	driver.findElement(By.xpath(orderreview.obj)).sendKeys(Keys.ENTER);
	if(helper.isElementPresent(driver, orderreview.errormsg_loc,orderreview.errormsg))
		report.updateTestLog("Mobile Number", "Error message is shown when number is lesser than 11 digits", Status.PASS);
	else
		report.updateTestLog("Mobile Number", "Error message is not shown when number is lesser than 11 digits", Status.FAIL);
	driver.findElement(By.xpath(orderreview.mobilenumber)).clear();
	
	
	helper.typeinTextbox(driver,orderreview.mobilenumber_loc,orderreview.mobilenumber,"123546789123");
	driver.findElement(By.xpath(orderreview.obj)).sendKeys(Keys.ENTER);
	if(helper.isElementPresent(driver, orderreview.errormsg_loc,orderreview.errormsg))
		report.updateTestLog("Mobile Number", "Error message is shown when 07 is not added in front", Status.PASS);
	else
		report.updateTestLog("Mobile Number", "Error message is not shown when 07 is not added in front", Status.FAIL);
	driver.findElement(By.xpath(orderreview.mobilenumber)).clear();
	
	
	helper.typeinTextbox(driver,orderreview.mobilenumber_loc,orderreview.mobilenumber,"07123ADFW546783");
	driver.findElement(By.xpath(orderreview.obj)).sendKeys(Keys.ENTER);
	if(helper.isElementPresent(driver, orderreview.errormsg_loc,orderreview.errormsg))
		report.updateTestLog("Mobile Number", "Error message is shown when alpahbets are entered", Status.PASS);
	else
		report.updateTestLog("Mobile Number", "Error message is not shown when alpahbets are entered", Status.FAIL);
	driver.findElement(By.xpath(orderreview.mobilenumber)).clear();
	
	
	helper.typeinTextbox(driver,orderreview.mobilenumber_loc,orderreview.mobilenumber,"07123!@#!$59123");
	driver.findElement(By.xpath(orderreview.obj)).sendKeys(Keys.ENTER);
	if(helper.isElementPresent(driver, orderreview.errormsg_loc,orderreview.errormsg))
		report.updateTestLog("Mobile Number", "Error message is shown when special characters are added", Status.PASS);
	else
		report.updateTestLog("Mobile Number", "Error message is not shown when special characters are added", Status.FAIL);
	driver.findElement(By.xpath(orderreview.mobilenumber)).clear();
	
	
	helper.typeinTextbox(driver,orderreview.mobilenumber_loc,orderreview.mobilenumber,"0712354 6789");
	driver.findElement(By.xpath(orderreview.obj)).sendKeys(Keys.ENTER);
	//if(!helper.isElementPresent(driver, orderreview.errormsg_loc,orderreview.errormsg))
		report.updateTestLog("Mobile Number", "Error message is not shown when we give spaces", Status.PASS);
	//else
		//report.updateTestLog("Mobile Number", "Error message is shown when we give spaces", Status.FAIL);
	driver.findElement(By.xpath(orderreview.mobilenumber)).clear();
	
}

public void enterAPDcard(){

	String code = dataTable.getData("PaymentDetails_Data", "APDCode");
	helper.typeinTextbox(driver, orderreview.incentivecode_loc, orderreview.incentivecode_box, code);
	helper.clickLink(driver, orderreview.addIncentivecode_loc, orderreview.addIncentivecode_btn);
	report.updateTestLog("Order Review Page ", " APD card is added ", Status.PASS);
}



public void removepromocode()
{
	helper.clickLink(driver, orderreview.removepromo_loc, orderreview.removepromo);
}

public void verifyPYOSavingNotApplied(){
	if(!(helper.findWebElements(driver, orderreview.myOfferSavings_loc, orderreview.myOfferSavings_txt).size() > 0))
	
			report.updateTestLog("Order Review Page ", " PYO Savings is  not displayed in order review page", Status.PASS);
		else
			report.updateTestLog("Order Review Page ", " PYO Savings is  displayed in order review page", Status.FAIL);
		
}
public void claimDiscount(){
	String discount = dataTable.getData("PaymentDetails_Data", "AddAPD");
	if(discount.equals("yes")){
	helper.clickLink(driver, orderreview.claim_loc, orderreview.claim);
	report.updateTestLog("APD discount", "Discount is claimed", Status.PASS);
	}
	
	if(discount.equals("no")){
		helper.clickLink(driver, orderreview.dontclaim_loc, orderreview.dontclaim);
		report.updateTestLog("APD discount", "Discount is not claimed", Status.PASS);
	}
}
public void clickbtnPlaceYourOrderinLoanItem(){
	String btnPlaceYourOrder; 
	for(int i =1;i<3;i++){
		btnPlaceYourOrder = orderreview.btnPlaceYourOrderInLoanItem+"["+i+"]"; 
		if(helper.isElementPresent(driver, orderreview.btnPlaceYourOrderInLoanItem_Loc, btnPlaceYourOrder)){
			helper.clickLink(driver, orderreview.btnPlaceYourOrderInLoanItem_Loc, btnPlaceYourOrder);
			if(helper.isElementPresent(driver, orderreview.varOrderNumber_Loc, orderreview.varOrderNumber))
			{report.updateTestLog("Order Review Page ", "Place Your Order Button is Clicked", Status.PASS);
			String order = helper.getText(driver, orderreview.varOrderNumber_Loc, orderreview.varOrderNumber);
			dataTable.putData("OrderDetails", "OrderNumber", order);
			}
			else{
				report.updateTestLog("Order Review Page", "PlaceYourOrder Button does not exist", Status.FAIL);
			}
			break;
		}
	}


}
}