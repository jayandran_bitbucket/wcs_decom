package functionallibraries.wcs;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.cognizant.framework.FrameworkException;
import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import functionallibraries.aws.ReusableComponents;
import pageobjects.aws.Header;
import pageobjects.wcs.BookSlot;
import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;


/**
 * Functional Library class
 * @author Cognizant
 */
public class Func_BookSlot extends ReusableLibrary
{
	/**
	 * Constructor to initialize the functional library
	 * @param scriptHelper The {@link ScriptHelper} object passed from the {@link DriverScript}
	 */
	public Func_BookSlot(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	ReusableComponents helper = new ReusableComponents(scriptHelper);
	BookSlot bookslot = new BookSlot();
	WebDriverWait Wait = new WebDriverWait(driver,20);
	JavascriptExecutor executor = (JavascriptExecutor) driver;


	public void chooseDeliveryType(){
		String Type = dataTable.getData("General_Data","DeliveryType");
		switch(Type){
		case "Delivery" : helper.clickLink(driver,bookslot.Deliver_loc, bookslot.Deliver_btn);
		report.updateTestLog("Book Slot", " Delivery is selected ", Status.DONE);
		break;
		case "Collection" : helper.clickLink(driver, bookslot.Collection_loc, bookslot.Collection_btn);
		report.updateTestLog("Book Slot", " Collection Slot is selected ", Status.DONE);
		break;
		case "ShopInBranch" : helper.clickLink(driver, bookslot.ShopInBranch_loc, bookslot.ShopInBranch_lnk);
		report.updateTestLog("Book Slot", " Shop in Branch, We Deliver Slot is selected ", Status.DONE);
		break;
		default :
		}
	}

	public void doPostCodeLookUpForDelivery(){
		String postcode = dataTable.getData("General_Data","PostCode");
		Wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(bookslot.bookslotheader_txt)));
		if(helper.isElementPresent(driver, bookslot.Delivertonewaddress_loc, bookslot.Delivertonewaddress_btn)){
			helper.clickLink(driver, bookslot.Delivertonewaddress_loc, bookslot.Delivertonewaddress_btn);
			helper.typeinTextbox(driver, bookslot.enterPostcode_loc, bookslot.enterPostcode_textbox, postcode);
			helper.clickLink(driver, bookslot.checkPostcode_loc, bookslot.checkPostcode_btn);
			report.updateTestLog("Book Slot", " Post Code entered and searched for slot", Status.DONE);
		}else{
			helper.typeinTextbox(driver, bookslot.enterPostcode1_loc, bookslot.enterPostcode1_textbox, postcode);
			helper.clickLink(driver, bookslot.PostcodesearchOk_loc, bookslot.PostcodesearchOk_btn);
			report.updateTestLog("Book Slot", " Post Code entered and searched for slot", Status.DONE);
		}
	}

	public void selectCollectionPoint(){
		Wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(bookslot.collectfromhere_btn)));
		String collectiontype,collectiontypeval = null;
		String collectiontypegiven = dataTable.getData("General_Data", "CollectionType").trim();
		switch(collectiontypegiven){
		case "G&E": collectiontypeval = "Groceries Entertaining";break;

		case "G" : collectiontypeval = "Groceries";break;

		case "E" : collectiontypeval = "Entertaining"; break;

		}
		searchPostCodeInCollection();
		String branch;
		int exe = 0,numberOfExecution=1;
		outofloop:{
			do{
				Wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(bookslot.collectfromhere_btn)));
				List<WebElement> allLocations=driver.findElements(By.xpath(bookslot.CollectionBranch_box+bookslot.CollectionNormalBranch_logo));
				System.out.println(allLocations.size());
				for(int i =1;i<=allLocations.size();i++){
					branch = "("+bookslot.CollectionBranch_box+bookslot.CollectionNormalBranch_logo+")["+i+"]";
					System.out.println(branch);
					if(driver.findElements(By.xpath(branch)).size()>0){
						driver.findElement(By.xpath(branch)).click();
						collectiontype = getCollectionType().trim();
						if(collectiontype.contains(collectiontypeval)){
							clickCollectFromHerebutton();
							break outofloop;
						}
					}
				}
				if(numberOfExecution==1){
					helper.clickLink(driver, bookslot.next5_loc, bookslot.next5_link);
				}else if(numberOfExecution==2){
					report.updateTestLog("Find a Click & Collect location", " Collection Location is not Selected", Status.FAIL);
					break outofloop;
				}
				numberOfExecution++;
			}while(exe == 0);

		}
	}
	
	
	public void selectMannedCollectionPoint(){
		Wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(bookslot.collectfromhere_btn)));
		String collectiontype,collectiontypeval = null;
		String collectiontypegiven = dataTable.getData("General_Data", "CollectionType").trim();
		switch(collectiontypegiven){
		case "G&E": collectiontypeval = "Groceries Entertaining";break;

		case "G" : collectiontypeval = "Groceries";break;

		case "E" : collectiontypeval = "Entertaining"; break;

		}
		searchPostCodeInCollection();
		String branch;
		String mannedcollectionstore = dataTable.getData("General_Data", "MannedCollectionStore");
		int exe = 0,numberOfExecution=1;
		outofloop:{
			do{
				Wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(bookslot.collectfromhere_btn)));
				List<WebElement> allLocations=driver.findElements(By.xpath(bookslot.CollectionBranch_box+"//strong[contains(text(),'"+mannedcollectionstore+"')]"));
				System.out.println(allLocations.size());
				for(int i =1;i<=allLocations.size();i++){
					branch = "("+bookslot.CollectionBranch_box+"//strong[contains(text(),'"+mannedcollectionstore+"')])["+i+"]";
					System.out.println(branch);
					if(driver.findElements(By.xpath(branch)).size()>0){
						driver.findElement(By.xpath(branch)).click();
						collectiontype = getCollectionType().trim();
						if(collectiontype.contains(collectiontypeval)){
							clickCollectFromHerebutton();
							break outofloop;
						}
					}
				}
				if(numberOfExecution==1){
					helper.clickLink(driver, bookslot.next5_loc, bookslot.next5_link);
				}else if(numberOfExecution==2){
					report.updateTestLog("Find a Click & Collect location", " Collection Location is not Selected", Status.FAIL);
					break outofloop;
				}
				numberOfExecution++;
			}while(exe == 0);

		}
	}
	
	

	public void clickCollectFromHerebutton(){
		returnPageSource(500);
		Wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(bookslot.collectfromhere_btn)));
		List<WebElement> collectfrmhere = helper.findWebElements(driver, bookslot.collectfromhere_loc, bookslot.collectfromhere_btn);
		System.out.println("Collect From Here Button Size : "+collectfrmhere.size());
		String elementxpath ;
		for(int i =1;i<=collectfrmhere.size();i++){
			elementxpath = bookslot.collectfromhere_btn+"["+i+"]";
			if(driver.findElement(By.xpath(elementxpath)).isDisplayed()){
				Wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(elementxpath)));
				helper.clickObject(driver, bookslot.collectfromhere_loc, elementxpath);
				report.updateTestLog("Find a Click & Collect location", " Collect from here is Selected", Status.DONE);
				break;
			}
			if(i == collectfrmhere.size()){
				report.updateTestLog("Find a Click & Collect location", " Collect from here is not displayed ", Status.FAIL);
			}
			/*if(helper.isElementPresent(driver, bookslot.collectfromhere_loc, elementxpath)){
				Wait.until(ExpectedConditions.elementToBeClickable(By.xpath(elementxpath)));
				//helper.clickObject(driver, bookslot.collectfromhere_loc, elementxpath);
				helper.clickLink(driver, bookslot.collectfromhere_loc, elementxpath);
				report.updateTestLog("Find a Click & Collect location", " Collect from here is Selected", Status.DONE);
				break;
			}*/
		}
		/*for(WebElement c : collectfrmhere){
			if(c.isDisplayed()){
				//c.click();
				Wait.until(ExpectedConditions.elementToBeClickable(c));
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].click();", c);
				report.updateTestLog("Find a Click & Collect location", " Collect from here is Selected", Status.DONE);
				break;
			}
		}	*/		
	}

	public void returnPageSource(int val){
		String ase;
		for(int i =1;i<5;i++){
			try{
				Thread.sleep(val);
			}catch(Exception e){
				
			}
		ase = ((JavascriptExecutor) driver).executeScript("return document.readyState").toString();
		System.out.println(ase);
		if(ase.contains("complete")){
			System.out.println("Page Load Completed on : "+i*val +"  ms");
		break;
		}
		}
	}

	public String getCollectionType(){
		String typ = "";
		List<WebElement> collectiontype = helper.findWebElements(driver, bookslot.CollectionBranchType_loc, bookslot.CollectionBranchType_txt);
		int size = collectiontype.size();
		int i=0;
		for(WebElement c : collectiontype){
			if(c.isDisplayed()){
				typ =  c.getAttribute("class");
				break;
			}
			i++;
		}
		return typ;
	}

	public void searchPostCodeInCollection(){
		String postcode = dataTable.getData("General_Data", "CollectionPostcode");
		Wait.until(ExpectedConditions.elementToBeClickable(By.id(bookslot.CollectionPostcodeSearch_txt)));
		helper.typeinTextbox(driver, bookslot.CollectionPostcodeSearch_loc, bookslot.CollectionPostcodeSearch_txt, postcode);
		helper.clickLink(driver, bookslot.CollectionPostcodeFind_loc, bookslot.CollectionPostcodeFind_btn);
		Wait.until(ExpectedConditions.elementToBeClickable(By.xpath(bookslot.collectfromhere_btn)));
	}

	public void selectSlot() {
		int days = Integer.parseInt(dataTable.getData("General_Data","Slotdayahead"));
		for (int iter=1;iter<=days/7;iter++){
			returnPageSource(1000);
			Wait.until(ExpectedConditions.elementToBeClickable(By.className(bookslot.next7days_link)));
			helper.clickLink(driver, bookslot.next7days_loc, bookslot.next7days_link);
		}
		int day = days%7;
		int count = 3;
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		WebElement element;
		returnPageSource(3000);
		slot:
			for(int i =1,y=1;i<count;i++){
				String ExpectedSlotColumn_Obj=bookslot.SlotTable_table+"["+(day+1)+"]/li";
				WebDriverWait Wait = new WebDriverWait(driver,20);
				//Wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(ExpectedSlotColumn_Obj)));
				Wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(ExpectedSlotColumn_Obj)));
				int No_of_ExpectedSlotColumn_Obj=driver.findElements(By.xpath(ExpectedSlotColumn_Obj)).size();
				for(int k=2; k<=No_of_ExpectedSlotColumn_Obj; k++){
					String ExpectedSlot_Obj=ExpectedSlotColumn_Obj+"["+k+"]/a/span";
					System.out.println("investgating row details with cell details : "+ExpectedSlot_Obj);
					element = driver.findElement(By.xpath(ExpectedSlot_Obj));
					String ExpectedSlot_ObjText=(jse.executeScript("return arguments[0].innerHTML", element)).toString();
					System.out.println("Text available under inspection cell : "+ExpectedSlot_ObjText);
					element = driver.findElement(By.xpath(ExpectedSlotColumn_Obj+"["+k+"]/a"));
					String fullyBooked_ObjText=(jse.executeScript("return arguments[0].innerHTML", element)).toString();
					System.out.println("Text available check for fully booked : "+fullyBooked_ObjText);
					if((!ExpectedSlot_ObjText.contains("Slot unavailable")) && (!fullyBooked_ObjText.contains("Fully booked"))){	
						if(y==1){
							y=2;
							continue;
						}
						String slotObject=ExpectedSlot_Obj+"[2]";
						System.out.println("booked slot details : "+ExpectedSlot_ObjText);
						Wait.until(ExpectedConditions.elementToBeClickable(By.xpath(slotObject)));
						helper.clickObject(driver, "xpath", slotObject);
						//driver.findElement(By.xpath(slotObject)).click();
						String validationdate = ExpectedSlot_ObjText.substring(0,ExpectedSlot_ObjText.indexOf(','));
						dataTable.putData("OrderDetails", "SlotDate_DB", validationdate);
						saveOFDate(validationdate);
						saveAWSDate(validationdate);
						report.updateTestLog("Book Slot", "Slot is confirmed for :  "+ExpectedSlot_ObjText, Status.DONE);
						break slot;
					}
				}
				if(days>7){
					helper.clickLink(driver, bookslot.next7days_loc, bookslot.next7days_link);
					returnPageSource(2000);
					days = 1;
				}else{
					days = days+1;	
				}
			}

	}


	public void clickContinueBtnInDeliverySlotOverlay(){
		System.out.println("Executing Continue Btn ");
		WebDriverWait Wait = new WebDriverWait(driver,5);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		Wait.until(ExpectedConditions.elementToBeClickable(By.xpath(bookslot.slotconfirmcontinueInDelivery_btn)));
		helper.clickLink(driver, bookslot.slotconfirmcontinueInDelivery_loc, bookslot.slotconfirmcontinueInDelivery_btn);
		report.updateTestLog("Book Slot", " Slot Confirmation Continue Button is selected ", Status.PASS);
	}
	
	public void clickConfirmReservationInDeliveryServiceOverlay(){
		System.out.println("Executing Confirm Reservation Btn ");
		WebDriverWait Wait = new WebDriverWait(driver,20);
		Wait.until(ExpectedConditions.elementToBeClickable(By.xpath(bookslot.slotconfirmDeliveryServiceReserve_btn)));
		helper.clickLink(driver, bookslot.slotconfirmDeliveryServiceReserve_loc, bookslot.slotconfirmDeliveryServiceReserve_btn);
		report.updateTestLog("Book Slot", " Delivery Service Slot Reservation Button is selected ", Status.PASS);
	}

	public void clickContinueBtnInCollectionSlotOverlay(){
		Wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//a[@class='start-shopping-click'])[2]")));
		//driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		helper.clickLink(driver, "xpath", "(//a[@class='start-shopping-click'])[2]");
		returnPageSource(1000);
		/*Wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(bookslot.collectionslotconfirmation_header)));
		List<WebElement> continuebtn = helper.findWebElements(driver, bookslot.slotconfirmcontinueInCollection_loc, bookslot.slotconfirmcontinueInCollection_btn);
		int i = 1,size = continuebtn.size();
		for(WebElement c:continuebtn){
			if(c.isDisplayed()){
				Wait.until(ExpectedConditions.elementToBeClickable(c));
				executor.executeScript("arguments[0].click();", c);
				report.updateTestLog("Book Slot", " Slot Confirmation Continue Button is selected ", Status.PASS);
			} 
			if(i == size){
				report.updateTestLog("Book Slot", " Slot Confirmation Continue Button is not selected ", Status.FAIL);
			}
			i++;
		}*/
		System.out.println("ssde");
	}


	public void saveOFDate(String datee){
	System.out.println("Given Date : "+datee);
		SimpleDateFormat dd = new SimpleDateFormat("yyyy-MM-dd");
		Date de;
		try {
			de = dd.parse(datee);
			String ese = dd.format(de);
			SimpleDateFormat ds = new SimpleDateFormat("E d MMM yyyy");
			BookSlot.slotdatee = ds.format(de);
			System.out.println(BookSlot.slotdatee);
			dataTable.putData("OrderDetails", "SlotDate", BookSlot.slotdatee);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

	}
	
	public void getSlotTimeInWCS(){
		Wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(bookslot.slottime_txt)));
		String slottime = helper.getText(driver, bookslot.slottime_loc, bookslot.slottime_txt).trim();
		System.out.println("Slot time : "+slottime);
	}
	
	public void saveAWSDate(String datee){
		SimpleDateFormat dd = new SimpleDateFormat("yyyy-MM-dd");
		Date de;
		try {
			de = dd.parse(datee);
			String ese = dd.format(de);
			SimpleDateFormat ds = new SimpleDateFormat("d");
			String awsdate = ds.format(de);
			//Header.AWSSlotDate = ds.format(de);
			dataTable.putData("OrderDetails", "AWS_SlotDate", awsdate);
			ds = new SimpleDateFormat("MMM");
			String awsmonth = ds.format(de);
			//Header.AWSSlotMonth = ds.format(de);
			dataTable.putData("OrderDetails", "AWS_SlotDate", awsmonth);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
	
	public void clickEntertainingTab(){
		returnPageSource(1000);
		helper.clickLink(driver, bookslot.Entertaining_loc, bookslot.Entertaining_tab);
		report.updateTestLog("Book Slot", " Entertaining Tab is selected ", Status.PASS);
	}
	public void clickGroceryTab(){
		returnPageSource(1000);
		helper.clickLink(driver, bookslot.grocery_loc, bookslot.grocery_tab);
		report.updateTestLog("Book Slot", " Grocery Tab is selected ", Status.PASS);
	}
	
	public void verifyEditSlotDisabled(){
		try{
			if(helper.isElementPresent(driver, bookslot.EditSlotDisabled_loc, bookslot.EditSlotDisabled_btn)){
				report.updateTestLog("Book Slot", " Edit Slot Button is disabled ", Status.PASS);	
			}else{
				report.updateTestLog("Book Slot", " Edit Slot Button is not disabled ", Status.FAIL);
			}	
		}catch(Exception e){
			report.updateTestLog("Book Slot", " Edit Slot Button is not disabled ", Status.FAIL);
		}
		
	}
	
	
	public void workAroundCollectionSlot(){
		chooseDeliveryType();
		selectCollectionPoint();
		selectSlot();
		clickContinueBtnInCollectionSlotOverlay();
		helper.checkPageReadyState(2000);
		driver.findElement(By.xpath("//div[@id='headerMiniTrolley']//button[text()='Checkout']")).click();
		//homepage.clickCheckOutBtn();
		
	}
	
	public void workAroundDeliverySlot(){
		chooseDeliveryType();
		selectCollectionPoint();
		selectSlot();
		clickContinueBtnInCollectionSlotOverlay();
		helper.checkPageReadyState(2000);
		driver.findElement(By.xpath("//div[@id='headerMiniTrolley']//button[text()='Checkout']")).click();
	}
	
	public void getWCSSlotDate(){
		String slotdate = helper.getText(driver, bookslot.SlotDate_loc, bookslot.SlotDate_txt);
		SimpleDateFormat inSDF = new SimpleDateFormat("dd/mm/yyyy");
		SimpleDateFormat outSDF = new SimpleDateFormat("yyyy-mm-dd");
		if(slotdate != ""){
			try {
				Date date = inSDF.parse(slotdate);
				String validationdate = outSDF.format(date);
				System.out.println("Slot Date : "+validationdate);
				saveOFDate(validationdate);
				saveAWSDate(validationdate);
				report.updateTestLog("Book Slot", "Slot Date : "+validationdate, Status.PASS);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				report.updateTestLog("Book Slot", "Exception occured while converting date "+slotdate, Status.FAIL);
			}
		}else{
			report.updateTestLog("Book Slot", "Slot date is not retrieved : "+slotdate, Status.FAIL);
		}

		  
	}
	
	public void selectSlotBasedOnGivenSlotDay() {
		int days = Integer.parseInt(dataTable.getData("General_Data","Slotdayahead"));
		String slotday = dataTable.getData("General_Data","SlotDay").trim();
		for (int iter=1;iter<=days/7;iter++){
			returnPageSource(1000);
			Wait.until(ExpectedConditions.elementToBeClickable(By.className(bookslot.next7days_link)));
			helper.clickLink(driver, bookslot.next7days_loc, bookslot.next7days_link);
		}
		int day = days%7;
		int count = 7;
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		WebElement element;
		returnPageSource(3000);
		slot:
			for(int i =1,y=1;i<count;i++){
				y = 1;
				String ExpectedSlotColumn_Obj=bookslot.SlotTable_table+"["+(day+1)+"]/li";
				WebDriverWait Wait = new WebDriverWait(driver,20);
				//Wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(ExpectedSlotColumn_Obj)));
				Wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(ExpectedSlotColumn_Obj)));
				int No_of_ExpectedSlotColumn_Obj=driver.findElements(By.xpath(ExpectedSlotColumn_Obj)).size();
				for(int k=2; k<No_of_ExpectedSlotColumn_Obj; k++){
					String ExpectedSlot_Obj=ExpectedSlotColumn_Obj+"["+k+"]/a/span";
					//System.out.println("investgating row details with cell details : "+ExpectedSlot_Obj);
					element = driver.findElement(By.xpath(ExpectedSlot_Obj));
					String ExpectedSlot_ObjText=(jse.executeScript("return arguments[0].innerHTML", element)).toString();
					//System.out.println("Text available under inspection cell : "+ExpectedSlot_ObjText);
					element = driver.findElement(By.xpath(ExpectedSlotColumn_Obj+"["+k+"]/a"));
					String fullyBooked_ObjText=(jse.executeScript("return arguments[0].innerHTML", element)).toString();
					System.out.println("Text available check for fully booked : "+fullyBooked_ObjText);
					if((!ExpectedSlot_ObjText.contains("Slot unavailable")) && (!fullyBooked_ObjText.contains("Fully booked"))){	
						String slotdayz = ExpectedSlot_ObjText.substring(0, ExpectedSlot_ObjText.indexOf(','));
						if(y<4 || !slotday.equalsIgnoreCase(manipulateDateToDay(slotdayz))){
							y++;
							continue;
						}
						String slotObject=ExpectedSlot_Obj+"[2]";
						System.out.println("booked slot details : "+ExpectedSlot_ObjText);
						Wait.until(ExpectedConditions.elementToBeClickable(By.xpath(slotObject)));
						helper.clickObject(driver, "xpath", slotObject);
						//driver.findElement(By.xpath(slotObject)).click();
						String validationdate = ExpectedSlot_ObjText.substring(0,ExpectedSlot_ObjText.indexOf(','));
						dataTable.putData("OrderDetails", "SlotDate_DB", validationdate);
						saveOFDate(validationdate);
						saveAWSDate(validationdate);
						report.updateTestLog("Book Slot", "Slot is confirmed for :  "+ExpectedSlot_ObjText, Status.DONE);
						break slot;
					}
				}
				if(day>=6){
					helper.clickLink(driver, bookslot.next7days_loc, bookslot.next7days_link);
					returnPageSource(2000);
					day = 0;
				}else{
					day = day+1;	
				}
			}

	}
	
	public String manipulateDateToDay(String datee){
		SimpleDateFormat dd = new SimpleDateFormat("yyyy-MM-dd");
		String finalDay = "";
		Date de;
		try {
			de = dd.parse(datee);
			DateFormat format2=new SimpleDateFormat("EEEE"); 
			finalDay=format2.format(de);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return finalDay; 
	}
	
	public void clickChristmasSeasonalSlot(){
		if(helper.findWebElements(driver, bookslot.christmasslot_loc, bookslot.christmasslot_btn).size() > 0){
			helper.clickLink(driver, bookslot.christmasslot_loc, bookslot.christmasslot_btn);
			report.updateTestLog("Book Slot", " Christmas Slot is selected ", Status.DONE);
		}else{
			report.updateTestLog("Book Slot", " Christmas Slot is displayed ", Status.FAIL);
		}
	}
		
	public void clickAlcoholContinue(){
		try{
			if(helper.isElementPresent(driver, bookslot.alcoholcontinue_loc, bookslot.alcoholcontinue_btn)){
				helper.clickObject(driver, bookslot.alcoholcontinue_loc, bookslot.alcoholcontinue_btn);
				report.updateTestLog("Book Slot", " Alcohol continue button is clicked", Status.DONE);
			}
				
		}catch(Exception e){
			System.out.println("Alcohol overlay is not displayed");
		}
	}
	
	public void verifyBookedTextIsDisplayedInSlotSelection(){
		if(helper.isElementPresent(driver, bookslot.bookedslot_loc, bookslot.bookedslot_txt))
		report.updateTestLog("Book Slot", " Booked slot is displayed for amend order", Status.PASS);
		else report.updateTestLog("Book Slot", " Booked slot is displayed for amend order", Status.FAIL);
		
	}
	
	public void chooseDeliveryType_D(){
	 helper.clickLink(driver,bookslot.Deliver_loc, bookslot.Deliver_btn);
		report.updateTestLog("Book Slot", " Delivery is selected ", Status.DONE);
		
	}
public void chooseDeliveryType_DS(){
	  helper.clickLink(driver, bookslot.ShopInBranch_loc, bookslot.ShopInBranch_lnk);
		report.updateTestLog("Book Slot", " Shop in Branch, We Deliver Slot is selected ", Status.DONE);
		
	}
	public void chooseDeliveryType_C(){
		helper.clickLink(driver, bookslot.Collection_loc, bookslot.Collection_btn);
			report.updateTestLog("Book Slot", " Collection Slot is selected ", Status.DONE);
			
		}
	public void clickWeddingCakeOption(){
		Wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(bookslot.weddingcake_lnk)));
		helper.clickLink(driver, bookslot.weddingcake_loc, bookslot.weddingcake_lnk);
		if(helper.isElementPresent(driver, bookslot.calender_loc, bookslot.calender_icon)){
			helper.clickLink(driver, bookslot.calender_loc, bookslot.calender_icon);
			helper.clickLink(driver, bookslot.activedate_loc, bookslot.activedate_lnk);
			helper.clickLink(driver, bookslot.reservethisdate_loc, bookslot.reservethisdate_btn);
			report.updateTestLog("Book Slot", " Wedding cake slot is booked ", Status.PASS);
			
		}
			
	}
	
	public void checkPostcode()
	{
		if(helper.isElementPresent(driver, bookslot.check_loc, bookslot.check_btn))
		helper.clickLink(driver, bookslot.check_loc, bookslot.check_btn);
	}
	
	public void registerAndBookSlot(){
		if(helper.isElementPresent(driver, bookslot.registerandbookslot_loc, bookslot.registerandbookslot)){
			helper.clickLink(driver, bookslot.registerandbookslot_loc, bookslot.registerandbookslot);
			report.updateTestLog("Register and Book slot", "Register and book slot button is clicked", Status.PASS);
			}
		else
			report.updateTestLog("Register and Book slot", "Register and book slot button is not clicked", Status.FAIL);
	}
	
	
	
	
	public void deliveryChargeInterruptionOverlay(){
		if(helper.isElementPresent(driver, bookslot.dcoverlaycontinue_loc, bookslot.dcoverlaycontinue)){
			report.updateTestLog("Interruption overlay", "Delivery charge overlay is present", Status.PASS);
			helper.clickLink(driver, bookslot.dcoverlaycontinue_loc, bookslot.dcoverlaycontinue);
			report.updateTestLog("Interruption overlay", "Delivery charge overlay is handled", Status.DONE);
		}
		else
			report.updateTestLog("Interruption overlay", "Delivery charge overlay is NOT  present", Status.DONE);
			//report.updateTestLog("Interruption overlay", "Delivery charge overlay is NOT handled", Status.DONE);
	}
	public void verifyDCOverlayPresent()
	{
		if(helper.isElementPresent(driver, "xpath", "//div[@class='book-slot book-delivery-slot']/h2")){
			report.updateTestLog("Delivery Charge overlay", "Delivery charge overlay is not present",Status.PASS);
		}
		else
			report.updateTestLog("Delivery Charge overlay", "Delivery charge overlay is present",Status.FAIL);
	}
	
	//a[contains(text(),'Confirm slot change')]
	
	public void enterPostcodeForLightRegisteredUser_OF(){
		String postcode = dataTable.getData("DeliveryDetails_Data", "Postcode");
		helper.typeinTextbox(driver, bookslot.lightpostcode_loc, bookslot.lightpostcode_txt, postcode);
		helper.clickLink(driver, bookslot.postcodecheck_loc, bookslot.postcodecheck_btn);
		//Wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(bookslot.bookslotforlight_btn)));
		//helper.clickLink(driver, bookslot.bookslotforlight_loc, bookslot.bookslotforlight_btn);
		report.updateTestLog("Book Slot", " Postcode is enetered for the light registered user - from book a slot overlay ", Status.PASS);
	}
	
	public void badNewsPopup(){
		Wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(bookslot.badNews_btn)));
		helper.clickLink(driver, bookslot.badNews_loc, bookslot.badNews_btn);
	/*	String postcode = dataTable.getData("DeliveryDetails_Data", "Postcode");
		helper.typeinTextbox(driver, bookslot.postcodecheckbadnewspopup_loc, bookslot.postcodecheckbadnewspopup_btn, postcode);
		helper.clickLink(driver, bookslot.checkpostcodebadnewspopup_loc, bookslot.checkpostcodebadnewspopup_loc);*/
			
	}
	
	public void enterPostcodeForLightRegisteredUser(){
		String postcode = dataTable.getData("DeliveryDetails_Data", "Postcode");
		helper.typeinTextbox(driver, bookslot.lightpostcode_loc, bookslot.lightpostcode_txt, postcode);
		helper.clickLink(driver, bookslot.postcodecheck_loc, bookslot.postcodecheck_btn);
		Wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(bookslot.bookslotforlight_btn)));
		helper.clickLink(driver, bookslot.bookslotforlight_loc, bookslot.bookslotforlight_btn);
		report.updateTestLog("Book Slot", " Postcode is enetered for the light registered user - from book a slot overlay ", Status.PASS);
	}
	
	public void selectFirstSlot() {
		int days = Integer.parseInt(dataTable.getData("General_Data","Slotdayahead"));
		for (int iter=1;iter<=days/7;iter++){
			returnPageSource(1000);
			Wait.until(ExpectedConditions.elementToBeClickable(By.className(bookslot.next7days_link)));
			helper.clickLink(driver, bookslot.next7days_loc, bookslot.next7days_link);
		}
		int day = days%7;
		int count = 3;
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		WebElement element;
		returnPageSource(3000);
		slot:
			for(int i =1,y=1;i<count;i++){
				String ExpectedSlotColumn_Obj=bookslot.SlotTable_table+"["+(day+1)+"]/li";
				WebDriverWait Wait = new WebDriverWait(driver,20);
				//Wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(ExpectedSlotColumn_Obj)));
				Wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(ExpectedSlotColumn_Obj)));
				int No_of_ExpectedSlotColumn_Obj=driver.findElements(By.xpath(ExpectedSlotColumn_Obj)).size();
				for(int k=1; k<=No_of_ExpectedSlotColumn_Obj; k++){
					String ExpectedSlot_Obj=ExpectedSlotColumn_Obj+"["+k+"]/a/span";
					System.out.println("investgating row details with cell details : "+ExpectedSlot_Obj);
					element = driver.findElement(By.xpath(ExpectedSlot_Obj));
					String ExpectedSlot_ObjText=(jse.executeScript("return arguments[0].innerHTML", element)).toString();
					System.out.println("Text available under inspection cell : "+ExpectedSlot_ObjText);
					element = driver.findElement(By.xpath(ExpectedSlotColumn_Obj+"["+k+"]/a"));
					String fullyBooked_ObjText=(jse.executeScript("return arguments[0].innerHTML", element)).toString();
					System.out.println("Text available check for fully booked : "+fullyBooked_ObjText);
					if((!ExpectedSlot_ObjText.contains("Slot unavailable")) && (!fullyBooked_ObjText.contains("Fully booked"))){	
						if(y==1){
							y=2;
							break;
						}
						String slotObject=ExpectedSlot_Obj+"[1]";
						System.out.println("booked slot details : "+ExpectedSlot_ObjText);
						Wait.until(ExpectedConditions.elementToBeClickable(By.xpath(slotObject)));
						helper.clickObject(driver, "xpath", slotObject);
						//driver.findElement(By.xpath(slotObject)).click();
						String validationdate = ExpectedSlot_ObjText.substring(0,ExpectedSlot_ObjText.indexOf(','));
						dataTable.putData("OrderDetails", "SlotDate_DB", validationdate);
						saveOFDate(validationdate);
						saveAWSDate(validationdate);
						report.updateTestLog("Book Slot", "Slot is confirmed for :  "+ExpectedSlot_ObjText, Status.DONE);
						break slot;
					}
				}
				if(days>7){
					helper.clickLink(driver, bookslot.next7days_loc, bookslot.next7days_link);
					returnPageSource(2000);
					days = 1;
				}else{
					days = days+1;	
				}
			}

	}
	
	public void doPostCodeLookUpForBadnews(){
		String postcode = dataTable.getData("General_Data","PostCode");
		Wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(bookslot.postenterbox_txt)));
		if(helper.isElementPresent(driver, bookslot.postenterbox_txt, bookslot.postenterbox_txt)){
			helper.typeinTextbox(driver, bookslot.enterPostcodeForBadnews_loc, bookslot.enterPostcodeForBadnews_textbox, postcode);
			helper.clickLink(driver, bookslot.checkPostcodeForBadnews_loc, bookslot.checkPostcodeForBadnews_btn);
			report.updateTestLog("Book Slot", " Post Code entered and searched for slot", Status.DONE);
		}else{
			helper.typeinTextbox(driver, bookslot.enterPostcode2_loc, bookslot.enterPostcode2_textbox, postcode);
			helper.clickLink(driver, bookslot.checkPostcodeForBadnews_loc, bookslot.checkPostcodeForBadnews_btn);
			report.updateTestLog("Book Slot", " Post Code entered and searched for slot", Status.DONE);
			report.updateTestLog("Book Slot", "Sorry, we don't currently deliver to given postcode ", Status.SCREENSHOT);
		}
	}
	



/*
 * 
 */
	
	public void doPostCodeLookUpForGoodnews(){
		String postcode = dataTable.getData("General_Data","PostCode1");
		Wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(bookslot.postenternewbox_txt)));
		if(helper.isElementPresent(driver, bookslot.postenternewbox_txt, bookslot.postenternewbox_txt)){
			helper.typeinTextbox(driver, bookslot.doPostCodeLookUpForGoodnews_loc, bookslot.doPostCodeLookUpForGoodnews_textbox, postcode);
			helper.clickLink(driver, bookslot.PostCodeLookUpForGoodnews_loc, bookslot.PostCodeLookUpForGoodnews_btn);
			report.updateTestLog("Book Slot", " Post Code entered and searched for slot", Status.DONE);
		}else{
			helper.typeinTextbox(driver, bookslot.enterPostcode3_loc, bookslot.enterPostcode3_textbox, postcode);
			helper.clickLink(driver, bookslot.PostCodeLookUpForGoodnews_loc, bookslot.PostCodeLookUpForGoodnews_btn);
			report.updateTestLog("Book Slot", " Post Code entered and searched for slot", Status.DONE);
			report.updateTestLog("Book Slot", "yes can deliver to given postcode ", Status.SCREENSHOT);
		}
	}
	
	public void selectSlot1() {
		int days = Integer.parseInt(dataTable.getData("General_Data","Slotdayahead"));
		for (int iter=1;iter<=days/7;iter++){
			returnPageSource(1000);
			Wait.until(ExpectedConditions.elementToBeClickable(By.className(bookslot.next7days_link)));
			helper.clickLink(driver, bookslot.next7days_loc, bookslot.next7days_link);
		}
		int day = days%7;
		int count = 3;
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		WebElement element;
		returnPageSource(3000);
		slot:
			for(int i =1,y=1;i<count;i++){
				String ExpectedSlotColumn_Obj=bookslot.SlotTable_table+"["+(day+1)+"]/li";
				System.out.println(ExpectedSlotColumn_Obj);
				WebDriverWait Wait = new WebDriverWait(driver,20);
				//Wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(ExpectedSlotColumn_Obj)));
				//Wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(ExpectedSlotColumn_Obj)));
				int No_of_ExpectedSlotColumn_Obj=driver.findElements(By.xpath(ExpectedSlotColumn_Obj)).size();
				for(int k=2; k<=No_of_ExpectedSlotColumn_Obj; k++){
					String ExpectedSlot_Obj=ExpectedSlotColumn_Obj+"["+k+"]/a/span";
					System.out.println("investgating row details with cell details : "+ExpectedSlot_Obj);
					element = driver.findElement(By.xpath(ExpectedSlot_Obj));
					String ExpectedSlot_ObjText=(jse.executeScript("return arguments[0].innerHTML", element)).toString();
					System.out.println("Text available under inspection cell : "+ExpectedSlot_ObjText);
					element = driver.findElement(By.xpath(ExpectedSlotColumn_Obj+"["+k+"]/a"));
					String fullyBooked_ObjText=(jse.executeScript("return arguments[0].innerHTML", element)).toString();
					System.out.println("Text available check for fully booked : "+fullyBooked_ObjText);
					if((!ExpectedSlot_ObjText.contains("Slot unavailable")) && (!fullyBooked_ObjText.contains("Fully booked"))){	
						if(y==1){
							y=2;
							continue;
						}
						String slotObject="("+ExpectedSlot_Obj+"[2])[2]";
						System.out.println("booked slot details : "+ExpectedSlot_ObjText);
						Wait.until(ExpectedConditions.elementToBeClickable(By.xpath(slotObject)));
						helper.clickObject(driver, "xpath", slotObject);
						//driver.findElement(By.xpath(slotObject)).click();
						String validationdate = ExpectedSlot_ObjText.substring(0,ExpectedSlot_ObjText.indexOf(','));
						dataTable.putData("OrderDetails", "SlotDate_DB", validationdate);
						saveOFDate(validationdate);
						saveAWSDate(validationdate);
						report.updateTestLog("Book Slot", "Slot is confirmed for :  "+ExpectedSlot_ObjText, Status.DONE);
						break slot;
					}
				}
				if(days>7){
					helper.clickLink(driver, bookslot.next7days_loc, bookslot.next7days_link);
					returnPageSource(2000);
					days = 1;
				}else{
					days = days+1;	
				}
			}

	}	


}