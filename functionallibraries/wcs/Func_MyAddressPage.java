package functionallibraries.wcs;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.FrameworkException;
import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import functionallibraries.aws.ReusableComponents;
import pageobjects.wcs.DeliveryAddress;
import pageobjects.wcs.MyAddress;
import pageobjects.wcs.MyOrders;
import pageobjects.wcs.OrderReview;
import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;


/**
 * Functional Library class
 * @author Cognizant
 */
public class Func_MyAddressPage extends ReusableLibrary
{
	/**
	 * Constructor to initialize the functional library
	 * @param scriptHelper The {@link ScriptHelper} object passed from the {@link DriverScript}
	 */
	public Func_MyAddressPage(ScriptHelper scriptHelper){
		super(scriptHelper);
	}
	WebDriverWait Wait = new WebDriverWait(driver,20);
	MyAddress myaddress = new MyAddress();
	DeliveryAddress delivery = new DeliveryAddress();
	ReusableComponents helper = new ReusableComponents(scriptHelper);
	
	public void clickAddNewAddress(){
		Wait.until(ExpectedConditions.elementToBeClickable(By.className( myaddress.addnewaddress_btn)));
		helper.clickLink(driver, myaddress.addnewaddress_loc, myaddress.addnewaddress_btn);
		report.updateTestLog("Home Page", "Add new address button is clicked ", Status.PASS);
	}
	
	public void enterNewAddress(){
		String postcode = dataTable.getData("DeliveryDetails_Data", "Postcode");

		Wait.until(ExpectedConditions.elementToBeClickable(By.xpath( myaddress.checkpostcode_btn)));
		helper.typeinTextbox(driver, myaddress.postcode_loc, myaddress.postcode_txt, postcode);
		helper.clickObject(driver, myaddress.checkpostcode_loc, myaddress.checkpostcode_btn);
		
		// Select first address from the list
		Wait.until(ExpectedConditions.elementToBeClickable(By.className( delivery.postcodeaddress_link)));
		List<WebElement> addresslinks = helper.findWebElements(driver, delivery.postcodeaddress_loc, delivery.postcodeaddress_link);
		for(WebElement w:addresslinks){
			w.click();
			break;
		}
		
	
			Date myDate = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd:HH-mm-ss");
			String myDateString = sdf.format(myDate);
			String nickname = dataTable.getData("DeliveryDetails_Data", "NickName");
			nickname = nickname+myDateString;
			System.out.println(nickname);
			String title = dataTable.getData("DeliveryDetails_Data", "Title");
			String firstname = dataTable.getData("DeliveryDetails_Data", "FirstName");
			String lastname = dataTable.getData("DeliveryDetails_Data", "LastName");
			String phonenumber = dataTable.getData("DeliveryDetails_Data", "PhoneNumber");
			
			Wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(delivery.nickName_txt)));
			helper.typeinTextbox(driver, delivery.nickName_loc, delivery.nickName_txt, nickname);
			helper.typeinTextbox(driver, delivery.title_loc, delivery.title_txt, title);
			helper.typeinTextbox(driver, delivery.firstName_loc, delivery.firstName_txt, firstname);
			helper.typeinTextbox(driver, delivery.lastName_loc, delivery.lastName_txt, lastname);
			helper.typeinTextbox(driver, delivery.phonenumber_loc, delivery.phonenumber_txt, phonenumber);
			helper.clickObject(driver, myaddress.savethisaddress_loc, myaddress.savethisaddress_btn);
			
			// check the address is saved or not
			Wait.until(ExpectedConditions.elementToBeClickable(By.xpath(myaddress.defaultaddress_chkbx)));
			if(helper.isElementPresent(driver, myaddress.defaultaddress_loc, myaddress.defaultaddress_chkbx))
			report.updateTestLog ("My Address Page", "Address is added successfully Postcode: " +postcode, Status.PASS);
			else report.updateTestLog ("My Address Page", "Address is added successfully Postcode: " +postcode, Status.FAIL);
		
		
	}
	
	
}