package functionallibraries.wcs;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.cognizant.framework.Status;

import functionallibraries.aws.ReusableComponents;
import pageobjects.wcs.FastMail;
import pageobjects.wcs.Login;
import pageobjects.wcs.OrderConfirmation;
import pageobjects.wcs.OrderReview;
import pageobjects.wcs.PYO;
import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;
import functionallibraries.wcs.Func_Login;

public class Func_FastMail extends ReusableLibrary{

	public Func_FastMail(ScriptHelper scriptHelper){
		super(scriptHelper);
	}

	ReusableComponents helper = new ReusableComponents(scriptHelper);
	FastMail fastmail = new FastMail();
	OrderConfirmation confirmation = new OrderConfirmation();
	OrderReview orderreview = new OrderReview();
	PYO pyo = new PYO();
	Func_Login login = new Func_Login(scriptHelper);
	public void verifyOrderConfirmationMail(){
		LaunchFastMailApplication();
		performLogin();
		checkOrderConfirmationMail();
	}
	
	public void verifyAmendOrderConfirmationMail(){
		LaunchFastMailApplication();
		performLogin();
		checkAmendOrderConfirmationMail();
	}
	
	public void LaunchFastMailApplication(){
		//String url = dataTable.getData("FastMail", "URL_FastMail");
		String url = properties.getProperty("FastMailURL");
		driver.get(url);
		report.updateTestLog("Fast Mail ", " Launched Fast Mail application ", Status.DONE);
		if(helper.findWebElement(driver, fastmail.login_loc, fastmail.login_txt).isDisplayed()){
			report.updateTestLog("Fast Mail ", " Login Page is displayed ", Status.PASS);
		}else
			report.updateTestLog("Fast Mail ", " Login Page is not displayed ", Status.FAIL);
		}
	
	public void LaunchFastMailApplicationforrestpasswordmail(){
		//String url = dataTable.getData("FastMail", "URL_FastMail");
		String url = properties.getProperty("FastMailURL");
		driver.get(url);
		report.updateTestLog("Fast Mail ", " Launched Fast Mail application ", Status.DONE);
		//if(helper.findWebElement(driver, fastmail.login_loc, fastmail.login_txt).isDisplayed()){
			report.updateTestLog("Fast Mail ", " Login Page is displayed ", Status.PASS);
		//}else	
		report.updateTestLog("Fast Mail ", " Login Page is not displayed ", Status.FAIL);
		}
	
	public void performLogin(){
		/*String username = dataTable.getData("FastMail", "fastmailusername");
		String password = dataTable.getData("FastMail", "fastmailpassword");*/
		String username = properties.getProperty("fastmailusername");
		String password = properties.getProperty("fastmailpassword");
		helper.typeinTextbox(driver, fastmail.emailid_loc, fastmail.emailid_box, username);
		helper.typeinTextbox(driver, fastmail.password_loc, fastmail.password_box, password);
		helper.clickLink(driver, fastmail.logon_loc, fastmail.logon_btn);
		report.updateTestLog("Fast Mail ", " Performed Fastmail Login ", Status.PASS);
	}
	
	public void checkOrderConfirmationMail(){
		try{
		String orderid = dataTable.getData("OrderDetails", "OrderNumber").trim();
		if(!orderid.equalsIgnoreCase("")){
			report.updateTestLog("Fast Mail ", " Order ID :  "+orderid, Status.DONE);
			int k =1;
			do{
				helper.typeinTextbox(driver, fastmail.searchmail_loc, fastmail.searchmail_box, orderid);
				helper.findWebElement(driver, fastmail.searchmail_loc, fastmail.searchmail_box).sendKeys(Keys.ENTER);
				k++;
				if(helper.findWebElements(driver, fastmail.mailList_loc, fastmail.mailList_lnk).size() >0){
					break;
				}
				helper.checkPageReadyState(500);
				}while(k<8);
			report.updateTestLog("Fast Mail ", " Mail search is done ", Status.DONE);
			List<WebElement> mail = helper.findWebElements(driver, fastmail.mailList_loc, fastmail.mailList_lnk);
			report.updateTestLog("Fast Mail ", " Number of Mails displayed is :  "+mail.size(), Status.DONE);
			String xpath,content;
			if(mail.size() <= 0){
				report.updateTestLog("Fast Mail ", "No Mail is displayed for the order : "+orderid, Status.FAIL);
			}
			for(int i =1;i<=mail.size();i++){
				xpath = "";
				xpath = fastmail.mailList_lnk+"["+i+"]";
				System.out.println(xpath);
				if(driver.findElement(By.xpath(xpath)).isDisplayed()){
					xpath = xpath + "//span[@class='v-MailboxItem-subject u-ellipsis']";
					System.out.println(xpath);
					content = driver.findElement(By.xpath(xpath)).getAttribute("title");
					System.out.println(content);
					if(content.trim().equalsIgnoreCase("Your order confirmation - "+orderid)){
						report.updateTestLog("Fast Mail ", " Order Confirmation mail is received ", Status.PASS);
						timedelay();
						driver.findElement(By.xpath(xpath)).click();
						helper.checkPageReadyState(500);
						report.updateTestLog("Fast Mail ", " Screenshot for reference ", Status.SCREENSHOT);
						break;
					}
				}
				if(i==mail.size()){
					report.updateTestLog("Fast Mail ", " Order Confirmation mail is not received ", Status.FAIL);
				}
			}
			
		}else{
			report.updateTestLog("Fast Mail ", " Order id is not updated in sheet ", Status.FAIL);
		}
	
	}
	catch(Exception e){
		e.printStackTrace();
	}
	}
	
	public void checkAmendOrderConfirmationMail(){
		String orderid = dataTable.getData("OrderDetails", "OrderNumber").trim();
		if(!orderid.equalsIgnoreCase("")){
			report.updateTestLog("Fast Mail ", " Order ID :  "+orderid, Status.DONE);
			int k =1;
			do{
				helper.typeinTextbox(driver, fastmail.searchmail_loc, fastmail.searchmail_box, orderid);
				helper.findWebElement(driver, fastmail.searchmail_loc, fastmail.searchmail_box).sendKeys(Keys.ENTER);
				k++;
				if(helper.findWebElements(driver, fastmail.mailList_loc, fastmail.mailList_lnk).size() >0){
					break;
				}
				helper.checkPageReadyState(8000);
				}while(k<8);
			report.updateTestLog("Fast Mail ", " Mail search is done ", Status.DONE);
			List<WebElement> mail = helper.findWebElements(driver, fastmail.mailList_loc, fastmail.mailList_lnk);
			report.updateTestLog("Fast Mail ", " Number of Mails displayed is :  "+mail.size(), Status.DONE);
			String xpath,content;
			if(mail.size() <= 0){
				report.updateTestLog("Fast Mail ", "No Mail is displayed for the order : "+orderid, Status.FAIL);
			}
			for(int i =1;i<=mail.size();i++){
				xpath = "";
				xpath = fastmail.mailList_lnk+"["+i+"]";
				System.out.println(xpath);
				if(driver.findElement(By.xpath(xpath)).isDisplayed()){
					xpath = xpath + "//span[@class='v-MailboxItem-subject u-ellipsis']";
					System.out.println(xpath);
					content = driver.findElement(By.xpath(xpath)).getAttribute("title");
					System.out.println(content);
					if(content.trim().equalsIgnoreCase("Amendment to your order - "+orderid)){
						report.updateTestLog("Fast Mail ", "Amend Order Confirmation mail is received ", Status.PASS);
						driver.findElement(By.xpath(xpath)).click();
						helper.checkPageReadyState(30000);
						report.updateTestLog("Fast Mail ", "Screenshot for reference ", Status.SCREENSHOT);
						break;
					}
				}
				if(i==mail.size()){
					report.updateTestLog("Fast Mail ", " Order Confirmation mail is not received ", Status.FAIL);
				}
			}
			
		}else{
			report.updateTestLog("Fast Mail ", " Order id is not updated in sheet ", Status.FAIL);
		}
	}
	
	public void validateGiftVoucherApplied(){
		if(helper.isElementPresent(driver, "xpath", "//td[contains(text(),'"+confirmation.vouchervalue+"')]"))
			report.updateTestLog("Fast Mail ", " Gift voucher is applied  ", Status.PASS);
		else report.updateTestLog("Fast Mail ", " Gift voucher is not applied  ", Status.FAIL);
			
	}
	public void validateGiftCardApplied(){
		if(helper.isElementPresent(driver, "xpath", "//td[contains(text(),'"+confirmation.cardvalue+"')]"))
			report.updateTestLog("Fast Mail ", " Gift card is applied  ", Status.PASS);
		else report.updateTestLog("Fast Mail ", " Gift card is not applied  ", Status.FAIL);
			
	}
	
	public void validateIncentiveCodeApplied(){
		String Incentive = dataTable.getData("PaymentDetails_Data", "IncentiveCode");
		if(helper.isElementPresent(driver, "xpath", "//td[contains(text(),'"+Incentive+"')]"))
			report.updateTestLog("Fast Mail ", "Incentive code is applied  ", Status.PASS);
		else report.updateTestLog("Fast Mail ", " Incentive code is not applied  ", Status.FAIL);
			
	}
	public void validatePYOSavings(){
		if(helper.isElementPresent(driver, "xpath", "//*[contains(text(),'My Offers Savings')]"))
			report.updateTestLog("Fast Mail ", " PYO saving is displayed  ", Status.PASS);
		else report.updateTestLog("Fast Mail ", "PYO saving is not displayed  ", Status.FAIL);
			
	}
	
	public void searchEmailID(){
		String emailid = dataTable.getData("General_Data","Username").trim();
		int k =1;
		do{
			helper.typeinTextbox(driver, fastmail.searchmail_loc, fastmail.searchmail_box, emailid);
			helper.findWebElement(driver, fastmail.searchmail_loc, fastmail.searchmail_box).sendKeys(Keys.ENTER);
			k++;
			if(helper.findWebElements(driver, fastmail.mailList_loc, fastmail.mailList_lnk).size() >0){
				break;
			}
			helper.checkPageReadyState(500);
			}while(k<8);
		report.updateTestLog("Fast Mail ", " Mail search is done ", Status.DONE);
	}
	
	public void openPYOConfirmationMail(){
		helper.clickLink(driver, fastmail.pyoconfirmation_loc, fastmail.pyoconfirmation_lnk);
		if(helper.isElementPresent(driver,"xpath", "//h2[contains(text(),'Your selected Pick Your Own offers are shown below:')]"))
		report.updateTestLog("Fast Mail ", " PYO confirmation mail is recieved ", Status.PASS);
		else report.updateTestLog("Fast Mail ", " PYO confirmation mail is not recieved ", Status.FAIL);
	}
	
	public void validateSelectedPYOProducts(){
		int count = pyo.PYOProductID.size();
		int i=0;
	for(String prodid :pyo.PYOProductID){
		if(helper.isElementPresent(driver, "xpath", "//img[contains(@src,'"+prodid+"')]")){
			i++;
			report.updateTestLog("Fast Mail ", " PYO selected product: "+prodid+" is displayed in mail", Status.DONE);
		}else report.updateTestLog("Fast Mail ", " PYO selected product: "+prodid+" is displayed in mail", Status.FAIL);
	}
	
		if(count==i)
		report.updateTestLog("Fast Mail ", "All the products in set builder is available in mail", Status.PASS);
		else 	report.updateTestLog("Fast Mail ", "All the products in set builder is not available in mail", Status.FAIL);
	}
	
	public void logout(){
		helper.clickLink(driver, fastmail.myaccount_loc, fastmail.myaccount_lnk);
		helper.clickLink(driver, fastmail.logout_loc, fastmail.logout_lnk);
		report.updateTestLog("Fast Mail ", "Logout is successfull", Status.DONE);
	}
	
	public void validateCarrierBag(){
		if(helper.isElementPresent(driver, "xpath", "//td[contains(text(),'"+orderreview.carrierbagchargevalue+"')]"))
			report.updateTestLog("Fast Mail ", "Carrier charge is applied  ", Status.PASS);
		else report.updateTestLog("Fast Mail ", " Carrier charge is not applied  ", Status.FAIL);
	}
	
	public void validateAPDdetails()
	{
		String text = helper.getText(driver,fastmail.apdtext_loc , fastmail.apdtext);
		if(helper.isElementPresent(driver,fastmail.apdtext_loc , fastmail.apdtext)){
			String txt = helper.getText(driver,fastmail.apdtext_loc , fastmail.apdtext);
			System.out.println(txt);
			report.updateTestLog("APD text", "Partnership discount text is available", Status.PASS);
		}
		String Incentive = dataTable.getData("PaymentDetails_Data", "APDCode");
		try{
			if(!helper.isElementPresent(driver, "xpath", "//td[contains(text(),'"+Incentive+"')]"))
			report.updateTestLog("Fast Mail ", "Parnership card number is not displayed in the mail", Status.PASS);
		else report.updateTestLog("Fast Mail ", "Parnership card number is not displayed in the mail", Status.FAIL);
	}
		catch(Exception e){
			report.updateTestLog("Fast mail", "partnership card number is not displayed", Status.PASS);	
}
	}
	public void validateMannedCollectionLocation(){
		String mannedcollectionlocation = dataTable.getData("General_Data", "MannedCollectionStore");	
		if(helper.isElementPresent(driver, "xpath", "//strong[contains(text(),'"+mannedcollectionlocation+"')]"))
			report.updateTestLog("Validate manned collection location in mail", "Manned collection location is selected as "+mannedcollectionlocation,Status.PASS);
		else
			report.updateTestLog("Validate manned collection location in mail", "Manned collection location is not present" ,Status.FAIL);	
				
	}
	
	public void openResetLoginDetailsMail(){
		try{
			
			Thread.sleep(30000);
			driver.navigate().refresh();
		}catch (Exception e){
		}
		helper.clickLink(driver, fastmail.resetlogindeatials_loc, fastmail.resetlogindeatials_lnk);
		if(helper.isElementPresent(driver,"xpath", "(//div[contains(text(),' You have requested to create a new password ')])"))
		report.updateTestLog("Fast Mail ", " resetlogindeatials mail is recieved ", Status.PASS);
		else report.updateTestLog("Fast Mail ", " resetlogindeatials mail is not recieved ", Status.FAIL);
	}
	//span[contains(text(),'waitrose.com password successfully reset')]
	public void openResetLoginDetailsMailToChecklinkExpired(){

		helper.clickLink(driver, fastmail.resetlogindeatials_loc, fastmail.resetlogindeatials_lnk);
		if(helper.isElementPresent(driver,"xpath", "//h1[contains(text(),'Sorry, this link has now expired')]"))
			report.updateTestLog("Fast Mail ", " resetlogindeatials mail is expired ", Status.PASS);
		else report.updateTestLog("Fast Mail ", " resetlogindeatials mail is not expired ", Status.FAIL);
	}
	
	public void passwordSuccessfullyReset(){
		try{
			
			Thread.sleep(8000);
			driver.navigate().refresh();
		}catch (Exception e){
		}
		helper.isElementPresent(driver, "xpath", "//span[contains(text(),'waitrose.com password successfully reset')]");
		report.updateTestLog("Fast Mail ", " resetlogindeatials mail is recieved ", Status.PASS);
		report.updateTestLog("Fast Mail ", " resetlogindeatials mail is not recieved ", Status.FAIL);
	}
	public void createnewpasswordlink(){
		String parentHandle = driver.getWindowHandle();
		helper.clickLink(driver, fastmail.createnewpasswordlink_loc, fastmail.createnewpasswordlink_lnk);
		for (String winHandle : driver.getWindowHandles()) {
		    driver.switchTo().window(winHandle); 
		    }
		
		if(helper.isElementPresent(driver,"xpath", "//h1[contains(text(),'Create a new password')]")){
			login.createNewPassword();
			login.clickContinuelink();
			driver.close();
			driver.switchTo().window(parentHandle);
			report.updateTestLog("password", "createnewpasswordlink is clicked", Status.PASS);}
		else 
			report.updateTestLog("password", "createnewpasswordlink is not clicked", Status.FAIL);
	}
	public void timedelay() throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(driver,30);
	    boolean pageLoaded =false;
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			do{
				if((executor.executeScript("return document.readyState").equals("complete")))
				{
					System.out.println("Page completely loaded successfully");
					pageLoaded=true;

				} else{
					Thread.sleep(1000);
				}
			}
			while(pageLoaded==false);
	}

}
	