package functionallibraries.wcs;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.FrameworkException;
import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import allocator.Allocator;
import functionallibraries.aws.ReusableComponents;
import pageobjects.wcs.DeliveryAddress;
import pageobjects.wcs.PaymentDetails;
import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;




/**
 * Functional Library class
 * @author Cognizant
 */
public class Func_PaymentDetails extends ReusableLibrary
{
	/**
	 * Constructor to initialize the functional library
	 * @param scriptHelper The {@link ScriptHelper} object passed from the {@link DriverScript}
	 */
	public Func_PaymentDetails(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}
	WebDriverWait Wait = new WebDriverWait(driver,20);
	PaymentDetails payment = new PaymentDetails();
	DeliveryAddress delivery = new DeliveryAddress();
	ReusableComponents helper = new ReusableComponents(scriptHelper);

	public void enterPaymentDetailsAndSave(){
		String cardNumber = dataTable.getData("PaymentDetails_Data", "CardNumber");
		String endMonth = dataTable.getData("PaymentDetails_Data", "EndMonth");
		String endYear = dataTable.getData("PaymentDetails_Data", "EndYear");
		String cardName = dataTable.getData("PaymentDetails_Data", "CardName");
		Wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(payment.paymentpage_header)));
		helper.typeinTextbox(driver, payment.cardNumber_loc, payment.cardNumber_txt, cardNumber);
		helper.SelectValuefromDropDown1(driver, payment.endMonth_loc, payment.endMonth_dropdown, endMonth);
		helper.SelectValuefromDropDown1(driver, payment.endYear_loc, payment.endYear_dropdown, endYear);
		helper.typeinTextbox(driver, payment.enterName_loc, payment.enterName_txt, cardName);
		//helper.clickLink(driver, payment.save_loc,payment.save_chckbox);
		report.updateTestLog("Payment Details Page", " Entered Payment Card Details and saved ", Status.DONE);
	}
	
	public void enterPaymentDetailsAndSave2(){
		String cardNumber = dataTable.getData("PaymentDetails_Data", "CardNumber1");
		String endMonth = dataTable.getData("PaymentDetails_Data", "EndMonth");
		String endYear = dataTable.getData("PaymentDetails_Data", "EndYear");
		String cardName = dataTable.getData("PaymentDetails_Data", "CardName");
		Wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(payment.paymentpage_header)));
		helper.typeinTextbox(driver, payment.cardNumber_loc, payment.cardNumber_txt, cardNumber);
		helper.SelectValuefromDropDown1(driver, payment.endMonth_loc, payment.endMonth_dropdown, endMonth);
		helper.SelectValuefromDropDown1(driver, payment.endYear_loc, payment.endYear_dropdown, endYear);
		helper.typeinTextbox(driver, payment.enterName_loc, payment.enterName_txt, cardName);
		//helper.clickLink(driver, payment.save_loc,payment.save_chckbox);
		report.updateTestLog("Payment Details Page", " Entered Payment Card Details and saved ", Status.DONE);
	}

	public void clickPlaceOrder_Res(){
		Wait.until(ExpectedConditions.elementToBeClickable(By.id(payment.placeorder_res)));
		helper.checkPageReadyState(2000);
		
			if(helper.isElementPresent(driver, payment.placeorder_res_loc, payment.placeorder_res)){
				helper.clickObject(driver, payment.placeorder_res_loc, payment.placeorder_res);
				report.updateTestLog("Place order", "Place order button is clicked", Status.PASS);}
			else
				report.updateTestLog("Place order", "Place order button is not clicked", Status.FAIL);
	}
	
	
	public void clickPlaceOrderUpper(){
		Wait.until(ExpectedConditions.elementToBeClickable(By.id(payment.placeorderupper_btn)));
		helper.checkPageReadyState(2000);
		//helper.clickLink(driver, payment.placeorderupper_loc, payment.placeorderupper_btn);
		if(helper.isElementPresent(driver,payment.placeorderupper_loc, payment.placeorderupper_btn)){
			for(int i =1;i<3;i++){
				try{
				if(helper.isElementPresent(driver, payment.placeorderupper_loc, payment.placeorderupper_btn)){
					helper.clickObject(driver, payment.placeorderupper_loc, payment.placeorderupper_btn);
					helper.checkPageReadyState(2000);
				}
				}catch(Exception e){
					
				}
			}
			
			report.updateTestLog("Payment Details Page", " Place Order is selected ", Status.PASS);
		}else{
			report.updateTestLog("Payment Details Page", " Place Order is not displayed ", Status.FAIL);
		}
	}

	public void applyGiftCard(){
		String giftcard = dataTable.getData("PaymentDetails_Data", "GiftCard");
		String giftcard_pin = dataTable.getData("PaymentDetails_Data", "Giftcard_Pin");
		helper.clickLink(driver, payment.viewGiftCard_loc, payment.viewGiftCard_lnk);
		helper.typeinTextbox(driver, payment.GiftcardNum_loc, payment.GiftcardNum_box, giftcard);
		helper.typeinTextbox(driver, payment.GiftPin_loc, payment.GiftPin_box, giftcard_pin);
		helper.clickLink(driver, payment.ApplyGift_loc, payment.ApplyGift_btn);
		report.updateTestLog("Payment Details Page", " Gift card applied ", Status.DONE);
	}
	
	public void applyGiftCard2(){
		String giftcard = dataTable.getData("PaymentDetails_Data", "GiftCard1");
		helper.checkPageReadyState(1000);
		String giftcard_pin = dataTable.getData("PaymentDetails_Data", "Giftcard_Pin1");
	//	helper.clickLink(driver, payment.viewGiftCard_loc, payment.viewGiftCard_lnk);
		helper.typeinTextbox(driver, payment.GiftcardNum_loc, payment.GiftcardNum_box, giftcard);
		helper.typeinTextbox(driver, payment.GiftPin_loc, payment.GiftPin_box, giftcard_pin);
		helper.clickObject(driver, payment.ApplyGift_loc, payment.ApplyGift_btn);
		report.updateTestLog("Payment Details Page", " Gift card applied ", Status.DONE);
	}
	
	public void clickIWantClaimButton(){
		Wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(payment.acceptAPDDiscount_btn)));
		if(helper.isElementPresent(driver, payment.acceptAPDDiscount_loc, payment.acceptAPDDiscount_btn)){
			report.updateTestLog("APD Card Overlay", " APD Overlay is displayed ", Status.DONE);
			helper.clickLink(driver,payment.acceptAPDDiscount_loc, payment.acceptAPDDiscount_btn);
			report.updateTestLog("APD Card Overlay", " I want discount button is selected", Status.PASS);
		}else{
			report.updateTestLog("APD Card Overlay", " APD Overlay is not displayed ", Status.FAIL);
		}
		
	}
	
	public void clickIDontWantClaimButton(){
		Wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(payment.rejectAPDDiscount_btn)));
		if(helper.isElementPresent(driver, payment.rejectAPDDiscount_loc, payment.rejectAPDDiscount_btn)){
			report.updateTestLog("APD Card Overlay", " APD Overlay is displayed ", Status.DONE);
			helper.clickLink(driver,payment.rejectAPDDiscount_loc, payment.rejectAPDDiscount_btn);
			report.updateTestLog("APD Card Overlay", " I Dont want discount button is selected", Status.PASS);
		}else{
			report.updateTestLog("APD Card Overlay", " APD Overlay is not displayed ", Status.FAIL);
		}
	}
	
	public void choosePayOnCollection(){
		Wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(payment.payoncollection_btn)));
		helper.clickLink(driver, payment.payoncollection_loc, payment.payoncollection_btn);
		report.updateTestLog("Payment", "Pay on collection is choosen", Status.PASS);
	}
	
	public void removeGiftCard(){
		if(helper.isElementPresent(driver, payment.removegiftcard_loc, payment.removegiftcard_lnk)){
		helper.clickLink(driver, payment.removegiftcard_loc, payment.removegiftcard_lnk);
		report.updateTestLog("Payment", "Gift card is removed", Status.PASS);
		
		}else report.updateTestLog("Payment", "Gift card is not removed", Status.FAIL);
	}
	
	public void removeGiftVoucher(){
		if(helper.isElementPresent(driver, payment.removegiftvoucher_loc, payment.removegiftvoucher_lnk)){
			helper.clickLink(driver, payment.removegiftvoucher_loc, payment.removegiftvoucher_lnk);
			report.updateTestLog("Payment", "Gift voucher is removed", Status.PASS);
			
			}else report.updateTestLog("Payment", "Gift voucher is not removed", Status.FAIL);
	}
	
	public void addGiftVoucher(){
		String serialno =dataTable.getData("PaymentDetails_Data" , "GVSerialNumber");
		String code = dataTable.getData("PaymentDetails_Data" , "GVSecurityCode");
		helper.clickLink(driver, payment.viewgiftvoucher_loc, payment.viewgiftvoucher_lnk);
		helper.checkPageReadyState(1000);
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,300)",payment.voucherserialno_txt);
		if(helper.isElementPresent(driver, payment.voucherserialno_loc, payment.voucherserialno_txt)){
			helper.typeinTextbox(driver, payment.voucherserialno_loc, payment.voucherserialno_txt,serialno );
			helper.typeinTextbox(driver, payment.voucherserialcode_loc, payment.voucherserialcode_txt, code);
			helper.clickLink(driver, payment.addgiftvoucher_loc, payment.addgiftvoucher_btn);
			report.updateTestLog("Gift Voucher ", "Gift Voucher textbox exists", Status.DONE);
		}
		else{
			report.updateTestLog("Gift Voucher ", "Gift Voucher textbox does not exist", Status.FAIL);
		}	

	}
	
	public void addGiftVoucher2(){
		String serialno =dataTable.getData("PaymentDetails_Data" , "GVSerialNumber1");
		String code = dataTable.getData("PaymentDetails_Data" , "GVSecurityCode1");
	//	helper.clickLink(driver, payment.viewgiftvoucher_loc, payment.viewgiftvoucher_lnk);
		if(helper.isElementPresent(driver, payment.voucherserialno_loc, payment.voucherserialno_txt)){
			helper.typeinTextbox(driver, payment.voucherserialno_loc, payment.voucherserialno_txt,serialno );
			helper.typeinTextbox(driver, payment.voucherserialcode_loc, payment.voucherserialcode_txt, code);
			helper.clickLink(driver, payment.addgiftvoucher_loc, payment.addgiftvoucher_btn);
			report.updateTestLog("Gift Voucher ", "Gift Voucher textbox exists", Status.DONE);
		}
		else{
			report.updateTestLog("Gift Voucher ", "Gift Voucher textbox does not exist", Status.FAIL);
		}	

	}
	
	public void checkGiftVoucherAndCardAreNonRemovable(){
		if(helper.isElementPresent(driver, payment.vouchercannotberemoved_loc, payment.vouchercannotberemoved_txt))
			report.updateTestLog("Gift Voucher - Amend order", "Gift Voucher cannot be removed message is displayed", Status.PASS);
		else report.updateTestLog("Gift Voucher - Amend order", "Gift Voucher cannot be removed message is not displayed", Status.FAIL);
		
		if(helper.isElementPresent(driver, payment.giftcardcannotberemoved_loc, payment.giftcardcannotberemoved_txt))
			report.updateTestLog("Gift Voucher - Amend order", "Gift card cannot be removed message is displayed", Status.PASS);
		else report.updateTestLog("Gift Voucher - Amend order", "Gift card cannot be removed message is not displayed", Status.FAIL);
	}
public void clickPayWithDifferentCard(){
	if(helper.isElementPresent(driver, payment.paywithdiffcard_loc, payment.paywithdiffcard_lnk)){
		helper.clickLink(driver, payment.paywithdiffcard_loc, payment.paywithdiffcard_lnk);
		report.updateTestLog("Amend Order", "Pay with different card is clicked", Status.DONE);
	}
}

public void addNewBillingAddress(){
	helper.clickLink(driver, payment.newbilladd_loc, payment.newbilladd_radio);
	String title = dataTable.getData("PaymentDetails_Data", "Title");
	String firstname = dataTable.getData("PaymentDetails_Data", "FirstName");
	String lastname = dataTable.getData("PaymentDetails_Data", "LastName");
	String phonenumber = dataTable.getData("PaymentDetails_Data", "PhoneNumber");
	String postcode = dataTable.getData("PaymentDetails_Data", "Postcode");
	Wait.until(ExpectedConditions.elementToBeClickable(By.xpath(delivery.findMyAddress_btn)));
	//Wait.until(ExpectedConditions.presenceOfElementLocated(By.id(delivery.nickName_loc)));
	
	helper.typeinTextbox(driver, delivery.title_loc, delivery.title_txt, title);
	helper.typeinTextbox(driver, delivery.firstName_loc, delivery.firstName_txt, firstname);
	helper.typeinTextbox(driver, delivery.lastName_loc, delivery.lastName_txt, lastname);
	if(driver.findElement(By.id("zipCode")).isEnabled()){
		helper.typeinTextbox(driver, delivery.postcode_loc, delivery.postcode_txt, postcode);
	}
	helper.clickLink(driver, delivery.findMyAddress_loc, delivery.findMyAddress_btn);
	List<WebElement> addresslinks = helper.findWebElements(driver, delivery.postcodeaddress_loc, delivery.postcodeaddress_link);
	for(WebElement w:addresslinks){
		w.click();
		break;
	}
	helper.typeinTextbox(driver, delivery.phonenumber_loc, delivery.phonenumber_txt, phonenumber);
	helper.clickLink(driver, delivery.savebillingaddress_loc, delivery.savebillingaddress);
	report.updateTestLog("New Billing Address", "New billing address in added", Status.PASS);
	
}

public void validatePayWithDifferentCardLInkNotAvailable(){
	List<WebElement> paydiff = driver.findElements(By.xpath(payment.paywithdiffcard_lnk));
	if(paydiff.size()==0)	
		report.updateTestLog("Amend Order", "Pay with different card is not present", Status.PASS);
	else 	report.updateTestLog("Amend Order", "Pay with different card is not present", Status.FAIL);
	
}

public void addBillingAddress(){
	//helper.clickLink(driver, payment.newbilladd_loc, payment.newbilladd_radio);
	String title = dataTable.getData("PaymentDetails_Data", "Title");
	String firstname = dataTable.getData("PaymentDetails_Data", "FirstName");
	String lastname = dataTable.getData("PaymentDetails_Data", "LastName");
	String phonenumber = dataTable.getData("PaymentDetails_Data", "PhoneNumber");
	String postcode = dataTable.getData("PaymentDetails_Data", "Postcode");
	Wait.until(ExpectedConditions.elementToBeClickable(By.xpath(delivery.findMyAddress_btn)));
	//Wait.until(ExpectedConditions.presenceOfElementLocated(By.id(delivery.nickName_loc)));
	
	helper.typeinTextbox(driver, delivery.title_loc, delivery.title_txt, title);
	helper.typeinTextbox(driver, delivery.firstName_loc, delivery.firstName_txt, firstname);
	helper.typeinTextbox(driver, delivery.lastName_loc, delivery.lastName_txt, lastname);
	if(driver.findElement(By.id("zipCode")).isEnabled()){
		helper.typeinTextbox(driver, delivery.postcode_loc, delivery.postcode_txt, postcode);
	}
	helper.clickLink(driver, delivery.findMyAddress_loc, delivery.findMyAddress_btn);
	List<WebElement> addresslinks = helper.findWebElements(driver, delivery.postcodeaddress_loc, delivery.postcodeaddress_link);
	for(WebElement w:addresslinks){
		w.click();
		break;
	}
	helper.typeinTextbox(driver, delivery.phonenumber_loc, delivery.phonenumber_txt, phonenumber);
	report.updateTestLog("New Billing Address", "New billing address in added", Status.PASS);
	
}

public void enterPaymentDetailsAndSave_Myaccount(){
	String cardNumber = dataTable.getData("PaymentDetails_Data", "CardNumber");
	String endMonth = dataTable.getData("PaymentDetails_Data", "EndMonth");
	String endYear = dataTable.getData("PaymentDetails_Data", "EndYear");
	String cardName = dataTable.getData("PaymentDetails_Data", "CardName");
	Wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(payment.paymentpage_header)));
	helper.typeinTextbox(driver, payment.cardNumbertxt_loc, payment.cardNumber, cardNumber);
	helper.SelectValuefromDropDown1(driver, payment.endMonth_loc, payment.endMonth_dropdown, endMonth);
	helper.SelectValuefromDropDown1(driver, payment.endYear_loc, payment.endYear_dropdown, endYear);
	helper.typeinTextbox(driver, payment.Nameoncard_loc, payment.Nameoncard_txt, cardName);
	helper.clickLink(driver, payment.savebtn_loc,payment.savebtn);
	report.updateTestLog("Payment Details Page", " Entered Payment Card Details and saved ", Status.DONE);
}

}
