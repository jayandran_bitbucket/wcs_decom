package functionallibraries.wcs;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.FrameworkException;
import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import functionallibraries.aws.ReusableComponents;
import pageobjects.wcs.HomePage;
import pageobjects.wcs.Interstitials;
import pageobjects.wcs.Search;
import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;


/**
 * Functional Library class
 * @author Cognizant
 */
public class Func_Interstitials extends ReusableLibrary{
	
	public Func_Interstitials(ScriptHelper scriptHelper){
		super(scriptHelper);
	}
	ReusableComponents helper = new ReusableComponents(scriptHelper);
	Interstitials interstitials = new Interstitials();
	HomePage home_ui = new HomePage();
	Search search = new Search();
	Func_HomePage HomePage = new Func_HomePage(scriptHelper);
	JavascriptExecutor executor = (JavascriptExecutor) driver;
	int check = 1;
	
	public String getInterstitialHeader(){
		WebDriverWait Wait = new WebDriverWait(driver,20);
		Wait.until(ExpectedConditions.elementToBeClickable(helper.findWebElement(driver, interstitials.interstitialheader_loc, interstitials.interstitialheader_txt)));
		WebElement header = helper.findWebElement(driver, interstitials.interstitialheader_loc, interstitials.interstitialheader_txt);
		String headertxt = header.findElement(By.tagName("H1")).getText();
		return headertxt;
	}
	
	public void clickFinishedHere(){
		try{
			Thread.sleep(2000);
		}catch(Exception e){
	}
		String pageheader = getInterstitialHeader().toLowerCase();
		int i = 0;
		do{
		if(pageheader.contains("miss")){
			report.updateTestLog(" Interstitials Page "," Dont Miss Out Page is displayed ", Status.DONE);
			finishedherelogic();
			pageheader = getInterstitialHeader().toLowerCase();
			i = 1;
		}else if(pageheader.contains("did")){
			report.updateTestLog(" Interstitials Page "," Did You Forget Page is displayed ", Status.DONE);
			finishedherelogic();
			pageheader = getInterstitialHeader().toLowerCase();
			i = 1;
		}else if(pageheader.contains("before")){
			report.updateTestLog(" Interstitials Page "," Before You Go Page is displayed ", Status.DONE);
			finishedherelogic();
			//proceedHardConflict();
			clickContinueInSoftConflict();
			i = 0;
		}
		}while(i==1);
	}
	
	public void finishedherelogic(){
		List<WebElement> finishedhere = helper.findWebElements(driver, interstitials.finishedhere_loc, interstitials.finishedhere_btn);
		for(WebElement w: finishedhere){
		if(w.isDisplayed()){
			w.click();
			report.updateTestLog(" Interstitials Page "," Finished Here button is selected ", Status.PASS);
			break;
		}
		}
	}
	
	public void clickContinueInSoftConflict(){
		helper.checkPageReadyState(1000);
		if(helper.findWebElements(driver, interstitials.continuesoftconflict_loc, interstitials.continuesoftconflict_btn).size() > 0){
		List<WebElement> continuebtn = helper.findWebElements(driver, interstitials.continuesoftconflict_loc, interstitials.continuesoftconflict_btn);
		for(WebElement c:continuebtn){
			if(c.isDisplayed()){
				c.click();
				report.updateTestLog(" Interstitials Page "," Soft Conflict continue button is selected ", Status.PASS);
				break;
			}
		}
	}else{
		report.updateTestLog(" Interstitials Page "," Soft Conflict continue button is  not displayed ", Status.DONE);
	}
}
	
	public void proceedHardConflict(){
		helper.checkPageReadyState(3000);
		if(check ==1){
		if(helper.findWebElements(driver, interstitials.removefromtrolley_loc, interstitials.removefromtrolley_lnk).size() > 0){
			report.updateTestLog(" Interstitials Page "," Hard Conflict is displayed ", Status.DONE);
			removeProductFromTrolley();
			clickContinueInSoftConflict();
			clickContinueToShopping();
			performProductSearchAndAddProduct();
			HomePage.clickCheckOutBtn();
			check = -1;
			clickFinishedHere();
		}else{
			report.updateTestLog(" Interstitials Page "," Hard Conflict is not displayed ", Status.DONE);
		}
		}else{
			report.updateTestLog(" Interstitials Page "," Hard Conflict is already performed ", Status.DONE);
		}
	}
	public void proceedHardConflict1(){
		helper.checkPageReadyState(3000);
		if(check ==1){
		if(helper.findWebElements(driver, interstitials.removefromtrolley_loc, interstitials.removefromtrolley_lnk).size() > 0){
			report.updateTestLog(" Interstitials Page "," Hard Conflict is displayed ", Status.DONE);
			removeProductFromTrolley();
			clickContinueInSoftConflict();
//			clickContinueToShopping();
			//performProductSearchAndAddProduct();
			
			check = -1;
			//clickFinishedHere();
		}else{
			report.updateTestLog(" Interstitials Page "," Hard Conflict is not displayed ", Status.DONE);
		}
		}else{
			report.updateTestLog(" Interstitials Page "," Hard Conflict is already performed ", Status.DONE);
		}
	}
	
	public void removeProductFromTrolley(){
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		List<WebElement> removelink = helper.findWebElements(driver,interstitials.removefromtrolley_loc, interstitials.removefromtrolley_lnk);
		for(WebElement r:removelink){
			if(r.isDisplayed()){
				executor.executeScript("arguments[0].click();", r);
			}
		}
		report.updateTestLog(" Hard Conflict Overlay "," Remove conflicted product from trolley is completed ", Status.DONE);
	}
	
	public void clickContinueToShopping(){
		helper.checkPageReadyState(1000);
		if(helper.findWebElements(driver, interstitials.continueShopping_loc, interstitials.continueShopping_btn).size() > 0){
			helper.clickLink(driver, interstitials.continueShopping_loc, interstitials.continueShopping_btn);
			report.updateTestLog(" Continue Shopping Overlay "," Continue Shopping button is selected ", Status.DONE);
		}else{
			report.updateTestLog(" Continue Shopping Overlay "," Continue Shopping button is not displayed ", Status.WARNING);
		}
	}
	
	public void performProductSearchAndAddProduct(){
		String productid = dataTable.getData("General_Data", "Search_ProductID");
		helper.typeinTextbox(driver, home_ui.searchbox_loc, home_ui.search_box, productid);
		helper.findWebElement(driver, home_ui.searchbox_loc, home_ui.search_box).sendKeys(Keys.ENTER);
		helper.typeinTextbox(driver, search.PDPquantity_loc, search.PDPquantity_box, "50");
		helper.clickLink(driver, search.PDPAddtotrolley_loc, search.PDPAddtotrolley_btn);
		report.updateTestLog(" PDP "," Searched and added product to enable minimum checkout ", Status.PASS);
	}
	
	public void addProductFromBeforeYouGoPageAndContinue(){
		String pageheader = getInterstitialHeader().toLowerCase();
		int i = 0;
		do{
		if(pageheader.contains("miss")){
			report.updateTestLog(" Interstitials Page "," Dont Miss Out Page is displayed ", Status.DONE);
			finishedherelogic();
			pageheader = getInterstitialHeader().toLowerCase();
			i = 1;
		}else if(pageheader.contains("did")){
			report.updateTestLog(" Interstitials Page "," Did You Forget Page is displayed ", Status.DONE);
			finishedherelogic();
			pageheader = getInterstitialHeader().toLowerCase();
			i = 1;
		}else if(pageheader.contains("before")){
			report.updateTestLog(" Interstitials Page "," Before You Go Page is displayed ", Status.DONE);
			addPrdFromBYG();
			finishedherelogic();
			//proceedHardConflict();
			clickContinueInSoftConflict();
			i = 0;
		}
		}while(i==1);
	}
	
	public void addPrdFromBYG(){
		List<WebElement> addbtn = helper.findWebElements(driver,search.PLPproductAdd_loc, search.PLPproductAdd_btn);
		if(addbtn.size() >0){
			int i =1 ;
			for(WebElement a:addbtn){
				if(a.isDisplayed()){
					executor.executeScript("arguments[0].click();", a);
					report.updateTestLog(" Interstitials Page - BYG  "," Product is added in Before You Go Page ", Status.PASS);
					break;
				}
				if(i == addbtn.size()){
					report.updateTestLog(" Interstitials Page - BYG  "," Product are not displayed in Before You Go Page ", Status.FAIL);
					break;
				}
				i++;
			}
			
		}else{
			report.updateTestLog(" Interstitials Page - BYG  "," No Products are displayed in Before You Go Page ", Status.FAIL);
		}
	}
	
	
	public void addProductFromDidYouForgetPageAndContinue(){
		String pageheader = getInterstitialHeader().toLowerCase();
		int i = 0;
		do{
		if(pageheader.contains("miss")){
			report.updateTestLog(" Interstitials Page "," Dont Miss Out Page is displayed ", Status.DONE);
			finishedherelogic();
			pageheader = getInterstitialHeader().toLowerCase();
			i = 1;
		}else if(pageheader.contains("did")){
			report.updateTestLog(" Interstitials Page "," Did You Forget Page is displayed ", Status.DONE);
			addPrdFromDYF();
			finishedherelogic();
			pageheader = getInterstitialHeader().toLowerCase();
			i = 1;
		}else if(pageheader.contains("before")){
			report.updateTestLog(" Interstitials Page "," Before You Go Page is displayed ", Status.DONE);
			finishedherelogic();
			//proceedHardConflict();
			clickContinueInSoftConflict();
			i = 0;
		}
		}while(i==1);
	}
	
	
	public void addPrdFromDYF(){
		HomePage hp = new HomePage();
		String validate = hp.favproductid.get(hp.favproductid.size()-1);
		System.out.println("Product ID to validate : "+validate);
		List<WebElement> prds = helper.findWebElements(driver, interstitials.productGrid_loc, interstitials.productGrid_btn);
		for(WebElement p:prds){
			String value = p.getAttribute("data-linenumber");
			if(validate.equalsIgnoreCase(value)){
				report.updateTestLog(" Interstitials Page - DYF  "," Expected product is displayed in Did You Forget Page, Product ID : "+validate, Status.PASS);
			}else{
				report.updateTestLog(" Interstitials Page - DYF  "," Wrong product were displayed in Did You Forget Page ", Status.FAIL);
				report.updateTestLog(" Interstitials Page - DYF  "," Expected Product ID :  "+validate, Status.DONE);
				report.updateTestLog(" Interstitials Page - DYF  "," Actual Product ID :  "+value, Status.DONE);
			}
		}
		List<WebElement> addbtn = helper.findWebElements(driver,search.PLPproductAdd_loc, search.PLPproductAdd_btn);
		if(addbtn.size() >0){
			int i =1 ;
			for(WebElement a:addbtn){
				if(a.isDisplayed()){
					executor.executeScript("arguments[0].click();", a);
					report.updateTestLog(" Interstitials Page - DYF  "," Product is added in Did You Forget Page ", Status.PASS);
					break;
				}
				if(i == addbtn.size()){
					report.updateTestLog(" Interstitials Page - BYG  "," Product are not displayed in Did You Forget Page ", Status.FAIL);
					break;
				}
				i++;
			}
			
		}else{
			report.updateTestLog(" Interstitials Page - BYG  "," No Products are displayed in Did You Forget Page ", Status.FAIL);
		}
	}
	
	
	
	public void addProductFromAllIntertitialPageAndContinue(){
		String pageheader = getInterstitialHeader().toLowerCase();
		int i = 0;
		do{
		if(pageheader.contains("miss")){
			report.updateTestLog(" Interstitials Page "," Dont Miss Out Page is displayed ", Status.DONE);
			completeOfferInMissedOut();
			finishedherelogic();
			pageheader = getInterstitialHeader().toLowerCase();
			i = 1;
		}else if(pageheader.contains("did")){
			report.updateTestLog(" Interstitials Page "," Did You Forget Page is displayed ", Status.DONE);
			addPrdFromDYF();
			finishedherelogic();
			pageheader = getInterstitialHeader().toLowerCase();
			i = 1;
		}else if(pageheader.contains("before")){
			report.updateTestLog(" Interstitials Page "," Before You Go Page is displayed ", Status.DONE);
			addPrdFromBYG();
			finishedherelogic();
			//proceedHardConflict();
			clickContinueInSoftConflict();
			i = 0;
		}
		}while(i==1);
	}
	public void completeOfferInMissedOut(){
		String offertxt = helper.getText(driver, interstitials.addmoremissout_loc, interstitials.addmoremissout_txt);
		offertxt = offertxt.substring(4, 5);
		System.out.println(offertxt);
		int qty = Integer.parseInt(offertxt);
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,300)",interstitials.incproductquantity_btn);
		for(int i=1;i<=qty;i++){
			helper.clickLink(driver, interstitials.incproductquantity_loc, interstitials.incproductquantity_btn);
			
		}
		if(helper.isElementPresent(driver, interstitials.offercomplete_loc, interstitials.offercomplete_txt))
			report.updateTestLog(" Interstitials Page "," Offer complete message is displayed ", Status.PASS);
		else report.updateTestLog(" Interstitials Page "," Offer complete message is not displayed ", Status.FAIL);
		jse.executeScript("window.scrollBy(0,-500)",interstitials.incproductquantity_btn);
		
	}
	
	public void seeAllThisOffer(){
		helper.clickLink(driver, interstitials.seeallthisoffer_loc, interstitials.seeallthisoffer_btn);
		report.updateTestLog(" Interstitials Page "," See All this offer button is clicked ", Status.DONE);
	}
	
	public void CompleteOfferAndClickSeeAllThisOffer(){
		String pageheader = getInterstitialHeader().toLowerCase();

	
		if(pageheader.contains("miss")){
			report.updateTestLog(" Interstitials Page "," Dont Miss Out Page is displayed ", Status.DONE);
			completeOfferInMissedOut();
			seeAllThisOffer();
		
		}
		
	}
	
	
	public void hardConflictValidations(){

		if(helper.isElementPresent(driver, interstitials.hardconflictviewtrolley_loc, interstitials.hardconflictviewtrolley_btn))
			report.updateTestLog(" Hard Conflict ","View Trolley button is displayed ", Status.DONE);
		else report.updateTestLog(" Hard Conflict ","View Trolley button is not displayed ", Status.FAIL);
		if(helper.isElementPresent(driver, interstitials.hardconflictchangeslot_loc, interstitials.hardconflictchangeslot_btn))
			report.updateTestLog(" Hard Conflict ","Change slot button is displayed ", Status.DONE);
		else report.updateTestLog(" Hard Conflict ","Change slot button is not displayed ", Status.FAIL);
		if(helper.isElementPresent(driver, interstitials.continuebtndisabled_loc, interstitials.continuebtndisabled_btn))
			report.updateTestLog(" Hard Conflict ","Continue button is disabled ", Status.DONE);
		else report.updateTestLog(" Hard Conflict ","Continue button is not disabled ", Status.FAIL);
	}
	
	public void viewTrolleyFromHArdConflict(){
		helper.clickLink(driver, interstitials.hardconflictviewtrolley_loc, interstitials.hardconflictviewtrolley_btn);
			report.updateTestLog(" Hard Conflict ","View Trolley button is clicked ", Status.DONE);
	}

	public void joinmyWaitroseAfterCheckout(){
	
		helper.selectRadiobutton(driver, "xpath", "//input[@id='waitrose-2']");
		helper.clickLink(driver, "xpath", "//input[@id='myWaitroseNoThanksSubmit']");
		report.updateTestLog("Join myWaitrose notification while placing order", "No, thanks is selected", Status.PASS);
		
	}
	
}