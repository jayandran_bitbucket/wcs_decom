package functionallibraries.wcs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.cognizant.framework.Status;

import functionallibraries.aws.ReusableComponents;
import pageobjects.wcs.BrowseShop;
import pageobjects.wcs.Search;
import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;

public class Func_BrowseShop extends ReusableLibrary{
	
	public Func_BrowseShop(ScriptHelper scriptHelper){
		super(scriptHelper);
	}
	JavascriptExecutor jse = (JavascriptExecutor)driver;
	WebDriverWait Wait = new WebDriverWait(driver,20);
	ReusableComponents helper = new ReusableComponents(scriptHelper);
	BrowseShop browseshop = new BrowseShop();
	Search search = new Search();
	
	public void performBrowseShopNavigation(){
		
		
		List<String> aw = Arrays.asList(dataTable.getData("General_Data", "BrowseNavigation").split(">"));
		int i =1;
		for(String s : aw){
			if(i == 1){
				selectBrowseCategory(s);
				helper.checkPageReadyState(1000);
			}else if(i == 2){
				selectFirstCategory(s);
				helper.checkPageReadyState(1000);
			}else if(i == 3){
				selectSecondCategory(s);
				helper.checkPageReadyState(1000);
			}else if(i == 4){
				selectThirdCategory(s);
				helper.checkPageReadyState(1000);
			}
			
			if(i == aw.size() && i!=4){
				String element = "//div[@class='dropdown-menu megaMenu']//a[@title='View all "+s+"']";
				helper.clickLink(driver, "xpath", element);
				break;
			}
			i++;
			
			
		}
	}
public void performBrowseShopNavigation1(){
		
		
		List<String> aw = Arrays.asList(dataTable.getData("General_Data", "BrowseNavigation1").split(">"));
		int i =1;
		for(String s : aw){
			if(i == 1){
				selectBrowseCategory(s);
				helper.checkPageReadyState(1000);
			}else if(i == 2){
				selectFirstCategory(s);
				helper.checkPageReadyState(1000);
			}else if(i == 3){
				selectSecondCategory(s);
				helper.checkPageReadyState(1000);
			}else if(i == 4){
				selectThirdCategory(s);
				helper.checkPageReadyState(1000);
			}
			
			if(i == aw.size() && i!=4){
				String element = "//div[@class='dropdown-menu megaMenu']//a[@title='View all "+s+"']";
				helper.clickLink(driver, "xpath", element);
				break;
			}
			i++;
			
			
		}
	}
	
	public void selectBrowseCategory(String s){
		//String category = helper.getText(driver, browseshop.browsecategoryactive_loc, browseshop.browsecategoryactive_lnk).toLowerCase().trim();
		helper.clickLink(driver, "xpath", browseshop.browsecategoryactive_lnk);
		WebElement cate = driver.findElement(By.xpath(browseshop.browsecategoryactive_lnk));
		String category=(jse.executeScript("return arguments[0].innerHTML", cate)).toString();
		if(s.toLowerCase().equalsIgnoreCase(category)){
			report.updateTestLog("Browse Shop", " Default Category is selected  :  "+s, Status.PASS);
		}else{
			helper.clickLink(driver, browseshop.browsecategory_loc, browseshop.browsecategory_lnk);
			report.updateTestLog("Browse Shop", " Default Category is changed to  :  "+s, Status.PASS);
		}
	}
	
	public void selectFirstCategory(String s){
		List<WebElement> firstcategory = helper.findWebElements(driver, browseshop.firstcategory_loc, browseshop.firstcategory_lnk);
		int i =1;
		for(WebElement f:firstcategory){
			if(f.getAttribute("title").toLowerCase().equalsIgnoreCase(s.toLowerCase().trim())){
				f.click();
				report.updateTestLog("Browse S", "Selected isle for first category is :  "+s, Status.PASS);
				break;
			}else if(i == firstcategory.size()){
				report.updateTestLog("Browse S", "first category for isle is not selected, searched category :  "+s, Status.FAIL);
				break;
			}
		}
	}
	
	
	public void selectSecondCategory(String s){
		List<WebElement> firstcategory = helper.findWebElements(driver, browseshop.secondcategory_loc, browseshop.secondcategory_lnk);
		int i =1;
		for(WebElement f:firstcategory){
			if(f.getAttribute("title").toLowerCase().equalsIgnoreCase(s.toLowerCase().trim())){
				f.click();
				report.updateTestLog("Browse S", "Selected isle for Second category is :  "+s, Status.PASS);
				break;
			}else if(i == firstcategory.size()){
				report.updateTestLog("Browse S", "Second category for isle is not selected, searched category :  "+s, Status.FAIL);
				break;
			}
		}
	}
	
	
	public void selectThirdCategory(String s){
		List<WebElement> firstcategory = helper.findWebElements(driver, browseshop.thirdcategory_loc, browseshop.thirdcategory_lnk);
		int i =1;
		for(WebElement f:firstcategory){
			if(f.getAttribute("title").toLowerCase().equalsIgnoreCase(s.toLowerCase().trim())){
				f.click();
				report.updateTestLog("Browse S", "Selected isle for Third category is :  "+s, Status.PASS);
				break;
			}else if(i == firstcategory.size()){
				report.updateTestLog("Browse S", "Third category for isle is not selected, searched category :  "+s, Status.FAIL);
				break;
			}
		}
	}
	
	public void getFilterText(){
		Wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(browseshop.filtersec_lnk)));
		List<WebElement> filter = helper.findWebElements(driver, browseshop.filtersec_loc, browseshop.filtersec_lnk);
		List<String> aw = Arrays.asList(dataTable.getData("General_Data", "BrowseNavigation").split(">"));
		String validation = aw.get(aw.size()-1);
		BrowseShop.filtertags = new ArrayList<String>();
		String tag;
		for(WebElement f:filter){
			tag = f.getText();
			System.out.println(tag);
			if(tag.contains(validation)){
				BrowseShop.filtertags.add(tag.replace(validation, "").trim());
			}
		}
		System.out.println(BrowseShop.filtertags);
		
	}
	
	public void addOfferProductsForMissedOutIntertitial(){
		String offertxt = helper.getText(driver, browseshop.offerred_loc, browseshop.offerred_txt);
		offertxt = offertxt.substring(4, 5);
		System.out.println(offertxt);
		int qty = Integer.parseInt(offertxt);
		String quantity = Integer.toString(qty-1);
		helper.clickLink(driver, browseshop.offerred_loc, browseshop.offerred_txt);
		
		
		int NumberOfProducts = Integer.parseInt(dataTable.getData("General_Data", "NumberOfProducts"));
		List<WebElement> qtyfields = helper.findWebElements(driver, search.PLPproductQty_loc, search.PLPproductQty_field);
		List<WebElement> addBtns = helper.findWebElements(driver, search.PLPproductAdd_loc, search.PLPproductAdd_btn);
		int size = qtyfields.size();
		if(NumberOfProducts<=size){
			report.updateTestLog("Search Page ", NumberOfProducts+" Products are displayed in PLP to add", Status.DONE);
			for(int i=1;i<=NumberOfProducts;i++){
				if(qtyfields.get(i-1).isDisplayed()){
				qtyfields.get(i-1).clear();
				qtyfields.get(i-1).sendKeys(quantity);
				Wait.until(ExpectedConditions.elementToBeClickable(addBtns.get(i-1)));
				//addBtns.get(i-1).sendKeys(Keys.DOWN);
				//addBtns.get(i-1).click();
				jse.executeScript("arguments[0].click();", addBtns.get(i-1));
				}
			}	
			report.updateTestLog("Search Page ", NumberOfProducts+" Products are added to trolley with "+quantity+" each", Status.PASS);
		}else {
			for(int i=1;i<=size;i++){
				report.updateTestLog("Search Page ", size+" Products are displayed in PLP to add", Status.DONE);
				qtyfields.get(i-1).sendKeys(quantity);
				Wait.until(ExpectedConditions.elementToBeClickable(addBtns.get(i-1)));
				//addBtns.get(i-1).sendKeys(Keys.DOWN);
				//addBtns.get(i-1).click();
				jse.executeScript("arguments[0].click();", addBtns.get(i-1));
			}
			report.updateTestLog("Search Page ", size+" Products are added to trolley with "+quantity+" each", Status.PASS);
		}
		
	}
	
	public void chooseOfferFilter(){
		String offercat = dataTable.getData("General_Data", "OfferCatagory"); // For Add 2 for 7, xpath willbe //a not //span
		if(helper.isElementPresent(driver, "xpath", "//span[contains(text(),'"+offercat+"')]")){ 
			helper.clickLink(driver, "xpath", "//span[contains(text(),'"+offercat+"')]");
			report.updateTestLog("Offer category ","Offer filter"+offercat+ " is clicked", Status.PASS);
		}
		
	}
	
	public void chooseOfferFilter1(){
		String offercat = dataTable.getData("General_Data", "OfferCatagory1");
		if(helper.isElementPresent(driver, "xpath", "//span[contains(text(),'"+offercat+"')]")){
			helper.clickLink(driver, "xpath", "//span[contains(text(),'"+offercat+"')]");
			report.updateTestLog("Offer category ","Offer filter"+offercat+ " is clicked", Status.PASS);
		}
	}

public void chooseOnOfferInFilter(){
	helper.clickLink(driver, browseshop.productthatare_loc, browseshop.productthatare_filter);
	if(helper.isElementPresent(driver, browseshop.onoffer_loc, browseshop.onoffer_chkbx)){
		helper.clickLink(driver, browseshop.onoffer_loc, browseshop.onoffer_chkbx);
		report.updateTestLog("Offer Filter ","On offer checkbox is clicked", Status.PASS);
	}
}
}