package functionallibraries.aws;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Status;
import com.sun.java.swing.plaf.windows.resources.windows_es;

import pageobjects.aws.Search;
import pageobjects.wcs.HomePage;
import pageobjects.wcs.PYO;
import supportlibraries.*;


/**
 * Functional library template
 * @author Cognizant
 */
public class Func_AWSSearch extends ReusableLibrary
{
	/**
	 * Constructor to initialize the functional library
	 * @param scriptHelper The {@link ScriptHelper} object passed from the {@link DriverScript}
	 */
	public Func_AWSSearch(ScriptHelper scriptHelper){
		super(scriptHelper);
	}
	WebDriverWait Wait = new WebDriverWait(driver,30);
	Search search = new Search();

	ReusableComponents helper = new ReusableComponents(scriptHelper);
	JavascriptExecutor jse = (JavascriptExecutor)driver;

	public void validateSearchResultsInPLP(){
		String keyword = dataTable.getData("General_Data", "Search").toLowerCase();
		String ProductName;
		helper.checkPageReadyState(2000);
		Wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(search.PLPproductNames_txt)));
		List<WebElement> productName = driver.findElements(By.xpath(search.PLPproductNames_txt));
		int size = productName.size();
		int i =1;
		for(WebElement w:productName){
			ProductName = w.getText().toLowerCase();
			if(!ProductName.contains(keyword)){
				report.updateTestLog("Home Page",  i+" th Product Name is not having "+keyword+" keyword ", Status.FAIL);
				report.updateTestLog("Home Page",  " Resulted product name :  "+ProductName, Status.DONE);
			}else if(i==size){
				report.updateTestLog("Home Page", keyword+" : Keyword Result is displayed for all listed products ", Status.PASS);
				break;
			}
			i++;
		}

		report.updateTestLog("Home Page", " Search Result Validated for : "+i+" Products", Status.DONE);
	}


	public void validateAWSSearchResultsInPLP(){
		String keyword = dataTable.getData("General_Data", "AWS_Search").toLowerCase();
		String ProductName;
		helper.checkPageReadyState(2000);
		Wait.until(ExpectedConditions.presenceOfElementLocated(By.id(search.PLPproduct_table)));
		List<WebElement> productName = driver.findElements(By.id(search.PLPproduct_table));
		int size = productName.size();
		int i =1;
		for(WebElement w:productName){
			ProductName = w.findElement(By.xpath(search.PLPproductNames_txt)).getText().toLowerCase();
			if(!ProductName.contains(keyword)){
				report.updateTestLog("Home Page",  i+" th Product Name is not having "+keyword+" keyword ", Status.FAIL);
				report.updateTestLog("Home Page",  " Resulted product name :  "+ProductName, Status.DONE);
			}else if(i==size){
				report.updateTestLog("Home Page", keyword+" : Keyword Result for AWS Search is displayed for all listed products ", Status.PASS);
				break;
			}
			i++;
		}

		report.updateTestLog("Home Page", " Search Result Validated for : "+i+" Products", Status.DONE);
	}
	
	public void validateAWSSearchResultsInPLP2(){
		String keyword = dataTable.getData("General_Data", "AWS_Search2").toLowerCase();
		String ProductName;
		helper.checkPageReadyState(2000);
		Wait.until(ExpectedConditions.presenceOfElementLocated(By.id(search.PLPproduct_table)));
		List<WebElement> productName = driver.findElements(By.id(search.PLPproduct_table));
		int size = productName.size();
		int i =1;
		for(WebElement w:productName){
			ProductName = w.findElement(By.xpath(search.PLPproductNames_txt)).getText().toLowerCase();
			if(!ProductName.contains(keyword)){
				report.updateTestLog("Home Page",  i+" th Product Name is not having "+keyword+" keyword ", Status.FAIL);
				report.updateTestLog("Home Page",  " Resulted product name :  "+ProductName, Status.DONE);
			}else if(i==size){
				report.updateTestLog("Home Page", keyword+" : Keyword Result for AWS Search is displayed for all listed products ", Status.PASS);
				break;
			}
			i++;
		}

		report.updateTestLog("Home Page", " Search Result Validated for : "+i+" Products", Status.DONE);
	}

	public void enterQtyandAddProductInPLP(){
		Wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(search.PLPproductAdd_btn)));
		String qtyvalue = dataTable.getData("General_Data", "PLP_Qty");
		int qty = Integer.parseInt(dataTable.getData("General_Data", "PLP_Qty"));
		int NumberOfProducts = Integer.parseInt(dataTable.getData("General_Data", "NumberOfProducts"));
		List<WebElement> qtyfields = helper.findWebElements(driver, search.PLPproductQty_loc, search.PLPproductQty_field);
		List<WebElement> addBtns = helper.findWebElements(driver, search.PLPproductAdd_loc, search.PLPproductAdd_btn);
		int size = qtyfields.size();
		if(NumberOfProducts<=size){
			report.updateTestLog("Search Page ", qty+" Products are displayed in PLP to add", Status.DONE);
			for(int i=1;i<=NumberOfProducts;i++){
				if(qtyfields.get(i-1).isDisplayed()){
					helper.checkPageReadyState(1000);
					//helper.checkPageReadyState(300);
					qtyfields.get(i-1).clear();
					qtyfields.get(i-1).sendKeys(qtyvalue);
					Wait.until(ExpectedConditions.elementToBeClickable(addBtns.get(i-1)));
					//addBtns.get(i-1).sendKeys(Keys.DOWN);
					//addBtns.get(i-1).click();
					jse.executeScript("arguments[0].click();", addBtns.get(i-1));
					
				}
			}	
			report.updateTestLog("Search Page ", qty+" Products are added to trolley with "+qtyvalue+" each", Status.PASS);
		}else {
			for(int i=1;i<=size;i++){
				report.updateTestLog("Search Page ", size+" Products are displayed in PLP to add", Status.DONE);
				qtyfields.get(i-1).sendKeys(qtyvalue);
				Wait.until(ExpectedConditions.elementToBeClickable(addBtns.get(i-1)));
				//addBtns.get(i-1).sendKeys(Keys.DOWN);
				//addBtns.get(i-1).click();
				jse.executeScript("arguments[0].click();", addBtns.get(i-1));
			}
			report.updateTestLog("Search Page ", size+" Products are added to trolley with "+qtyvalue+" each", Status.PASS);
		}
	}

	public void performAWSSearch(){
		String searchterm = dataTable.getData("General_Data", "AWS_Search");
		helper.checkPageReadyState(1000);
		if(driver.findElement(By.id("searchClear")).isDisplayed()){
			driver.findElement(By.id("searchClear")).click();
			helper.checkPageReadyState(2000);
		}
		helper.typeinTextbox(driver,search.searchTerm_loc, search.searchTerm_box, searchterm);
		helper.clickLink(driver, search.searchbtn_loc, search.searchbtn);
		report.updateTestLog("AWS Search Page ", "Searched with keyword : "+searchterm, Status.PASS);
	}
	
	public void performAWSSearch2(){
		String searchterm = dataTable.getData("General_Data", "AWS_Search2");
		if(driver.findElement(By.id("searchClear")).isDisplayed()){
			driver.findElement(By.id("searchClear")).click();
			helper.checkPageReadyState(2000);
		}
		helper.typeinTextbox(driver,search.searchTerm_loc, search.searchTerm_box, searchterm);
		helper.clickLink(driver, search.searchbtn_loc, search.searchbtn);
		report.updateTestLog("AWS Search Page ", "Searched with keyword : "+searchterm, Status.PASS);
	}

	public void verifySuggestedTagsIsDisplayed(){
		List<WebElement> we = helper.findWebElements(driver, search.suggestedtag_loc, search.suggestedtag_slide);
		if(we.size() >0){
			int i =1;
			for(WebElement w:we){
				if(w.isDisplayed()){
					report.updateTestLog("AWS Search Page ", "Suggested Tag results are displayed ", Status.PASS);
					report.updateTestLog("AWS Search Page ", " Screenshot for ref ", Status.SCREENSHOT);
					break;
				}
				if(i == we.size()){
					report.updateTestLog("AWS Search Page ", "Suggested Tag results are not displayed ", Status.FAIL);
					break;
				}
			}
		}else{
			report.updateTestLog("AWS Search Page ", "Suggested Tag results are displayed ", Status.FAIL);
		}
	}

	public void clickEntertainingLink(){
		Wait.until(ExpectedConditions.visibilityOf(helper.findWebElement(driver, search.entertaining_loc, search.entertaining_lnk)));
		helper.clickLink(driver, search.entertaining_loc, search.entertaining_lnk);
		report.updateTestLog("AWS Search Page ", "Entertaining Link is selected ", Status.PASS);
	}
	
	public void signInAWS(){
		//Wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[text()='Sign in']")));
		//helper.clickLink(driver, "xpath", "//a[text()='Sign in']");
		driver.get("https://qa.ecom.waitrose.com/pimkj07l4f945vjqk");
		helper.typeinTextbox(driver, "id", "loginEmail", dataTable.getData("General_Data", "Username"));
		helper.typeinTextbox(driver, "id", "loginPassword", dataTable.getData("General_Data", "Password"));
		helper.clickLink(driver, "id", "loginButton");
		report.updateTestLog("AWS Search Page ", "Temporary Login done ", Status.DONE);
		helper.checkPageReadyState(2000);
		//helper.clickLink(driver, "xpath", "//a[text()='Waitrose.com']");
		//driver.navigate().refresh();
	}
	
	public void searchFavProductAndAddToTrolley(){
		if(HomePage.favproductid.size() > 0){
			int i =1;
			for(String s: HomePage.favproductid){
				if(i == HomePage.favproductid.size()){
					break;
				}
				if(helper.isElementPresent(driver,search.clearall_loc , search.clearall_btn)){
					helper.clickLink(driver,search.clearall_loc , search.clearall_btn);
					helper.checkPageReadyState(1000);
				}
				helper.typeinTextbox(driver,search.searchTerm_loc, search.searchTerm_box, s);
				helper.clickLink(driver, search.searchbtn_loc, search.searchbtn);
				helper.checkPageReadyState(2000);
				report.updateTestLog("AWS Search Page ", " Product Search : "+s, Status.PASS);
				List<WebElement> addBtns = helper.findWebElements(driver, search.PLPproductAdd_loc, search.PLPproductAdd_btn);
				List<WebElement> qtyfields = helper.findWebElements(driver, search.PLPproductQty_loc, search.PLPproductQty_field);
				Wait.until(ExpectedConditions.elementToBeClickable(addBtns.get(addBtns.size()-1)));
				qtyfields.get(qtyfields.size()-1).clear();
				qtyfields.get(qtyfields.size()-1).sendKeys("50");
				jse.executeScript("arguments[0].click();", addBtns.get(addBtns.size()-1));
				verifyFavLogo();
				i++;
				
			}
		}else{
			report.updateTestLog("AWS Search Page ", " No Product ID are updated to validate ", Status.FAIL);
		}
		
	}
	
	public void addSingleProductInPLP(){
		int NumberOfProducts = Integer.parseInt(dataTable.getData("General_Data", "NumberOfProducts"));
		List<WebElement> qtyfields = helper.findWebElements(driver, search.PLPproductQty_loc, search.PLPproductQty_field);
		List<WebElement> addBtns = helper.findWebElements(driver, search.PLPproductAdd_loc, search.PLPproductAdd_btn);
		int size = qtyfields.size();
		if(addBtns.size() > 0){
			for(int i=1;i<=NumberOfProducts;i++){
				if(qtyfields.get(i-1).isDisplayed()){
					helper.checkPageReadyState(1000);
					//helper.checkPageReadyState(300);
					qtyfields.get(i-1).clear();
					qtyfields.get(i-1).sendKeys("1");
					Wait.until(ExpectedConditions.elementToBeClickable(addBtns.get(i-1)));
					//addBtns.get(i-1).sendKeys(Keys.DOWN);
					//addBtns.get(i-1).click();
					jse.executeScript("arguments[0].click();", addBtns.get(i-1));
					break;
					
				}
			}	
			report.updateTestLog("Search Page ", " Added single quantity of the product", Status.PASS);
		}else {
			report.updateTestLog("Search Page ", " No Product is displayed ", Status.FAIL);
		}
	}
	
	public void performPYOProductsSearchAndAddToTrolley(){
		if(PYO.PYOProductID.size() > 0){
			for(String s : PYO.PYOProductID){
				Wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.id(search.clearall_btn)));
				if(helper.isElementPresent(driver,search.clearall_loc , search.clearall_btn)){
					helper.clickLink(driver,search.clearall_loc , search.clearall_btn);
					helper.checkPageReadyState(1000);
				}
				helper.typeinTextbox(driver,search.searchTerm_loc, search.searchTerm_box, s);
				helper.clickLink(driver, search.searchbtn_loc, search.searchbtn);
				report.updateTestLog("AWS Search Page ", " Searched PYO Product ID : "+s, Status.PASS);
				helper.checkPageReadyState(2000);
				addSingleProductInPLP();
				verifyPYOLogo();
				verifyFavLogo();
				break;
				
			}
		}else{
			report.updateTestLog("AWS Search Page ", "No PYO Products is captured", Status.FAIL);
		}
	}
	
	public void verifyPYOLogo(){
		if(helper.findWebElements(driver, search.Pyo_loc, search.Pyo_logo).size() > 0 ){
			if(helper.isElementPresent(driver, search.Pyo_loc, search.Pyo_logo)){
				report.updateTestLog("AWS Search Page ", "PYO Logo is displayed", Status.PASS);
			}else{
				report.updateTestLog("AWS Search Page ", "PYO Logo is not displayed", Status.FAIL);
			}
		}else{
			report.updateTestLog("AWS Search Page ", "PYO Logo is not displayed", Status.FAIL);
		}
	}
	
	public void verifyFavLogo(){
		if(helper.findWebElements(driver, search.fav_loc, search.fav_logo).size() > 0 ){
			if(helper.isElementPresent(driver,search.fav_loc, search.fav_logo)){
				report.updateTestLog("AWS Search Page ", "PYO Logo is displayed", Status.PASS);
			}else{
				report.updateTestLog("AWS Search Page ", "PYO Logo is not displayed", Status.FAIL);
			}
		}else{
			report.updateTestLog("AWS Search Page ", "PYO Logo is not displayed", Status.FAIL);
		}
	}



}