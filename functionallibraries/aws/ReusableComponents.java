package functionallibraries.aws;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;

public class ReusableComponents extends ReusableLibrary {
	public ReusableComponents(ScriptHelper scriptHelper) {
		super(scriptHelper);
	}

	public boolean isAlertPresent(WebDriver driver) {
		try {
			driver.switchTo().alert();
			return true;
		} 
		catch (NoAlertPresentException Ex) {
			return false;
		} 
	}
	
	public void acceptAlert(){
		WebDriverWait Wait = new WebDriverWait(driver,20);
		Wait.until(ExpectedConditions.alertIsPresent());
		Alert alert = driver.switchTo().alert();
		alert.accept();
	}

	// Description : Function to get List Count

	public int getListCount(WebDriver driver, String identifyBy, String locator) {
		int intListCount = 0;
		if (identifyBy.equalsIgnoreCase("xpath")) {
			if (isElementPresent(driver, "xpath", locator)) {
				List<WebElement> element = driver.findElements(By.xpath(locator));
				intListCount = element.size();
			}
		} else if (identifyBy.equalsIgnoreCase("id")) {
			if (isElementPresent(driver, "id", locator)) {
				List<WebElement> element = driver.findElements(By.id(locator));
				intListCount = element.size();
			}
		} else if (identifyBy.equalsIgnoreCase("name")) {
			if (isElementPresent(driver, "name", locator)) {
				List<WebElement> element = driver.findElements(By.name(locator));
				intListCount = element.size();
			}
		} else if (identifyBy.equalsIgnoreCase("cssSelector")) {
			if (isElementPresent(driver, "cssSelector", locator)) {
				List<WebElement> element = driver.findElements(By.cssSelector(locator));
				intListCount = element.size();
			}
		} else if (identifyBy.equalsIgnoreCase("className")) {
			if (isElementPresent(driver, "className", locator)) {
				List<WebElement> element = driver.findElements(By.className(locator));
				intListCount = element.size();
			}
		} else if (identifyBy.equalsIgnoreCase("linkText")) {
			if (isElementPresent(driver, "linkText", locator)) {
				List<WebElement> element = driver.findElements(By.linkText(locator));
				intListCount = element.size();
			}
		} else if (identifyBy.equalsIgnoreCase("partialLinkText")) {
			if (isElementPresent(driver, "partialLinkText", locator)) {
				List<WebElement> element = driver.findElements(By.partialLinkText(locator));
				intListCount = element.size();
			}
		}
		return intListCount;
	}

	// Description : Function to close the Javascript Popup
	public void ignorePopup(WebDriver driver, Alert alert) {
		try {
			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			alert = driver.switchTo().alert();
			String str = alert.getText();
			System.out.println("Alert-" + str);
			alert.accept();
		} catch (Exception e) {
			System.out.println("Alert Not appearing");
		}
	}

	// Description : Function to text from the WebPage
	public String getText(WebDriver driver, String identifyBy, String locator) {
		String strText = null;
		if (identifyBy.equalsIgnoreCase("xpath")) {
			if (isElementPresent(driver, "xpath", locator)) {
				strText = driver.findElement(By.xpath(locator)).getText();
			}
		} else if (identifyBy.equalsIgnoreCase("id")) {
			if (isElementPresent(driver, "id", locator)) {
				strText = driver.findElement(By.id(locator)).getText();
			}
		} else if (identifyBy.equalsIgnoreCase("name")) {
			if (isElementPresent(driver, "name", locator)) {
				strText = driver.findElement(By.name(locator)).getText();
			}
		} else if (identifyBy.equalsIgnoreCase("cssSelector")) {
			if (isElementPresent(driver, "cssSelector", locator)) {
				strText = driver.findElement(By.cssSelector(locator)).getText();
			}
		} else if (identifyBy.equalsIgnoreCase("className")) {
			if (isElementPresent(driver, "className", locator)) {
				strText = driver.findElement(By.className(locator)).getText();
			}
		} else if (identifyBy.equalsIgnoreCase("linkText")) {
			if (isElementPresent(driver, "linkText", locator)) {
				strText = driver.findElement(By.linkText(locator)).getText();
			}
		} else if (identifyBy.equalsIgnoreCase("partialLinkText")) {
			if (isElementPresent(driver, "partialLinkText", locator)) {
				strText = driver.findElement(By.partialLinkText(locator)).getText();
			}
		}
		return strText;
	}

	// Description : Function to validate the existence of an element
	public boolean isElementPresent(WebDriver driver, String identifyBy, String locator) {
		boolean isPresent = false;
		if (identifyBy.equalsIgnoreCase("xpath")) {
			if (driver.findElement(By.xpath(locator)).isDisplayed()) {
				isPresent = true;
			} else {
				isPresent = false;
			}

		} else if (identifyBy.equalsIgnoreCase("id")) {

			if (driver.findElement(By.id(locator)).isDisplayed()) {
				isPresent = true;
			} else {
				isPresent = false;
			}

		} else if (identifyBy.equalsIgnoreCase("name")) {
			if (driver.findElement(By.name(locator)).isDisplayed()) {
				isPresent = true;
			} else {
				isPresent = false;
			}
		} else if (identifyBy.equalsIgnoreCase("linkText")) {
			if (driver.findElement(By.linkText(locator)).isDisplayed()) {
				isPresent = true;
			} else {
				isPresent = false;
			}
		} else if (identifyBy.equalsIgnoreCase("partialLinkText")) {
			if (driver.findElement(By.partialLinkText(locator)).isDisplayed()) {
				isPresent = true;
			} else {
				isPresent = false;
			}
		} else if (identifyBy.equalsIgnoreCase("cssSelector")) {
			if (driver.findElement(By.cssSelector(locator)).isDisplayed()) {
				isPresent = true;
			} else {
				isPresent = false;
			}

		} else if (identifyBy.equalsIgnoreCase("className")) {
			if (driver.findElement(By.className(locator)).isDisplayed()) {
				isPresent = true;
			} else {
				isPresent = false;
			}
		}
		return isPresent;

	}

	// Description : Function to click the Link
	public boolean clickLink(WebDriver driver, String identifyBy, String locator) {
		boolean isPresent = false;
		if (identifyBy.equalsIgnoreCase("xpath")) {
			if (isElementPresent(driver, "xpath", locator)) {
				driver.findElement(By.xpath(locator)).click();
				isPresent = true;
			}
		} else if (identifyBy.equalsIgnoreCase("id")) {
			if (isElementPresent(driver, "id", locator)) {
				isPresent = true;
				driver.findElement(By.id(locator)).click();
			}
		}else if (identifyBy.equalsIgnoreCase("className")) {
			if (isElementPresent(driver, "className", locator)) {
				isPresent = true;
				driver.findElement(By.className(locator)).click();
			}
		}else if (identifyBy.equalsIgnoreCase("name")) {
			if (isElementPresent(driver, "name", locator)) {
				isPresent = true;
				driver.findElement(By.name(locator)).click();
			}
		} else if (identifyBy.equalsIgnoreCase("linkText")) {
			if (isElementPresent(driver, "linkText", locator)) {
				isPresent = true;
				driver.findElement(By.linkText(locator)).click();
			}
		} else if (identifyBy.equalsIgnoreCase("partialLinkText")) {
			if (isElementPresent(driver, "partialLinkText", locator)) {
				isPresent = true;
				driver.findElement(By.partialLinkText(locator)).click();
			}
		} else if (identifyBy.equalsIgnoreCase("cssSelector")) {
			if (isElementPresent(driver, "cssSelector", locator)) {
				isPresent = true;
				driver.findElement(By.cssSelector(locator)).click();
			}
		}
		return isPresent;
	}

	// Description : Function to click the Link
	public boolean clickObject(WebDriver driver, String identifyBy, String locator) {
		boolean isPresent = false;
		if (identifyBy.equalsIgnoreCase("xpath")) {
			if (isElementPresent(driver, "xpath", locator)) {
				isPresent = true;
				WebElement element = driver.findElement(By.xpath(locator));
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].click();", element);
				// driver.findElement(By.xpath(locator)).click();
			}
		} else if (identifyBy.equalsIgnoreCase("id")) {
			if (isElementPresent(driver, "id", locator)) {
				isPresent = true;
				WebElement element = driver.findElement(By.id(locator));
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].click();", element);
			}
		} else if (identifyBy.equalsIgnoreCase("name")) {
			if (isElementPresent(driver, "name", locator)) {
				WebElement element = driver.findElement(By.name(locator));
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].click();", element);
				isPresent = true;

			}
		} else if (identifyBy.equalsIgnoreCase("linkText")) {
			if (isElementPresent(driver, "linkText", locator)) {
				WebElement element = driver.findElement(By.linkText(locator));
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].click();", element);
				isPresent = true;

			}
		} else if (identifyBy.equalsIgnoreCase("partialLinkText")) {
			if (isElementPresent(driver, "partialLinkText", locator)) {
				WebElement element = driver.findElement(By.partialLinkText(locator));
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].click();", element);
				isPresent = true;
			}
		} else if (identifyBy.equalsIgnoreCase("cssSelector")) {
			if (isElementPresent(driver, "cssSelector", locator)) {
				WebElement element = driver.findElement(By.cssSelector(locator));
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].click();", element);
				isPresent = true;
			}
		}
		return isPresent;
	}

	// Description : Function to type in text box
	public boolean typeinTextbox(WebDriver driver, String identifyBy, String locator, String valuetoType) {
		boolean isPresent = false;
		if (identifyBy.equalsIgnoreCase("xpath")) {
			if (isElementPresent(driver, "xpath", locator)) {
				isPresent = true;
				driver.findElement(By.xpath(locator)).clear();
				driver.findElement(By.xpath(locator)).sendKeys(valuetoType);
			}
		} else if (identifyBy.equalsIgnoreCase("id")) {
			if (isElementPresent(driver, "id", locator)) {
				isPresent = true;
				driver.findElement(By.id(locator)).clear();
				driver.findElement(By.id(locator)).sendKeys(valuetoType);
			}
		} else if (identifyBy.equalsIgnoreCase("name")) {
			if (isElementPresent(driver, "name", locator)) {
				isPresent = true;
				driver.findElement(By.name(locator)).clear();
				driver.findElement(By.name(locator)).sendKeys(valuetoType);
			}
		} else if (identifyBy.equalsIgnoreCase("cssSelector")) {
			if (isElementPresent(driver, "cssSelector", locator)) {
				isPresent = true;
				driver.findElement(By.cssSelector(locator)).clear();
				driver.findElement(By.cssSelector(locator)).sendKeys(valuetoType);
			}
		}
		return isPresent;

	}

	// Description : Function to Select Radio button
	public void selectRadiobutton(WebDriver driver, String identifyBy, String locator) {
		if (identifyBy.equalsIgnoreCase("xpath")) {
			if (isElementPresent(driver, "xpath", locator)) {
				// System.out.println("clicking radio button" +locator);
				driver.findElement(By.xpath(locator)).click();
			}
		} else if (identifyBy.equalsIgnoreCase("id")) {
			if (isElementPresent(driver, "id", locator)) {
				driver.findElement(By.id(locator)).click();
			}
		} else if (identifyBy.equalsIgnoreCase("name")) {
			if (isElementPresent(driver, "name", locator)) {
				driver.findElement(By.name(locator)).click();
			}
		} else if (identifyBy.equalsIgnoreCase("cssSelector")) {
			if (isElementPresent(driver, "cssSelector", locator)) {
				driver.findElement(By.cssSelector(locator)).click();
			}
		}

	}

	
	// Description : Function to get Element from the WebPage
	public WebElement getElement(WebDriver driver, String identifyBy, String locator) {
		WebElement Element = null;
		if (identifyBy.equalsIgnoreCase("xpath")) {
			if (isElementPresent(driver, "xpath", locator)) {
				Element = driver.findElement(By.xpath(locator));
			}
		} else if (identifyBy.equalsIgnoreCase("id")) {
			if (isElementPresent(driver, "id", locator)) {
				Element = driver.findElement(By.id(locator));
			}
		} else if (identifyBy.equalsIgnoreCase("name")) {
			if (isElementPresent(driver, "name", locator)) {
				Element = driver.findElement(By.name(locator));
			}
		} else if (identifyBy.equalsIgnoreCase("cssSelector")) {
			if (isElementPresent(driver, "cssSelector", locator)) {
				Element = driver.findElement(By.cssSelector(locator));
			}
		} else if (identifyBy.equalsIgnoreCase("className")) {
			if (isElementPresent(driver, "className", locator)) {
				Element = driver.findElement(By.className(locator));
			}
		} else if (identifyBy.equalsIgnoreCase("linkText")) {
			if (isElementPresent(driver, "linkText", locator)) {
				Element = driver.findElement(By.linkText(locator));
			}
		} else if (identifyBy.equalsIgnoreCase("partialLinkText")) {
			if (isElementPresent(driver, "partialLinkText", locator)) {
				Element = driver.findElement(By.partialLinkText(locator));
			}
		}
		return Element;
	}

	// Description : Function to check is element is enabled
	public boolean isEnabled(WebDriver driver, String identifyBy, String locator) {
		boolean isPresent = false;
		if (identifyBy.equalsIgnoreCase("xpath")) {
			if (driver.findElement(By.xpath(locator)).isEnabled()) {
				isPresent = true;
			}
		} else if (identifyBy.equalsIgnoreCase("id")) {
			if (driver.findElement(By.id(locator)).isEnabled()) {
				isPresent = true;
			}
		} else if (identifyBy.equalsIgnoreCase("name")) {
			if (driver.findElement(By.name(locator)).isEnabled()) {
				isPresent = true;
			}
		} else if (identifyBy.equalsIgnoreCase("cssSelector")) {
			if (driver.findElement(By.cssSelector(locator)).isEnabled()) {
				isPresent = true;
			}
		}
		return isPresent;

	}

	// Description : Function to get any property value of the element
	public String getAttribute(WebDriver driver, String identifyBy, String locator, String property) {
		String strText = null;
		if (identifyBy.equalsIgnoreCase("xpath")) {
			if (isElementPresent(driver, "xpath", locator)) {
				strText = driver.findElement(By.xpath(locator)).getAttribute(property);
			}
		} else if (identifyBy.equalsIgnoreCase("id")) {
			if (isElementPresent(driver, "id", locator)) {
				strText = driver.findElement(By.id(locator)).getAttribute(property);
			}
		} else if (identifyBy.equalsIgnoreCase("name")) {
			if (isElementPresent(driver, "name", locator)) {
				strText = driver.findElement(By.name(locator)).getAttribute(property);
			}
		} else if (identifyBy.equalsIgnoreCase("cssSelector")) {
			if (isElementPresent(driver, "cssSelector", locator)) {
				strText = driver.findElement(By.cssSelector(locator)).getAttribute(property);
			}
		} else if (identifyBy.equalsIgnoreCase("className")) {
			if (isElementPresent(driver, "className", locator)) {
				strText = driver.findElement(By.className(locator)).getAttribute(property);
			}
		} else if (identifyBy.equalsIgnoreCase("linkText")) {
			if (isElementPresent(driver, "linkText", locator)) {
				strText = driver.findElement(By.linkText(locator)).getAttribute(property);
			}
		} else if (identifyBy.equalsIgnoreCase("partialLinkText")) {
			if (isElementPresent(driver, "partialLinkText", locator)) {
				strText = driver.findElement(By.partialLinkText(locator)).getAttribute(property);
			}
		}
		return strText;
	}

	public void SelectValuefromDropDown1(WebDriver driver, String identifyBy, String locator,
			String valuetoSelect) {
		// WebElement select =(driver.findElement(By.id("unitOfMeasure")));
		// select.sendKeys(value);
		if (identifyBy.equalsIgnoreCase("id")) {
			if (isElementPresent(driver, "id", locator)) {
				// System.out.println("clicking radio button" +locator);
				Select select1 = new Select(driver.findElement(By.id(locator)));
				//select1.selectByIndex(Integer.parseInt(valuetoSelect));
				select1.selectByValue(valuetoSelect);
			}
		} else if (identifyBy.equalsIgnoreCase("xpath")) {
			if (isElementPresent(driver, "xpath", locator)) {
				// System.out.println("clicking radio button" +locator);
				Select select1 = new Select(driver.findElement(By.xpath(locator)));
				select1.selectByValue(valuetoSelect);
			}
		} else if (identifyBy.equalsIgnoreCase("name")) {
			if (isElementPresent(driver, "name", locator)) {
				// System.out.println("clicking radio button" +locator);
				Select select1 = new Select(driver.findElement(By.name(locator)));
				select1.selectByValue(valuetoSelect);
			}
		}
	}

	public void SelectValuefromDropDown(WebDriver driver, String identifyBy, String locator,
			String valuetoSelect) {
		// WebElement select =(driver.findElement(By.id("unitOfMeasure")));
		// select.sendKeys(value);
		if (identifyBy.equalsIgnoreCase("id")) {
			if (isElementPresent(driver, "id", locator)) {
				// System.out.println("clicking radio button" +locator);
				WebElement select = driver.findElement(By.id(locator));
				select.sendKeys(valuetoSelect);
			}
		} else if (identifyBy.equalsIgnoreCase("xpath")) {
			if (isElementPresent(driver, "xpath", locator)) {
				// System.out.println("clicking radio button" +locator);
				WebElement select = driver.findElement(By.xpath(locator));
				select.sendKeys(valuetoSelect);
			}
		} else if (identifyBy.equalsIgnoreCase("name")) {
			if (isElementPresent(driver, "name", locator)) {
				// System.out.println("clicking radio button" +locator);
				WebElement select = driver.findElement(By.name(locator));
				select.sendKeys(valuetoSelect);
			}
		}
	}

	// Description : Function to get list of webelements
	public List<WebElement> findWebElements(WebDriver driver, String identifyBy, String locator) {
		List<WebElement> we = null;
		if (identifyBy.equalsIgnoreCase("xpath")) {
			we = driver.findElements(By.xpath(locator));
		} else if (identifyBy.equalsIgnoreCase("id")) {
			we = driver.findElements(By.id(locator));
		}else if (identifyBy.equalsIgnoreCase("className")) {
			we = driver.findElements(By.className(locator));
		}else if (identifyBy.equalsIgnoreCase("name")) {
			we = driver.findElements(By.className(locator));
		} else if (identifyBy.equalsIgnoreCase("linkText")) {
			we = driver.findElements(By.linkText(locator));
		} else if (identifyBy.equalsIgnoreCase("partialLinkText")) {
			we = driver.findElements(By.partialLinkText(locator));
		} else if (identifyBy.equalsIgnoreCase("cssSelector")) {
			we = driver.findElements(By.cssSelector(locator));
		}
		return we;
	}

	// Description : Function to get list of webelements
	public WebElement findWebElement(WebDriver driver, String identifyBy, String locator) {
		WebElement we = null;
		if (identifyBy.equalsIgnoreCase("xpath")) {
			we = driver.findElement(By.xpath(locator));
		} else if (identifyBy.equalsIgnoreCase("id")) {
			we = driver.findElement(By.id(locator));
		}else if (identifyBy.equalsIgnoreCase("className")) {
			we = driver.findElement(By.className(locator));
		}else if (identifyBy.equalsIgnoreCase("name")) {
			we = driver.findElement(By.className(locator));
		} else if (identifyBy.equalsIgnoreCase("linkText")) {
			we = driver.findElement(By.linkText(locator));
		} else if (identifyBy.equalsIgnoreCase("partialLinkText")) {
			we = driver.findElement(By.partialLinkText(locator));
		} else if (identifyBy.equalsIgnoreCase("cssSelector")) {
			we = driver.findElement(By.cssSelector(locator));
		}
		return we;
	}
	
	public void checkPageReadyState(int val){
		String ase;
		for(int i =1;i<5;i++){
			try{
				Thread.sleep(val);
			}catch(Exception e){
				
			}
		ase = ((JavascriptExecutor) driver).executeScript("return document.readyState").toString();
		System.out.println(ase);
		if(ase.contains("complete")){
			System.out.println("Page Load Completed on : "+i*val +"  ms");
		break;
		}
		}
	}
	
	public static boolean isImagePresentInTable(WebDriver driver, String identifyBy,
			String locator, int Row) {
		int timeout = 3;
		boolean isPresent = false;		
		locator=locator.replaceAll("<>","["+Row+"]");		
		if (identifyBy.equalsIgnoreCase("xpath")) {
			try {
				
				
					if (driver.findElement(By.xpath(locator)).isDisplayed()) 
						isPresent = true;				

			} catch (Exception e) {

			}
		}
		return isPresent;
}
	
}