package functionallibraries.aws;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Status;
import com.sun.java.swing.plaf.windows.resources.windows_es;

import pageobjects.aws.Header;
import pageobjects.aws.Search;
import supportlibraries.*;


/**
 * Functional library template
 * @author Cognizant
 */
public class Func_AWSHeader extends ReusableLibrary
{
	/**
	 * Constructor to initialize the functional library
	 * @param scriptHelper The {@link ScriptHelper} object passed from the {@link DriverScript}
	 */
	public Func_AWSHeader(ScriptHelper scriptHelper){
		super(scriptHelper);
	}
	WebDriverWait Wait = new WebDriverWait(driver,30);
	ReusableComponents helper = new ReusableComponents(scriptHelper);
	JavascriptExecutor jse = (JavascriptExecutor)driver;
	Header awsheader = new Header(); 

	public void getTrolleyValuesBefore(){
		awsheader.AWSTrolleypriceBfr = getTrolleyPrice();
		awsheader.AWSTrolleyquantityBfr = getTrolleyQuantity();
	}

	public void getTrolleyValuesAfter(){
		awsheader.AWSTrolleypriceAfr = getTrolleyPrice();
		awsheader.AWSTrolleyquantityAfr = getTrolleyQuantity();
	}
	
	public void getAWSTrolleyValue(){
		awsheader.AwsTrolleyprice = getTrolleyPrice();
		awsheader.AwsTrolleyquantity = getTrolleyQuantity();
	}

	public void validateIncrementTrolleyValue(){
		if(awsheader.AWSTrolleypriceAfr > awsheader.AWSTrolleypriceBfr){
			report.updateTestLog(" Trolley Value ", " Before : "+awsheader.AWSTrolleypriceBfr, Status.DONE);
			report.updateTestLog(" Trolley Value ", " After : "+awsheader.AWSTrolleypriceAfr, Status.DONE);
			report.updateTestLog(" Trolley Validation - Increment ", " Trolley value is increased ", Status.PASS);
		}else{
			report.updateTestLog(" Trolley Value ", " Before : "+awsheader.AWSTrolleypriceBfr, Status.DONE);
			report.updateTestLog(" Trolley Value ", " After : "+awsheader.AWSTrolleypriceAfr, Status.DONE);
			report.updateTestLog(" Trolley Validation - Increment ", "Trolley value is not increased ", Status.FAIL);
		}
	}

	public void validateDecrementTrolleyValue(){
		if(awsheader.AWSTrolleypriceAfr < awsheader.AWSTrolleypriceBfr){
			report.updateTestLog(" Trolley Value ", " Before : "+awsheader.AWSTrolleypriceBfr, Status.DONE);
			report.updateTestLog(" Trolley Value ", " After : "+awsheader.AWSTrolleypriceAfr, Status.DONE);
			report.updateTestLog(" Trolley Validation - Decrement ", " Trolley value is decreased ", Status.PASS);
		}else{
			report.updateTestLog(" Trolley Value ", " Before : "+awsheader.AWSTrolleypriceBfr, Status.DONE);
			report.updateTestLog(" Trolley Value ", " After : "+awsheader.AWSTrolleypriceAfr, Status.DONE);
			report.updateTestLog(" Trolley Validation - Decrement ", " Trolley value is not decreased ", Status.FAIL);
		}
	}


	public double getTrolleyPrice(){
		helper.checkPageReadyState(2000);
		String trolleyprice = helper.getText(driver, awsheader.AWSTrolleytotal_loc, awsheader.AWSTrolleytotal_txt);
		int loc = trolleyprice.indexOf('�');
		double price = Double.parseDouble(trolleyprice.substring(loc+1).trim());
		System.out.println("Trolley Price : "+price);
		return price;
	}

	public int getTrolleyQuantity(){
		//helper.checkPageReadyState(2000);
		WebElement element = helper.findWebElement(driver, awsheader.AWSTrolleyquantity_loc, awsheader.AWSTrolleyquantity_txt);
		String trolleyquantity = jse.executeScript("return arguments[0].innerHTML", element).toString();
		int quantity = Integer.parseInt(trolleyquantity);
		System.out.println("Trolley Quantity : "+quantity);
		return quantity;
	}

	public void getTrolleySavings(){
		//String sav = helper.getText(driver, awsheader.AWSTrolleySavings_loc, awsheader.AWSTrolleySavings_txt);
		WebElement element = helper.findWebElement(driver, awsheader.AWSTrolleySavings_loc, awsheader.AWSTrolleySavings_txt);
		String sav = jse.executeScript("return arguments[0].innerHTML", element).toString();
		System.out.println(sav);
		if(sav.contains("Est. savings")){
			sav = sav.replace("Est. savings", "");
		}
		if(sav.indexOf('p') != 0){
			sav = "0."+sav;
		}
		sav = sav.replaceAll("[^\\d.]", "");
		dataTable.putData("OrderDetails", "AWSSavings", sav);
		report.updateTestLog(" AWS Trolley Savings ", " AWS Trolley Savings :  "+sav, Status.PASS);
	}

	public void verifySlotDateInAWS(){
		String wcsslotdate = dataTable.getData("OrderDetails", "AWS_SlotDate");
		String wcsslotmonth = dataTable.getData("OrderDetails", "AWS_SlotMonth");
		String slotdate = helper.getText(driver, awsheader.slotdate_loc, awsheader.slotdate_txt);
		//if(slotdate.contains(awsheader.AWSSlotDate) && slotdate.contains(awsheader.AWSSlotMonth)){
		if(slotdate.contains(wcsslotdate) && slotdate.contains(wcsslotmonth)){
			report.updateTestLog(" Slot Details ", " Slot details are displayed same between WCS & AWS , Slot detail is : "+slotdate, Status.PASS);
		}else{
			report.updateTestLog(" Slot Details ", " Slot details are not same between WCS & AWS "+awsheader.AWSSlotDate+" "+awsheader.AWSSlotMonth, Status.FAIL);
			report.updateTestLog(" Slot Details ", " WCS Slot Details :  "+wcsslotdate+" "+wcsslotmonth, Status.DONE);
			report.updateTestLog(" Slot Details ", " AWS Slot Details :  "+slotdate, Status.DONE);
		}
	}
	
	public void clickCheckOutButton(){
		if(helper.findWebElement(driver, awsheader.checkout_loc, awsheader.checkout_btn).isEnabled()){
			helper.clickLink(driver, awsheader.checkout_loc, awsheader.checkout_btn);
			report.updateTestLog(" AWS Header ", " Checkout button is enabled and selected  ", Status.PASS);
		}else{
			report.updateTestLog(" AWS Header ", " Checkout button is disabled  ", Status.FAIL);
		}
	}

	public void clickSlotLink(){
		helper.checkPageReadyState(2000);
		if(helper.isElementPresent(driver, awsheader.slot_loc, awsheader.slot_link)){
		helper.clickLink(driver, awsheader.slot_loc, awsheader.slot_link);
		report.updateTestLog(" AWS Header ", " Slot link is selected  ", Status.PASS);
		}else{
			report.updateTestLog(" AWS Header ", " Slot link is not displayed  ", Status.FAIL);	
		}
		//helper.checkPageReadyState(1000);
		//System.out.println(driver.getCurrentUrl());
		//driver.get(driver.getCurrentUrl().replace("http", "https"));
	}
	
	
	public void getPYOSavings(){
		//String sav = helper.getText(driver, awsheader.AWSTrolleySavings_loc, awsheader.AWSTrolleySavings_txt);
		WebElement element = helper.findWebElement(driver, awsheader.AWSTrolleySavings_loc, awsheader.AWSTrolleySavings_txt);
		//String sav = jse.executeScript("return arguments[0].innerHTML", element).toString();
		String sav = element.getText();
		System.out.println(sav);
		if(sav.contains("Est. savings")){
			sav = sav.replace("Est. savings", "");
		}
		if(sav.indexOf('p') != 0){
			sav = "0."+sav;
		}
		sav = sav.replaceAll("[^\\d.]", "");
		dataTable.putData("OrderDetails", "AWS_PYOSavings", sav);
		report.updateTestLog(" AWS Trolley Savings ", " AWS Trolley Savings for PYO Product :  "+sav, Status.PASS);
	}

	public void clickTrolleyLink(){
		if(helper.findWebElement(driver, awsheader.AWSTrolleyHeader_loc, awsheader.AWSTrolleyHeader_lnk).isDisplayed()){
			helper.clickLink(driver, awsheader.AWSTrolleyHeader_loc, awsheader.AWSTrolleyHeader_lnk);
			report.updateTestLog(" AWS Header ", " Trolley is selected  ", Status.PASS);
		}else{
			report.updateTestLog(" AWS Header ", " Trolley is not selected  ", Status.FAIL);
		}
	}

}