package functionallibraries.OF;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Status;

import functionallibraries.aws.ReusableComponents;
import pageobjects.OF.*;
import supportlibraries.*;

public class Func_OFCustomerSearch extends ReusableLibrary
{

	public Func_OFCustomerSearch(ScriptHelper scriptHelper){
		super(scriptHelper);
	}
	CustomerSearch cust = new CustomerSearch();
	ReusableComponents helper = new ReusableComponents(scriptHelper);
	WebDriverWait Wait = new WebDriverWait(driver,20);

	public void performCustomerSearch(){
		String emailId  = dataTable.getData("General_Data", "Username");
		Wait.until(ExpectedConditions.elementToBeClickable(By.name(cust.emailId_txt)));
		helper.typeinTextbox(driver, cust.emailId_loc, cust.emailId_txt, emailId);
		try{
			Thread.sleep(15000);
		}
			catch (Exception e) {
				// TODO: handle exception
			}
		helper.clickLink(driver, cust.searchCustomer_loc, cust.searchCustomer_btn);
		report.updateTestLog("OF Customer Search ", " Customer Email ID is entered and performed search ", Status.DONE);
	}

	public void clickCustomerEmailLink(){
		String emailId  = dataTable.getData("General_Data", "Username");
		Wait.until(ExpectedConditions.elementToBeClickable(By.linkText(emailId)));
		helper.clickLink(driver, "linkText", emailId);
		report.updateTestLog("OF Customer Search ", " Customer Email ID is selected ", Status.DONE);
	}

	public void clickPlaceOrderButtonInLoan(){
		Wait.until(ExpectedConditions.elementToBeClickable(By.xpath(cust.placeorder_btn)));
		helper.clickLink(driver, cust.placeorder_loc, cust.placeorder_btn);
		helper.acceptAlert();
		report.updateTestLog("OF Customer Details Page ", " Place Order Button is selected ", Status.PASS);
	}

	public void clickWeDeliverButton(){
		Wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(cust.wedeliver_btn)));
		helper.clickLink(driver, cust.wedeliver_loc, cust.wedeliver_btn);
		report.updateTestLog("OF Customer Details Page ", " We Deliver Button is selected ", Status.PASS);
	}

	public void clickWeCollectButton(){
		Wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(cust.wecollect_btn)));
		helper.clickLink(driver, cust.wecollect_loc, cust.wecollect_btn);
		report.updateTestLog("OF Customer Details Page ", " We Collect Button is selected ", Status.PASS);
	}

	public void clickSignOnAsCustomerLink(){
		Wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(cust.SOAC_link)));
		helper.clickLink(driver, cust.SOAC_loc, cust.SOAC_link);
		report.updateTestLog("OF Customer Details Page ", " SOAC Link is selected ", Status.PASS);	
	}

	public void clickBookDeliveryButton(){
		Wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(cust.bookdelivery_lnk)));
		helper.clickLink(driver, cust.bookdelivery_loc, cust.bookdelivery_lnk);
		report.updateTestLog("OF Customer Details Page ", " Book delivery Button is selected ", Status.PASS);
	}
	
	public void verifyLatestOrderNumber()
	{
		String latestOrder= dataTable.getData("OrderDetails", "OrderNumber");
		try
		{
			WebElement orderNum=driver.findElement(By.xpath(cust.latestOrder));
			String order1=orderNum.getText();
			System.out.println(order1);
			if(latestOrder.equals(order1))
			{
				report.updateTestLog("Compare Order Number", "the Latest Order number is displayed"+order1, Status.PASS);
			}
			else
			    report.updateTestLog("Compare Order Number", "the Latest Order number is Not displayed", Status.FAIL);
			if(helper.isImagePresentInTable(driver, cust.imgdelivery_loc, cust.imgdelivery, 1))
			{
				report.updateTestLog("Place Branch ICON", "Place Branch icon is displayed", Status.PASS);
			}
			else
				report.updateTestLog("Place Branch ICON", "Place Branch icon is  nOT displayed", Status.FAIL);
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
			report.updateTestLog("Compare Order Number", "the Latest Order number is Not displayed"+e.toString(), Status.FAIL);
			report.updateTestLog("Place Branch ICON", "Place Branch icon is nOT displayed", Status.FAIL);
		}
	}
	
	public void verifyorderstatus(){
		String orderstatus = dataTable.getData("OrderDetails", "OrderStatus");
		String oforderstatus = helper.getText(driver, "xpath", "(//td[@class='text tabular_display  status_data'])[1]");
		if(orderstatus.contains("Pending") && oforderstatus.equalsIgnoreCase("Placed"))
			report.updateTestLog("verfying order status", "Order status in WCS:"+orderstatus+" Order status in OF: "+oforderstatus, Status.PASS);
		else if(orderstatus.equalsIgnoreCase("cancelled") && oforderstatus.equalsIgnoreCase("cancelled"))
			report.updateTestLog("verfying order status", "Order status in WCS:"+orderstatus+" Order status in OF: "+oforderstatus, Status.PASS);
		else
			report.updateTestLog("verfying order status", "Order status is not reflected properly.Order status in WCS:"+orderstatus+" Order status in OF: "+oforderstatus, Status.FAIL);
	}
	
	public void verifycancelledorderstatus(){
		String order = dataTable.getData("OrderDetails", "Cancelled_Order");
		String orderstatus = dataTable.getData("OrderDetails", "Cancelled_status");
		String oforder = helper.getText(driver,"xpath","(//td[@class='text tabular_display  orderid_data']/a)[2]");
		String oforderstatus = helper.getText(driver, "xpath", "(//td[@class='text tabular_display  status_data'])[2]");
		if(oforder.equals(order)){
			report.updateTestLog("cancelled order", "Cancelled order "+oforder+" is present", Status.PASS);
		}
		if(orderstatus.equalsIgnoreCase("cancelled") && oforderstatus.equalsIgnoreCase("cancelled"))
			report.updateTestLog("verfying order status", "Order status in WCS:"+orderstatus+" Order status in OF: "+oforderstatus, Status.PASS);
		else
			report.updateTestLog("verfying order status", "Order status is not reflected properly.Order status in WCS:"+orderstatus+" Order status in OF: "+oforderstatus, Status.FAIL);
		
	}
	
	public void clicklatestOrder()
	{
		try
		{
			Thread.sleep(3000);
			helper.clickLink(driver, cust.latestOrder_loc, cust.latestOrder);
			report.updateTestLog("Latest Order NUMBER", "Latest Order Number is clicked", Status.DONE);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			report.updateTestLog("Latest Order NUMBER", "Latest Order NUMBER button Not clicked"+toString(), Status.FAIL);
		}
	}
	
	
}