package functionallibraries.OF;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Status;

import functionallibraries.aws.ReusableComponents;
import pageobjects.OF.*;
import supportlibraries.*;

public class Func_OFHomePage extends ReusableLibrary
{

	public Func_OFHomePage(ScriptHelper scriptHelper){
		super(scriptHelper);
	}
	WebDriverWait Wait = new WebDriverWait(driver,20);
	HomePage Home = new HomePage();
	
	
	ReusableComponents helper = new ReusableComponents(scriptHelper);
	
	public void clickOrderSearchLink(){
		
		Wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Home.ordersearch_link)));
		helper.clickLink(driver, Home.ordersearch_loc, Home.ordersearch_link);
		report.updateTestLog("OF - HomePage ", " Order Search link is selected ", Status.DONE);
	}
	
	public void clickEquipmentLoanOrderLink(){
		helper.clickLink(driver, Home.equipmentloanorder_loc, Home.equipmentloanorder_link);
		report.updateTestLog("OF - HomePage ", " Equipment Loan Order link is selected ", Status.DONE);
	}
	
	public void clickShopInBranchWeDeliverLink(){
		helper.clickLink(driver, Home.shopinbrachwedeliver_loc, Home.shopinbrachwedeliver_lnk);
		report.updateTestLog("OF - HomePage ", " Shop in Branch We Deliver link is selected ", Status.DONE);
	}
	
	public void clickCustomerSearchLink(){
		helper.clickLink(driver, Home.custsearch_loc, Home.custsearch_link);
		report.updateTestLog("OF - HomePage ", " Customer Search link is selected ", Status.DONE);
	}
	
	public void clickWaitroseEntertainingLink(){
		helper.clickLink(driver, Home.waitroseentertaining_loc, Home.waitroseentertaining_link);
		report.updateTestLog("OF - HomePage ", " Waitrose Entertaining link is selected ", Status.DONE);
	}

public void clickHomePageLink(){
	Wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Home.homepage_lnk)));
	helper.clickLink(driver, Home.homepage_loc, Home.homepage_lnk);
	report.updateTestLog("OF - HomePage ", " Home page link is selected ", Status.DONE);
}

public void clickFindMessagesLink(){
	helper.clickLink(driver, Home.findmessages_loc, Home.findmessages_lnk);
	report.updateTestLog("OF - HomePage ", " Find messages link is selected ", Status.DONE);
}

public void clickDailySlotsLink(){
	helper.clickLink(driver, Home.dailyslots_loc, Home.dailyslots_lnk);
	report.updateTestLog("OF - HomePage ", "Daily Slots link is selected ", Status.DONE);
}

public void clickSignOff(){
	helper.clickLink(driver, Home.signout_loc, Home.signout_lnk);
	report.updateTestLog("OF - HomePage ", "Sign Off is clicked", Status.DONE);
	
}
}