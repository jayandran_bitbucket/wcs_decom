package functionallibraries.OF;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Status;

import functionallibraries.aws.ReusableComponents;
import pageobjects.OF.*;
import pageobjects.wcs.MyAccount;
import pageobjects.wcs.OrderReview;
import supportlibraries.*;


public class Func_OFIntegratorMessages extends ReusableLibrary
{

	public Func_OFIntegratorMessages(ScriptHelper scriptHelper){
		super(scriptHelper);
	}
	OFIntegratorMessages ofmessage = new OFIntegratorMessages();
	
	MyAccount myaccount_ui = new MyAccount();
	OrderReview orderreview = new OrderReview();
	WebDriverWait Wait = new WebDriverWait(driver,20);

	ReusableComponents helper = new ReusableComponents(scriptHelper);

	public void enterDateWhenMessageSent(){
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		System.out.println(dateFormat.format(date));
		helper.typeinTextbox(driver, ofmessage.date_loc, ofmessage.date_txt, dateFormat.format(date));
		report.updateTestLog("Enter Date Message was sent","Date entered successfully" , Status.PASS);


	}

	public void searchOrderNumber(){
		String orderid = dataTable.getData("OrderDetails", "OrderNumber");
		helper.typeinTextbox(driver, ofmessage.ordersearch_loc, ofmessage.ordersearch_txt, orderid);
		report.updateTestLog("Find messages","Order number is enetered" , Status.PASS);
	}

	public void clickSerachButton(){
		/*helper.clickLink(driver, ofmessage.selectSearchButton_Loc, ofmessage.selectSearchButton);
		if(helper.isElementPresent(driver, ofmessage.linkErrorMessageNo_Loc, ofmessage.linkErrorMessageNo))
			report.updateTestLog("Find messages","Order Search message is successful" , Status.PASS);
		else report.updateTestLog("Find messages","Order Search message is not successful" , Status.PASS);*/
		try{
			Thread.sleep(26000);
		}
			catch (Exception e) {
				// TODO: handle exception
			}
		
		int i=1;
		do{
			helper.clickLink(driver, ofmessage.selectSearchButton_Loc, ofmessage.selectSearchButton);
			i++;
			if(helper.findWebElements(driver, "xpath",ofmessage.message ).size() >0){
				break;
			}
			driver.navigate().refresh();
			}while(i<8);
		
	
	}

	public void verifyNoErrorMessagesFound(){
		if(helper.isElementPresent(driver, ofmessage.verifyMessage_loc, ofmessage.verifyMessage))
			report.updateTestLog("Find messages","No error message is found" , Status.PASS);
		else 	report.updateTestLog("Find messages","No error message is not found" , Status.FAIL);
	}

	public void clickErrorMsgRefNo(){
		if(helper.isElementPresent(driver, ofmessage.linkErrorMessageNo_Loc, ofmessage.linkErrorMessageNo)){
			helper.clickLink(driver, ofmessage.linkErrorMessageNo_Loc, ofmessage.linkErrorMessageNo);
			report.updateTestLog("Find messages","Error Ref No is clicked" , Status.PASS);


		}
		else 	report.updateTestLog("Find messages","Error Ref No is not clicked" , Status.FAIL);


	}

	public void verifyXMLOrderDetails(){
		String deltype = dataTable.getData("General_Data", "DeliveryType").toLowerCase();
		String cardtype = dataTable.getData("PaymentDetails_Data", "CardType").toLowerCase();
		if(helper.isElementPresent(driver, "xpath", "//textarea[contains(text(),'<type>"+deltype+"</type>')]"))

			report.updateTestLog("Find messages","Delivery Type: "+deltype+" is displayed in messsage" , Status.PASS);
		else report.updateTestLog("Find messages","Delivery Type: "+deltype+" is not displayed in messsage" , Status.FAIL);

		if(helper.isElementPresent(driver, "xpath", "//textarea[contains(text(),'<type>"+cardtype+"')]"))

				report.updateTestLog("Find messages","card Type: "+cardtype+" is displayed in messsage" , Status.PASS);
		else report.updateTestLog("Find messages","card Type: "+cardtype+" is not displayed in messsage" , Status.FAIL);

	}
	
	
	public void clickmessage(){
		Wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(ofmessage.message)));
		if(helper.isElementPresent(driver, ofmessage.message_loc, ofmessage.message))
			helper.clickLink(driver, ofmessage.message_loc, ofmessage.message);
		
	}
	
	
	public void clickAmendmessage()
	{
		Wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(ofmessage.amended_msg)));
		if(helper.isElementPresent(driver, ofmessage.amended_msg_loc, ofmessage.amended_msg))
		helper.clickLink(driver, ofmessage.amended_msg_loc, ofmessage.amended_msg);
	}
	
	
	public void verifymessagedetails(){
		
		if(myaccount_ui.preference.equals("yes")){
		if(helper.isElementPresent(driver, "xpath", "//textarea[contains(text(),'<smsPrefInd>"+true+"</smsPrefInd>')]"))
			report.updateTestLog("Find messages","Order preference is set as true",Status.PASS);}
		if(myaccount_ui.preference.equals("no")){
			if(helper.isElementPresent(driver, "xpath", "//textarea[contains(text(),'<smsPrefInd>"+false+"</smsPrefInd>')]"))
				report.updateTestLog("Find messages","Order preference is set as false",Status.PASS);}
}
	
	
	public void verifyAPDcarddetails(){
		String card = dataTable.getData("PaymentDetails_Data", "APDCode");
		
		if((helper.isElementPresent(driver, "xpath", "//textarea[contains(text(),'<discountCardNumber>"+card+"</discountCardNumber>')]") && (helper.isElementPresent(driver, "xpath", "//textarea[contains(text(),'<partnershipDiscountType>red</partnershipDiscountType>')]")))){
			report.updateTestLog("APD card details","APD card number is present in the message and the flag is set as true",Status.PASS);
		}
		else 
			report.updateTestLog("APD card details","APD card number is not present in the message",Status.FAIL);
	}
	
	public void verifyPromocode(){
		
		//double saving = (double) Math.round(orderreview.promodiscount * 100.0) / 100.0;;
		
		
		if((helper.isElementPresent(driver, "xpath", "//textarea[contains(text(),'<partnershipDiscountType>no discount</partnershipDiscountType>')]")))
		{
			report.updateTestLog("Promotion code details", "promotion dicount is present in the message ", Status.PASS);
		}
		else
			report.updateTestLog("Promotion code details","promotion discount is not present in the message",Status.FAIL);
		
		if((helper.isElementPresent(driver, "xpath", "//textarea[contains(text(),'<estimatedOfferSavings>"+orderreview.promodiscount+"</estimatedOfferSavings>')]")))
			report.updateTestLog("savings discount", "savings is present in the xml message", Status.PASS);
			
		else
			report.updateTestLog("savings discount", "savings is not present in the xml message", Status.FAIL);
		
	
	}
}

