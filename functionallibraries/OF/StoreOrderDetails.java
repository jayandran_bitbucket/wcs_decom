package functionallibraries.OF;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;

import org.apache.commons.io.IOUtils;

import com.cognizant.framework.Status;

import functionallibraries.aws.ReusableComponents;
import pageobjects.OF.Login;
import supportlibraries.*;

public class StoreOrderDetails extends ReusableLibrary
{

	public StoreOrderDetails(ScriptHelper scriptHelper){
		super(scriptHelper);
	}


	public synchronized StringBuilder readCSV(String path){
		String theString = null;
		try {
			theString = IOUtils.toString(new FileInputStream(new File(path)), "UTF-8");

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		StringBuilder builder = new StringBuilder();
		builder.append(theString);
		return builder;
	}
	
	public synchronized void writeCSV(String path,String[] data){
		StringBuilder builder = readCSV(path);
		if(builder.toString().equals("")){
			for(String s:data){
				builder.append(s);
				builder.append(",");
			}
		}else{
			builder.append("\n");
			for(String s:data){
				builder.append(s);
				builder.append(",");
			}
		}
		try {
			PrintWriter pw = new PrintWriter(new File(path));
			pw.write(builder.toString());
	        pw.close();
	        
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
	}

}