package functionallibraries.OF;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.cognizant.framework.Status;

import functionallibraries.aws.ReusableComponents;
import pageobjects.OF.Login;
import supportlibraries.*;

public class Func_OFLogin extends ReusableLibrary
{

	public Func_OFLogin(ScriptHelper scriptHelper){
		super(scriptHelper);
	}
	Login login = new Login();


	ReusableComponents helper = new ReusableComponents(scriptHelper);

	public void launchOFApp(){
		driver.get(properties.getProperty("OFUrl"));
		report.updateTestLog("OF APPLICATION ", " Launching OF Application , URL : "+properties.getProperty("OFUrl"), Status.PASS);
	}

	public void loginOF(){
		String userid = dataTable.getData("General_Data", "OFUserId");
		String password = dataTable.getData("General_Data", "OFPassword");
		helper.typeinTextbox(driver, login.userid_loc, login.userid_txt, userid);
		helper.typeinTextbox(driver, login.passwd_loc, login.passwd_txt, password);
		helper.clickLink(driver, login.signon_loc, login.signon_btn);
		report.updateTestLog("OF - Login ", " Login is successfull with user id : "+userid, Status.PASS);
	}

	public void returnToAdmin(){
		if(helper.findWebElements(driver, login.returnToAdminPhone_loc, login.returnToAdminPhone_lnk).size() > 0){
			helper.clickLink(driver, login.returnToAdminPhone_loc, login.returnToAdminPhone_lnk);
			report.updateTestLog(" Return To Admin Link ", " Selected return to admin link for Phone", Status.PASS);
		}else{
//			WebElement btn = driver.findElement(By.xpath( login.returnToAdminPerson_lnk));
//			Actions builder = new Actions(driver);
//			builder.moveToElement(btn).perform();
			helper.clickLink(driver, login.returnToAdminPerson_loc, login.returnToAdminPerson_lnk);
			report.updateTestLog(" Return To Admin Link ", " Selected return to admin link for Person", Status.PASS);
		}
	}



}