package functionallibraries.OF;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.cognizant.framework.Status;
import com.thoughtworks.selenium.SeleniumException;

import functionallibraries.aws.ReusableComponents;
import pageobjects.OF.*;
import supportlibraries.*;


public class Func_OFDailySlots extends ReusableLibrary
{

	public Func_OFDailySlots(ScriptHelper scriptHelper){
		super(scriptHelper);
	}
	HomePage Home = new HomePage();
	DailySlots OfDailySlot= new DailySlots();
	
	
	ReusableComponents helper = new ReusableComponents(scriptHelper);
	/********************************************************
	*FUNCTION    :getSlotDetails
	*AUTHOR      :Jayandran
	*DATE        :12-Mar-15
	*DESCRIPTION :function to get the SLot Details from DailySlot Page
	*PAGE        :Comp_DailySlotPage 
	********************************************************/
	public void getSlotDetailsInDailySlots(){
	try {
	String slotDetails = null;
	String slotCheck,value;

	int i;
	if(helper.isElementPresent(driver, OfDailySlot.txtSlotDateDropdown_Loc	, OfDailySlot.txtSlotDateDropdown)){
	slotCheck = driver.findElement(By.xpath("//input[@name='slot_date']")).getAttribute("value");
	try{
		List<WebElement> subOptions=driver.findElements(By.xpath("(//select[@name='date_select']//option)"));
		for(WebElement we : subOptions){
			value = we.getAttribute("value");
			if(value.equalsIgnoreCase(slotCheck)){
				slotDetails = we.getText();
				OfDailySlot.slotDetails = slotDetails ;
				OfDailySlot.slotDate = value;
				break;
			}
		}
	}catch(Exception e){

	}
	/*helper.getText(driver, OfDailySlot.txtSelectedSLot_Loc, OfDailySlot.txtSelectedSLot);
	slotDetails = helper.getText(driver, OfDailySlot.txtSelectedSLot_Loc, OfDailySlot.txtSelectedSLot);*/
	System.out.println("Slot Details : "+slotDetails);
	report.updateTestLog("Daily slot Page","Selected Slot Details "+slotDetails , Status.PASS);
	}else
	report.updateTestLog("Daily SLot Page","Slot details are not displayed" , Status.FAIL);	
	}catch(SeleniumException e){
	e.printStackTrace();
	report.updateTestLog("Daily SLot Page","Exception occured while validating Slot Details"+e.toString(), Status.FAIL);}
	}


	/********************************************************
	*FUNCTION    :selectBranchDetailsDropdown
	*AUTHOR      :Jayandran
	*DATE        :13-Mar-15
	*DESCRIPTION :function to select Branch Name in BranchDetails Dropdown 
	*PAGE        :Comp_DailySlotPage 
	********************************************************/
	public void selectBranchDetailsDropdownInDailySlots(){
	try {

	String userBranchCode,value = null;

	int i;
	if(helper.isElementPresent(driver, OfDailySlot.txtBranchNameDropdown_Loc, OfDailySlot.txtBranchNameDropdown)){
	userBranchCode = dataTable.getData("DeliveryDetails_Data" , "Branch").trim();				
	try{
		List<WebElement> subOptions=driver.findElements(By.xpath(OfDailySlot.txtBranchNameOptionDropdown));
		for(WebElement we : subOptions){
			value = we.getText();
			if(value.contains(userBranchCode)){
				System.out.println("Selected Branch Details : "+value);
				we.click();							
				break;
			}
		}
		report.updateTestLog("Daily slot Page","Selected Slot Details "+value , Status.PASS);
	}catch(Exception e){
		System.out.println("Exception occured while validating Branch Detials"+e.toString());
		report.updateTestLog("Daily slot Page","Exception occured while validating Branch Name"+e.toString(), Status.FAIL);
	}

	}else
	report.updateTestLog("Daily SLot Page","Branch Name Dropdown is not displayed " , Status.FAIL);	
	}catch(SeleniumException e){
	e.printStackTrace();
	report.updateTestLog("Daily SLot Page","Exception occured while validating BranchDetails"+e.toString(), Status.FAIL);}
	}


	/********************************************************
	*FUNCTION    :selectServiceTypeDropdown
	*AUTHOR      :Jayandran
	*DATE        :13-Mar-15
	*DESCRIPTION :function to select Service Type option 
	*PAGE        :Comp_DailySlotPage 
	********************************************************/
	public void selectServiceTypeDropdownInDailySlots(){
	try {

	String userServiceType,value = null;

	int i;
	if(helper.isElementPresent(driver, OfDailySlot.txtServiceDropdown_Loc, OfDailySlot.txtServiceDropdown)){
	userServiceType = dataTable.getData("DeliveryDetails_Data" , "Servicetype").trim();				
	try{
		List<WebElement> subOptions=driver.findElements(By.xpath(OfDailySlot.txtServiceSelectionOptionDropdown));
		for(WebElement we : subOptions){
			value = we.getText();
			if(value.contains(userServiceType)){
				System.out.println("Selected Service type : "+value);
				we.click();							
				break;
			}
		}
		report.updateTestLog("Daily slot Page","Selected Service Type "+value , Status.PASS);
	}catch(Exception e){
		System.out.println("Exception occured while selecting Service Type"+e.toString());
		report.updateTestLog("Daily slot Page","Exception occured while selecting Service Type"+e.toString(), Status.FAIL);
	}

	}else
	report.updateTestLog("Daily SLot Page","Service TYpe is not displayed " , Status.FAIL);	
	}catch(SeleniumException e){
	e.printStackTrace();
	report.updateTestLog("Daily SLot Page","Exception occured while validating service type "+e.toString(), Status.FAIL);}
	}


	/********************************************************
	*FUNCTION    :selectLocationTypeDropdown
	*AUTHOR      :Jayandran
	*DATE        :13-Mar-15
	*DESCRIPTION :function to select Service Type option 
	*PAGE        :Comp_DailySlotPage 
	********************************************************/
	public void selectLocationDropdownInDailySlots(){
	try {

	String userLocation,value = null;

	int i = 0;
	if(helper.isElementPresent(driver, OfDailySlot.txtLocationDropdown_Loc, OfDailySlot.txtLocationDropdown)){
	userLocation = dataTable.getData("General_Data" , "Location_dailySLot").trim();

	try{
		List<WebElement> subOptions=driver.findElements(By.xpath(OfDailySlot.txtLocationSelectionOptionDropdown));
		subOptions.size();
		for(WebElement we : subOptions){
			value = we.getText();
			if(value.contains(userLocation)){
				System.out.println("Location Selection type : "+value);
				we.click();							
				break;
			}
		}
		report.updateTestLog("Daily slot Page","Selected Location Type "+value , Status.PASS);
	}catch(Exception e){
		System.out.println("Exception occured while selecting Location Type"+e.toString());
		report.updateTestLog("Daily slot Page","Exception occured while selecting Location Type"+e.toString(), Status.FAIL);
	}

	}else
	report.updateTestLog("Daily SLot Page","LocationSelection dropdown is not displayed " , Status.FAIL);	
	}catch(SeleniumException e){
	e.printStackTrace();
	report.updateTestLog("Daily SLot Page","Exception occured while validating Location type "+e.toString(), Status.FAIL);}
	}



	/********************************************************
	*FUNCTION    :clickGoButton
	*AUTHOR      :Jayandran
	*DATE        :13-Mar-15
	*DESCRIPTION :function to select Go Button 
	*PAGE        :Comp_DailySlotPage 
	********************************************************/
	public void clickGoButtonInDailySlots(){
	try {


	if(helper.isElementPresent(driver, OfDailySlot.linkGoButton_Loc, OfDailySlot.linkGoButton)){
	helper.clickLink(driver, OfDailySlot.linkGoButton_Loc, OfDailySlot.linkGoButton);
	report.updateTestLog("Daily SLot Page","Go Button is Selected" , Status.PASS);
	}else
	report.updateTestLog("Daily SLot Page","Go Button is not displayed " , Status.FAIL);	
	}catch(SeleniumException e){
	e.printStackTrace();
	report.updateTestLog("Daily SLot Page","Exception occured while selecting Go Button "+e.toString(), Status.FAIL);}
	}


	/********************************************************
	*FUNCTION    :getTotalSlotCapacityInDailySlot
	*AUTHOR      :Jayandran
	*DATE        :13-Mar-15
	*DESCRIPTION :function to get TotalSlotCapacity in Daily Slot Page
	*PAGE        :Comp_DailySlotPage 
	********************************************************/
	public void getTotalSlotCapacityInDailySlot(){
	try {

	String capacity,capacity2 = null;
	if(helper.isElementPresent(driver, OfDailySlot.txtTotalSlotCapacity_Loc, OfDailySlot.txtTotalSlotCapacity)){
	capacity =	helper.getText(driver, OfDailySlot.txtTotalSlotCapacity_Loc, OfDailySlot.txtTotalSlotCapacity);

	if(helper.isElementPresent(driver, OfDailySlot.txtRestrictedTotalSlotCapacity_Loc, OfDailySlot.txtRestrictedTotalSlotCapacity)){
		capacity2 = helper.getText(driver, OfDailySlot.txtRestrictedTotalSlotCapacity_Loc, OfDailySlot.txtRestrictedTotalSlotCapacity);
	}
	OfDailySlot.slotCapacity = Integer.parseInt(capacity);
	if(capacity2 != null)
		OfDailySlot.slotCapacity = OfDailySlot.slotCapacity + Integer.parseInt(capacity2);
	report.updateTestLog("Daily SLot Page","Slot Capacity Screenshot", Status.SCREENSHOT);
	report.updateTestLog("Daily SLot Page","Total Slot Capacity : "+OfDailySlot.slotCapacity , Status.PASS);
	}else
	report.updateTestLog("Daily SLot Page","Total Slot Capacity is not displayed " , Status.FAIL);	
	}catch(SeleniumException e){
	e.printStackTrace();
	report.updateTestLog("Daily SLot Page","Exception occured while validating total slot capacity "+e.toString(), Status.FAIL);}
	}


	/********************************************************
	*FUNCTION    :selectDateDropdownInDailySlot
	*AUTHOR      :Jayandran
	*DATE        :16-Mar-15
	*DESCRIPTION :select the Slot Date in Daily SLot Page
	*PAGE        :Comp_OFDeliverySlotMaintenance 
	********************************************************/

	public void selectDateDropdownInDailySlot(){

	try {
	String slotdate,value;
	String slotDate = dataTable.getData("DeliveryDetails_Data" , "Date");

	if(helper.isElementPresent(driver, OfDailySlot.txtSlotDateDropdown_Loc	, OfDailySlot.txtSlotDateDropdown)){

	if(slotDate !=null){
		try{
			int i=0;
			List<WebElement> subOptions=driver.findElements(By.xpath("(//select[@name='date_select']//option)"));
			for(WebElement we : subOptions){
				value = we.getText();
		//		String slotArray[]=value.split("");
		//		value=slotArray[1];
				i++;
				if(value.contains(slotDate)){
					we.click();
					report.updateTestLog("Daily SLot Page", "Slot date is selected"+value, Status.PASS);
					break;
				}else if(subOptions.size() == i){
					String windowbef = driver.getWindowHandle();
					helper.clickLink(driver, "xpath", "//img[contains(@src,'calendarIcon')]");
					for(String win: driver.getWindowHandles()){
						driver.switchTo().window(win);
						}
					
					driver.manage().window().maximize();
					
					System.out.println("//a[contains(text(),'"+slotDate+"')]");
					WebElement topframe= driver.findElement(By.xpath("//frame[@name='topCalFrame']"));
					WebElement frame = driver.findElement(By.xpath("//frame[@name='bottomCalFrame']"));
					//driver.switchTo().frame(topframe);
					//helper.clickLink(driver, "xpath", "(//input[@type='BUTTON'])[4]");
					//driver.switchTo().parentFrame(); // if we want the date from next month, we have to enable this code
					driver.switchTo().frame(frame);
					driver.findElement(By.xpath("//a[contains(text(),'"+slotDate+"')]")).click();
					driver.switchTo().window(windowbef);
					report.updateTestLog("Daily SLot Page", "Slot date is selected", Status.PASS);
					break;
				}
			}
		}catch(Exception e){
			System.out.println("Exception occured while validating date"+e.toString());
			report.updateTestLog("Daily SLot Page", "Exception occured while validating date dropdown"+e.toString(), Status.FAIL);
		}
	}
	
		//else if(Header.slotDate != null){
//		slotdate = Header.slotDate.substring(0, 6);
//		System.out.println("Given Slot Day : "+slotdate);
//		try{
//			int i=0;
//			List<WebElement> subOptions=driver.findElements(By.xpath("(//select[@name='date_select']//option)"));
//			for(WebElement we : subOptions){
//				value = we.getText();
//				i++;
//				if(value.contains(slotdate)){
//					we.click();
//					report.updateTestLog("Daily SLot Page", "Slot date is selected"+value, Status.PASS);
//					break;
//				}else if(subOptions.size() == i){
//					report.updateTestLog("Daily SLot Page", "Slot date is not selected", Status.FAIL);
//					break;
//				}
//			}
//		}catch(Exception e){
//			System.out.println("Exception occured while validating date"+e.toString());
//			report.updateTestLog("Daily SLot Page", "Exception occured while validating date dropdown"+e.toString(), Status.FAIL);
//		}
//	}
		else{
		System.out.println("Slot Day is not selected");
		report.updateTestLog("Daily SLot Page", "Slot Day is Not selected", Status.FAIL);
	}
	}else{
	System.out.println("Dropdown is not displayed");
	}
	}catch (Exception e) {
	e.printStackTrace();
	System.out.println("Exception occured while validating Date Dropdown"+e.toString());
	}
	}

	/********************************************************
	*FUNCTION    :getFreeSlotCapacityOfFirstSlotInDailySlot
	*AUTHOR      :Jayandran
	*DATE        :16-Mar-15
	*DESCRIPTION :function to get Free slot capacity of First slot in Daily Slot Page
	*PAGE        :Comp_DailySlotPage 
	********************************************************/
	public void getFreeSlotCapacityOfFirstSlotInDailySlot(){
	try {			
	String capacity;
	int val,rowno = 1;
	List<WebElement> we = driver.findElements(By.xpath("//tr[@class='normal']"));
	Row:{
	for(int i=1;i<=we.size();i++){
		if(driver.findElement(By.xpath("((//tr[@class='normal'])["+i+"]//td)[2]")).isDisplayed()){
			val=Integer.parseInt(driver.findElement(By.xpath("((//tr[@class='normal'])["+i+"]//td)[2]")).getText());
			if(val>0){
				rowno = i;
				report.updateTestLog("Daily SLot Page","Slot is available from "+i+" Line", Status.DONE);
				break Row;
			}
		}
	}
	}
	String xpath1 = OfDailySlot.txtFreeSlotCapacityOfFirstSlot.replaceFirst("1", Integer.toString(rowno));
	System.out.println(xpath1);
	if(helper.isElementPresent(driver, OfDailySlot.txtFreeSlotCapacityOfFirstSlot_Loc,xpath1)){
	capacity =	helper.getText(driver,  OfDailySlot.txtFreeSlotCapacityOfFirstSlot_Loc, xpath1);
	OfDailySlot.freeSlotCapacity = Integer.parseInt(capacity);
	System.out.println("Free Slot Capacity of FIrst Slot :"+OfDailySlot.freeSlotCapacity);
	report.updateTestLog("Daily SLot Page","Free Slot Capacity of First Slot : "+OfDailySlot.freeSlotCapacity , Status.PASS);
	}else
	report.updateTestLog("Daily SLot Page","Free Slot Capacity of First Slot is not displayed " , Status.FAIL);

	}catch(SeleniumException e){
	e.printStackTrace();
	report.updateTestLog("Daily SLot Page","Exception occured while validating Free slot capacity "+e.toString(), Status.FAIL);}
	}

	/********************************************************
	*FUNCTION    :getPlacedSlotCapacityOfFirstSlotInDailySlot
	*AUTHOR      :Jayandran
	*DATE        :16-Mar-15
	*DESCRIPTION :function to get Placed slot capacity of First slot in Daily Slot Page
	*PAGE        :Comp_DailySlotPage 
	********************************************************/
	public void getPlacedSlotCapacityOfFirstSlotInDailySlot(){
	try {			
	String capacity,capacity1,capacity2;
	int index,index1;
	int val,rowno = 1;
	List<WebElement> we = driver.findElements(By.xpath("//tr[@class='normal']"));
	Row:{
	for(int i=1;i<=we.size();i++){
		if(driver.findElement(By.xpath("((//tr[@class='normal'])["+i+"]//td)[2]")).isDisplayed()){
			val=Integer.parseInt(driver.findElement(By.xpath("((//tr[@class='normal'])["+i+"]//td)[2]")).getText());
			if(val>0){
				rowno = i;
				report.updateTestLog("Daily SLot Page","Slot is available from "+i+" Line", Status.DONE);
				break Row;
			}
		}
	}
	}
	String xpath1 = OfDailySlot.txtPlacedSlotCapacityOfFirstSlot.replaceFirst("1", Integer.toString(rowno));
	System.out.println(xpath1);
	if(helper.isElementPresent(driver, OfDailySlot.txtPlacedSlotCapacityOfFirstSlot_Loc,xpath1)){
	capacity =	helper.getText(driver,  OfDailySlot.txtPlacedSlotCapacityOfFirstSlot_Loc, xpath1);
	if(capacity.contains("+")){
		System.out.println("Restricted Slot Capacity is displayed :  "+capacity);
		index = capacity.indexOf("+");
		capacity1 = capacity.substring(index+1);
		if(capacity1.contains(")")){
			index1 = capacity1.indexOf(")");
			System.out.println("Index of ) : "+index1);
			capacity1 = capacity1.substring(0, index1);
			System.out.println(capacity1);
		}
		capacity2 = capacity.substring(0, index-1).trim();
		System.out.println("Normal Placed Slot Capcity :  "+capacity2);
		System.out.println("Restricted Placed Slot Capcity :  "+capacity1);
		OfDailySlot.placedSlotCapacity = Integer.parseInt(capacity2);
		OfDailySlot.restrictedplacedSlotCapacity = Integer.parseInt(capacity1);
	}else{
		OfDailySlot.placedSlotCapacity = Integer.parseInt(capacity);
		System.out.println("Placed Slot Capacity of FIrst Slot :"+OfDailySlot.placedSlotCapacity);
		report.updateTestLog("Daily SLot Page","Placed Slot Capacity of First Slot : "+OfDailySlot.placedSlotCapacity , Status.PASS);
	}
	}else
	report.updateTestLog("Daily SLot Page","Placed Slot Capacity of First Slot is not displayed " , Status.FAIL);	
	}catch(SeleniumException e){
	e.printStackTrace();
	report.updateTestLog("Daily SLot Page","Exception occured while validating Placed slot capacity "+e.toString(), Status.FAIL);}
	}

	/********************************************************
	*FUNCTION    :validateFreeSlotCapacityOfFirstSlotInDailySlot
	*AUTHOR      :Jayandran
	*DATE        :16-Mar-15
	*DESCRIPTION :function to validate Free slot capacity of First slot after order placing in Daily Slot Page
	*PAGE        :Comp_DailySlotPage 
	********************************************************/
	public void validateFreeSlotCapacityOfFirstSlotInDailySlot(){
	try {			
	String capacity;

	int val,rowno = 1;
	List<WebElement> we = driver.findElements(By.xpath("//tr[@class='normal']"));
	Row:{
	for(int i=1;i<=we.size();i++){
		if(driver.findElement(By.xpath("((//tr[@class='normal'])["+i+"]//td)[2]")).isDisplayed()){
			val=Integer.parseInt(driver.findElement(By.xpath("((//tr[@class='normal'])["+i+"]//td)[2]")).getText());
			if(val>0){
				rowno = i;
				report.updateTestLog("Daily SLot Page","Slot is available from "+i+" Line", Status.DONE);
				break Row;
			}
		}
	}
	}
	String xpath1 = OfDailySlot.txtFreeSlotCapacityOfFirstSlot.replaceFirst("1", Integer.toString(rowno));
	System.out.println(xpath1);
	if(OfDailySlot.freeSlotCapacity != -1){
		
	if(helper.isElementPresent(driver, OfDailySlot.txtFreeSlotCapacityOfFirstSlot_Loc, xpath1)){
		capacity =	helper.getText(driver,  OfDailySlot.txtFreeSlotCapacityOfFirstSlot_Loc, xpath1);
		if(OfDailySlot.freeSlotCapacity-1 == Integer.parseInt(capacity)){
			System.out.println("Free Slot Capacity of FIrst Slot is decreased, Before Placing Order : "+OfDailySlot.freeSlotCapacity);
			System.out.println("After Placing Order : "+capacity);
			report.updateTestLog("Daily SLot Page","Free Slot Capacity of First Slot is reduced, current slot capacity is :  "+capacity , Status.PASS);
		}else{
			System.out.println("Free Slot Capacity of FIrst Slot is not decreased, Before Placing Order : "+OfDailySlot.freeSlotCapacity);
			System.out.println("After Placing Order : "+capacity);
			report.updateTestLog("Daily SLot Page","Free Slot Capacity of First Slot is not reduced, current slot capacity is :  "+capacity , Status.FAIL);
		}
	}else
		report.updateTestLog("Daily SLot Page","Free Slot Capacity of First Slot is not displayed " , Status.FAIL);	
	}else{
	capacity =	helper.getText(driver,  OfDailySlot.txtFreeSlotCapacityOfFirstSlot_Loc,xpath1);
	report.updateTestLog("Daily SLot Page","Free Slot Capacity of First Slot is not validated, because previous slot value is not captured. Current Slot value is  : "+capacity , Status.DONE);
	}
	}catch(SeleniumException e){
	e.printStackTrace();
	report.updateTestLog("Daily SLot Page","Exception occured while validating Free slot capacity "+e.toString(), Status.FAIL);}
	}

	/********************************************************
	*FUNCTION    :validatePlacedSlotCapacityOfFirstSlotInDailySlot
	*AUTHOR      :Jayandran
	*DATE        :16-Mar-15
	*DESCRIPTION :function to validate Placed slot capacity of First slot after order placing in Daily Slot Page
	*PAGE        :Comp_DailySlotPage 
	********************************************************/
	public void validatePlacedSlotCapacityOfFirstSlotInDailySlot(){
	try {			
	String capacity;
	int val,rowno = 1;
	List<WebElement> we = driver.findElements(By.xpath("//tr[@class='normal']"));
	Row:{
	for(int i=1;i<=we.size();i++){
		if(driver.findElement(By.xpath("((//tr[@class='normal'])["+i+"]//td)[2]")).isDisplayed()){
			val=Integer.parseInt(driver.findElement(By.xpath("((//tr[@class='normal'])["+i+"]//td)[2]")).getText());
			if(val>0){
				rowno = i;
				report.updateTestLog("Daily SLot Page","Slot is available from "+i+" Line", Status.DONE);
				break Row;
			}
		}
	}
	}
	String xpath1 = OfDailySlot.txtPlacedSlotCapacityOfFirstSlot.replaceFirst("1", Integer.toString(rowno));
	System.out.println(xpath1);
	if(OfDailySlot.placedSlotCapacity != -1){
	if(helper.isElementPresent(driver, OfDailySlot.txtPlacedSlotCapacityOfFirstSlot_Loc, xpath1)){
		capacity =	helper.getText(driver, OfDailySlot.txtPlacedSlotCapacityOfFirstSlot_Loc, xpath1);
		if(OfDailySlot.placedSlotCapacity == Integer.parseInt(capacity)-1){
			System.out.println("Placed Slot Capacity of FIrst Slot is Increased, Before Placing Order : "+OfDailySlot.placedSlotCapacity);
			System.out.println("After Placing Order : "+capacity);
			report.updateTestLog("Daily SLot Page","Placed Slot Capacity of First Slot is Increased, current slot capacity is :  "+capacity , Status.PASS);
		}else{
			System.out.println("Placed Slot Capacity of FIrst Slot is not Increased, Before Placing Order : "+OfDailySlot.freeSlotCapacity);
			System.out.println("After Placing Order : "+capacity);
			report.updateTestLog("Daily SLot Page","Placed Slot Capacity of First Slot is not Increased, current slot capacity is :  "+capacity , Status.FAIL);
		}
	}else
		report.updateTestLog("Daily SLot Page","Placed Slot Capacity of First Slot is not displayed " , Status.FAIL);	
	}else{
	capacity =	helper.getText(driver,  OfDailySlot.txtPlacedSlotCapacityOfFirstSlot_Loc, xpath1);
	report.updateTestLog("Daily SLot Page","Placed Slot Capacity of First Slot is not validated, because previous slot value is not captured. Current Slot value is  : "+capacity , Status.DONE);
	}
	}catch(SeleniumException e){
	e.printStackTrace();
	report.updateTestLog("Daily SLot Page","Exception occured while validating Placed slot capacity "+e.toString(), Status.FAIL);}
	}



	/********************************************************
	*FUNCTION    :getSlotCapacityOfFirstSlotInDailySlot
	*AUTHOR      :Jayandran
	*DATE        :17-Mar-15
	*DESCRIPTION :function to get slot capacity of First slot in Daily Slot Page
	*PAGE        :Comp_DailySlotPage 
	********************************************************/
	public void getSlotCapacityOfFirstSlotInDailySlot(){
	try {			
	String capacity;
	if(helper.isElementPresent(driver, OfDailySlot.txtSlotCapacityOfFirstSlot_Loc, OfDailySlot.txtSlotCapacityOfFirstSlot)){
	capacity =	helper.getText(driver,  OfDailySlot.txtSlotCapacityOfFirstSlot_Loc, OfDailySlot.txtSlotCapacityOfFirstSlot);
	OfDailySlot.SlotCapacityOfFirstSlot = Integer.parseInt(capacity);
	System.out.println("Slot Capacity of FIrst Slot :"+OfDailySlot.SlotCapacityOfFirstSlot);
	report.updateTestLog("Daily SLot Page","Slot Capacity of First Slot : "+OfDailySlot.SlotCapacityOfFirstSlot , Status.PASS);
	}else
	report.updateTestLog("Daily SLot Page","Slot Capacity of First Slot is not displayed " , Status.FAIL);	
	}catch(SeleniumException e){
	e.printStackTrace();
	report.updateTestLog("Daily SLot Page","Exception occured while validating slot capacity "+e.toString(), Status.FAIL);}
	}

//	/********************************************************
//	*FUNCTION    :validateSlotCapacityOfFirstSlotInDailySlot
//	*AUTHOR      :Jayandran
//	*DATE        :17-Mar-15
//	*DESCRIPTION :function to get slot capacity of First slot in Daily Slot Page with Delivery Maintenance Page 
//	*PAGE        :Comp_DailySlotPage 
//	********************************************************/
//	public void validateSlotCapacityOfFirstSlotInDailySlot(){
//	try {			
//	String capacity;
//	if(OfDailySlot.SlotCapacityOfFirstSlot != -1 && OfDeliverySlotMaintenance.txtSlotCapacityValueForFirstSlot != -1){				
//	if(OfDeliverySlotMaintenance.txtSlotCapacityValueForFirstSlot == 0 || OfDeliverySlotMaintenance.txtSlotCapacityValueForFirstSlot == 1){
//		if(OfDailySlot.SlotCapacityOfFirstSlot == 2){
//			report.updateTestLog("Daily SLot Page","Slot Capacity value is same as Delivery Maintenance Page  "+OfDailySlot.SlotCapacityOfFirstSlot, Status.PASS);
//		}else{
//			report.updateTestLog("Daily SLot Page","Slot Capacity value is Not same as Delivery Maintenance Page  "+OfDailySlot.SlotCapacityOfFirstSlot, Status.FAIL);
//		}
//	}else if(OfDailySlot.SlotCapacityOfFirstSlot == OfDeliverySlotMaintenance.txtSlotCapacityValueForFirstSlot -1){
//		report.updateTestLog("Daily SLot Page","Slot Capacity value is same as Delivery Maintenance Page  "+OfDailySlot.SlotCapacityOfFirstSlot, Status.PASS);
//	}else{						
//		report.updateTestLog("Daily SLot Page"," Displayed Value "+OfDailySlot.SlotCapacityOfFirstSlot , Status.DONE);
//		report.updateTestLog("Delivery Maintenance Page"," Displayed Value "+(OfDeliverySlotMaintenance.txtSlotCapacityValueForFirstSlot-1) , Status.DONE);
//	}
//	}else{
//	capacity =	helper.getText(driver,  OfDailySlot.txtSlotCapacityOfFirstSlot_Loc, OfDailySlot.txtSlotCapacityOfFirstSlot);
//	report.updateTestLog("Daily SLot Page","Slot Capacity of First Slot is not validated, because previous slot value is not captured. Current Slot value is  : "+capacity , Status.DONE);
//	}
//	}catch(SeleniumException e){
//	e.printStackTrace();
//	report.updateTestLog("Daily SLot Page","Exception occured while validating Placed slot capacity "+e.toString(), Status.FAIL);}
//	}

	/********************************************************
	*FUNCTION    :getRestrictedFreeSlotCapacityOfFirstSlotInDailySlot
	*AUTHOR      :Jayandran
	*DATE        :19-Mar-15
	*DESCRIPTION :function to get Restricted - Free slot capacity of First slot in Daily Slot Page
	*PAGE        :Comp_DailySlotPage 
	********************************************************/
	public void getRestrictedFreeSlotCapacityOfFirstSlotInDailySlot(){
	try {			
	String capacity;
	if(helper.isElementPresent(driver, OfDailySlot.txtRestrictedFreeSlotCapacityOfFirstSlot_Loc, OfDailySlot.txtRestrictedFreeSlotCapacityOfFirstSlot)){
	capacity =	helper.getText(driver,  OfDailySlot.txtRestrictedFreeSlotCapacityOfFirstSlot_Loc, OfDailySlot.txtRestrictedFreeSlotCapacityOfFirstSlot);
	OfDailySlot.restrictedfreeSlotCapacity = Integer.parseInt(capacity);
	System.out.println("Restricted Free Slot Capacity of FIrst Slot :"+OfDailySlot.restrictedfreeSlotCapacity);
	report.updateTestLog("Daily SLot Page","Restricted - Free Slot Capacity of First Slot : "+OfDailySlot.restrictedfreeSlotCapacity , Status.PASS);
	}else
	report.updateTestLog("Daily SLot Page","Restricted - Free Slot Capacity of First Slot is not displayed " , Status.FAIL);	
	}catch(SeleniumException e){
	e.printStackTrace();
	report.updateTestLog("Daily SLot Page","Exception occured while validating Restricted - Free slot capacity "+e.toString(), Status.FAIL);}
	}

	/********************************************************
	*FUNCTION    :getRestrictedPlacedSlotCapacityOfFirstSlotInDailySlotUpdated
	*AUTHOR      :Jayandran
	*DATE        :17-Apr-15
	*DESCRIPTION :function to get Restricted - Free slot capacity of First slot in Daily Slot Page
	*PAGE        :Comp_DailySlotPage 
	********************************************************/
	public void getRestrictedPlacedSlotCapacityOfFirstSlotInDailySlotUpdated(){
	try {			
		String capacity;
	if(helper.isElementPresent(driver, OfDailySlot.txtRestrictedPlacedSlotCapacityOfFirstSlot_Loc, OfDailySlot.txtRestrictedPlacedSlotCapacityOfFirstSlot)){
	capacity =	helper.getText(driver,  OfDailySlot.txtRestrictedPlacedSlotCapacityOfFirstSlot_Loc, OfDailySlot.txtRestrictedPlacedSlotCapacityOfFirstSlot);
	OfDailySlot.restrictedplacedSlotCapacity = Integer.parseInt(capacity);
	System.out.println("Restricted Placed Slot Capacity of FIrst Slot :"+OfDailySlot.restrictedplacedSlotCapacity);
	report.updateTestLog("Daily SLot Page","Restricted - Placed Slot Capacity of First Slot : "+OfDailySlot.restrictedplacedSlotCapacity , Status.PASS);
	}else
	report.updateTestLog("Daily SLot Page","Restricted - Placed Slot Capacity of First Slot is not displayed " , Status.FAIL);
	}catch(Exception e){
	e.printStackTrace();
	report.updateTestLog("Daily SLot Page","Exception occured while validating Restricted - Placed slot capacity "+e.toString(), Status.FAIL);}

	}


	/********************************************************
	*FUNCTION    :getRestrictedPlacedSlotCapacityOfFirstSlotInDailySlot
	*AUTHOR      :Jayandran
	*DATE        :19-Mar-15
	*DESCRIPTION :function to get Restricted - Free slot capacity of First slot in Daily Slot Page
	*PAGE        :Comp_DailySlotPage 
	********************************************************/
	public void getRestrictedPlacedSlotCapacityOfFirstSlotInDailySlot(){
	try {			
	/*		String capacity;
	if(helper.isElementPresent(driver, OfDailySlot.txtRestrictedPlacedSlotCapacityOfFirstSlot_Loc, OfDailySlot.txtRestrictedPlacedSlotCapacityOfFirstSlot)){
	capacity =	helper.getText(driver,  OfDailySlot.txtRestrictedPlacedSlotCapacityOfFirstSlot_Loc, OfDailySlot.txtRestrictedPlacedSlotCapacityOfFirstSlot);
	OfDailySlot.restrictedplacedSlotCapacity = Integer.parseInt(capacity);
	System.out.println("Restricted Placed Slot Capacity of FIrst Slot :"+OfDailySlot.restrictedplacedSlotCapacity);
	report.updateTestLog("Daily SLot Page","Restricted - Placed Slot Capacity of First Slot : "+OfDailySlot.restrictedplacedSlotCapacity , Status.PASS);
	}else
	report.updateTestLog("Daily SLot Page","Restricted - Placed Slot Capacity of First Slot is not displayed " , Status.FAIL);*/
	String capacity,capacity1,capacity2;
	int index,index1;
	if(helper.isElementPresent(driver, OfDailySlot.txtPlacedSlotCapacityOfFirstSlot_Loc, OfDailySlot.txtPlacedSlotCapacityOfFirstSlot)){
	capacity =	helper.getText(driver,  OfDailySlot.txtPlacedSlotCapacityOfFirstSlot_Loc, OfDailySlot.txtPlacedSlotCapacityOfFirstSlot);
	if(capacity.contains("+")){
		System.out.println("Restricted Slot Capacity is displayed :  "+capacity);
		index = capacity.indexOf("+");
		capacity1 = capacity.substring(index+1);
		if(capacity1.contains(")")){
			index1 = capacity1.indexOf(")");
			System.out.println("Index of ) : "+index1);
			capacity1 = capacity1.substring(0, index1);
			System.out.println(capacity1);
		}
		capacity2 = capacity.substring(0, index-1).trim();
		System.out.println("Normal Placed Slot Capcity :  "+capacity2);
		System.out.println("Restricted Placed Slot Capcity :  "+capacity1);
		OfDailySlot.placedSlotCapacity = Integer.parseInt(capacity2);
		OfDailySlot.restrictedplacedSlotCapacity = Integer.parseInt(capacity1);
		report.updateTestLog("Daily SLot Page","Normal - Placed Slot Capacity of First Slot : "+OfDailySlot.placedSlotCapacity , Status.DONE);
		report.updateTestLog("Daily SLot Page","Restricted - Placed Slot Capacity of First Slot : "+OfDailySlot.restrictedplacedSlotCapacity , Status.DONE);
	}else{
		OfDailySlot.placedSlotCapacity = Integer.parseInt(capacity);
		OfDailySlot.restrictedplacedSlotCapacity = 0;
		System.out.println("Restricted Placed Slot is not displayed for FIrst Slot :"+OfDailySlot.placedSlotCapacity);
		report.updateTestLog("Daily SLot Page","No Restricted Placed Slot Capacity for First Slot, Only Normal Placed slot is displayed : "+OfDailySlot.placedSlotCapacity , Status.DONE);
	}
	}else
	report.updateTestLog("Daily SLot Page","Placed Slot Capacity of First Slot is not displayed " , Status.FAIL);		
	}catch(SeleniumException e){
	e.printStackTrace();
	report.updateTestLog("Daily SLot Page","Exception occured while validating Restricted - Placed slot capacity "+e.toString(), Status.FAIL);}
	}


	/********************************************************
	*FUNCTION    :validateRestrictedPlacedSlotCapacityOfFirstSlotInDailySlot
	*AUTHOR      :Jayandran
	*DATE        :19-Mar-15
	*DESCRIPTION :function to get slot capacity of First slot in Daily Slot Page with Delivery Maintenance Page 
	*PAGE        :Comp_DailySlotPage 
	********************************************************/
	public void validateRestrictedPlacedSlotCapacityOfFirstSlotInDailySlot(){
	try {			
	/*String capacity;
	int value;
	if(OfDailySlot.restrictedplacedSlotCapacity > 0){			
	capacity = helper.getText(driver,  OfDailySlot.txtRestrictedPlacedSlotCapacityOfFirstSlot_Loc, OfDailySlot.txtRestrictedPlacedSlotCapacityOfFirstSlot);
	value = Integer.parseInt(capacity);
	if(OfDailySlot.restrictedplacedSlotCapacity == value -1){
			report.updateTestLog("Daily SLot Page","Restricted Placed Slot Capacity value is increased  "+value, Status.PASS);
		}else{						
			report.updateTestLog("Daily SLot Page","Restricted Placed Slot : Old value "+OfDailySlot.restrictedplacedSlotCapacity, Status.DONE);
			report.updateTestLog("Delivery Maintenance Page","Restricted Placed SLot Displayed Value "+capacity , Status.FAIL);
		}
	}else{
	capacity =	helper.getText(driver, OfDailySlot.txtRestrictedPlacedSlotCapacityOfFirstSlot_Loc, OfDailySlot.txtRestrictedPlacedSlotCapacityOfFirstSlot);
	report.updateTestLog("Daily SLot Page","Restricted Placed Slot Capacity of First Slot is not validated, because previous slot value is not captured. Current Slot value is  : "+capacity , Status.DONE);
	}*/
	String capacity,capacity1,capacity2;
	int index,index1;
	if(OfDailySlot.restrictedplacedSlotCapacity != -1){
	if(helper.isElementPresent(driver, OfDailySlot.txtPlacedSlotCapacityOfFirstSlot_Loc, OfDailySlot.txtPlacedSlotCapacityOfFirstSlot)){
		capacity =	helper.getText(driver,  OfDailySlot.txtPlacedSlotCapacityOfFirstSlot_Loc, OfDailySlot.txtPlacedSlotCapacityOfFirstSlot);
		if(capacity.contains("+")){
			System.out.println("Restricted Slot Capacity is displayed :  "+capacity);
			index = capacity.indexOf("+");
			capacity1 = capacity.substring(index+1);
			if(capacity1.contains(")")){
				index1 = capacity1.indexOf(")");
				System.out.println("Index of ) : "+index1);
				capacity1 = capacity1.substring(0, index1);
				System.out.println(capacity1);
			}
			capacity2 = capacity.substring(0, index-1).trim();
			System.out.println("Normal Placed Slot Capcity :  "+capacity2);
			System.out.println("Restricted Placed Slot Capcity :  "+capacity1);
			int val = Integer.parseInt(capacity1);
			if(OfDailySlot.restrictedplacedSlotCapacity == val-1){
				report.updateTestLog("Daily SLot Page","Restricted Placed slot is updated : "+val, Status.PASS);	
			}else{					
				report.updateTestLog("Daily SLot Page","Restricted - Placed Slot Capacity is not updated : "+val , Status.FAIL);
			}
		}else{
			report.updateTestLog("Daily SLot Page","No Restricted Placed Slot Capacity for First Slot, Only Normal Placed slot is displayed : "+capacity , Status.FAIL);
		}
	}else
		report.updateTestLog("Daily SLot Page","Placed Slot Capacity of First Slot is not displayed " , Status.FAIL);
	}else{
	capacity =	helper.getText(driver,  OfDailySlot.txtPlacedSlotCapacityOfFirstSlot_Loc, OfDailySlot.txtPlacedSlotCapacityOfFirstSlot);
	report.updateTestLog("Daily SLot Page","No Previous Restricted Placed Slot Capacity is captured, So Unable to validate Restricted SLot Capacity. Current Placed Slot capacity  : "+capacity , Status.FAIL);
	}

	}catch(SeleniumException e){
	e.printStackTrace();
	report.updateTestLog("Daily SLot Page","Exception occured while validating Restricted Placed slot capacity "+e.toString(), Status.FAIL);}
	}


	/********************************************************
	*FUNCTION    :validateRestrictedPlacedSlotCapacityOfFirstSlotInDailySlot
	*AUTHOR      :Jayandran
	*DATE        :17-Apr-15
	*DESCRIPTION :function to get slot capacity of First slot in Daily Slot Page with Delivery Maintenance Page 
	*PAGE        :Comp_DailySlotPage 
	********************************************************/
	public void validateRestrictedPlacedSlotCapacityOfFirstSlotInDailySlotUpdated(){
	try {			
	String capacity;
	int value;
	if(OfDailySlot.restrictedplacedSlotCapacity > 0){			
	capacity = helper.getText(driver,  OfDailySlot.txtRestrictedPlacedSlotCapacityOfFirstSlot_Loc, OfDailySlot.txtRestrictedPlacedSlotCapacityOfFirstSlot);
	value = Integer.parseInt(capacity);
	if(OfDailySlot.restrictedplacedSlotCapacity == value -1){
			report.updateTestLog("Daily SLot Page","Restricted Placed Slot Capacity value is increased  "+value, Status.PASS);
		}else{						
			report.updateTestLog("Daily SLot Page","Restricted Placed Slot : Old value "+OfDailySlot.restrictedplacedSlotCapacity, Status.DONE);
			report.updateTestLog("Delivery Maintenance Page","Restricted Placed SLot Displayed Value "+capacity , Status.FAIL);
		}
	}else{
	capacity =	helper.getText(driver, OfDailySlot.txtRestrictedPlacedSlotCapacityOfFirstSlot_Loc, OfDailySlot.txtRestrictedPlacedSlotCapacityOfFirstSlot);
	report.updateTestLog("Daily SLot Page","Restricted Placed Slot Capacity of First Slot is not validated, because previous slot value is not captured. Current Slot value is  : "+capacity , Status.DONE);
	}
	}catch(Exception e){
	report.updateTestLog("Daily SLot Page","Restricted Placed Slot Capacity of First Slot is not validated, Exception occured :  "+e.toString(), Status.FAIL);
	}
	}


	/********************************************************
	*FUNCTION    :validateRestrictedFreeSlotCapacityOfFirstSlotInDailySlot
	*AUTHOR      :Jayandran
	*DATE        :19-Mar-15
	*DESCRIPTION :function to validate capacity of First slot in Daily Slot Page with Delivery Maintenance Page 
	*PAGE        :Comp_DailySlotPage 
	********************************************************/
	public void validateRestrictedFreeSlotCapacityOfFirstSlotInDailySlot(){
	try {			
	String capacity;
	int value;
	if(OfDailySlot.restrictedfreeSlotCapacity >= 0){			
	capacity = helper.getText(driver,  OfDailySlot.txtRestrictedFreeSlotCapacityOfFirstSlot_Loc, OfDailySlot.txtRestrictedFreeSlotCapacityOfFirstSlot);
	value = Integer.parseInt(capacity);
	if(OfDailySlot.restrictedfreeSlotCapacity == value -1){
		report.updateTestLog("Daily SLot Page","Restricted Free Slot Capacity value is decreased  "+value, Status.PASS);
	}else{						
		report.updateTestLog("Daily SLot Page","Restricted Free Slot : Old value "+OfDailySlot.restrictedfreeSlotCapacity, Status.DONE);
		report.updateTestLog("Delivery Maintenance Page","Restricted Free SLot Displayed Value "+capacity , Status.FAIL);
	}
	}else{
	capacity =	helper.getText(driver, OfDailySlot.txtRestrictedPlacedSlotCapacityOfFirstSlot_Loc, OfDailySlot.txtRestrictedPlacedSlotCapacityOfFirstSlot);
	report.updateTestLog("Daily SLot Page","Restricted Free Slot Capacity of First Slot is not validated, because previous slot value is not captured. Current Slot value is  : "+capacity , Status.DONE);
	}
	}catch(SeleniumException e){
	e.printStackTrace();
	report.updateTestLog("Daily SLot Page","Exception occured while validating Restricted Free slot capacity "+e.toString(), Status.FAIL);}
	}


//	/********************************************************
//	*FUNCTION    :validateRestrictedTotalSlotCapacityOfFirstSlotInDailySlot
//	*AUTHOR      :Jayandran
//	*DATE        :09-Apr-15
//	*DESCRIPTION :function to validate Restricted - Total slot capacity of First slot in Daily Slot Page
//	*PAGE        :Comp_DailySlotPage 
//	********************************************************/
//	public void validateRestrictedTotalSlotCapacityOfFirstSlotInDailySlot(){
//	try {			
//	String capacity;	
//	int val;
//	if(helper.isElementPresent(driver, OfDailySlot.txtSlotRestrictedCapacityOfFirstSlot_Loc, OfDailySlot.txtSlotRestrictedCapacityOfFirstSlot)){
//	capacity =	helper.getText(driver, OfDailySlot.txtSlotRestrictedCapacityOfFirstSlot_Loc, OfDailySlot.txtSlotRestrictedCapacityOfFirstSlot);
//	val = Integer.parseInt(capacity);
//	System.out.println("Current Restricted First Slot Capacity : "+val);
//	if(OfDeliverySlotMaintenance.txtSlotCapacityValueForFirstSlot == -1){
//		report.updateTestLog("Daily SLot Page","Old Slot Capacity is not captured and current Restricted - Total Slot Capacity of First Slot : "+capacity , Status.FAIL);
//	}else if(OfDeliverySlotMaintenance.txtSlotCapacityValueForFirstSlot == 0){					
//		if(val == 10){
//			report.updateTestLog("Daily SLot Page","Restricted SLot Capacity is updated as per Delivery Slot Maintenance Page, Slot Capacity : "+capacity , Status.PASS);
//		}else{
//			report.updateTestLog("Daily SLot Page","Restricted SLot Capacity is not updated as per Delivery Slot Maintenance Page, Slot Capacity : "+capacity , Status.FAIL);
//		}
//	}else{
//		if(OfDeliverySlotMaintenance.txtSlotCapacityValueForFirstSlot == val){
//			report.updateTestLog("Daily SLot Page","Restricted SLot Capacity is updated as per Delivery Slot Maintenance Page, Slot Capacity : "+capacity , Status.PASS);
//		}else {
//			report.updateTestLog("Daily SLot Page","Restricted SLot Capacity is not updated as per Delivery Slot Maintenance Page, Slot Capacity : "+capacity , Status.FAIL);
//		}
//	}
//
//	}else
//	report.updateTestLog("Daily SLot Page","Restricted - Free Slot Capacity of First Slot is not displayed " , Status.FAIL);	
//	}catch(SeleniumException e){
//	e.printStackTrace();
//	report.updateTestLog("Daily SLot Page","Exception occured while validating Restricted - Free slot capacity "+e.toString(), Status.FAIL);}
//	}


	/********************************************************
	*FUNCTION    :validatePageElementsInDailySlot
	*AUTHOR      :Jayandran
	*DATE        :16-Apr-15
	*DESCRIPTION :function to validate Headers(Window,Freeslot,PlacedSlot) and slot details  in Daily Slot Page
	*PAGE        :Comp_DailySlotPage 
	********************************************************/
	public void validatePageElementsInDailySlot(){
	try {		
	if(helper.isElementPresent(driver, OfDailySlot.txtWindowHeader_Loc, OfDailySlot.txtWindowHeader)){
	report.updateTestLog("Daily SLot Page : Table Header ","Window Header is displayed ", Status.PASS);
	}else
	report.updateTestLog("Daily SLot Page : Table Header ","Window Header is not displayed ", Status.FAIL);

	if(helper.isElementPresent(driver, OfDailySlot.txtCapacityHeader_Loc, OfDailySlot.txtCapacityHeader)){
	report.updateTestLog("Daily SLot Page : Table Header ","Capacity All Header is displayed ", Status.PASS);
	}else
	report.updateTestLog("Daily SLot Page : Table Header ","Capacity All Header is not displayed ", Status.FAIL);

	if(helper.isElementPresent(driver, OfDailySlot.txtCapacityAllPlacedOrderHeader_Loc, OfDailySlot.txtCapacityAllPlacedOrderHeader)){
	report.updateTestLog("Daily SLot Page : Table Header ","Capacity All - Placed Order Header is displayed ", Status.PASS);
	}else
	report.updateTestLog("Daily SLot Page : Table Header ","Capacity All - Placed Order Header is not displayed ", Status.FAIL);

	if(helper.isElementPresent(driver, OfDailySlot.txtCapacityAllFreeOrderHeader_Loc, OfDailySlot.txtCapacityAllFreeOrderHeader)){
	report.updateTestLog("Daily SLot Page : Table Header ","Capacity All - Free Order Header is displayed ", Status.PASS);
	}else
	report.updateTestLog("Daily SLot Page : Table Header ","Capacity All - Free Order Header is not displayed ", Status.FAIL);

	if(helper.isElementPresent(driver, OfDailySlot.txtSlotTiming_Loc, OfDailySlot.txtSlotTiming)){
	report.updateTestLog("Daily SLot Page : Table Header ","Slot Timings are displayed ", Status.PASS);
	List<WebElement> slotTimings=driver.findElements((By.xpath(OfDailySlot.txtSlotTiming)));
	int slotTime=slotTimings.size();
	System.out.println("Number of Slots Displayed  :  "+slotTime);
	int i = 1;
	String slot;
	for(WebElement we : slotTimings){
		slot = we.getText();
		if(we.isDisplayed()){
			System.out.println(" "+i+" Slot Time is displayed ");
			System.out.println(" "+i+" Slot Time : "+slot);
		}else
			System.out.println(" "+i+" Slot Time is not displayed ");
		
		i++;
	}
	report.updateTestLog("Daily SLot Page : Table Header ","No. of Slot Timings :  "+slotTime, Status.DONE);
	}else
	report.updateTestLog("Daily SLot Page : Table Header ","Slot Timings are not displayed ", Status.FAIL);


	}catch(SeleniumException e){
	e.printStackTrace();
	report.updateTestLog("Daily SLot Page","Exception occured while validating Restricted - Free slot capacity "+e.toString(), Status.FAIL);}
	}

	/********************************************************
	*FUNCTION    :verifyReduceAllFunctionalityInDailySlot
	*AUTHOR      :Jayandran
	*DATE        :20-Apr-15
	*DESCRIPTION :function to validate Total Slot Capacity in Daily SLot Page
	*PAGE        :Comp_DailySlotPage 
	********************************************************/
	public void verifyReduceAllFunctionalityInDailySlot(){
	try {

	String totalslot;
	int val;
	if(helper.isElementPresent(driver, OfDailySlot.txtTotalSlotCapacity_Loc, OfDailySlot.txtTotalSlotCapacity)){
	totalslot = helper.getText(driver,  OfDailySlot.txtTotalSlotCapacity_Loc, OfDailySlot.txtTotalSlotCapacity);
	val = Integer.parseInt(totalslot);
	if(val == 0){
		report.updateTestLog("Branch Slots Mass Page","Reduce All functionality is updated" , Status.PASS);
	}else{
		report.updateTestLog("Branch Slots Mass Page","Reduce All functionality is not updated" , Status.FAIL);
	}

	}else
	report.updateTestLog("Branch Slots Mass Page","Total Slot Capacity is not displayed " , Status.FAIL);	
	}catch(SeleniumException e){
	e.printStackTrace();
	report.updateTestLog("Branch Slots Mass Page","Exception occured while verifying Total Slot  "+e.toString(), Status.FAIL);}
	}


//	public void selectEndDate(){
//
//	String slotdate,value;
//	//String slotDate = dataTable.getData("General_Data" , "SlotDate");
//
//	if(helper.isElementPresent(driver, OfDailySlot.txtEndSlot_Loc	, OfDailySlot.txtEndSLot)){
//	driver.findElement(By.name("end_date_select")).click();
//	driver.findElement(By.xpath("(//select[@name='end_date_select']//option)[7]")).click();
//	System.out.println("End date clicked");
//	helper.clickLink(driver, OfSearchOrderBySlot.orderSearchbtn_loc, OfSearchOrderBySlot.orderSearchbtn);
//	}
//	}
	
}