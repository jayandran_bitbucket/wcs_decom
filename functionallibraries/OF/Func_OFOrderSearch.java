package functionallibraries.OF;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Status;

import functionallibraries.aws.ReusableComponents;
import functionallibraries.wcs.Func_BookSlot;
import pageobjects.OF.*;
import pageobjects.wcs.OrderConfirmation;
import supportlibraries.*;

public class Func_OFOrderSearch extends ReusableLibrary
{

	public Func_OFOrderSearch(ScriptHelper scriptHelper){
		super(scriptHelper);
	}
	HomePage Home = new HomePage();
	OrderConfirmation confirmation = new OrderConfirmation();
	OrderSearch ordersearch = new OrderSearch();
	Func_BookSlot BookSlot = new Func_BookSlot(scriptHelper);
	WebDriverWait Wait = new WebDriverWait(driver,30);
	String orderid;
	
	
	ReusableComponents helper = new ReusableComponents(scriptHelper);
	
	public void enterOrderNumberAndSearch(){
		//String orderid = OrderConfirmation.orderNumber;
		orderid = dataTable.getData("OrderDetails", "OrderNumber");
		System.out.println("Order Number : "+orderid);
		helper.typeinTextbox(driver, ordersearch.searchorder_loc, ordersearch.searchorder_txt, orderid);
		int i =1;
		BookSlot.returnPageSource(10000);
		do{
			driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
			helper.clickLink(driver, ordersearch.orderSearchbtn_loc, ordersearch.orderSearchbtn);
		i++;
		if(helper.findWebElements(driver, "linkText", orderid).size() >0){
			break;
		}
		BookSlot.returnPageSource(5000);
		}while(i<8);
		report.updateTestLog("OF - Order Search Page ", " Performed order search with order id : "+orderid, Status.DONE);
		clickOrderNumber();
	}
	
	public void clickOrderNumber(){
		orderid = dataTable.getData("OrderDetails", "OrderNumber");
		//Wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText(orderid)));
		helper.clickLink(driver, "linkText", orderid);
		report.updateTestLog("OF - Order Search Page ", " Selected order id : "+orderid, Status.PASS);
		
	}
	

}