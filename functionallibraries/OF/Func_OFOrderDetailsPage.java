package functionallibraries.OF;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Status;

import allocator.Allocator;
import functionallibraries.aws.ReusableComponents;
import pageobjects.OF.*;
import pageobjects.wcs.BookSlot;
import pageobjects.wcs.OrderConfirmation;
import pageobjects.wcs.OrderReview;
import supportlibraries.*;

public class Func_OFOrderDetailsPage extends ReusableLibrary
{

	public Func_OFOrderDetailsPage(ScriptHelper scriptHelper){
		super(scriptHelper);
	}
	HomePage Home = new HomePage();
	OrderConfirmation confirmation = new OrderConfirmation();
	OrderSearch ordersearch = new OrderSearch();
	WebDriverWait Wait = new WebDriverWait(driver,20);
	OrderReview orderreview = new OrderReview();
	StoreOrderDetails storedata = new StoreOrderDetails(scriptHelper);
	OrderDetailsPage orderdetails = new OrderDetailsPage();
	

	ReusableComponents helper = new ReusableComponents(scriptHelper);

	public void verifyOrderDetails(){
		Wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[text()='Order Summary']")));
		driver.navigate().refresh();
		verifySlotDay();
		verifyOrderLineNumbers();
	}
	
	public void verifyEditOrderDetails(){
		Wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[text()='Order Summary']")));
		verifyEditOrderSlotDay();
		verifyOrderLineNumbers();
	}
	
	public void verifyLoanOrderDetails(){
		Wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[text()='Order Summary']")));
		verifySlotDay();
		verifyLoanOrderLineNumbers();
	}

	public void writeOrderDetails(){
		String[] data;
		int siz = OrderReview.productslinenumber.length;
		data = new String[siz+2];
		data[0] = OrderConfirmation.orderNumber;
		data[1] = BookSlot.slotdatee;
		int i =2;
		for(String s : OrderReview.productslinenumber){
			data[i] = s;
			i++;
		}
		Allocator allocator = new Allocator();
		storedata.writeCSV(allocator.getReportPath()+"/data.csv", data);
	}

	public void verifySlotDay(){
		String slotdate = dataTable.getData("OrderDetails", "SlotDate");
		System.out.println("Slot Date : "+slotdate);
		String elmt = "//td[contains(text(),'"+slotdate+"')]";
		try{
			if(helper.isElementPresent(driver, "xpath", elmt)){
				report.updateTestLog("OF Order Details Page", " Slot date is displayed properly ", Status.PASS);
			}else{
				report.updateTestLog("OF Order Details Page", " Slot date is not same as WCS ", Status.FAIL);
			}
		}catch(Exception e){
			e.printStackTrace();
			report.updateTestLog("OF Order Details Page", " Slot date is not same as WCS, Exception occured : "+e.toString(), Status.FAIL);
		}
	}
	
	public void verifyEditOrderSlotDay(){
		String slotdate = dataTable.getData("OrderDetails", "SlotDate");
		System.out.println("Slot Date : "+slotdate);
		String elmt = "//td[contains(text(),'"+slotdate+"')]";
		try{
			for(int i =1;i<=3;i++){
			if(helper.findWebElements(driver, "xpath", elmt).size() > 0){
				if(helper.isElementPresent(driver, "xpath", elmt)){
					report.updateTestLog("OF Order Details Page", " Slot date is displayed properly : "+slotdate, Status.PASS);
					break;
				}
			}else{
				if(i==3){
					report.updateTestLog("OF Order Details Page", " Slot date is not same as WCS  ", Status.FAIL);
					report.updateTestLog("OF Order Details Page", " Exepected Slot Date "+slotdate, Status.DONE);
					break;
				}
				helper.checkPageReadyState(3000);
				driver.navigate().refresh();
			}
			}
			
		}catch(Exception e){
			e.printStackTrace();
			report.updateTestLog("OF Order Details Page", " Slot date is not same as WCS, Exception occured : "+e.toString(), Status.FAIL);
		}
	}

	public void verifyOrderLineNumbers(){

		List<String> prd = new ArrayList<String>();
		String column = "Product";
		String val;
		for(int i=0,j=1;i<10;i++,j++){
			val = dataTable.getData("OrderDetails", column+j);
			if(val == null || val.equals("")){
				break;
			}else{
				prd.add(val);
				val = "";
			}
		}
		System.out.println("Number of Products in Order : "+prd.size());
		String elmt;
		try{
			for(String s:prd){
				elmt = "//div[@id='ordered_details']//*[text()='"+s+"']";
				if(helper.isElementPresent(driver, "xpath", elmt)){
					report.updateTestLog("OF Order Details Page", " Line Number is displayed in order : "+s, Status.PASS);
				}else{
					report.updateTestLog("OF Order Details Page", " Line Number is not displayed in order : "+s, Status.FAIL);
				}
			}
		}catch(Exception e){
			report.updateTestLog("OF Order Details Page", " Exception occured while validating line numbers in Order "+e.toString(), Status.FAIL);
		}


	}
	
	public void verifyOrderLineNumberForEntertainingOrder(){
		List<String> prd = new ArrayList<String>();
		String column = "Product";
		String val;
		for(int i=0,j=1;i<10;i++,j++){
			val = dataTable.getData("OrderDetails", column+j);
			if(val == null || val.equals("")){
				break;
			}else{
				prd.add(val);
				val = "";
			}
		}
		System.out.println("Number of Products in Order : "+prd.size());
		String elmt;
		try{
			for(String s:prd){
				elmt = "//div[@id='ordered_details']//*[text()='"+s+"']";
				for(int i = 1;i<=3;i++){
				if(helper.findWebElements(driver, "xpath", elmt).size() > 0){
					report.updateTestLog("OF Order Details Page", " Line Number is displayed in order : "+s, Status.PASS);
					break;
				}else{
					driver.navigate().refresh();
					helper.checkPageReadyState(2000);
				}
				
				if(i == 3){
					report.updateTestLog("OF Order Details Page", " Line Number is not displayed in order : "+s, Status.FAIL);
					break;
				}
				}
				
			}
		}catch(Exception e){
			report.updateTestLog("OF Order Details Page", " Exception occured while validating line numbers in Order "+e.toString(), Status.FAIL);
		}
	}

	public void verifyOrderDeliveryAddressPostcode(){
		String psdcode = dataTable.getData("OrderDetails", "OrderPostCode");
		System.out.println(" Delivery Address PostCode : "+psdcode);
		String elmt = helper.getText(driver, orderdetails.orderAddress_loc, orderdetails.orderAddress_txt);
		if(elmt.contains(psdcode)){
			report.updateTestLog("OF Order Details Page", " Order Postcode is displayed properly : "+psdcode, Status.PASS);
		}else{
			report.updateTestLog("OF Order Details Page", " Order Postcode is displayed properly : "+psdcode, Status.FAIL);
		}
	}


	public void verifyLoanOrderLineNumbers(){

		List<String> prd = new ArrayList<String>();
		String column = "Loan";
		String val;
		for(int j=1;j<10;j++){
			val = dataTable.getData("OrderDetails", column+j);
			if(val == null || val.equals("")){
				break;
			}else{
				prd.add(val);
				val = "";
			}
		}
		System.out.println("Number of Products in Order : "+prd.size());
		String elmt;
		try{
			for(String s:prd){
				elmt = "//div[@id='ordered_details']//*[text()='"+s+"']";
				if(helper.isElementPresent(driver, "xpath", elmt)){
					report.updateTestLog("OF Order Details Page", " Line Number is displayed in order : "+s, Status.PASS);
				}else{
					report.updateTestLog("OF Order Details Page", " Line Number is not displayed in order : "+s, Status.FAIL);
				}
			}
		}catch(Exception e){
			report.updateTestLog("OF Order Details Page", " Exception occured while validating line numbers in Order "+e.toString(), Status.FAIL);
		}


	}
	
	public void verifyGreetingMsg(){
		String msg = dataTable.getData("General_Data", "PersonalizedMsg").trim();
		if(!msg.equalsIgnoreCase("") || msg!= null){
		String xpath = "//*[contains(text(),'"+msg+"')]";
		if(helper.findWebElements(driver, "xpath", xpath).size() > 0){
			report.updateTestLog("OF Order Details Page", " Greeting Message is displayed ", Status.PASS);
		}else{
			report.updateTestLog("OF Order Details Page", " Greeting Message is not displayed ", Status.FAIL);
		}
		}else{
			report.updateTestLog("OF Order Details Page", " No Message is updated in Data Sheet ", Status.FAIL);
		}
	}
	
	public void verifyIncentiveSavings(){
		String code = dataTable.getData("PaymentDetails_Data", "IncentiveCode").trim();
		if(!code.equalsIgnoreCase("") || code!= null){
			String xpath = "//*[contains(text(),'"+code+"')]";
			if(helper.findWebElements(driver, "xpath", xpath).size() > 0){
				report.updateTestLog("OF Order Details Page", " Incentive Code is displayed ", Status.PASS);
			}else{
				report.updateTestLog("OF Order Details Page", " Incentive Code is not displayed ", Status.FAIL);
			}
			}else{
				report.updateTestLog("OF Order Details Page", " No Incentive Code is updated in Data Sheet ", Status.FAIL);
			}
	}
	
	public void verifyDeliveryNoteInOFOrderDetails(){
		String msg = dataTable.getData("General_Data", "DeliveryNote").trim();
		if(helper.isElementPresent(driver, "xpath", "//tr//td[contains(text(),'delivery')]/following-sibling::td[contains(text(),'"+msg+"')]" ))
			report.updateTestLog("OF Order Details Page", "Delivery note is displayed ", Status.PASS);
		else report.updateTestLog("OF Order Details Page", "Delivery note is not displayed ", Status.FAIL);
	}
	
	public void clickEditButtonInOF(){
		helper.clickLink(driver, orderdetails.edit_loc, orderdetails.edit_btn);
		report.updateTestLog("OF Order Details Page", "Edit button is clicked ", Status.DONE);
		
	}
	
	public void validateGiftCardApplied(){
		if(helper.getText(driver, orderdetails.giftcard_loc, orderdetails.giftcard_txt).contains(confirmation.cardvalue))
			report.updateTestLog("OF Order Details Page", "Gift card value is not displayed correctly"+ confirmation.cardvalue, Status.PASS);
		else report.updateTestLog("OF Order Details Page", "Gift card value is displayed correctly"+ confirmation.cardvalue, Status.FAIL);
	}
public void validateGiftVoucherApplied(){
	if(helper.isElementPresent(driver, orderdetails.giftvoucher_loc, orderdetails.giftvoucher_txt))
		report.updateTestLog("OF Order Details Page", "Gift Voucher value is not displayed correctly"+ confirmation.vouchervalue, Status.PASS);
	else report.updateTestLog("OF Order Details Page", "Gift Voucher value is displayed correctly"+ confirmation.vouchervalue, Status.FAIL);
	}

public void validateCarrierBagCharge(){
	String carriercharge = orderreview.carrierbagchargevalue ;
	if(helper.getText(driver, orderdetails.carriercharge_loc, orderdetails.carriercharge_txt).contains(carriercharge))
		report.updateTestLog("OF Order Details Page", "Carrier charge is applied"+ carriercharge, Status.PASS);
		else report.updateTestLog("OF Order Details Page", "Carrier charge is not applied"+ carriercharge, Status.FAIL);
}

public void validateNoCarrierBagCharge(){
	List<WebElement> carrierbag = driver.findElements(By.xpath( orderdetails.carriercharge_txt));
	if(carrierbag.size()==0)
		report.updateTestLog("OF Order Details Page", "Carrier charge is not applied", Status.PASS);
		else report.updateTestLog("OF Order Details Page", "Carrier charge is applied", Status.FAIL);
}

public void verfiyPartnerDiscount()
{
	String discount_label = helper.getText(driver, orderdetails.partnerdiscounttext_loc, orderdetails.partnerdiscounttext);
	String discount_value  =  helper.getText(driver, orderdetails.partnerdiscount_loc, orderdetails.partnerdiscount);
	String payment_label  = helper.getText(driver, orderdetails.paymentcardtext_loc, orderdetails.paymentcardtext);
	String payment_value  = helper.getText(driver, orderdetails.paymentcard_loc, orderdetails.paymentcard);
	System.out.println(discount_label);
	System.out.println(discount_value);
	System.out.println(payment_label);
	System.out.println(payment_value);
	
	
	if((discount_value.equals("Yes"))) // && (payment_value.equals("PSCD"))
		report.updateTestLog("Partner Discount", "Partner discount is applied and present in order details page", Status.PASS);
	else
		report.updateTestLog("Partner Discount", "Partner discount not present in order details page", Status.FAIL);
}

public void totalValueInOFCofirm()
{

	try
	{
		if(helper.isElementPresent(driver, orderdetails.txtEstTotal_loc, orderdetails.txtEstTotal))
		{
			WebElement total=driver.findElement(By.xpath(orderdetails.txtEstTotal));
			String totalValue= total.getText().trim();
			orderdetails.txtOFTotal=totalValue;
			System.out.println(totalValue);
			report.updateTestLog("Total Value", "Total value in OF Order confirmation page is"+orderdetails.txtOFTotal, Status.PASS);
			/*if((OfOrderSummary.txtOFTotal).contains(OrderConfirmation.total))
			{
				report.updateTestLog("Total Value", "Total value in OF and Order confirmation page is same"+OfOrderSummary.txtOFTotal, Status.PASS);
			}
			else
				report.updateTestLog("Total Value", "Total value in OF and Order confirmation page is NOT same"+OfOrderSummary.txtOFTotal, Status.FAIL);
		}*/
		}else
			report.updateTestLog("Total Value", "Total value in OF Order confirmation page is not displayed", Status.FAIL);
	}
	catch(Exception e)
	{
		e.printStackTrace();
		report.updateTestLog("Total Value", "Total value in OF Order confirmation page is not displayed"+e.toString(), Status.FAIL);
	}

}
}